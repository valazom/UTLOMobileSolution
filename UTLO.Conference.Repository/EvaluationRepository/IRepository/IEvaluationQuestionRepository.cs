﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.Repository.EvaluationRepository.IRepository
{
    public interface IEvaluationQuestionRepository
    {
        #region insert Evaluation Questions

        Task insertEvaluationQuestion(EvaluationQuestions questtions);

        #endregion
        #region Get Evaluation Questions
        Task<EvaluationQuestions> GetEvaluationQuestionById(int Id);

        Task<List<EvaluationQuestions>> GetAllEvaluationQuestions();

        #endregion
        #region Update Evaluation Questions
        Task UpdateEvaluationQuestions(EvaluationQuestions question);
        #endregion
        #region Delete Evaluation Questions
        Task DeleteEvaluations(EvaluationQuestions question);
        
        #endregion
    }
}
