﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.Repository.EvaluationRepository.IRepository
{
    public interface IAnswerRepository
    {
        #region Insert Answer
        Task InsertAnswer(Answers answers);
        #endregion
        #region Get Answer
        Task<Answers> GetAnswer(int Id);
        Task<List<Answers>> GetsAllAnswers();
        #endregion
        #region Update Answers
        Task Update(Answers answers);
        #endregion
        #region Delete(Answers answers);
        Task Delete(Answers answers);
        #endregion
    }
}
