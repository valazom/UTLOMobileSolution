﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Model.Data.Repository;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.Repository.EvaluationRepository.Repository
{
    public class AnswerRepository : IAnswerRepository
    {
        private RepositoryIntializer<Answers> _getAnswerRepository;

        public AnswerRepository(EvaluationDbContext context)
        {
            _getAnswerRepository = new RepositoryIntializer<Answers>(context);
        }
        public async Task Delete(Answers answers)
        {
            await _getAnswerRepository.GetGenericRepository.Delete(answers);
        }

        public async Task<Answers> GetAnswer(int Id)
        {
            return (await _getAnswerRepository.GetGenericRepository.FindBy(c => c.AnswerId == Id)).SingleOrDefault();
        }

        public async Task<List<Answers>> GetsAllAnswers()
        {
            return (await _getAnswerRepository.GetGenericRepository.All()).ToList();
        }

        public async Task InsertAnswer(Answers answers)
        {
            await _getAnswerRepository.GetGenericRepository.Insert(answers);
        }

        public async Task Update(Answers answers)
        {
            await _getAnswerRepository.GetGenericRepository.Update(answers);
        }
    }
}
