﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Model.Data.Repository;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.Repository.EvaluationRepository.Repository
{
    public class EvaluationQuestionsRepository : IEvaluationQuestionRepository
    {
        private RepositoryIntializer<EvaluationQuestions> _getEvaluationQuestionRepository;

        public EvaluationQuestionsRepository(EvaluationDbContext context)
        {
            _getEvaluationQuestionRepository = new RepositoryIntializer<EvaluationQuestions>(context);
        }
        public async Task DeleteEvaluations(EvaluationQuestions question)
        {
            await _getEvaluationQuestionRepository.GetGenericRepository.Delete(question);
        }

        public async Task<List<EvaluationQuestions>> GetAllEvaluationQuestions()
        {
            return (await _getEvaluationQuestionRepository.GetGenericRepository
                .AllIncludes(c => c.Answer)).ToList();
        }

        public async Task<EvaluationQuestions> GetEvaluationQuestionById(int Id)
        {
            return (await _getEvaluationQuestionRepository.GetGenericRepository
                .FindByInclude(c => c.EvaluationQuestionsId == Id, c => c.Answer))
                .SingleOrDefault();
        }

        public async Task insertEvaluationQuestion(EvaluationQuestions questions)
        {
            await _getEvaluationQuestionRepository
                .GetGenericRepository.Insert(questions);
        }

        public async Task UpdateEvaluationQuestions(EvaluationQuestions question)
        {
            await _getEvaluationQuestionRepository.GetGenericRepository.Update(question);
        }
    }
}
