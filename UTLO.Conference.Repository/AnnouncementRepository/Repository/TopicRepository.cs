﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class TopicRepository : ITopicRepository
    {
        RepositoryIntializer<Topic> _topicRepository;
        public TopicRepository(AnnouncementDbContext announcementDbContext) 
        {
            _topicRepository = new RepositoryIntializer<Topic>(announcementDbContext);
        }

        #region Insert Topic related data
        public async Task InsertTopic(Topic topic)
        {
            await _topicRepository.GetGenericRepository.Insert(topic);
        }
        public async Task InsertRangeTopic(List<Topic> topics)
        {
            await _topicRepository.GetGenericRepository.InsertRange(topics);
        }
        #endregion

        #region Get Topic related data
        public async Task<Topic> GetTopicById(int topicId)
        {
            return await _topicRepository.GetGenericRepository.FinDByKey(topicId);
        }
        public async Task<List<Topic>> GetTopicByAnnouncementId(int announcementId)
        {
            return (await _topicRepository.GetGenericRepository
                .FindBy(c => c.AnnouncementId == announcementId)).ToList();
        }
        public async Task<List<Topic>> GetAllTopics()
        {
            return (await _topicRepository.GetGenericRepository.All()).ToList();
        }
        #endregion

        #region Update Topic related data
        public async Task UpdateTopic(Topic topic)
        {
            await _topicRepository.GetGenericRepository.Update(topic);
        }
        #endregion

        #region Delete Topic related data
        public async Task DeleteTopic(Topic topic)
        {
            await _topicRepository.GetGenericRepository.Delete(topic);
        }
        public async Task DeleteRangeTopic(List<Topic> topics)
        {
            await _topicRepository.GetGenericRepository.DeleteRange(topics);
        }
        #endregion
    }
}
