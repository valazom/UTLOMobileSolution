﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class AnnoucementRepository : IAnnouncementRespository
    {
        RepositoryIntializer<UTLO_NewModel.Domain.Announcement> _announcementRepository;
        public AnnoucementRepository(AnnouncementDbContext announcementDbContext)
        {
            _announcementRepository =
                new RepositoryIntializer<UTLO_NewModel.Domain.Announcement>(announcementDbContext);
        }

        #region Insert announcement related data
        public async Task InsertAnnouncement(UTLO_NewModel.Domain.Announcement announcement)
        {
            await _announcementRepository.GetGenericRepository.Insert(announcement);
        }
        public async Task InsertRangeAnnouncements(List<UTLO_NewModel.Domain.Announcement> announcements)
        {
            await _announcementRepository.GetGenericRepository.InsertRange(announcements);
        }
        #endregion

        #region Get announcement related data
        public async Task<List<Topic>> GetTpoicsByAnnouncementId(int announcementId)
        {
            return (await _announcementRepository.GetGenericRepository.
                FindByInclude(c => c.AnnouncementId == announcementId, c => c.Topics))
                .SingleOrDefault().Topics.ToList();
        }
        public async Task<UTLO_NewModel.Domain.Announcement> GetAnnouncementById(int announcementId)
        {
            return await _announcementRepository.GetGenericRepository.FinDByKey(announcementId);
        }
        public async Task<UTLO_NewModel.Domain.Announcement> GetAnnouncementWithTopicById(int id)
        {
            return (await _announcementRepository
                .GetGenericRepository
                .FindByInclude(c => c.AnnouncementId == id, 
                c => c.Topics)).SingleOrDefault();
        }
        public async Task<List<UTLO_NewModel.Domain.Announcement>> GetAllAnnouncements()
        {
            return (await _announcementRepository.GetGenericRepository.AllIncludes(c => c.Topics)).ToList();
        }
        #endregion

        #region Update announcement related data
        public async Task UpdateAnnouncement(UTLO_NewModel.Domain.Announcement announcement)
        {
            await _announcementRepository.GetGenericRepository.Update(announcement);
        }
        #endregion

        #region Delete announcement related data
        public async Task DeleteAnnouncement(UTLO_NewModel.Domain.Announcement announcement)
        {
            await _announcementRepository.GetGenericRepository.Delete(announcement);
        }
        public async Task DeleteRangeAnnouncement(List<UTLO_NewModel.Domain.Announcement> announcements)
        {
            await _announcementRepository.GetGenericRepository.DeleteRange(announcements);
        }
        #endregion
    }
}
