﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IAnnouncementRespository
    {
        #region Insert announcement related data
        Task InsertAnnouncement(UTLO_NewModel.Domain.Announcement announcement);

        Task InsertRangeAnnouncements(List<UTLO_NewModel.Domain.Announcement> announcements);

        #endregion

        #region Get announcement related data
        Task<List<Topic>> GetTpoicsByAnnouncementId(int announcementId);

        Task<UTLO_NewModel.Domain.Announcement> GetAnnouncementById(int announcementId);

        Task<List<UTLO_NewModel.Domain.Announcement>> GetAllAnnouncements();
        Task<UTLO_NewModel.Domain.Announcement> GetAnnouncementWithTopicById(int id);

        #endregion

        #region Update announcement related data
        Task UpdateAnnouncement(UTLO_NewModel.Domain.Announcement announcement);

        #endregion

        #region Delete announcement related data
        Task DeleteAnnouncement(UTLO_NewModel.Domain.Announcement announcement);

        Task DeleteRangeAnnouncement(List<UTLO_NewModel.Domain.Announcement> announcements);
        
        #endregion
    }
}
