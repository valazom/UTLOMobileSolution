﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface ITopicRepository
    {
        #region Insert Topic related data
        Task InsertTopic(Topic topic);

        Task InsertRangeTopic(List<Topic> topics);

        #endregion

        #region Get Topic related data
        Task<Topic> GetTopicById(int topicId);

        Task<List<Topic>> GetTopicByAnnouncementId(int announcementId);

        Task<List<Topic>> GetAllTopics();

        #endregion

        #region Update Topic related data
        Task UpdateTopic(Topic topic);

        #endregion

        #region Delete Topic related data
        Task DeleteTopic(Topic topic);

        Task DeleteRangeTopic(List<Topic> topics);
        
        #endregion
    }
}
