﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Model.Generic.Repository;

namespace UTLO.Model.Data.Repository
{
    public class RepositoryIntializer<TEntity> where TEntity : class
    {
        GenericRepository<TEntity> _genericRepository;
        public RepositoryIntializer(DbContext context)
        {
            _genericRepository = new GenericRepository<TEntity>(context);
        }
        public GenericRepository<TEntity> GetGenericRepository
        {
            get
            {
                return _genericRepository;
            }
        }
    }
}
