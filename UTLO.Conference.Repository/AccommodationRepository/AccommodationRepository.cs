﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Accommodation.Data;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class AccommodationRepository
    {
        RepositoryIntializer<UTLO_NewModel.Domain.Accommodation> _accommodationRepository;
        public AccommodationRepository(AccommodationDbContext accommodationDbContext)
        {
            _accommodationRepository = 
                new RepositoryIntializer<UTLO_NewModel.Domain.Accommodation>(accommodationDbContext);
        }

        #region Insert Accommodation related data
        public void InsertAccommodation(UTLO_NewModel.Domain.Accommodation accommodation)
        {
            _accommodationRepository.GetGenericRepository.Insert(accommodation);
        }
        public void InsertRangeAccommodations(List<UTLO_NewModel.Domain.Accommodation> accommodations)
        {
            _accommodationRepository.GetGenericRepository.InsertRange(accommodations);
        }
        #endregion

        #region Get Accommodation related data
        public UTLO_NewModel.Domain.Accommodation GetAccommodationById(int accommodationId)
        {
            return _accommodationRepository.GetGenericRepository.FinDByKey(accommodationId);
        }
        public List<UTLO_NewModel.Domain.Accommodation> GetAllAccommodations()
        {
            return _accommodationRepository.GetGenericRepository.All().ToList();
        }
        #endregion

        #region Update Accommodation related data
        public void UpdateAccommodation(UTLO_NewModel.Domain.Accommodation accommodattion)
        {
            _accommodationRepository.GetGenericRepository.Update(accommodattion);
        }
        #endregion

        #region Delete Accommodation related data
        public void DeleteAccommodation(UTLO_NewModel.Domain.Accommodation accommodation)
        {
            _accommodationRepository.GetGenericRepository.Delete(accommodation);
        }
        public void DeleteRangeAccommodation(List<UTLO_NewModel.Domain.Accommodation> accommodations)
        {
            _accommodationRepository.GetGenericRepository.DeleteRange(accommodations);
        }
        #endregion
    }
}
