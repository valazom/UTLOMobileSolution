﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Accommodation.Data;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class AccommodationRepository : IAccommodationRepository
    {
        RepositoryIntializer<UTLO_NewModel.Domain.Accommodation> _accommodationRepository;
        public AccommodationRepository(AccommodationDbContext accommodationDbContext)
        {
            _accommodationRepository = 
                new RepositoryIntializer<UTLO_NewModel.Domain.Accommodation>(accommodationDbContext);
        }

        #region Insert Accommodation related data
        public async Task InsertAccommodation(UTLO_NewModel.Domain.Accommodation accommodation)
        {
            await _accommodationRepository.GetGenericRepository.Insert(accommodation);
        }
        public async Task InsertRangeAccommodations(List<UTLO_NewModel.Domain.Accommodation> accommodations)
        {
            await _accommodationRepository.GetGenericRepository.InsertRange(accommodations);
        }
        #endregion

        #region Get Accommodation related data
        public async Task<UTLO_NewModel.Domain.Accommodation> GetAccommodationById(int accommodationId)
        {
            return await _accommodationRepository.GetGenericRepository.FinDByKey(accommodationId);
        }
        public async Task<List<UTLO_NewModel.Domain.Accommodation>> GetAllAccommodations()
        {
            return (await _accommodationRepository.GetGenericRepository.All()).ToList();
        }
        #endregion

        #region Update Accommodation related data
        public async Task UpdateAccommodation(UTLO_NewModel.Domain.Accommodation accommodattion)
        {
            await _accommodationRepository.GetGenericRepository.Update(accommodattion);
        }
        #endregion

        #region Delete Accommodation related data
        public async Task DeleteAccommodation(UTLO_NewModel.Domain.Accommodation accommodation)
        {
            await _accommodationRepository.GetGenericRepository.Delete(accommodation);
        }
        public async Task DeleteRangeAccommodation(List<UTLO_NewModel.Domain.Accommodation> accommodations)
        {
            await _accommodationRepository.GetGenericRepository.DeleteRange(accommodations);
        }
        #endregion
    }
}
