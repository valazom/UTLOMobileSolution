﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Accommodation.Data;
using UTLO_NewModel.Domain;
namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IAccommodationRepository
    {
        #region Insert Accommodation related data
        Task InsertAccommodation(UTLO_NewModel.Domain.Accommodation accommodation);

        Task InsertRangeAccommodations(List<UTLO_NewModel.Domain.Accommodation> accommodations);

        #endregion

        #region Get Accommodation related data
        Task<UTLO_NewModel.Domain.Accommodation> GetAccommodationById(int accommodationId);


        Task<List<UTLO_NewModel.Domain.Accommodation>> GetAllAccommodations();

        #endregion

        #region Update Accommodation related data
        Task UpdateAccommodation(UTLO_NewModel.Domain.Accommodation accommodattion);

        #endregion

        #region Delete Accommodation related data
        Task DeleteAccommodation(UTLO_NewModel.Domain.Accommodation accommodation);

        Task DeleteRangeAccommodation(List<UTLO_NewModel.Domain.Accommodation> accommodations);
        
        #endregion
    }
}
