﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class RateRepository : IRateRepository
    {
        RepositoryIntializer<Rate> _getRateRepository;
        public RateRepository(ConferenceDbContext conferenceDbContext) 
        {
            _getRateRepository = new RepositoryIntializer<Rate>(conferenceDbContext);
        }
        #region Insert presentation rating related data
        public async Task InsertRating(Rate rate)
        {
            await _getRateRepository.GetGenericRepository.Insert(rate);
        }
        public async Task InsertRatingForPresentations(Rate rate, Presentations presentation)
        {
            if(presentation != null)
            {
                rate.presentationId = presentation.PresentationsId;
            }
            await InsertRating(rate);
        }
        #endregion

        #region Get Rate related data
        public async Task<Rate> GetRateById(int rateId)
        {
            return await _getRateRepository.GetGenericRepository.FinDByKey(rateId);
        }
        public async Task<List<Rate>> GetRateBByPresentationId(int id)
        {
            return (await _getRateRepository
                .GetGenericRepository
                .FindBy(c => c.presentationId == id)).ToList();
        }
        public async Task<List<Rate>> GetAllRatings()
        {
            return (await _getRateRepository.GetGenericRepository.All()).ToList();
        }
        #endregion

        #region Update Rate related data
        public async Task UpdateRate(Rate rate)
        {
            await _getRateRepository.GetGenericRepository.Update(rate);
        }
        #endregion

        #region Delete Rate related data
        public async Task DeleteRate(Rate rate)
        {
            await _getRateRepository.GetGenericRepository.Delete(rate);
        }
        public async Task DeleteRange(List<Rate> Ratings)
        {
            await _getRateRepository.GetGenericRepository.DeleteRange(Ratings);
        }
        #endregion
    }
}
