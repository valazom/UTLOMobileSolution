﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class SuitesRepository : ISuitesRepository
    {
        RepositoryIntializer<Suites> _getSuitesRepository;
        public SuitesRepository(ConferenceDbContext conferenceDbConetext)
        {
            _getSuitesRepository = new RepositoryIntializer<Suites>(conferenceDbConetext);
        }

        #region Insert suite related data
        public async Task InsertSuite(Suites suite)
        {
            await _getSuitesRepository.GetGenericRepository.Insert(suite);
        }
        public async Task InsertSuiteWithNavigationProperties(Suites suite,Presentations presentation)
        {
            if (presentation != null)
                suite.presentations.Add(presentation);
            await InsertSuite(suite);
        }
        public async Task InsertRangeSuite(List<Suites> suites)
        {
            await _getSuitesRepository.GetGenericRepository.InsertRange(suites);
        }
        #endregion

        #region Get suites related data
        public async Task<List<Presentations>> GetProgrammesBySuiteID(int SuiteId)
        {
            var results = (await _getSuitesRepository.GetGenericRepository.
                FindBy(c => c.SuitesId == SuiteId)).SingleOrDefault().presentations;
            return results.ToList();
        }
        public async Task<List<Suites>> GetSuitesByProgrammeId(int id)
        {
            var results = (await _getSuitesRepository
                .GetGenericRepository
                .FindByInclude(c => c.ProgrammeId == id, c => c.presentations))
                .ToList();
            return results;
        }
        public async Task<List<Suites>> GetAllSuites()
        {
           return (await _getSuitesRepository.GetGenericRepository.All()).ToList();
        }
        
        #endregion

        #region Update suites related data
        public async Task UpdateSuite(Suites suite)
        {
            await _getSuitesRepository.GetGenericRepository.Update(suite);
        }
        
        #endregion

        #region Delete suite related data
        public async Task DeleteSuite(Suites suite)
        {
            await _getSuitesRepository.GetGenericRepository.Delete(suite);
        }
        public async Task DeleteRangeSuite(List<Suites> suites)
        {
            await _getSuitesRepository.GetGenericRepository.DeleteRange(suites);
        }
        #endregion
    }
}
