﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class ProgrammeRepository : IProgrammeRepository
    {
        RepositoryIntializer<Programme> _programmeRepository;

        public ProgrammeRepository(ConferenceDbContext conferenceDbContext)
        {
            _programmeRepository = new RepositoryIntializer<Programme>(conferenceDbContext);
        }

        #region Insert programme into database
        public async Task InsertProgramme(Programme programme)
        {
            await _programmeRepository.GetGenericRepository.Insert(programme);
        }
        public async Task InsertProgramme(Programme programme, ConferenceDay Day)
        {
            var ConfDbContext = _programmeRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            ConfDbContext.ProgrammeObjects.Add(programme);
            ConfDbContext.ConferenceDayObjects.Attach(Day);
            programme.ConferenceDayId = Day.ConferenceDayId;
            await ConfDbContext.SaveChangesAsync();
        }
       
        public async Task InsertRangeProgramme(List<Programme> programmes)
        {
            await _programmeRepository.GetGenericRepository.InsertRange(programmes);
        }
        #endregion

        #region Get programme information from the database
        public async Task<List<Programme>> GetProgrammeByDay(ConferenceDay Day)
        {
            var results = await _programmeRepository.GetGenericRepository
                .FindByInclude(c => c.ConferenceDayId == Day.Daynumber,
                 c=>c.Suite,c=>c.presenters);
            return results.ToList();
        }
        public async Task<List<Programme>> GetAllProgramme()
        {
            var results = await _programmeRepository.GetGenericRepository
                .AllIncludes(c => c.presenters,c=>c.chairpersons, c => c.Suite);
            return results.ToList();
        }
        public async Task<Programme> GetProgrammeWithPresenterById(int programmeId)
        {
            var results = (await _programmeRepository.GetGenericRepository
                .AllIncludes(c => c.presenters))
                .Where(c=>c.ProgrammeId == programmeId).SingleOrDefault();

            return results;
        }
        public async Task<Programme> GetProgrammeWithChairpersonsById(int programmeId)
        {
            var results = (await _programmeRepository.GetGenericRepository
                          .AllIncludes(c => c.chairpersons))
                          .Where(c => c.ProgrammeId == programmeId).SingleOrDefault();
            return results;
        }

        public async Task<List<Suites>> GetSuitesByProgrammeId(int programmeId)
        {
            var results = (await _programmeRepository.GetGenericRepository.
                FindByInclude(c => c.ProgrammeId == programmeId, c => c.Suite)).SingleOrDefault().Suite.ToList();
            return results;
        }
        #endregion

        #region Update programme related data
        public async Task UpdateProgramme(Programme programme)
        {
            await _programmeRepository.GetGenericRepository.Update(programme);
        }
        public async Task UpdateProgrammeWithPresenter(Programme programme,Attendees attendee)
        {
            var context = _programmeRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (programme.presenters.Any(c=>c.AttendeesId != attendee.AttendeesId) || programme.presenters.Count == 0) //make sure we dont add same attendee
            {
                context.AttendeeObjects.Attach(attendee);
                context.ProgrammeObjects.Attach(programme);
                programme.presenters.Add(attendee);
                await context.SaveChangesAsync();
            }
            
        }
        public async Task UpdateProgrammeWithChairpersons(Programme programme, Attendees attendee)
        {
            var context = _programmeRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (programme.chairpersons.Any(c => c.AttendeesId != attendee.AttendeesId) || programme.chairpersons.Count == 0) //make sure we dont add same attendee
            {
                context.AttendeeObjects.Attach(attendee);
                context.ProgrammeObjects.Attach(programme);
                programme.chairpersons.Add(attendee);
                await context.SaveChangesAsync();
            }

        }
        #endregion

        #region Delete programme related data 
        public async Task DeleteProgramme(Programme programme)
        {
            await _programmeRepository.GetGenericRepository.Delete(programme);
        }
        public async Task RemovePresenterFromProgramme(Programme programme,Attendees attendees)
        {
            var context = _programmeRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            context.ProgrammeObjects.Attach(programme);
            var attendee = programme.presenters
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            programme.presenters.Remove(attendee);
            
            context.Entry(programme).State = System.Data.Entity.EntityState.Modified;
            
            await context.SaveChangesAsync();
        }
        public async Task RemoveChairpersonsFromProgramme(Programme programme, Attendees attendees)
        {
            var context = _programmeRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            context.ProgrammeObjects.Attach(programme);
            var attendee = programme.chairpersons
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            programme.chairpersons.Remove(attendee);

            context.Entry(programme).State = System.Data.Entity.EntityState.Modified;

            await context.SaveChangesAsync();
        }
        public async Task DeleteRangeProgramme(List<Programme> programmes)
        {
            await _programmeRepository.GetGenericRepository.DeleteRange(programmes);
        }
        #endregion
    }
}
