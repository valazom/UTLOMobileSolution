﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;


namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class AttendeeRepository : IAttendeeRepository
    {
        RepositoryIntializer<Attendees> _attendeeRepository;
        public AttendeeRepository(ConferenceDbContext conferenceDbContext)
        {
            _attendeeRepository = new RepositoryIntializer<Attendees>(conferenceDbContext);
        }

        #region Insert Attendee to database
        public async Task AddAttendees(Attendees attendee)
        {
            await _attendeeRepository.GetGenericRepository.Insert(attendee);
        }
        public async Task AddAttendeeWithNavigationProperties(Attendees attendee, 
            Abstracts abstracts=null,
            Presentations presentation=null,
            Programme programme = null)
        {
            if (abstracts != null)
                //attendee.abstracts.Add(abstracts);
            if (presentation != null)
                //attendee.presentations.Add(presentation);
            if (programme != null)
                ///attendee.programmes.Add(programme);
            await AddAttendees(attendee);
        }
        
        public async Task AddRangeAttendees(List<Attendees> attendees)
        {
            await _attendeeRepository.GetGenericRepository.InsertRange(attendees);
        }
        #endregion

        #region get Attendee related data from database
        public async Task<Attendees> GetAttendeeByID(int ID)
        {
            return await _attendeeRepository.GetGenericRepository.FinDByKey(ID);
        }
        public async Task<List<Attendees>> GetKeyNoteSpeakers()
        {
            return (await _attendeeRepository.GetGenericRepository.FindBy(c => c.IsKeynoteSpeaker == true)).ToList();
        }
        public async Task<List<Attendees>> GetSpeakers()
        {
            //var Results = _attendeeRepository.GetGenericRepository
            //    .FindByInclude(c => c.abstracts.Count != 0, c => c.abstracts);
            return new List<Attendees>();//Results.ToList();
        }
        public async Task<List<Attendees>> GetAllSpeakers()
        {
            return (await _attendeeRepository.GetGenericRepository.All()).ToList();
        }
        
        public async Task<List<Abstracts>> GetAbstractsByAttendee(int attendeeId)
        {
            return new List<Abstracts>(); //_attendeeRepository.GetGenericRepository.
                //FindBy(c => c.AttendeesId == attendeeId).SingleOrDefault().abstracts.ToList();
        }
        #endregion

        #region Update Attendee related data
        public async Task UpdateAttendee(Attendees attendee)
        {
            await _attendeeRepository.GetGenericRepository.Update(attendee);
        }
        #endregion

        #region Delete Attendee Related Data
        public async Task DeleteAttendee(Attendees attendee)
        {
            await _attendeeRepository.GetGenericRepository.Delete(attendee);
        }
        public async Task DeleteRangeAttendee(List<Attendees> attendees)
        {
            await _attendeeRepository.GetGenericRepository.DeleteRange(attendees);
        }
        #endregion
    }
}
