﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConferenceRepository
{
    public class PresentationRepository : IPresentationRepository
    {
        RepositoryIntializer<Presentations> _getPresentationRepository;
        public PresentationRepository(ConferenceDbContext conferenceDbConetext)
        {
            _getPresentationRepository = new RepositoryIntializer<Presentations>(conferenceDbConetext);   
        }

        #region Insert presentation related data
        public async Task InsertPresention(Presentations presentation)
        {
            await _getPresentationRepository.GetGenericRepository.Insert(presentation);
        }
        public async Task InsertPresntationWithNavigationProperties(Presentations presenatation,Attendees Author=null)
        {
            if (Author != null)
                presenatation.Authors.Add(Author);
            await InsertPresention(presenatation);
        }
        public async Task InsertRangePresentation(List<Presentations> presentations)
        {
            await _getPresentationRepository.GetGenericRepository.InsertRange(presentations);
        }
        #endregion

        #region Get Presentation related data
        public async Task<List<Presentations>> GetAllPresentations()
        {
            return (await _getPresentationRepository.GetGenericRepository
                .AllIncludes(c => c.Authors, c => c.chairPersons, c => c.Panelists,c=>c.Ratings)).ToList();
        }
        public async Task<List<Rate>> GetRatingsForPresentation(int PresentationId)
        {
            return (await _getPresentationRepository.GetGenericRepository.
                FindByInclude(c => c.PresentationsId == PresentationId, c => c.Ratings))
                .SingleOrDefault().Ratings.ToList();
        }
        public async Task<List<Presentations>> GetPresentationBySuiteId(int suiteId)
        {
            return (await _getPresentationRepository.GetGenericRepository
                .FindBy(c => c.SuiteId == suiteId)).ToList();
                
        }
        public async Task<List<Presentations>> GetPresentationWithAllBySuiteId(int suiteId)
        {
            return (await _getPresentationRepository.GetGenericRepository
                .FindByInclude(c => c.SuiteId == suiteId, 
                c => c.Authors, c => c.chairPersons, 
                c => c.Panelists)).ToList();
        }
        public async Task<Presentations> GetPresentationWithAuthorById(int presentationId)
        {
            var results = (await _getPresentationRepository.GetGenericRepository
                .AllIncludes(c => c.Authors)).Where(c=>c.PresentationsId == presentationId)
                .SingleOrDefault();
            return results;
        }
        public async Task<Presentations> GetPresentationWithChairpersonById(int presentationId)
        {
            var results = (await _getPresentationRepository.GetGenericRepository
                          .AllIncludes(c => c.chairPersons)).Where(c=>c.PresentationsId == presentationId)
                          .SingleOrDefault();
            return results;
        }
        public async Task<Presentations> GetPresentationWithPanelistById(int presentationId)
        {
            var results = (await _getPresentationRepository.GetGenericRepository
                .AllIncludes(c => c.Panelists)).Where(c=>c.PresentationsId == presentationId)
                .SingleOrDefault();
            return results;
        }
        #endregion

        #region Update Presentation related data
        public async Task UpdatePresentation(Presentations presentation)
        {
            await _getPresentationRepository.GetGenericRepository.Update(presentation);
        }
        public async Task UpdatePresentationWithAuthor(Presentations presentation, Attendees attendee)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (presentation.Authors.All(c => c.AttendeesId != attendee.AttendeesId)
                || presentation.Authors.Count == 0)
            {
                context.AttendeeObjects.Attach(attendee);
                context.PresentationObjects.Attach(presentation);
                presentation.Authors.Add(attendee);
                await context.SaveChangesAsync();
            }
        }
        public async Task UpdatePresentationWithChaiperson(Presentations presentation, Attendees attendees)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (presentation.chairPersons.All(c => c.AttendeesId != attendees.AttendeesId)
                || presentation.chairPersons.Count == 0)
            {
                context.AttendeeObjects.Attach(attendees);
                context.PresentationObjects.Attach(presentation);
                presentation.chairPersons.Add(attendees);
                await context.SaveChangesAsync();
            }
        }
        public async Task UpdatePresentationWithPanelist(Presentations presentation, Attendees attendees)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (presentation.Panelists.All(c => c.AttendeesId != attendees.AttendeesId)
                || presentation.Panelists.Count == 0)
            {
                context.AttendeeObjects.Attach(attendees);
                context.PresentationObjects.Attach(presentation);
                presentation.Panelists.Add(attendees);
                await context.SaveChangesAsync();
            }
        }
        #endregion

        #region Delete presentation related data
        public async Task DeletePresentation(Presentations pressentation)
        {
            await _getPresentationRepository.GetGenericRepository.Delete(pressentation);
        }
        public async Task DeleteRangePresentations(List<Presentations> presentations)
        {
            await _getPresentationRepository.GetGenericRepository.DeleteRange(presentations);
        }
        
        public async Task RemoveAuthorFromPresentation(Presentations presentation,Attendees attendees)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            context.PresentationObjects.Attach(presentation);
            var attendee = presentation.Authors
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            presentation.Authors.Remove(attendee);
            await context.SaveChangesAsync();
        }
        public async Task RemoveChaipersonFromPresentation(Presentations presentation,Attendees attendees)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            context.PresentationObjects.Attach(presentation);
            var attendee = presentation.chairPersons
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            presentation.chairPersons.Remove(attendee);
            await context.SaveChangesAsync();
        }
        public async Task RemovePanelistFromPresentation(Presentations presentation,Attendees attendees)
        {
            var context = _getPresentationRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            context.PresentationObjects.Attach(presentation);
            var attendee = presentation.Panelists
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            presentation.Panelists.Remove(attendee);
            await context.SaveChangesAsync();
        }
        #endregion

    }
}
