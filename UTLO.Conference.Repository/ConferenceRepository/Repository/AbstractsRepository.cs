﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class AbstractsRepository : IAbstractRespository
    {
        RepositoryIntializer<Abstracts> _getAbstractsRepository;
        public AbstractsRepository(ConferenceDbContext conferenceDbContext)
        {
            _getAbstractsRepository = new RepositoryIntializer<Abstracts>(conferenceDbContext);
        }
        #region Insert Abstract related data
        public async Task InsertAbstracts(Abstracts abstracts)
        {
            await _getAbstractsRepository.GetGenericRepository.Insert(abstracts);
        }
        public async Task InsertAbstractsWithNavigationProperties(Abstracts abstracts,Attendees authors = null)
        {
            if (authors != null)
                abstracts.Authors.Add(authors);
            await InsertAbstracts(abstracts);
        }
        public async Task InsertRangeAbstracts(List<Abstracts> abstracts)
        {
            await
                _getAbstractsRepository.GetGenericRepository.InsertRange(abstracts);
        }
        #endregion

        #region Get Abstract related data
        public async Task<List<Attendees>> GetAttendeesByAbstracts(int abstractsId)
        {
            var results = (await _getAbstractsRepository.GetGenericRepository.
                AllIncludes(c => c.Authors))
                .Where(c => c.abstractsId == abstractsId)
                .SingleOrDefault().Authors.ToList();
            return results;
        }
        public async Task<Abstracts> GetAbstractsById(int abstractsId)
        {
            var results = (await _getAbstractsRepository.GetGenericRepository
                .FindByInclude(c => c.abstractsId == abstractsId, x => x.Authors))
                .FirstOrDefault();
            return results;
        }
        public async Task<List<Abstracts>> GetAllAbstracts()
        {
            return (await _getAbstractsRepository.GetGenericRepository
                .AllIncludes(c=>c.Authors)).ToList();
        }
        public async Task<List<Abstracts>> GetAllAbstractsWithAttendees()
        {
            var abstracts =  await GetAllAbstracts();
            foreach(var abs in abstracts)
            {
                var attendees =  await GetAttendeesByAbstracts(abs.abstractsId);
                abs.Authors = attendees;
            }
            return abstracts;
        }
        public async Task<List<Attendees>> GetAllAuthors()
        {
            return (await _getAbstractsRepository.GetGenericRepository.
                All()).SelectMany(c=>c.Authors).ToList();
        }
        #endregion

        #region Update Abstract related data
        public async Task UpdateAbstracts(Abstracts abstracts)
        {
            await _getAbstractsRepository.GetGenericRepository.Update(abstracts);
        }
        public async Task UpdateAbstractsWithAuthor(Abstracts abstracts,Attendees author)
        {
            var context = _getAbstractsRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            if (abstracts.Authors.All(c=>c.AttendeesId != author.AttendeesId) || abstracts.Authors.Count == 0)
            {
                context.AttendeeObjects.Attach(author);
                context.AbstractObjects.Attach(abstracts);
                abstracts.Authors.Add(author);
                await context.SaveChangesAsync();
            }
        }
        #endregion

        #region Delete abstracts related data
        public async Task DeleteAbstracts(Abstracts abstracts)
        {
            await _getAbstractsRepository.GetGenericRepository.Delete(abstracts);
        }
        public async Task DeleteRangeAbstracts(List<Abstracts> abstracts)
        {
            await _getAbstractsRepository.GetGenericRepository.DeleteRange(abstracts);
        }
        public async Task RemoveAuthorFromAbstracts(Abstracts abstracts,Attendees attendees)
        {
            var context = _getAbstractsRepository.GetGenericRepository.GetDbContext as ConferenceDbContext;
            context.Entry(attendees).State = System.Data.Entity.EntityState.Detached;
            var attendee = abstracts.Authors
                .Where(c => c.AttendeesId == attendees.AttendeesId).SingleOrDefault();
            context.AbstractObjects.Attach(abstracts);
            abstracts.Authors.Remove(attendee);
            await context.SaveChangesAsync();
        }

        
        #endregion
    }
}
