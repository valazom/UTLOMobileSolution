﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;
using UTLO.Model.Data.Repository;
using UTLO_NewModel;
using UTLO_NewModel.Domain;
using UTLO_NewModel.Enums;

namespace UTLO.Conference.Repository.ConferenceRepository.Repository
{
    public class SettingsRepository : ISettingsRepository
    {
        private RepositoryIntializer<Settings> _getSettingsRepository;

        public SettingsRepository(ConferenceDbContext context)
        {
            _getSettingsRepository = new RepositoryIntializer<Settings>(context);
        }
        public async Task DeleteAllSettings()
        {
            await _getSettingsRepository
                .GetGenericRepository.DeleteRange(await GetAllSettings());
        }

        public async Task DeleteSettingById(int Id)
        {
            await _getSettingsRepository
                .GetGenericRepository.Delete(await GetSettingsById(Id));
        }
        public async Task DeleteSetting(Settings settings)
        {
            await _getSettingsRepository.GetGenericRepository.Delete(settings);
        }
        public async Task<List<Settings>> GetAllSettings()
        {
            return (await _getSettingsRepository.GetGenericRepository.All()).ToList();
        }
        public async Task<Settings> GetSettingsByName(string name)
        {
            return (await _getSettingsRepository.GetGenericRepository.FindBy(c => c.SettingName == name)).SingleOrDefault();
        }
        public async Task<Settings> GetSettingsById(int Id)
        {
            return await _getSettingsRepository.GetGenericRepository.FinDByKey(Id);
        }
        public async Task<List<Settings>> GetSettingsByType(SettingType settingType)
        {
            return (await _getSettingsRepository.GetGenericRepository
                .FindBy(c => c.SettingType == settingType)).ToList();
        }
        public async Task InsertRangeSettings(List<Settings> settings)
        {
            await _getSettingsRepository.GetGenericRepository.InsertRange(settings);
        }

        public async Task InsertSetting(Settings setting)
        {
            await _getSettingsRepository.GetGenericRepository.Insert(setting);
        }

        public async Task UpdateSettingsById(int Id)
        {
            await _getSettingsRepository
                .GetGenericRepository.Update(await GetSettingsById(Id));
        }
        public async Task UpdateSettings(Settings settings)
        {
            await _getSettingsRepository.GetGenericRepository.Update(settings);
        }
    }
}
