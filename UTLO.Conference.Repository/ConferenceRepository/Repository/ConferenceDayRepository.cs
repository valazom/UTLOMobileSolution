﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public class ConferenceDayRepository : IConferenceDayRepository
    {
        RepositoryIntializer<ConferenceDay> _getConferenceDayRepository;
        public ConferenceDayRepository(ConferenceDbContext conferenceDbContext)
        {
            _getConferenceDayRepository = new RepositoryIntializer<ConferenceDay>(conferenceDbContext);
        }
        #region Insert ConferenceDay related data
        public async Task InsertConferenceDay(ConferenceDay conferenceDay)
        {
            await _getConferenceDayRepository.GetGenericRepository.Insert(conferenceDay);
        }
        public async Task InsertConferenceDayWithNavigationProperties
            (ConferenceDay conferenceDay,Programme programme = null)
        {
            if (programme != null)
                conferenceDay.programme.Add(programme);
            await InsertConferenceDay(conferenceDay);
        }
        public async Task InsertRangeConferenceDay(List<ConferenceDay> conferenceDay)
        {
            await _getConferenceDayRepository.GetGenericRepository.InsertRange(conferenceDay);
        }
        #endregion

        #region Get ConferenceDay related data
        public async Task<List<Programme>> GetProgrammesByDay(ConferenceDay day)
        {
            return (await _getConferenceDayRepository.GetGenericRepository.
                FindByInclude(c => c.ConferenceDayId == day.ConferenceDayId, c => c.programme))
                .SingleOrDefault().programme.ToList();
        }
        public async Task<ConferenceDay> getConferenceDayById(int id)
        {
            return (await _getConferenceDayRepository
                .GetGenericRepository.FindBy(c=>c.ConferenceDayId == id))
                .SingleOrDefault();
        }
        public async Task<List<ConferenceDay>> GetAllConferenceDay()
        {
            return (await _getConferenceDayRepository.GetGenericRepository.All()).ToList();
        }
        public async Task<List<ConferenceDay>> GetAllConferenceIncludingProgramme()
        {
            var ListDay = (await _getConferenceDayRepository.GetGenericRepository.AllIncludes(c=>c.programme)).ToList();
            return ListDay;
        }
        #endregion

        #region Update conferenceDay related data
        public async Task UpdateConferenceDay(ConferenceDay conferenceDay)
        {
            await _getConferenceDayRepository.GetGenericRepository.Update(conferenceDay);
        }
        #endregion

        #region Delete conferenceDay related data
        public async Task DeleteConferenceDay(ConferenceDay conferenceDay)
        {
            await _getConferenceDayRepository.GetGenericRepository.Delete(conferenceDay);
        }
        public async Task DeleteRangeConference(List<ConferenceDay> conferenceDays)
        {
            await _getConferenceDayRepository.GetGenericRepository.DeleteRange(conferenceDays);
        }
        #endregion

    }
}
