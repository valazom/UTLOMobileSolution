﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;


namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IProgrammeRepository
    {
        #region Insert programme into database
        Task InsertProgramme(Programme programme);
        Task InsertProgramme(Programme programme,ConferenceDay Day);

        Task InsertRangeProgramme(List<Programme> programmes);

        #endregion

        #region Get programme information from the database
        Task<List<Programme>> GetProgrammeByDay(ConferenceDay Day);

        Task<List<Programme>> GetAllProgramme();

        Task<List<Suites>> GetSuitesByProgrammeId(int programmeId);

        Task<Programme> GetProgrammeWithPresenterById(int programmeId);
        Task<Programme> GetProgrammeWithChairpersonsById(int programmeId);

        #endregion

        #region Update programme related data
        Task UpdateProgramme(Programme programme);

        Task UpdateProgrammeWithPresenter(Programme programme, Attendees attendee);

        Task UpdateProgrammeWithChairpersons(Programme programme, Attendees attendee);

        #endregion

        #region Delete programme related data 
        Task DeleteProgramme(Programme programme);

        Task RemovePresenterFromProgramme(Programme programme, Attendees attendee);

        Task RemoveChairpersonsFromProgramme(Programme programme, Attendees attendee);

        Task DeleteRangeProgramme(List<Programme> programmes);
        
        #endregion
    }
}
