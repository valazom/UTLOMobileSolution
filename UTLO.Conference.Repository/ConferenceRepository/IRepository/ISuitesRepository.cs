﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface ISuitesRepository
    {
        #region Insert suite related data
        Task InsertSuite(Suites suite);

        Task InsertSuiteWithNavigationProperties(Suites suite, Presentations presentation);

        Task InsertRangeSuite(List<Suites> suites);

        #endregion

        #region Get suites related data
        Task<List<Presentations>> GetProgrammesBySuiteID(int SuiteId);
        
        Task<List<Suites>> GetSuitesByProgrammeId(int id);

        Task<List<Suites>> GetAllSuites();


        #endregion

        #region Update suites related data
        Task UpdateSuite(Suites suite);

        #endregion

        #region Delete suite related data
        Task DeleteSuite(Suites suite);

        Task DeleteRangeSuite(List<Suites> suites);
        
        #endregion
    }
}
