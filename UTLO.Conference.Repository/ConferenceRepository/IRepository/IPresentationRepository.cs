﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConferenceRepository
{
    public interface IPresentationRepository
    {
        #region Insert presentation related data
        Task InsertPresention(Presentations presentation);

        Task InsertPresntationWithNavigationProperties(Presentations presenatation, Attendees Author = null);

        Task InsertRangePresentation(List<Presentations> presentations);

        #endregion

        #region Get Presentation related data
        Task<List<Presentations>> GetAllPresentations();

        Task<List<Rate>> GetRatingsForPresentation(int PresentationId);

        Task<List<Presentations>> GetPresentationBySuiteId(int suiteId);
        Task<List<Presentations>> GetPresentationWithAllBySuiteId(int suiteId);
        Task<Presentations> GetPresentationWithAuthorById(int presentationId);
        Task<Presentations> GetPresentationWithChairpersonById(int presentationId);
        Task<Presentations> GetPresentationWithPanelistById(int presentationId);



        #endregion

        #region Update Presentation related data
        Task UpdatePresentation(Presentations presentation);
        Task UpdatePresentationWithAuthor(Presentations presentation, Attendees attendee);
        Task UpdatePresentationWithChaiperson(Presentations presentation, Attendees attendees);
        Task UpdatePresentationWithPanelist(Presentations presentation, Attendees attendees);

        #endregion

        #region Delete presentation related data
        Task DeletePresentation(Presentations pressentation);

        Task DeleteRangePresentations(List<Presentations> presentations);

        Task RemoveAuthorFromPresentation(Presentations presentation, Attendees attendees);
        Task RemoveChaipersonFromPresentation(Presentations presentation, Attendees attendees);
        Task RemovePanelistFromPresentation(Presentations presentation, Attendees attendees);

        #endregion
    }
}
