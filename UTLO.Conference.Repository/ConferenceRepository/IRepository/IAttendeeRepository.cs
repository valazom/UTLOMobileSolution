﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IAttendeeRepository
    {
        #region Insert Attendee to database
        Task AddAttendees(Attendees attendee);

        Task AddAttendeeWithNavigationProperties(Attendees attendee,
            Abstracts abstracts = null,
            Presentations presentation = null,
            Programme programme = null);


        Task AddRangeAttendees(List<Attendees> attendees);

        #endregion

        #region get Attendee related data from database
        Task<Attendees> GetAttendeeByID(int ID);

        Task<List<Attendees>> GetKeyNoteSpeakers();

        Task<List<Attendees>> GetSpeakers();

        Task<List<Attendees>> GetAllSpeakers();

        Task<List<Abstracts>> GetAbstractsByAttendee(int attendeeId);

        #endregion

        #region Update Attendee related data
        Task UpdateAttendee(Attendees attendee);

        #endregion

        #region Delete Attendee Related Data
        Task DeleteAttendee(Attendees attendee);

        Task DeleteRangeAttendee(List<Attendees> attendees);
        
        #endregion
    }
}
