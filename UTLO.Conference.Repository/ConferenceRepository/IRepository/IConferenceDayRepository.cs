﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IConferenceDayRepository
    {
        #region Insert ConferenceDay related data
        Task InsertConferenceDay(ConferenceDay conferenceDay);

        Task InsertConferenceDayWithNavigationProperties
            (ConferenceDay conferenceDay, Programme programme = null);

        Task InsertRangeConferenceDay(List<ConferenceDay> conferenceDay);

        #endregion

        #region Get ConferenceDay related data
        Task<List<Programme>> GetProgrammesByDay(ConferenceDay day);

        Task<List<ConferenceDay>> GetAllConferenceDay();

        Task<List<ConferenceDay>> GetAllConferenceIncludingProgramme();
        Task<ConferenceDay> getConferenceDayById(int id);

        #endregion

        #region Update conferenceDay related data
        Task UpdateConferenceDay(ConferenceDay conferenceDay);

        #endregion

        #region Delete conferenceDay related data
        Task DeleteConferenceDay(ConferenceDay conferenceDay);

        Task DeleteRangeConference(List<ConferenceDay> conferenceDays);
        
        #endregion
    }
}
