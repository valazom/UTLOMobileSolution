﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;
using UTLO_NewModel.Enums;

namespace UTLO.Conference.Repository.ConferenceRepository.IRepository
{
    public interface ISettingsRepository
    {
        #region insert settings related data
        Task InsertSetting(Settings setting);
        Task InsertRangeSettings(List<Settings> settings);
        #endregion
        #region Get settings related data 
        Task<Settings> GetSettingsById(int Id);
        Task<List<Settings>> GetAllSettings();
        Task<Settings> GetSettingsByName(string name);
        Task<List<Settings>> GetSettingsByType(SettingType settingType);
        #endregion
        #region Update setting
        Task UpdateSettingsById(int Id);
        Task UpdateSettings(Settings settings);
        #endregion
        #region delete settings
        Task DeleteSettingById(int Id);
        Task DeleteAllSettings();
        Task DeleteSetting(Settings settings);
        #endregion
    }
}
