﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IRateRepository
    {
        #region Insert presentation rating related data
        Task InsertRating(Rate rate);

        Task InsertRatingForPresentations(Rate rate, Presentations presentation);

        #endregion

        #region Get Rate related data
        Task<Rate> GetRateById(int rateId);

        Task<List<Rate>> GetRateBByPresentationId(int id);
        Task<List<Rate>> GetAllRatings();

        #endregion

        #region Update Rate related data
        Task UpdateRate(Rate rate);

        #endregion

        #region Delete Rate related data
        Task DeleteRate(Rate rate);

        Task DeleteRange(List<Rate> Ratings);
        
        #endregion
    }
}
