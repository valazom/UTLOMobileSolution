﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;
using UTLO_NewModel.Domain;


namespace UTLO.Model.Data.Repository.ConfereenceRepository
{
    public interface IAbstractRespository
    {
        #region Insert Abstract related data
        Task InsertAbstracts(Abstracts abstracts);

        Task InsertAbstractsWithNavigationProperties(Abstracts abstracts, Attendees authors = null);

        Task InsertRangeAbstracts(List<Abstracts> abstracts);
        #endregion

        #region Get Abstract related data
        Task<List<Attendees>> GetAttendeesByAbstracts(int abstractsId);
        Task<Abstracts> GetAbstractsById(int abstractsId);
        Task<List<Attendees>> GetAllAuthors();
        Task<List<Abstracts>> GetAllAbstracts();
        Task<List<Abstracts>> GetAllAbstractsWithAttendees();

        #endregion

        #region Update Abstract related data
        Task UpdateAbstracts(Abstracts abstracts);
        Task UpdateAbstractsWithAuthor(Abstracts abstracts, Attendees author);

        #endregion

        #region Delete abstracts related data
        Task DeleteAbstracts(Abstracts abstracts);

        Task DeleteRangeAbstracts(List<Abstracts> abstracts);
        Task RemoveAuthorFromAbstracts(Abstracts abstracts, Attendees attendees);

        #endregion
    }
}
