﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.LegacyModels.Model
{
    public class ConfItems
    {
        private int imageId;
        private string conf_title;

        public ConfItems(int imgId, string c_title)
        {
            imageId = imgId;
            conf_title = c_title;
        }
        public int getImageId
        {
            get
            {
                return imageId;
            }
        }
        public string getConf_title
        {
            get
            {
                return conf_title;
            }
        }
    }
}
