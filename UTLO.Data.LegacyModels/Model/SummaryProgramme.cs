﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.LegacyModels.Model
{
    public class SummaryProgramme
    {
        public int RegImageId { get; set; }
        public int CalImageId { get; set; }
        public string TimeDesc { get; set; }
        public string TextDesc { get; set; }
        public string DayName { get; set; }
    }
}
