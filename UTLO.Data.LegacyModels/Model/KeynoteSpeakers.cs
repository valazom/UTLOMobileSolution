﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.LegacyModels.Model
{
    public class KeynoteSpeakers
    {
        public int SpeakerId { get; set; }
        public string Speakername { get; set; }
        public string position { get; set; }
        public string Affilation { get; set; }
        public string Status { get; set; }
        public string Biography { get; set; }
    }
}
