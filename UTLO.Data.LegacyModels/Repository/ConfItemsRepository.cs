﻿using System;
using System.Collections.Generic;
using System.Text;
using UTLO.Data.LegacyModels.Model;

namespace UTLO.Data.LegacyModels.Repository
{
    public class ConfItemsRepository
    {
        private static List<ConfItems> confItemsList;

        public ConfItemsRepository()
        {
            confItemsList = new List<ConfItems>();
        }
        public void AddItem(int imageId, string desc)
        {

            confItemsList.Add(new ConfItems(imageId, desc));
        }
        public ConfItems getItem(int imageId)
        {
            var confItem = confItemsList.Find(a => a.getImageId == imageId);
            return confItem;
        }
        public void deleteConfitems(int imageId)
        {
            var confItem = confItemsList.Find(a => a.getImageId == imageId);
            confItemsList.Remove(confItem);
        }
        public List<ConfItems> getAllConfitems
        {
            get
            {
                return confItemsList;
            }
        }
    }
}
