﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_NewModel.Enums
{
    public enum SettingType
    {
        Compulsory,
        Custom,
    }
    public enum CustomSettings
    {
        Title,
        SubTitle,
        Address,
        Longitude,
        Latitude,
        CachingTime
    }
}
