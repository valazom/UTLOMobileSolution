﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;

namespace UTLO_NewModel.Interfaces
{
    public interface IStateObject
    {
        ObjectState State { get; }
    }
}
