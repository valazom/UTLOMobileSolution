﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;
using UTLO_NewModel.Interfaces;

namespace UTLO_NewModel.Domain
{
    public class Presentations
    {
        [Key]
        public int PresentationsId { get; set; }
        [Required]
        public int SuiteId { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan StartTime { get; set; }
        [DataType(DataType.Time)]
        public TimeSpan EndTime { get; set; }
        public ICollection<Attendees> Authors { get; set; }
        [Required]
        public string Title { get; set; }
        public PresentationType type { get; set; }
        public ICollection<Attendees> chairPersons { get; set; }
        public ICollection<Attendees> Panelists { get; set; }
        public ICollection<Rate> Ratings { get; set; }
        [Required, MaxLength(250,
            ErrorMessage = "The length of the field is more than expected")]
        public string Venue { get; set; }

    }
}
