﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_NewModel.Domain
{
    public class Answers
    {
        [Key]
        public int AnswerId { get; set; }
        [Required]
        public int EvaluationQuestionId { get; set; }
        public string answer { get; set; }
        public int Rating { get; set; }
    }
}
