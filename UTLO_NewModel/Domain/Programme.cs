﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;
using UTLO_NewModel.Interfaces;

namespace UTLO_NewModel.Domain
{
    public class Programme
    {
        [Key]
        public int ProgrammeId { get; set; }
        [Required, MaxLength(256,
            ErrorMessage = "The length of the field is more than expected")]
        public string Title { get; set; }
        public ICollection<Attendees> presenters { get; set; }
        [Required]
        public TimeSpan StartTime { get;set; }
        [Required]
        public TimeSpan EndTime { get; set; }
        public ICollection<Suites> Suite { get; set; }
        [MaxLength(250,
            ErrorMessage = "The length of the field is more than expected")]
        public string Venue { get; set; }
        public ICollection<Attendees> chairpersons { get; set; }
        public ProgrammeType programmeType { get; set; }
        [Required]
        public int ConferenceDayId { get; set; }
     
        
    }
}
