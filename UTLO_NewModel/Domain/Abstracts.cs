﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_NewModel.Domain
{
    public class Abstracts
    {
        [Key]
        public int abstractsId { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Theme { get; set; }
        public ICollection<Attendees> Authors { get; set; } = new List<Attendees>();
    }
}
