﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_NewModel.Domain
{
    public class Topic
    {
        [Key]
        public int TopicId { get; set; }
        [Required]
        public int AnnouncementId { get; set; }
        public string Bulletpoint { get; set; }
    }
}
