﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_NewModel.Domain
{
    public class Accommodation
    {
        [Key]
        public int accommodationId { get; set; }
        [Required]
        public string accommodationName { get; set; }
        [Required]
        public string url { get; set; }
        
    }
}
