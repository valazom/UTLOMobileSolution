﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;

namespace UTLO_NewModel.Domain
{
    public class Settings
    {

        [Key]
        public int SettingId { get; set; }
        [StringLength(450)]
        [Index(IsUnique = true)]
        [Required]
        public string SettingName { get; set; }
        [Required]
        public string SettingValue { get; set; }
        [Required]
        public SettingType SettingType { get; set; }
        
    }
}
