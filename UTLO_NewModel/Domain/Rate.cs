﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;
using UTLO_NewModel.Interfaces;

namespace UTLO_NewModel.Domain
{
    public class Rate
    {
        [Key]
        public int RateId { get; set; }
        [Required]
        public int presentationId { get; set; }
        public int Ratings { get; set; }
       
    }
}
