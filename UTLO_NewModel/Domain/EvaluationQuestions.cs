﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;

namespace UTLO_NewModel.Domain
{
    public class EvaluationQuestions
    {
        [Key]
        public int EvaluationQuestionsId { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public QuestionType Qtype { get; set; }
        public bool QuestionsRequired { get; set; }
        public ICollection<Answers> Answer { get; set; }
    }
}
