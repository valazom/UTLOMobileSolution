﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using UTLO_NewModel;
using UTLO.Accommodation.Data;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Conference.Repository.EvaluationRepository.Repository;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;
using UTLO.Conference.Repository.ConferenceRepository.Repository;

namespace UTLO.Conference.WebApi
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                          .SetBasePath(env.ContentRootPath)
                          .AddJsonFile("appsettings.json")
                          .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped(_ => new ConferenceDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new EvaluationDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new AnnouncementDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new AccommodationDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IAbstractRespository, AbstractsRepository>();
            services.AddScoped<IAttendeeRepository, AttendeeRepository>();
            services.AddScoped<IConferenceDayRepository, ConferenceDayRepository>();
            services.AddScoped<IPresentationRepository, PresentationRepository>();
            services.AddScoped<IProgrammeRepository, ProgrammeRepository>();
            services.AddScoped<IRateRepository, RateRepository>();
            services.AddScoped<ISuitesRepository, SuitesRepository>();
            services.AddScoped<ISettingsRepository, SettingsRepository>();
            services.AddScoped<IAnnouncementRespository, AnnoucementRepository>();
            services.AddScoped<IEvaluationQuestionRepository, EvaluationQuestionsRepository>();
            services.AddScoped<IAnswerRepository, AnswerRepository>();
            services.AddScoped<IAccommodationRepository, AccommodationRepository>();
            services.AddSingleton<IConfiguration>(Configuration);
        }
        public IConfiguration Configuration { get; set; }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePages();
            app.UseMvc();
        }
    }
}
