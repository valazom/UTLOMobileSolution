﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO.Conference.WebApi.ViewModels;

namespace UTLO.Conference.WebApi
{
    public class CustomValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (AnswerViewModel)validationContext.ObjectInstance;
            if(model.Rating == 0 && string.IsNullOrEmpty(model.answer))
            {
                return new ValidationResult("Both rating and answer cannot be empty");
            }
            return ValidationResult.Success;
        }
    }
}
