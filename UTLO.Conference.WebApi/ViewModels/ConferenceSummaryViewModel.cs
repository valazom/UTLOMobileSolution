﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.WebApi.ViewModels
{
    public class ConferenceSummaryViewModel
    {
        public DateTime conferenceDate { get; set; }
        public List<Programme> programmes { get; set; }
    }
}
