﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApi.ViewModels
{
    public class RatingViewModel
    {
        [Required]
        public int Rating { get; set; }
    }
}
