﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApi.ViewModels
{
    public class AnswerViewModel
    {
        public int Rating { get; set; }
        [CustomValidator]
        public string answer { get; set; }
    }
}
