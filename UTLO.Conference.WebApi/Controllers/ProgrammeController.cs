﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Conference.WebApi.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/days")]
    public class ProgrammeController : Controller
    {
        private IConferenceDayRepository _conferenceDayRepository;
        private IProgrammeRepository _programmeRepository;

        // GET: /<controller>/
        public ProgrammeController(IConferenceDayRepository conferenceDayRepository,
                                    IProgrammeRepository programmeRepository)
        {
            _conferenceDayRepository = conferenceDayRepository;
            _programmeRepository = programmeRepository;
        }
        [HttpGet("summary")]
        public async Task<IActionResult> GetConferenceSummary()
        {
            try
            {
                var models = new List<ConferenceSummaryViewModel>();
                var conferenceData = await _conferenceDayRepository
                    .GetAllConferenceIncludingProgramme();
                if (conferenceData != null)
                {
                    foreach (var day in conferenceData)
                    {
                        models.Add(new ConferenceSummaryViewModel
                        {
                            conferenceDate = day.ConferenceDate,
                            programmes = day.programme.ToList()
                        });
                    }
                }
                return Ok(models);
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet]
        public async Task<IActionResult> AllConferenceDays()
        {
            try
            {
                var conferenceDays = await _conferenceDayRepository.GetAllConferenceDay();
                if(conferenceDays != null)
                {
                    return Ok(conferenceDays);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConferenceDayById(int id)
        {
            try
            {
                var conferenceDay = await _conferenceDayRepository.getConferenceDayById(id);
                if(conferenceDay != null)
                {
                    return Ok(conferenceDay);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("programmes")]
        public async Task<IActionResult> GetConferenceDaysWithProgrammes()
        {
            try
            {
                var conferenceDaysWithProgrammes = await _conferenceDayRepository
                       .GetAllConferenceIncludingProgramme();
                
                if(conferenceDaysWithProgrammes != null)
                {
                    foreach (var conferenceDay in conferenceDaysWithProgrammes)
                    {
                        conferenceDay.programme = conferenceDay.programme
                            .OrderBy(c => c.StartTime).ToList();
                    }
                    var confprogrammes = conferenceDaysWithProgrammes
                        .OrderBy(c => c.ConferenceDate).ToList();
                    return Ok(confprogrammes);
                }
                return NoContent();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpGet("{id}/programmes")]
        public async Task<IActionResult> GetConferenceDayWithProgramme(int id)
        {
            try
            {
                var conferenceDay = await _conferenceDayRepository.getConferenceDayById(id);
                if(conferenceDay != null)
                {
                    var conferenceDayProgrammes = await _programmeRepository
                        .GetProgrammeByDay(conferenceDay);
                    if (conferenceDayProgrammes != null)
                    {
                        return Ok(conferenceDayProgrammes);
                    }
                    return NoContent();
                }
                
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
