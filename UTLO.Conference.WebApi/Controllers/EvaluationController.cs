﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Conference.WebApi.ViewModels;
using UTLO_NewModel.Domain;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/evaluation")]
    public class EvaluationController : Controller
    {
        private IEvaluationQuestionRepository _evaluationRepository;
        private IAnswerRepository _answerRepository;


        // GET: /<controller>/
        public EvaluationController(IEvaluationQuestionRepository evaluationRepository,
            IAnswerRepository answerRepository)
        {
            _evaluationRepository = evaluationRepository;
            _answerRepository = answerRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllEvaluation()
        {
            try
            {
                var evaluations = await _evaluationRepository.GetAllEvaluationQuestions();
                if(evaluations != null)
                {
                    return Ok(evaluations);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetEvaluationById(int id)
        {
            try
            {
                var evaluation = await _evaluationRepository.GetEvaluationQuestionById(id);
                if(evaluation != null)
                {
                    return Ok(evaluation);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost("{id}/answer")]
        public async Task<IActionResult> Answer(int id,
            [FromBody] AnswerViewModel model)
        {
            try
            {
                if (ModelState.IsValid) 
                {
                    var answer = new Answers();
                    var evaluation = await _evaluationRepository.GetEvaluationQuestionById(id);
                    answer.EvaluationQuestionId = id;
                    if(evaluation.Qtype == UTLO_NewModel.Enums.QuestionType.Rating)
                        answer.Rating = model.Rating;
                    if(evaluation.Qtype == UTLO_NewModel.Enums.QuestionType.FillingTheGap)
                        answer.answer = model.answer;
                    await _answerRepository.InsertAnswer(answer);
                    return Ok();
                }
                ModelState.AddModelError("", "kindly check your entries");
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        private async Task<bool> ValidateAnswers(List<Answers> answers)
        {
            bool isValid = false;
            bool result = true;
            foreach (var answer in answers)
            {
                var currentQuestion = await _evaluationRepository.GetEvaluationQuestionById(answer.EvaluationQuestionId);
                    
                if (currentQuestion.QuestionsRequired
                    &&
                    currentQuestion.Qtype == UTLO_NewModel.Enums.QuestionType.FillingTheGap
                    && !string.IsNullOrEmpty(answer.answer))
                {
                    isValid = true;
                    result = isValid && result;
                }
                else if (currentQuestion.QuestionsRequired
                    &&
                    currentQuestion.Qtype == UTLO_NewModel.Enums.QuestionType.Rating
                    && answer.Rating != 0)
                {
                    isValid = true;
                    result = isValid && result;
                }
                else if (!currentQuestion.QuestionsRequired)
                {
                    isValid = true;
                    result = isValid && result;
                }
                else
                {
                    isValid = false;
                    result = isValid && result;
                }
            }
            return result;
        }
        [HttpPost("answers")]
        public async Task<IActionResult> Answers([FromBody] List<Answers> answers)
        {
            try
            {
                if(answers != null)
                {
                    if (await ValidateAnswers(answers))
                    {
                        foreach (var answer in answers)
                        {
                            await _answerRepository.InsertAnswer(answer);
                        }
                        return Ok();
                    }
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }

    }
}
