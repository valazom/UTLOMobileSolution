﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Conference.WebApi.ViewModels;
using UTLO_NewModel.Domain;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/presentation")]
    public class PresentationController : Controller
    {
        private ISuitesRepository _suitesRepository;
        private IPresentationRepository _presentationRepository;
        private IRateRepository _ratingRepository;

        // GET: /<controller>/
        public PresentationController(ISuitesRepository suiteRepository,
            IPresentationRepository presentationRepository,
            IRateRepository ratingRepository)
        {
            _suitesRepository = suiteRepository;
            _presentationRepository = presentationRepository;
            _ratingRepository = ratingRepository;
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPresentationBySuiteId(int id)
        {
            try
            {
                var presentation = await _presentationRepository.GetPresentationWithAllBySuiteId(id);
                
                if(presentation != null)
                {
                    return Ok(presentation);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost("{id}/rate")]
        public async Task<IActionResult> RatePresentation(int id,
                         [FromBody] RatingViewModel model)
        {
            try
            {
                
                if (ModelState.IsValid)
                {
                    var rating = new Rate();
                    rating.presentationId = id;
                    rating.Ratings = model.Rating;
                    await _ratingRepository.InsertRating(rating);
                    return Ok();
                }
                ModelState.AddModelError("", "please enter the right data");
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
