﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/accommodation")]
    public class AccommodationController : Controller
    {
        private IAccommodationRepository _accommodationRepository;

        // GET: /<controller>/
        public AccommodationController(IAccommodationRepository accommodationRepository)
        {
            _accommodationRepository = accommodationRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAccommodationInfo()
        {
            try
            {
                var accommodations = await _accommodationRepository.GetAllAccommodations();
                if(accommodations != null)
                {
                    return Ok(accommodations);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccommodationById(int id)
        {
            try
            {
                var accommodation = await _accommodationRepository.GetAccommodationById(id);
                if(accommodation != null)
                {
                    return Ok(accommodation);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
