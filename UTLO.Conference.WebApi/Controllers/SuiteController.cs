﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/suite")]
    public class SuiteController : Controller
    {
        private ISuitesRepository _suiteRepository;
        private IProgrammeRepository _programmeRepository;

        // GET: /<controller>/
        public SuiteController(ISuitesRepository suiteRepository,
            IProgrammeRepository programmeRepository)
        {
            _suiteRepository = suiteRepository;
            _programmeRepository = programmeRepository;
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSuitesbyProgrammeId(int id)
        {
            try
            {
                var suites = await _suiteRepository.GetSuitesByProgrammeId(id);
                if(suites != null)
                {
                    return Ok(suites);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
