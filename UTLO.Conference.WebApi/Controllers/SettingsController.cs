﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/settings")]
    public class SettingsController : Controller
    {
        private ISettingsRepository _settingsRepository;

        // GET: /<controller>/
        public SettingsController(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetSettings()
        {
            try
            {
                var conferenceSettings = await _settingsRepository.GetAllSettings();
                if(conferenceSettings != null)
                {
                    return Ok(conferenceSettings);
                }
                return NoContent();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
