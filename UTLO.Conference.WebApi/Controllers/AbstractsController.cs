﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/abstracts")]
    public class AbstractsController : Controller
    {
        private IAbstractRespository _abstractRepository;

        // GET: /<controller>/
        public AbstractsController(IAbstractRespository abstractRepository)
        {
            _abstractRepository = abstractRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAbstracts()
        {
            try
            {
                var abstracts = await _abstractRepository.GetAllAbstractsWithAttendees();
                if(abstracts != null)
                {
                    return Ok(abstracts);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAbstractsbyId(int id)
        {
            try
            {
                var abstracts = await _abstractRepository.GetAbstractsById(id);
                if(abstracts != null)
                {
                    return Ok(abstracts);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("authors/{id}")]
        public async Task<IActionResult> GetAuthorsbyAbstractId(int id)
        {
            try
            {
                var authors = await _abstractRepository.GetAttendeesByAbstracts(id);
                if(authors != null)
                {
                    return Ok(authors);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
