﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/announcement")]
    public class AnnouncementController : Controller
    {
        private IAnnouncementRespository _announcementRepository;

        // GET: /<controller>/

        public AnnouncementController(IAnnouncementRespository announcementRepository)
        {
            _announcementRepository = announcementRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAnnouncement()
        {
            try
            {
                var announcement = await _announcementRepository.GetAllAnnouncements();
                if(announcement != null)
                {
                    return Ok(announcement);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAnnouncementById(int id)
        {
            try
            {
                var announcement = await _announcementRepository.GetAnnouncementWithTopicById(id);
                if(announcement != null)
                {
                    return Ok(announcement);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
