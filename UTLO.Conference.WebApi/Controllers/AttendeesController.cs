﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApi.Controllers
{
    [Route("api/attendees")]
    public class AttendeesController : Controller
    {
        private IAttendeeRepository _attendeesRepository;
        private IAbstractRespository _abstractRepository;

        // GET: /<controller>/
        public AttendeesController(IAttendeeRepository attendeesRepository,
            IAbstractRespository abstractsRepository)
        {
            _attendeesRepository = attendeesRepository;
            _abstractRepository = abstractsRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetAllAttendees()
        {
            try
            {
                var attendees = await _attendeesRepository.GetAllSpeakers();
                if(attendees != null)
                {
                    return Ok(attendees);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("keynotespeakers")]
        public async Task<IActionResult> GetKeyNoteSpeakers()
        {
            try
            {
                var keynotespeakers = await _attendeesRepository.GetKeyNoteSpeakers();
                if(keynotespeakers != null)
                {
                    return Ok(keynotespeakers);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        [HttpGet("authors")]
        public async Task<IActionResult> GetAllAuthors()
        {
            try
            {
                var authors = await _abstractRepository.GetAllAuthors();
                if (authors != null)
                {
                    return Ok(authors);
                }
                return BadRequest();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
