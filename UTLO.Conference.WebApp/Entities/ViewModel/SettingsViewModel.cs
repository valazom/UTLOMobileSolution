﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class SettingsViewModel
    {
        [Required]
        public string SettingName { get; set; }
        [Required]
        public string SettingValue { get; set; }
    }
}
