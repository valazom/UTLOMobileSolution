﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;
using UTLO_NewModel.Enums;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class PresentationViewModel
    {
        [Title,Required]
        public string Title { get; set; }
        [Required]
        public string Venue { get; set; }
        [Required]
        public int presentationType { get; set; }
        public List<Attendees> chairPersons { get; set; }
        public List<Attendees> Panelists { get; set; }
        public List<Attendees> Authors { get; set; }
    }
}
