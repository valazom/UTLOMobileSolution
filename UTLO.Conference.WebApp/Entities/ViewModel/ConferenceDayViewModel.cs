﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class ConferenceDayViewModel
    {
        [Required]
        public int Daynumber { get; set; }
        [Required,DataType(DataType.DateTime)]
        public DateTime ConferenceDate { get; set; }
        public List<Programme> programme { get; set; }
    }
}
