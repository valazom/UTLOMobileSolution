﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class TopicViewModel
    {
        
        [Required]
        public string Bulletpoint { get; set; }
    }
}
