﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class SuitesViewModel
    {
        [Required]
        public string Venue { get; set; }
        public List<Presentations> presentations { get; set; }
        public List<Programme> programmes { get; set; }
        [Required]
        public int selectedProgrammeId { get; set; }
        
    }
}
