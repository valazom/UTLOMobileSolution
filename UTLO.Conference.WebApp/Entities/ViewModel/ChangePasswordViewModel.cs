﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required, DataType(DataType.Password)]
        [Compare(nameof(confirmpassword))]
        [ChangePasswordIsValid(ErrorMessage = "Minimum Password Length is 8" +
            ". Must contain least Upper, " +
                "lower case character, Special and numeric characters")]
        public string password { get; set; }
        [Required, DataType(DataType.Password)]
        public string confirmpassword { get; set; }
    }
}
