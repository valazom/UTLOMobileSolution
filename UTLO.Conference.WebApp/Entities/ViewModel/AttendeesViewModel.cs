﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class AttendeesViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Status { get; set; }
        public string Position { get; set; }
        public string Affiliation { get; set; }
        public string Address { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [DataType(DataType.Upload)]
        [ImageIsKeyNoteSpeaker]
        [ImageExtensions("jpg|jpeg|png", ErrorMessage = "Invalid File Format")]
        public IFormFile ImageUrl { get; set; }
        [Required,DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        public string Biography { get; set; }
        public bool IsKeyNoteSpeaker { get; set; }
    }
}
