﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class LoginViewModel
    {
        [Required,MaxLength(256)]
        public string Username { get; set; }
        [Required,DataType(DataType.Password)]
        public string password { get; set; }
        [Display(Description = "Remember me")]
        public bool rememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
}