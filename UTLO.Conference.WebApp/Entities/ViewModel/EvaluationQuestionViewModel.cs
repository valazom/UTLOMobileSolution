﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Enums;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class EvaluationQuestionViewModel
    {
        [Required]
        public string Question { get; set; }
        public bool IsRequired { get; set; }
        [Required]
        public int type { get; set; }
    }
}
