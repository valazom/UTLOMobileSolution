﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;
using UTLO_NewModel.Enums;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class ProgrammeViewModel
    {
        [Required]
        public string Title { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsMultiVenueProgramme { get; set; }
        [IsAMultiVenueProgramme]
        public string Venue { get; set; }
        public int ConferenceId { get; set; }

    }
}
