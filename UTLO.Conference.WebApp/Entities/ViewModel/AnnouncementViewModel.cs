﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class AnnouncementViewModel
    {
        [Required]
        public string Tile { get; set; }
        [Required]
        public DateTime DateAndTime { get; set; }
    }
}
