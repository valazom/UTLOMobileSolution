﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class AccommodationViewModel
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
