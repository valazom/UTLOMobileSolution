﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.WebApp.Entities.ViewModel
{
    public class RatingViewModel
    {
        public RatingViewModel()
        {
            Presenters = new List<Attendees>();
        }
        public string Title { get; set; }
        public List<Attendees> Presenters { get; set; }
        public int Rating { get; set; }
    }
}
