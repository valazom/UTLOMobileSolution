﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.Entities.Enums
{
    public enum AttendeeType
    {
        presenter,
        panelist,
        chairperson,
        Author
    }
    public enum AttendeeAction
    {
        Insert,
        Update
    }
}
