﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Conference.WebApp.Entities.ViewModel;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Accommodation
{

    public class AccommodationController : Controller
    {
        private IAccommodationRepository _accommodationRepository;

        // GET: /<controller>/
        public AccommodationController(IAccommodationRepository accommodationRepository)
        {
            _accommodationRepository = accommodationRepository;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int?page)
        {
            try
            {
                var AllAccommodation = await _accommodationRepository.GetAllAccommodations();
                ViewData["Accommodation"] = PaginatedList<UTLO_NewModel.Domain.Accommodation>
                    .Create(AllAccommodation, page ?? 1, 15);
                return View();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int? page,AccommodationViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var accommodation = new UTLO_NewModel.Domain.Accommodation();
                    accommodation.accommodationName = model.Name;
                    accommodation.url = model.Url;
                    await _accommodationRepository.InsertAccommodation(accommodation);
                    var AllAccommodation = await _accommodationRepository.GetAllAccommodations();
                    ViewData["Accommodation"] = PaginatedList<UTLO_NewModel.Domain.Accommodation>
                        .Create(AllAccommodation, page ?? 1, 15);
                    return View();
                }
                ModelState.AddModelError("", "Check your entries");
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var accommodation = await _accommodationRepository.GetAccommodationById(id);
                var model = new AccommodationViewModel();
                model.Name = accommodation.accommodationName;
                model.Url = accommodation.url.ToString();
                return View(model);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,AccommodationViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var accommodation = await _accommodationRepository.GetAccommodationById(id);
                    if(accommodation != null)
                    {
                        accommodation.accommodationName = model.Name;
                        accommodation.url = model.Url;
                        await _accommodationRepository.UpdateAccommodation(accommodation);
                        ViewData["Message"] = "Successfully updated the accommodation";
                        return View(model);
                    }
                    return View(model);
                }
                ModelState.AddModelError("", "Check your entries");
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var accommodation = await _accommodationRepository.GetAccommodationById(id);
                if(accommodation != null)
                {
                    await _accommodationRepository.DeleteAccommodation(accommodation);
                }
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var AllAccommodation = await _accommodationRepository.GetAllAccommodations();
                if(AllAccommodation != null)
                {
                    foreach(var accommodation in AllAccommodation)
                    {
                        await _accommodationRepository.DeleteAccommodation(accommodation);
                    }
                    return RedirectToAction("Create");
                }
                return NotFound();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
