﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using UTLO_NewModel.Enums;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using System.IO;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Evaluation
{
    [Authorize]
    public class EvaluationQuestionController : Controller
    {
        private IEvaluationQuestionRepository _evaluationQuestionRepository;
        private IAnswerRepository _answerRepository;
        private IConfiguration _configuration;

        // GET: /<controller>/
        public EvaluationQuestionController(IEvaluationQuestionRepository EvaluationQuestionRepository,
            IAnswerRepository answerRepository,
            IConfiguration configuration)
        {
            _evaluationQuestionRepository = EvaluationQuestionRepository;
            _answerRepository = answerRepository;
            _configuration = configuration;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int?page)
        {
            try
            {
                var AllEvaluationQuestions = await _evaluationQuestionRepository.GetAllEvaluationQuestions();
                ViewData["EvaluationQuestions"] = PaginatedList<EvaluationQuestions>
                    .Create(AllEvaluationQuestions, page ?? 1, 15);
                return View();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EvaluationQuestionViewModel model,int? page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var EvaluationQuestion = new EvaluationQuestions();
                    EvaluationQuestion.Question = model.Question;
                    EvaluationQuestion.Qtype = (QuestionType)model.type;
                    EvaluationQuestion.QuestionsRequired = model.IsRequired;
                    await _evaluationQuestionRepository.insertEvaluationQuestion(EvaluationQuestion);
                    var AllEvaluationQuestions = await _evaluationQuestionRepository.GetAllEvaluationQuestions();
                    ViewData["EvaluationQuestions"] = PaginatedList<EvaluationQuestions>
                        .Create(AllEvaluationQuestions, page ?? 1, 15);
                    return View();
                }
                ModelState.AddModelError("", "Kindly check your entries");
                return View();
            }
            catch (Exception ex)
            {

               return StatusCode(500,ex.Message);
            }
            
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var model = new EvaluationQuestionViewModel();
                var evaluationQuestion = await _evaluationQuestionRepository.GetEvaluationQuestionById(id);
                if(evaluationQuestion != null)
                {
                    model.Question = evaluationQuestion.Question;
                    model.type = (int)evaluationQuestion.Qtype;
                    model.IsRequired = evaluationQuestion.QuestionsRequired;
                }
                return View(model);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EvaluationQuestionViewModel model,int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var evaluationQuestion = await _evaluationQuestionRepository.GetEvaluationQuestionById(id);
                    evaluationQuestion.Question = model.Question;
                    evaluationQuestion.Qtype = (QuestionType)model.type;
                    evaluationQuestion.QuestionsRequired = model.IsRequired;
                    await _evaluationQuestionRepository.UpdateEvaluationQuestions(evaluationQuestion);
                    ViewData["Message"] = "Successfully updated the evaluation question";
                    return View(model);
                }
                ModelState.AddModelError("", "Kindly check your entries");
                return View(model);
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex.Message);
            }
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var evaluationQuestion = await _evaluationQuestionRepository.GetEvaluationQuestionById(id);
                await _evaluationQuestionRepository.DeleteEvaluations(evaluationQuestion);
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var AllEvaluations = await _evaluationQuestionRepository.GetAllEvaluationQuestions();
                if(AllEvaluations != null)
                {
                    foreach(var evaluation in AllEvaluations)
                    {
                        await _evaluationQuestionRepository.DeleteEvaluations(evaluation);
                    }
                    return RedirectToAction("Create");
                }
                return NotFound();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        public async Task<IActionResult> DownloadAnswersToExcel()
        {
            try
            {
                var AllEvaluationQuestions = await _evaluationQuestionRepository.GetAllEvaluationQuestions();
                var AllAnswers = await _answerRepository.GetsAllAnswers();
                if (AllEvaluationQuestions.Count != 0)
                {
                    var ExcelFileName = _configuration["Excel:Evaluation"];
                    var ContentType = _configuration["Excel:ContentType"];
                    MemoryStream fstream;
                    using (var excelPackage = new ExcelPackage())
                    {
                        var worksheet = excelPackage.Workbook.Worksheets.Add(_configuration["Excel:Filename"]);
                        //load headers
                        int row = 1;
                        for (int col = 1; col <= AllEvaluationQuestions.Count; col++)
                        {
                            worksheet.Cells[row, col].Value = AllEvaluationQuestions[col - 1].Question;
                            worksheet.Cells[row, col].Style.Font.Bold = true;
                        }
                        var AllAns = AllAnswers.Count != 0;
                        // populate answers if available
                        if (AllAns)
                        {
                            for (int col = 1; col <= AllEvaluationQuestions.Count; col++)
                            {
                                //load answers
                                var Answers = AllAnswers
                                                 .Where(c => c.EvaluationQuestionId
                                                 == AllEvaluationQuestions[col - 1].EvaluationQuestionsId)
                                                 .ToList();
                                for (row = 0; row < Answers.Count; row++)
                                {
                                    if (AllEvaluationQuestions[col - 1].Qtype == QuestionType.FillingTheGap)
                                        worksheet.Cells[row + 2, col].Value = Answers[row].answer;
                                    else if (AllEvaluationQuestions[col - 1].Qtype == QuestionType.Rating)
                                        worksheet.Cells[row + 2, col].Value = Answers[row].Rating;
                                    else
                                    {
                                        throw new Exception("Question type not supported");
                                    }
                                }
                            }
                        }
                        fstream = new MemoryStream();
                        excelPackage.SaveAs(fstream);
                        fstream.Position = 0;
                    }
                    var fsr = new FileStreamResult(fstream, ContentType);
                    fsr.FileDownloadName = _configuration["Excel:Filename"];
                    return fsr;
                }
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
