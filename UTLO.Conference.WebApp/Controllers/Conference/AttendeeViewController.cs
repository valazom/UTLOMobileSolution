﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Model.Data.Repository.ConferenceRepository;
using Microsoft.AspNetCore.Authorization;
using UTLO.Conference.WebApp.Entities.Enums;
using UTLO_NewModel.Domain;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class AttendeeViewController : Controller
    {
        private IAttendeeRepository _attendeeRepository;
        private IProgrammeRepository _programmeRepository;
        private IPresentationRepository _presentationRepository;
        private IAbstractRespository _abstractRepository;

        // GET: /<controller>/
        public AttendeeViewController(IAttendeeRepository attendeeRepository,
            IProgrammeRepository programmeRepository,
            IPresentationRepository presentationRepository,
            IAbstractRespository abstractRepository)
        {
            _attendeeRepository = attendeeRepository;
            _programmeRepository = programmeRepository;
            _presentationRepository = presentationRepository;
            _abstractRepository = abstractRepository;
        }
        public async Task<IActionResult> Create(int previouscontrollerId,
                                     int controllerId,
                                     string pname, 
                                     string controllername, 
                                     string actionname,
                                     int? prpage)
        {
            try
            {
                var controllerType = getControllerEnumType(controllername);
                var attendeeType = getAttendeeEnumType(pname);
                var attendees = await getAttendeesFromControllerType(controllerType, attendeeType, controllerId);
                MapToView(previouscontrollerId, controllerId, controllerType, 
                    actionname,pname,prpage);
                return View(attendees);
            }
            catch (Exception)
            {

                throw;
            }
        }
        private void  MapToView(int previouscontrollerId,int controllerId, RequestFromController controllerType,
                               string action, string name,int?prpage)
        {
            ViewData["Id"] = controllerId;
            ViewData["action"] = action;
            ViewData["name"] = name;
            ViewData["presentController"] = "AttendeeView";
            ViewData["previouscontrollerId"] = previouscontrollerId;
            ViewData["prpage"] = prpage;
            switch (controllerType)
            {
                case RequestFromController.Programme:
                    ViewData["Controller"] = "Programme";
                    break;
                case RequestFromController.Presentation:
                    ViewData["Controller"] = "Presentation";
                    break;
                case RequestFromController.Abstract:
                    ViewData["Controller"] = "Abstract";
                    break;
                default:
                    throw new Exception("Controller Type not supported");
            }
        }
        private RequestFromController getControllerEnumType(string controller)
        {
            var controllerType = (RequestFromController)Enum.Parse(typeof(RequestFromController), controller);
            return controllerType;
        }
        private AttendeeType getAttendeeEnumType(string name)
        {
            return (AttendeeType)Enum.Parse(typeof(AttendeeType), name);
        }
        private async Task<List<Attendees>> getAttendeesFromControllerType(RequestFromController controllerType,
                                                               AttendeeType attendeeType,int id)
        {
            List<Attendees> attendees = new List<Attendees>();
            try
            {
                switch (controllerType)
                {
                    case RequestFromController.Programme:
                        switch (attendeeType)
                        {
                            case AttendeeType.presenter:
                                
                                attendees = (await _programmeRepository.GetAllProgramme())
                                               .Where(c => c.ProgrammeId == id)
                                               .SingleOrDefault().presenters.ToList();
                                break;
                            case AttendeeType.chairperson:
                                attendees = (await _programmeRepository.GetAllProgramme())
                                               .Where(c => c.ProgrammeId == id)
                                               .SingleOrDefault().chairpersons.ToList();
                                break;
                        }
                        break;
                    case RequestFromController.Presentation:
                        switch (attendeeType)
                        {
                            case AttendeeType.panelist:
                                attendees = (await _presentationRepository.GetAllPresentations())
                                    .Where(c => c.PresentationsId == id)
                                    .SingleOrDefault().Panelists.ToList();
                                break;
                            case AttendeeType.chairperson:
                                attendees = (await _presentationRepository.GetAllPresentations())
                                    .Where(c => c.PresentationsId == id)
                                    .SingleOrDefault().chairPersons.ToList();
                                break;
                            case AttendeeType.Author:
                                attendees = (await _presentationRepository.GetAllPresentations())
                                    .Where(c => c.PresentationsId == id)
                                    .SingleOrDefault().Authors.ToList();
                                break;
                        }
                        break;
                    case RequestFromController.Abstract:
                        attendees = await _abstractRepository.GetAttendeesByAbstracts(id);
                        break;
                    default:
                        throw new Exception("Controller request source not supported");
                }
            }
            catch (Exception e)
            {

                throw;
            }
            return attendees;
        }
        private async Task RemoveAttendeesFromControllerType(RequestFromController controllerType,
                                                               AttendeeType attendeeType, int id,
                                                               Attendees attendee)
        {
            try
            {
                switch (controllerType)
                {
                    case RequestFromController.Programme:
                        
                        switch (attendeeType)
                        {
                            case AttendeeType.presenter:
                                var pprogramme = await _programmeRepository.GetProgrammeWithPresenterById(id);
                                await _programmeRepository.RemovePresenterFromProgramme(pprogramme, attendee);
                                break;
                            case AttendeeType.chairperson:
                                var cprogramme = await _programmeRepository.GetProgrammeWithChairpersonsById(id);
                                await _programmeRepository.RemoveChairpersonsFromProgramme(cprogramme, attendee);
                                break;
                        }
                        break;
                    case RequestFromController.Presentation:
                        switch (attendeeType)
                        {
                            case AttendeeType.Author:
                                var apresentation = await _presentationRepository.GetPresentationWithAuthorById(id);
                                await _presentationRepository.RemoveAuthorFromPresentation(apresentation, attendee);
                                break;
                            case AttendeeType.chairperson:
                                var cpresentation = await _presentationRepository.GetPresentationWithChairpersonById(id);
                                await _presentationRepository.RemoveChaipersonFromPresentation(cpresentation, attendee);
                                break;
                            case AttendeeType.panelist:
                                var ppresentation = await _presentationRepository.GetPresentationWithPanelistById(id);
                                await _presentationRepository.RemovePanelistFromPresentation(ppresentation, attendee);
                                break;
                            default:
                                throw new Exception("Attendee type not supported");
                        }
                        break;
                    case RequestFromController.Abstract:
                        var abstracts = (await _abstractRepository.GetAllAbstracts())
                            .Where(c => c.abstractsId == id).SingleOrDefault();
                        await _abstractRepository.RemoveAuthorFromAbstracts(abstracts, attendee);
                        break;
                    default:
                        throw new Exception("Controller request not supported");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public IActionResult Edit(int Id,int previouscontrollerId,int controllerId, 
                                   string pname, string controllername, string actionname,int?prpage)
        {
            return RedirectToAction("Edit", "Attendees", new
            {
                Id = Id,
                previouscontrollerId = previouscontrollerId,
                controllerId = controllerId,
                pname = pname,
                controllername = controllername,
                actionname = actionname,
                prpage = prpage
            });
        }
        public async Task<IActionResult> Delete(int Id, int previouscontrollerId, int controllerId,
                                   string pname, string controllername, string actionname,
                                   int?prpage)
        {

            try
            {
                var attendee = await _attendeeRepository.GetAttendeeByID(Id);
                var controllerType = getControllerEnumType(controllername);
                var attendeeType = getAttendeeEnumType(pname);
                await RemoveAttendeesFromControllerType(controllerType, attendeeType, controllerId, attendee);
                MapToView(previouscontrollerId, controllerId, controllerType, actionname, pname, prpage);
                return RedirectToAction("Create", new
                {
                    previouscontrollerId = previouscontrollerId,
                    controllerId = controllerId,
                    pname = pname,
                    controllername = controllername,
                    actionname = actionname,
                    prpage = prpage
                });


            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
