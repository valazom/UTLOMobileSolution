﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using Microsoft.AspNetCore.Http;
using System.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class AbstractController : Controller
    {
        private IAbstractRespository _abstractRepository;
        private IAttendeeRepository _attendeeRepository;

        public AbstractController(IAbstractRespository abstractRepository,
                                  IAttendeeRepository attendeeRepository)
        {
            _abstractRepository = abstractRepository;
            _attendeeRepository = attendeeRepository;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int? page)
        {
            try
            {
                var abstracts = await _abstractRepository.GetAllAbstracts();
                ViewData["Abstracts"] = PaginatedList<Abstracts>.Create(abstracts, page ?? 1, 15);
                return View();
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AbstractViewModel model,int?page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var abstracts = new Abstracts();
                    abstracts.Title = model.Title;
                    abstracts.Body = model.Body;
                    abstracts.Theme = model.Theme;
                    await _abstractRepository.InsertAbstracts(abstracts);
                    var loadabstracts = await _abstractRepository.GetAllAbstracts();
                    ViewData["abstracts"] = PaginatedList<Abstracts>.Create(loadabstracts, page ?? 1, 15);
                    return View();
                }
                ModelState.AddModelError("", "Kindly check your entries");

            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Unable to add new abstract at this time");
                return View();
            }
            return View();
        }
        [HttpGet]
        public IActionResult Upload()
        {
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(IFormFile upload)
        {
            try
            {
               if(upload !=null && upload.Length > 0)
               {
                    var abstractTable = Utils.UploadExcelFile(upload);
                    var abstracts = new Abstracts();
                    await UploadAbstracts(abstractTable, abstracts);
                    return RedirectToAction("Create");
               }
               ModelState.AddModelError("", "No file uploaded");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                return View();
            }
            return View();
        }

        private async Task UploadAbstracts(DataTable abstractTable, Abstracts abstracts)
        {
            var allAbstracts = await _abstractRepository.GetAllAbstracts();
            foreach (DataRow row in abstractTable.Rows)
            {
                foreach (DataColumn col in abstractTable.Columns)
                {
                    switch (col.ColumnName)
                    {
                        case "Title":
                            abstracts.Title = row[col.Ordinal].ToString();
                            break;
                        case "Theme":
                            abstracts.Theme = row[col.Ordinal].ToString();
                            break;
                        case "Body":
                            abstracts.Body = row[col.Ordinal].ToString();
                            break;
                        default:
                            throw new Exception("Excel Header not supported");

                    }
                }
                if (!string.IsNullOrEmpty(abstracts.Title) || 
                    !string.IsNullOrEmpty(abstracts.Theme) || 
                    !string.IsNullOrEmpty(abstracts.Body))
                {
                    await _abstractRepository.InsertAbstracts(abstracts);
                }
                abstracts = new Abstracts();
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int Id)
        {
            try
            {
                var abstracts = (await _abstractRepository.GetAllAbstracts())
                    .Where(c => c.abstractsId == Id).SingleOrDefault();
                var model = new AbstractViewModel();
                model.Body = abstracts.Body;
                model.Title = abstracts.Title;
                model.Theme = abstracts.Theme;
                return View(model);
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AbstractViewModel model,int Id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var abstracts = (await _abstractRepository.GetAllAbstracts())
                                   .Where(c => c.abstractsId == Id).SingleOrDefault();
                    abstracts.Body = model.Body;
                    abstracts.Title = model.Title;
                    abstracts.Theme = model.Theme;
                    await _abstractRepository.UpdateAbstracts(abstracts);
                    ViewData["Message"] = "Successfully updated abstract";
                    return View(model);
                }
                ModelState.AddModelError("", "Kindly check your entries");
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Unable to edit the abstract");
                return View(model);
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var abstracts = (await _abstractRepository.GetAllAbstracts())
                    .Where(c => c.abstractsId == id).SingleOrDefault();
                await _abstractRepository.DeleteAbstracts(abstracts);
                return RedirectToAction("Create");

            }
            catch (Exception)
            {

                throw;
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var AllAbstracts = await _abstractRepository.GetAllAbstracts();
                if(AllAbstracts != null)
                {
                    foreach(var abstracts in AllAbstracts)
                    {
                        await _abstractRepository.DeleteAbstracts(abstracts);
                    }
                    return RedirectToAction("Create");
                }
                return NotFound();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
