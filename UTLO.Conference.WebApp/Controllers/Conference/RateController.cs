﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO_NewModel.Domain;
using UTLO.Conference.WebApp.Entities.ViewModel;
using Microsoft.AspNetCore.Authorization;
using UTLO.Model.Data.Repository.ConfereenceRepository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class RateController : Controller
    {
        private IPresentationRepository _presentationRepository;
        private IRateRepository _rateRepository;

        // GET: /<controller>/
        public RateController(IPresentationRepository presentationRepository,
            IRateRepository rateRepository)
        {
            _presentationRepository = presentationRepository;
            _rateRepository = rateRepository;
        }
        public async Task<IActionResult> Create()
        {
            try
            {
                var presentations = await _presentationRepository.GetAllPresentations();
                var RatingModels = await UpdateRatingModel(presentations);
                return View(RatingModels);
            }
            catch (Exception e)
            {

                return StatusCode(500, e.Message);
            }
            
        }

        private async Task<List<RatingViewModel>> UpdateRatingModel(List<Presentations> presentations)
        {
            var ratingViewModels = new List<RatingViewModel>();

            foreach (var presentation in presentations)
            {
                var ratingViewModel = new RatingViewModel();
                ratingViewModel.Title = presentation.Title;
                var Ratings = await _rateRepository.GetRateBByPresentationId(presentation.PresentationsId);
                if (Ratings.Count != 0)
                    ratingViewModel.Rating = (int)Ratings.Average(c => c.Ratings);
                else
                {
                    ratingViewModel.Rating = 0;
                }
                foreach (var presenter in presentation.Authors)
                {
                    ratingViewModel.Presenters.Add(presenter);
                }
                ratingViewModels.Add(ratingViewModel);
            }
            return ratingViewModels.OrderByDescending(c => c.Rating).ToList();
        }
    }
}
