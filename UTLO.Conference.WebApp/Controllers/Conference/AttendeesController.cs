﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.Net.Http.Headers;
using UTLO.Conference.WebApp.Entities.Enums;
using Excel;
using System.Data;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{

    public class AttendeesController : Controller
    {
        private IAttendeeRepository _attendeesRepository;
        private IProgrammeRepository _programmeRepository;
        private IPresentationRepository _presentationRepository;
        private IAbstractRespository _abstractRepository;
        private IHostingEnvironment _environment;

        // GET: /<controller>/
        public AttendeesController(IAttendeeRepository attendeesRepository,
            IProgrammeRepository programmeRepository,
            IPresentationRepository presentationRepository,
            IAbstractRespository abstractRepository,
            IHostingEnvironment environment)
        {
            _attendeesRepository = attendeesRepository;
            _programmeRepository = programmeRepository;
            _presentationRepository = presentationRepository;
            _abstractRepository = abstractRepository;
            _environment = environment;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int? controllerId,
                                   int? previouscontrollerId,
                                   string controllername,
                                   string returncontroller,
                                   string pname,
                                   string actionname,
                                   int? prpage,
                                   int? page)
        {
            try
            {
                var attendees = (await _attendeesRepository.GetAllSpeakers())
                               .OrderByDescending(c => c.AttendeesId).ToList();
                ViewData["Attendees"] = PaginatedList<Attendees>
                    .Create(attendees, page ?? 1, 5);
                MapBacktoView(controllerId, previouscontrollerId,
                                controllername, returncontroller, actionname,
                                pname,"Create",prpage);


            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }

        private void MapBacktoView(int? controllerId,
                                   int? previouscontrollerId,
                                   string controller,
                                   string returncontroller,
                                   string action,
                                   string name,
                                   string actionType="Create",
                                   int? page = 1)
        {
            if (!string.IsNullOrEmpty(controller))
            {
                ViewData["Id"] = controllerId;
                ViewData["action"] = action;
                ViewData["returncontroller"] = returncontroller;
                ViewData["name"] = name;
                ViewData["previouscontrollerId"] = previouscontrollerId;
                ViewData["actionType"] = actionType;
                ViewData["cancel"] = false;
                ViewData["prpage"] = page;
                if (controller == "Programme")
                    ViewData["Controller"] = "Programme";
                else if (controller == "Presentation")
                    ViewData["Controller"] = "Presentation";
                else if (controller == "Abstract")
                    ViewData["Controller"] = "Abstract";
            }
            else
            {
                ViewData["Controller"] = string.Empty;
                ViewData["cancel"] = false;
                ViewData["Id"] = 0;
                ViewData["previouscontrollerId"] = 0;
                ViewData["actionType"] = actionType;
            }
        }

        public async Task<IActionResult> Add(int id,int controllerId,
                                  int previouscontrollerId,
                                  string controllername, 
                                  string returncontroller, 
                                  string actionname,
                                  string pname,
                                  int prpage)
        {
            try
            {
                var attendee = await _attendeesRepository.GetAttendeeByID(id);
                await AddConferenceItem(controllerId, controllername, null,attendee, AttendeeAction.Update,pname);
                return RedirectToAction(actionname, returncontroller, new
                {
                    controllerId = controllerId,
                    previouscontrollerId = previouscontrollerId,
                    controllername = controllername,
                    returncontroller = returncontroller,
                    actionname = actionname,
                    pname = pname,
                    prpage = prpage
                });
            }
            catch (Exception e)
            {
                ViewData["Message"] = "The Attendee was not added to " + controllername;
                return View();
            }

        }

        public async Task<IActionResult> Search(int controllerId,
                                  int previouscontrollerId,
                                  string controllername,
                                  string returncontroller, 
                                  string actionname,
                                  string pname,
                                  string Search,
                                  int? prpage,
                                  int? page)
        {
            try
            {
                var attendees = (await _attendeesRepository.GetAllSpeakers())
                                  .Where(c => c.Name.Contains(Search)).ToList();
                ViewData["Attendees"] = PaginatedList<Attendees>.Create(attendees, page ?? 1, 5);
                MapBacktoView(controllerId, previouscontrollerId,
                                controllername, returncontroller, 
                                actionname, pname,"Search",prpage);
                ViewData["cancel"] = true;

            }
            catch (Exception)
            {
                ViewData["Message"] = "Error occured while searching";
                return RedirectToAction("Create", new
                {
                    controllerId = controllerId,
                    previouscontrollerId = previouscontrollerId,
                    controller = controllername,
                    returncontroller = returncontroller,
                    actionname = actionname,
                    pname = pname,
                    prpage = prpage
                });
            }
            return View("~/Views/Attendees/Create.cshtml");
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(IFormFile upload)
        {
            try
            {
                
                if ((upload != null && upload.Length > 0) && 
                    (upload.FileName.EndsWith(".xls") || 
                    upload.FileName.EndsWith(".xlsx")))
                {
                    var attendeeDataTable = Utils.UploadExcelFile(upload);
                    var attendees = new Attendees();
                    await UploadAttendees(attendeeDataTable, attendees);
                    return RedirectToAction("Create");
                }
                ModelState.AddModelError("", "Check the file uploaded");

            }
            catch (Exception e)
            {
                ModelState.AddModelError("", e.Message);
                return View();
            }
            return View();
        }

        private async Task UploadAttendees(DataTable attendeeDataTable, Attendees attendees)
        {
            foreach (DataRow row in attendeeDataTable.Rows)
            {
                foreach (DataColumn col in attendeeDataTable.Columns)
                {
                    switch (col.ColumnName)
                    {
                        case "Name":
                            attendees.Name = row[col.Ordinal].ToString();
                            break;
                        case "EmailAddress":
                            attendees.emailAddress = row[col.Ordinal].ToString();
                            break;
                        case "Position":
                            attendees.Position = row[col.Ordinal].ToString();
                            break;
                        case "Affiliation":
                            attendees.Affiliation = row[col.Ordinal].ToString();
                            break;
                        case "Phonenumber":
                            attendees.phoneNumber = row[col.Ordinal].ToString();
                            break;
                        case "Address":
                            attendees.Address = row[col.Ordinal].ToString();
                            break;
                        case "Biography":
                            attendees.Biography = row[col.Ordinal].ToString();
                            break;
                        default:
                            throw new Exception("Excel Header not supported");

                    }
                }
                await _attendeesRepository.AddAttendees(attendees);
                attendees = new Attendees();
            }
            
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int? controllerId,
                                    int? previouscontrollerId,
                                    string controllername,
                                    string actionname,
                                    AttendeesViewModel model,
                                    string returncontroller,
                                    string pname,
                                    int? prpage,
                                    int? page)
        {
            try
            {
                var allAttendees = (await _attendeesRepository.GetAllSpeakers())
                    .OrderByDescending(c => c.AttendeesId).ToList();
                if (ModelState.IsValid)
                {
                    var attendee = new Attendees();
                    if (!string.IsNullOrEmpty(controllername))
                    {
                        await AddConferenceItem(controllerId, controllername, model, null, AttendeeAction.Insert, pname);
                        MapBacktoView(controllerId, previouscontrollerId, 
                            controllername, returncontroller, actionname, pname,"Create",prpage);
                        return RedirectToAction(actionname, returncontroller, new
                        {
                            controllerId = controllerId,
                            previouscontrollerId = previouscontrollerId,
                            controllername = controllername,
                            returncontroller = returncontroller,
                            actionname = actionname,
                            pname = pname,
                            prpage = prpage
                        });
                    }
                    else
                    {
                        attendee = UpdateAttendee(model);
                        await _attendeesRepository.AddAttendees(attendee);
                        allAttendees = (await _attendeesRepository.GetAllSpeakers())
                               .OrderByDescending(c => c.AttendeesId).ToList();
                        ViewData["Attendees"] = PaginatedList<Attendees>.Create(allAttendees, page ?? 1, 5);
                        MapBacktoView(controllerId, previouscontrollerId, controllername, 
                            returncontroller, actionname, pname,"Create",prpage);
                        return View();
                    }

                }
                ModelState.AddModelError("", "Please check your entries. If you inserted image it should not be greater than 5MB in size");
                ViewData["Attendees"] = PaginatedList<Attendees>.Create(allAttendees, page ?? 1, 5);
                MapBacktoView(controllerId, previouscontrollerId, controllername, 
                    returncontroller,
                    actionname, pname,"Create",prpage);
                return View();
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error in adding attendee");
                return View();
            }

        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var allAttendees = await _attendeesRepository.GetAllSpeakers();
                if (allAttendees != null)
                {
                    foreach(var attendee in allAttendees)
                    {
                        await _attendeesRepository.DeleteAttendee(attendee);
                    }
                    return RedirectToAction("Create");
                }
                return NotFound();

            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
        private RequestFromController getEnumType(string controller)
        {
            return (RequestFromController)Enum.
                Parse(typeof(RequestFromController), controller, true);
        }
        private AttendeeType getEnumAttendeeType(string name)
        {
            return (AttendeeType)Enum.Parse(typeof(AttendeeType), name, true);
        }
        private async Task AddConferenceItem(int? Id, string controller, AttendeesViewModel model, 
                                       Attendees attendee,AttendeeAction action,string name)
        {
            var controllerType = getEnumType(controller);
            var attendeeType = getEnumAttendeeType(name);
            switch (controllerType)
            {
                case RequestFromController.Programme:
                    switch (attendeeType)
                    {
                        case AttendeeType.presenter:
                            var pprogramme = await _programmeRepository.GetProgrammeWithPresenterById(Id ?? 0);
                            await AddingAttendees(pprogramme, controller, action, attendeeType, model, attendee);
                            break;
                        case AttendeeType.chairperson:
                            var cprogramme = await _programmeRepository.GetProgrammeWithChairpersonsById(Id ?? 0);
                            await AddingAttendees(cprogramme, controller, action, attendeeType, model, attendee);
                            break;
                        default:
                            throw new Exception("attendee type not supported");
                    }
                    
                    break;
                case RequestFromController.Presentation:
                    switch (attendeeType)
                    {
                        case AttendeeType.Author:
                            var aprogramme = await _presentationRepository.GetPresentationWithAuthorById(Id ?? 0);
                            await AddingAttendees(aprogramme, controller, action, attendeeType, model, attendee);
                            break;
                        case AttendeeType.chairperson:
                            var cprogramme = await _presentationRepository.GetPresentationWithChairpersonById(Id ?? 0);
                            await AddingAttendees(cprogramme, controller, action, attendeeType, model, attendee);
                            break;
                        case AttendeeType.panelist:
                            var pprogramme = await _presentationRepository.GetPresentationWithPanelistById(Id ?? 0);
                            await AddingAttendees(pprogramme, controller, action, attendeeType, model, attendee);
                            break;
                        default:
                            throw new Exception("AttendeeType not supported");
                    }
                    break;
                case RequestFromController.Abstract:
                    var abstracts = (await _abstractRepository.GetAllAbstracts())
                           .Where(c => c.abstractsId == Id).SingleOrDefault();
                    await AddingAttendees(abstracts, controller,action,attendeeType, model,attendee);
                    break;
                default:
                    throw new Exception("Controller request not supported");
            }
        }

        private async Task AddingAttendees(object item, string controller, AttendeeAction action,
                                        AttendeeType attendeeType,
                                        AttendeesViewModel model = null,
                                        Attendees attendees = null )
        {
            
            Attendees attendee = new Attendees();
            if (attendees == null && model != null)
            {
                attendees = UpdateAttendee(model);
                attendee = attendees;
            }
            else if(model == null && attendees != null)
            {
                attendee = attendees;
            }
            else
            {
                throw new ArgumentException("Attendee cannot be null");
            }
            var controllerType = getEnumType(controller);
            switch (controllerType)
            {
                case RequestFromController.Programme:
                    var programme = item as Programme;
                    await AttendeeRoutine(action,attendee);
                    switch (attendeeType)
                    {
                        case AttendeeType.presenter:
                            await _programmeRepository.UpdateProgrammeWithPresenter(programme, attendee);
                            break;
                        case AttendeeType.chairperson:
                            await _programmeRepository.UpdateProgrammeWithChairpersons(programme, attendee);
                            break;
                    }
                    
                    break;
                case RequestFromController.Presentation:
                    var presentation = item as Presentations;
                    await AttendeeRoutine(action, attendee);
                    switch (attendeeType)
                    {
                        case AttendeeType.Author:
                            await _presentationRepository.UpdatePresentationWithAuthor(presentation, attendee);
                            break;
                        case AttendeeType.chairperson:
                            await _presentationRepository.UpdatePresentationWithChaiperson(presentation, attendee);
                            break;
                        case AttendeeType.panelist:
                            await _presentationRepository.UpdatePresentationWithPanelist(presentation, attendee);
                            break;
                        default:
                            throw new Exception("Attendee Type not supported");
                    }
                    break;
                case RequestFromController.Abstract:
                    var abstracts = item as Abstracts;
                    await AttendeeRoutine(action, attendee);
                    await _abstractRepository.UpdateAbstractsWithAuthor(abstracts, attendee);
                    break;
                default:
                    throw new ArgumentException("The controller name supplied does not exist");
            }
            
        }

        private async Task AttendeeRoutine(AttendeeAction action,Attendees attendee)
        {
            switch (action)
            {
                case AttendeeAction.Insert:
                    await _attendeesRepository.AddAttendees(attendee);
                    break;
                case AttendeeAction.Update:
                    await _attendeesRepository.UpdateAttendee(attendee);
                    break;
                default:
                    throw new Exception("Attendee action method not supported");
            }                   
        }

        private string getUploadedFilePath(IFormFile file,string name,bool isKeynoteSpeaker)
        {
            string uploadpath = string.Empty;
            if(isKeynoteSpeaker)
                 uploadpath = Path.Combine(_environment.WebRootPath, @"Uploads\Keynotespeakers");
            else
                 uploadpath = Path.Combine(_environment.WebRootPath, @"Uploads\Attendees");
            if (file.Length > 0)
            {
                var filename = file.FileName;
                filename = name.Replace(" ","_") + "_" + filename;
                uploadpath = Path.Combine(uploadpath, filename);
                using (var filestream = new FileStream(uploadpath, FileMode.Create))
                {
                    file.CopyTo(filestream);
                }
                return filename;
            }
            return string.Empty;
        }
        private Attendees UpdateAttendee(AttendeesViewModel model)
        {
            Attendees attendee = new Attendees();
            
            attendee.Name = model.Name;
            //model.ImageUrl = HttpContext.Request.Form.Files[0]; //only one image uploaded at a time
            if(model.ImageUrl != null)
              attendee.ImageUrl = getUploadedFilePath(model.ImageUrl,model.Name,model.IsKeyNoteSpeaker);
            attendee.phoneNumber = model.PhoneNumber;
            attendee.Position = model.Position;
            attendee.status = model.Status;
            attendee.Address = model.Address;
            attendee.Affiliation = model.Affiliation;
            attendee.emailAddress = model.EmailAddress;
            attendee.Biography = model.Biography;
            attendee.IsKeynoteSpeaker = model.IsKeyNoteSpeaker;
            return attendee;
        }
        private AttendeesViewModel UpdateAttendeeViewModel(Attendees attendee)
        {
            AttendeesViewModel attendeeViewModel = new AttendeesViewModel();
            attendeeViewModel.Address = attendee.Address;
            attendeeViewModel.Affiliation = attendee.Affiliation;
            attendeeViewModel.Biography = attendee.Biography;
            attendeeViewModel.EmailAddress = attendee.emailAddress;
            attendeeViewModel.IsKeyNoteSpeaker = attendee.IsKeynoteSpeaker;
            attendeeViewModel.Name = attendee.Name;
            attendeeViewModel.PhoneNumber = attendee.phoneNumber;
            attendeeViewModel.Position = attendee.Position;
            attendeeViewModel.Status = attendee.status;
            return attendeeViewModel;
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int Id, int? controllerId,
                                    int? previouscontrollerId,
                                    string controllername,
                                    string returncontroller,
                                    string pname,
                                    string action,
                                    int? prpage)
        {
            try
            {
                var attendee = await _attendeesRepository.GetAttendeeByID(Id);
                var attendeeModel = UpdateAttendeeViewModel(attendee);
                MapBacktoView(controllerId,previouscontrollerId, controllername,
                    returncontroller, action,pname,"Create",prpage);
                return View(attendeeModel);
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int Id, int? controllerId, 
                                    int? previouscontrollerId,
                                    string controllername,
                                    string returncontroller,
                                    string pname,
                                    string actionname,
                                    int? prpage,
                                    AttendeesViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var dattendee = (await _attendeesRepository.GetAllSpeakers())
                        .Where(c => c.AttendeesId == Id).SingleOrDefault();
                    var attendee = UpdateAttendee(model);
                    attendee.AttendeesId = dattendee.AttendeesId;
                    await _attendeesRepository.UpdateAttendee(attendee);
                    ViewData["Message"] = "Successfully updated attendee";
                    
                    if(returncontroller == "AttendeeView")
                    {
                        return RedirectToAction(actionname, returncontroller, new
                        {
                            controllerId = controllerId,
                            previouscontrollerId = previouscontrollerId,
                            controllername = controllername,
                            returncontroller = returncontroller,
                            actionname = actionname,
                            pname = pname,
                            prpage = prpage
                        });
                    }
                    MapBacktoView(controllerId, previouscontrollerId,
                                controllername, returncontroller, actionname, 
                                pname,"Create",prpage);
                    return View(model);
                }
                ModelState.AddModelError("", "Please check your entries");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "The attendee information could not be updated");
                return View(model);
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int Id, int? controllerId,
                                    int previouscontrollerId,
                                    string controllername,
                                    string returncontroller,
                                    string pname,
                                    string actionname,
                                    int? page,
                                    int? prpage)
        {
            try
            {
                var attendee = await _attendeesRepository.GetAttendeeByID(Id);
                await _attendeesRepository.DeleteAttendee(attendee);
                if (controllername == "AttendeeView")
                {
                    return RedirectToAction(actionname, controllername, new
                    {
                        controllerId = controllerId,
                        previouscontrollerId = previouscontrollerId,
                        controller = controllername,
                        returncontroller = returncontroller,
                        actionname = actionname,
                        pname = pname,
                        prpage = prpage
                    });
                }
                var attendees = await _attendeesRepository.GetAllSpeakers();
                ViewData["Attendees"] = PaginatedList<Attendees>.Create(attendees, page ?? 1, 5);
                MapBacktoView(controllerId, previouscontrollerId,
                                controllername, returncontroller, 
                                actionname, pname,"Create",prpage);
                return RedirectToAction("Create");
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex.Message);
            }
        }

    }
}
