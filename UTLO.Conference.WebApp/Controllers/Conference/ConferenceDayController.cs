﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class ConferenceDayController : Controller
    {
        private IConferenceDayRepository _conferenceDay;
        private IMapper _mapper;

        // GET: /<controller>/
        public ConferenceDayController(IConferenceDayRepository conferenceDay, IMapper mapper)
        {
            _conferenceDay = conferenceDay;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> SetDate()
        {
            try
            {
                ViewData["AddedConferenceDay"] = await _conferenceDay.GetAllConferenceIncludingProgramme();
            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> SetDate(ConferenceDayViewModel model)
        {
            try
            {
                ViewData["AddedConferenceDay"] = await _conferenceDay
                    .GetAllConferenceIncludingProgramme();
            }
            catch (Exception)
            {

                throw;
            }
            if (ModelState.IsValid)
            {
                
                var ConferenceDayAdded = new ConferenceDay();
                try
                {
                    _mapper.Map(model, ConferenceDayAdded); // map view model to data model
                    var beforeConferenceDayAdded = await _conferenceDay.GetAllConferenceDay();
                    if(beforeConferenceDayAdded.Any(c=>c.Daynumber == ConferenceDayAdded.Daynumber 
                                   || c.ConferenceDate.Date == ConferenceDayAdded.ConferenceDate.Date))
                    {
                        ModelState.AddModelError("", "Duplicate conference day");
                        return View(model);
                    }
                    await _conferenceDay.InsertConferenceDay(ConferenceDayAdded);
                    ViewData["AddedConferenceDay"] = await _conferenceDay.GetAllConferenceIncludingProgramme();
                    return View();
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "An error occured while adding a conference day");
                    return View(model);
                    //throw;
                }


            }
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int Id)
        {
            try
            {
                var model = (await _conferenceDay.GetAllConferenceDay())
                    .Where(c => c.ConferenceDayId == Id).SingleOrDefault();
                if(model != null)
                {
                    ConferenceDayViewModel mappedViewModel = new ConferenceDayViewModel();
                    _mapper.Map(model, mappedViewModel);
                    return View(mappedViewModel);
                }
            }
            catch
            {
                throw;
            }
            return RedirectToAction("SetDate");
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ConferenceDayViewModel model, int Id)
        {
            if (ModelState.IsValid)
            {
                
                try
                {
                    var ConferenceDayToEdit = (await _conferenceDay.GetAllConferenceDay())
                        .Where(c => c.ConferenceDayId == Id).SingleOrDefault();
                    _mapper.Map(model, ConferenceDayToEdit); // map view model to data model
                    await _conferenceDay.UpdateConferenceDay(ConferenceDayToEdit);
                    ViewData["message"] = "Conference Successfully Edited";
                    return View(model);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "An error occured while adding a conference day");
                    return View(model);
                    //throw;
                }


            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int Id)
        {
            try
            {
                var ConferenceDayToDelete = (await _conferenceDay.GetAllConferenceDay())
                        .Where(c => c.ConferenceDayId == Id).SingleOrDefault();

                await _conferenceDay.DeleteConferenceDay(ConferenceDayToDelete);

                return RedirectToAction("SetDate");
            }
            catch
            {
                throw;
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var AllConferenceDays = await _conferenceDay.GetAllConferenceDay();
                if(AllConferenceDays != null)
                {
                    foreach(var conferenceDay in AllConferenceDays)
                    {
                        await _conferenceDay.DeleteConferenceDay(conferenceDay);
                    }
                    return RedirectToAction("SetDate");
                }
                return NotFound();
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
