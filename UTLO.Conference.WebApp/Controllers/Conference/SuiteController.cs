﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using AutoMapper;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class SuiteController : Controller
    {
        private IMapper _mapper;
        private ISuitesRepository _suites;
        private IProgrammeRepository _programme;

        // GET: /<controller>/
        public SuiteController(ISuitesRepository suites,
            IMapper mapper, IProgrammeRepository programme)
        {
            _mapper = mapper;
            _suites = suites;
            _programme = programme;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int Id,int?page)
        {
            try
            {
                var suites = await _programme.GetSuitesByProgrammeId(Id);
                ViewData["Suites"] = PaginatedList<Suites>.Create(suites, page ?? 1, 15);
                ViewData["Programmes"] = (await _programme.GetAllProgramme())
                    .Where(x => x.ProgrammeId == Id).SingleOrDefault();
                ViewData["page"] = page;
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Suites information could not be retrived at this instance");
                return View();
            }
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int Id,SuitesViewModel model,int?page)
        {
            try
            {
                ViewData["Programmes"] = (await _programme.GetAllProgramme())
                    .Where(x => x.ProgrammeId == Id).SingleOrDefault();
                if (ModelState.IsValid)
                {
                    var newSuite = new Suites();
                    newSuite.ProgrammeId = Id;
                    newSuite.Venue = model.Venue;
                    await _suites.InsertSuite(newSuite);

                    ViewData["Suites"] = PaginatedList<Suites>.Create(await _programme.GetSuitesByProgrammeId(Id), page ?? 1, 15);
                    ViewData["page"] = page;
                    return View();
                }
                ModelState.AddModelError("", "Please check suite entries");
            }
            catch (Exception)
            {

                ModelState.AddModelError("", "Error in adding suites");
                return View(model);
            }
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int Id, int?page)
        {
            try
            {
                var suite = (await _suites.GetAllSuites()).Where(c => c.SuitesId == Id).SingleOrDefault();
                var programmes = await _programme.GetAllProgramme();
                var model = new SuitesViewModel();
                model.Venue = suite.Venue;
                //model.presentations = suite.presentations.ToList();
                model.programmes = programmes;
                model.selectedProgrammeId = suite.ProgrammeId;
                ViewData["page"] = page;
                return View(model);
            }
            catch (Exception)
            {
                var model = new SuitesViewModel();
                ModelState.AddModelError("", "Error in updating suite");
                return View(model);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int Id,SuitesViewModel model,int?page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var suite = (await _suites.GetAllSuites()).Where(c => c.SuitesId == Id).SingleOrDefault();
                    suite.Venue = model.Venue;
                    var programmes = await _programme.GetAllProgramme();
                    model.programmes = programmes;
                    suite.ProgrammeId = model.selectedProgrammeId;

                    await _suites.UpdateSuite(suite);
                    ViewData["Message"] = "Successfully updated suite";
                    ViewData["page"] = page;
                    return View(model);

                }
                ModelState.AddModelError("", "Error, please check your entries");
            }
            catch (Exception)
            {
                //if we get here then we could edit the suite 
                ModelState.AddModelError("", "Error in editing suite");
                return View(model);
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int Id, int?page)
        {
            try
            {
                var suite = (await _suites.GetAllSuites())
                    .Where(c => c.SuitesId == Id).SingleOrDefault();
                int previousProgrammeId = suite.ProgrammeId;
                await _suites.DeleteSuite(suite);
                ViewData["page"] = page;
                return RedirectToAction("Create", new { Id = previousProgrammeId });

            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
