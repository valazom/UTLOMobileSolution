﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using Microsoft.AspNetCore.Authorization;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class PresentationController : Controller
    {
        private IPresentationRepository _presentation;
        private ISuitesRepository _suites;
        private IAbstractRespository _abstracts;

        // GET: /<controller>/
        public PresentationController(IPresentationRepository presentation,ISuitesRepository suites,
                       IAbstractRespository abstractRespository)
        {
            _presentation = presentation;
            _suites = suites;
            _abstracts = abstractRespository;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int Id,int?page)
        {
            try
            {
                var abstracts = await GetListOfAbstractsToDisplay();
                var presentations = await _presentation.GetPresentationBySuiteId(Id);
                ViewData["Presentations"] = PaginatedList<Presentations>.Create(presentations, page ?? 1, 15);
                var suite = (await _suites.GetAllSuites())
                    .Where(c => c.SuitesId == Id).SingleOrDefault();
                ViewData["Suite"] = suite.ProgrammeId;
                ViewData["page"] = page;
                ViewData["Abstracts"] = abstracts;
            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int Id, PresentationViewModel model,int?page)
        {
            try
            {
                

                if (ModelState.IsValid)
                {
                    var presentation = new Presentations();
                    var abstractId = int.Parse(model.Title);

                    var abstracts = await _abstracts.GetAbstractsById(abstractId);

                    if (abstracts != null)
                    {
                        presentation.Title = abstracts.Title;
                        presentation.type = (UTLO_NewModel.Enums.PresentationType)model.presentationType;
                        presentation.Venue = model.Venue;
                        presentation.SuiteId = Id;

                        await _presentation.InsertPresention(presentation);
                        //await _abstracts.DeleteAbstracts(abstracts);
                        if (abstracts.Authors != null)
                        {
                            foreach (var author in abstracts.Authors)
                            {
                                presentation.Authors = presentation.Authors ?? new List<Attendees>();
                                await _presentation.UpdatePresentationWithAuthor(presentation, author);
                            }
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "No Matching abstract found. Upload abstracts");
                    }

                    await UpdateViews(Id, page);
                    return View();
                }
                ModelState.AddModelError("", "Please kindly check your entry. No abstracts found");
                await UpdateViews(Id, page);
            }
            catch (Exception)
            {
                ModelState.AddModelError("", "Error occured in adding new presentation");
                await UpdateViews(Id, page);
                return View(model);
            }
            return View(model);
        }

        private async Task UpdateViews(int Id, int? page)
        {
            var Allabstracts = await GetListOfAbstractsToDisplay();
            var items = await _presentation.GetPresentationBySuiteId(Id);
            ViewData["Presentations"] = PaginatedList<Presentations>.Create(items, page ?? 1, 15);
            ViewData["Suite"] = (await _suites.GetAllSuites())
                .Where(c => c.SuitesId == Id).SingleOrDefault().ProgrammeId;
            ViewData["page"] = page;
            ViewData["Abstracts"] = Allabstracts;
        }

        private async Task<List<Abstracts>> GetListOfAbstractsToDisplay()
        {
            var allAbstracts = await _abstracts.GetAllAbstracts();
            var allPresentations = await _presentation.GetAllPresentations();
            foreach(var presentation in allPresentations)
            {
                allAbstracts.RemoveAll(c => c.Title == presentation.Title);
            }
            return allAbstracts = allAbstracts ?? new List<Abstracts>();
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int Id,int?page)
        {
            try
            {
                //var Allabstracts = await GetListOfAbstractsToDisplay();
                var presentation = (await _presentation.GetAllPresentations())
                    .Where(c => c.PresentationsId == Id).SingleOrDefault();
                var model = new PresentationViewModel();
                model.Title = presentation.Title;
                
                model.presentationType = (int)presentation.type;
                model.Venue = presentation.Venue;
                ViewData["Suite"] = presentation.SuiteId;
                ViewData["page"] = page;
                //ViewData["Abstracts"] = Allabstracts;
                return View(model);
            }
            catch (Exception e)
            {

                throw;
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,PresentationViewModel model,int? page)
        {
            try
            {
                //var Allabstracts = await GetListOfAbstractsToDisplay();
                if (ModelState.IsValid)
                {
                    var presentation = (await _presentation.GetAllPresentations())
                                       .Where(c => c.PresentationsId == id).SingleOrDefault();
                    presentation.Title = model.Title;
                    
                    presentation.type = (UTLO_NewModel.Enums.PresentationType)model.presentationType;
                    presentation.Venue = model.Venue;

                    await _presentation.UpdatePresentation(presentation);
                    ViewData["Message"] = "The presentation was successfully updated";
                    ViewData["Suite"] = presentation.SuiteId;
                    ViewData["page"] = page;
                    //jViewData["Abstracts"] = Allabstracts;
                    return View(model);
                }
                ModelState.AddModelError("", "Kindly check your entries.");
            }
            catch (Exception)
            {

                throw;
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int id,int? page)
        {
            try
            {
                var presentation = (await _presentation.GetAllPresentations())
                                       .Where(c => c.PresentationsId == id).SingleOrDefault();
                int suiteId = presentation.SuiteId;
                await _presentation.DeletePresentation(presentation);
                ViewData["page"] = page;

                return RedirectToAction("Create", new { Id = suiteId });

            }
            catch (Exception)
            {

                throw;
            }
        }    
    }
}
