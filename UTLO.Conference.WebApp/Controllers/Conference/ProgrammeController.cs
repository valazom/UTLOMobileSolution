﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using AutoMapper;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;
using Microsoft.AspNetCore.Authorization;
using PagedList;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    [Authorize]
    public class ProgrammeController : Controller
    {
        private IMapper _mapper;
        private IProgrammeRepository _programme;
        private IConferenceDayRepository _conferenceDay;

        // GET: /<controller>/
        public ProgrammeController(IProgrammeRepository programme,
            IConferenceDayRepository conferenceDay,
            IMapper mapper)
        {
            _mapper = mapper;
            _programme = programme;
            _conferenceDay = conferenceDay;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int Id,int? page)
        {
            try
            {
                var ConfDay =  (await _conferenceDay.GetAllConferenceDay())
                       .Where(c => c.ConferenceDayId == Id).SingleOrDefault();
                var items = (await _conferenceDay.GetProgrammesByDay(ConfDay)).OrderBy(c => c.StartTime).ToList();
                int pageIndex = page ?? 1;
                ViewData["Programmes"] = PaginatedList<Programme>.Create(items, pageIndex, 15);
                ViewData["page"] = page;
            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int Id,ProgrammeViewModel progViewModel,int? page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var ConfDay = (await _conferenceDay.GetAllConferenceDay())
                      .Where(c => c.ConferenceDayId == Id).SingleOrDefault();
                    var newprogramme = new Programme();
                    progViewModel.ConferenceId = Id;
                    newprogramme.ConferenceDayId = Id;
                    newprogramme.EndTime = progViewModel.EndTime.TimeOfDay;
                    newprogramme.StartTime = progViewModel.StartTime.TimeOfDay;
                    newprogramme.Title = progViewModel.Title;
                    if (!progViewModel.IsMultiVenueProgramme)
                    {
                        newprogramme.Venue = progViewModel.Venue;
                        newprogramme.programmeType = UTLO_NewModel.Enums.ProgrammeType.SingleVenue;
                    }
                    else
                    {
                        newprogramme.programmeType = UTLO_NewModel.Enums.ProgrammeType.MultiVenue;
                    }
                    await _programme.InsertProgramme(newprogramme);

                    var items = (await _conferenceDay.GetProgrammesByDay(ConfDay)).OrderBy(c => c.StartTime).ToList();
                    ViewData["Programmes"] = PaginatedList<Programme>.Create(items, page ?? 1, 15);
                    ViewData["page"] = page;
                    return View(progViewModel);
                }
                ModelState.AddModelError("", "Programme not added. Kindly check your entries");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("",e.Message);
                return View(progViewModel);
            }
            return View(progViewModel);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id,int?page)
        {
            try
            {
                var programme = (await _programme.GetAllProgramme())
                    .Where(c => c.ProgrammeId == id).SingleOrDefault();
                var model = new ProgrammeViewModel();
                model.Title = programme.Title;
                model.StartTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                DateTime.Now.Day, programme.StartTime.Hours, programme.StartTime.Minutes, programme.StartTime.Seconds);
                model.EndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                DateTime.Now.Day, programme.EndTime.Hours, programme.EndTime.Minutes, programme.EndTime.Seconds);
                model.ConferenceId = programme.ConferenceDayId;
                
                if(programme.programmeType == UTLO_NewModel.Enums.ProgrammeType.MultiVenue)
                {
                    model.IsMultiVenueProgramme = true;
                }
                else
                {
                    model.IsMultiVenueProgramme = false;
                    model.Venue = programme.Venue;
                }
                ViewData["page"] = page;

                return View(model);
            }
            catch (Exception e)
            {

                ModelState.AddModelError("", e.Message);
                var model = new ProgrammeViewModel();
                return View(model);
            }
            
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,ProgrammeViewModel model,int?page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var programme = (await _programme.GetAllProgramme())
                    .Where(c => c.ProgrammeId == id).SingleOrDefault();
                    programme.Title = model.Title;
                    programme.StartTime = model.StartTime.TimeOfDay;
                    programme.EndTime = model.EndTime.TimeOfDay;
                    if (!model.IsMultiVenueProgramme)
                    {
                        programme.Venue = model.Venue;
                        programme.programmeType = UTLO_NewModel.Enums.ProgrammeType.SingleVenue;
                    }
                    else
                    {
                        programme.programmeType = UTLO_NewModel.Enums.ProgrammeType.MultiVenue;
                    }
                    await _programme.UpdateProgramme(programme);
                    ViewData["Message"] = "Programme successfully updated";
                    ViewData["ConferenceDayId"] = programme.ConferenceDayId;
                    ViewData["page"] = page;
                    model.ConferenceId = programme.ConferenceDayId;
                    return View(model);
                }
                ModelState.AddModelError("", "Programme not updated. Kindly check your entries");
                ViewData["page"] = page;
                
            }
            catch (Exception)
            {

                ModelState.AddModelError("", "Error in updating programme");
                return View(model);
            }
            return View(model);
        }
        public async Task<IActionResult> Delete(int id,int?page)
        {
            try
            {
                var programme = (await _programme.GetAllProgramme())
                    .Where(c => c.ProgrammeId == id).SingleOrDefault();
                int conId = programme.ConferenceDayId;
                await _programme.DeleteProgramme(programme);

                ViewData["page"] = page;
                return RedirectToAction("Create", new { Id = conId });
            }
            catch (Exception e)
            {

                return StatusCode(500, e.Message);
            }
        }
    }
}
