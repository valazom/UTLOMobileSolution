﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;
using UTLO_NewModel.Enums;
using UTLO_NewModel.Domain;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO.Conference.WebApp.RemoteNotification;
using UTLO.Conference.WebApp.Entities.Enums;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Conference
{
    public class SettingsController : Controller
    {
        private ISettingsRepository _settingRepository;
        private List<SettingsViewModel> _settingsData;
        private SendNotification _sendNotification;

        // GET: /<controller>/
        public SettingsController(ISettingsRepository settingsRepository, SendNotification sendNotification)
        {
            _settingRepository = settingsRepository;
            _settingsData = Initialization();
            _sendNotification = sendNotification;
        }
        private List<SettingsViewModel> Initialization()
        {
            var settingData = new List<SettingsViewModel>();
            var customSettingsData = Enum.GetNames(typeof(CustomSettings));
            foreach (var settings in customSettingsData)
            {
                settingData.Add(new SettingsViewModel
                {
                    SettingName = settings
                });
            }
            return settingData;
        }
        private void UpdateSettingData(List<Settings> settings)
        {
            foreach (var setting in settings)
            {
                if (_settingsData.Any(c => c.SettingName == setting.SettingName))
                {
                    _settingsData[_settingsData.FindIndex(c => c.SettingName == setting.SettingName)]
                        .SettingValue = setting.SettingValue;
                }
            }
        }
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            try
            {
                var AllSettings = await _settingRepository.GetAllSettings();
                UpdateSettingData(AllSettings);
                var EnableCustomSettings = await IsEnableCustomSettings();
                ViewData["EnableCustomSettings"] = EnableCustomSettings;
                //ViewData["Settings"] = PaginatedList<Settings>.Create(AllSettings, page ?? 1, 15);
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
            return View(_settingsData);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SettingsViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var oldsettings = await _settingRepository.GetAllSettings();
                    var currentsettings = oldsettings?.Find(c => c.SettingName == model.SettingName);


                    if (currentsettings == null)
                    {
                        var newSettings = new Settings();
                        newSettings.SettingName = model.SettingName;
                        newSettings.SettingValue = model.SettingValue;
                        newSettings.SettingType = SettingType.Compulsory;

                        await _settingRepository.InsertSetting(newSettings);

                    }
                    else
                    {
                        currentsettings.SettingValue = model.SettingValue;
                        await _settingRepository.UpdateSettings(currentsettings);

                    }
                    await GetLocationSettings(model);


                    var AllSettings = await _settingRepository.GetAllSettings();
                    UpdateSettingData(AllSettings);
                    ViewData["EnableCustomSettings"] = await IsEnableCustomSettings();
                    //ViewData["Settings"] = PaginatedList<Settings>.Create(AllSettings, page ?? 1, 15);
                }
                else
                {
                    ViewData["Message"] = "Please check your input";
                }
                return View(_settingsData);
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }

        private async Task GetLocationSettings(SettingsViewModel model)
        {
            if (model.SettingName == CustomSettings.Address.ToString())
            {
                await SaveLocationData(model.SettingValue);
            }
        }

        private async Task SaveLocationData(string address)
        {

            if (!string.IsNullOrEmpty(address)) // connect google geo location api if the address is not empty
            {
                var location = await _sendNotification.GetLocationData(address); // get the lat and long of the addres
                //var oldsettings = await _settingRepository.GetAllSettings();
                //address from geo api
                if (location != null)
                {

                    var latitudeSettings = (await _settingRepository.GetAllSettings()).Find(c => c.SettingName == CustomSettings.Latitude.ToString());
                    if (latitudeSettings != null)
                    {
                        latitudeSettings.SettingValue = location.Latitude.ToString();
                        await _settingRepository.UpdateSettings(latitudeSettings);
                    }
                    else
                    {
                        latitudeSettings = new Settings();
                        latitudeSettings.SettingName = CustomSettings.Latitude.ToString();
                        latitudeSettings.SettingValue = location.Latitude.ToString();
                        latitudeSettings.SettingType = SettingType.Compulsory;
                        await _settingRepository.InsertSetting(latitudeSettings);
                    }

                    var longtitudeSettings = (await _settingRepository.GetAllSettings()).Find(c => c.SettingName == CustomSettings.Longitude.ToString());
                    if (longtitudeSettings != null)
                    {
                        longtitudeSettings.SettingValue = location.Longitude.ToString();
                        await _settingRepository.UpdateSettings(longtitudeSettings);
                    }
                    else
                    {
                        longtitudeSettings = new Settings();
                        longtitudeSettings.SettingName = CustomSettings.Longitude.ToString();
                        longtitudeSettings.SettingValue = location.Longitude.ToString();
                        longtitudeSettings.SettingType = SettingType.Compulsory;
                        await _settingRepository.InsertSetting(longtitudeSettings);
                    }

                }

            }

        }
        private async Task<bool> IsEnableCustomSettings()
        {
            var CompulsorySettings = await _settingRepository.GetSettingsByType(SettingType.Compulsory);
            var CompulsorySettingsNum = Enum.GetNames(typeof(SettingType)).Length;
            if (CompulsorySettings != null)
            {
                if (CompulsorySettings.Count != CompulsorySettingsNum)
                {
                    ViewData["Message"] = "Fill in all compulsory settings to enable custom settings";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
