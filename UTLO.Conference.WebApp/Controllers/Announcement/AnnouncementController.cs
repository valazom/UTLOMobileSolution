﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO_NewModel.Domain;
using UTLO.Conference.WebApp.Entities.ViewModel;
using Microsoft.AspNetCore.Authorization;
using UTLO.Conference.WebApp.RemoteNotification;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Announcement
{
    [Authorize]
    public class AnnouncementController : Controller
    {
        private IAnnouncementRespository _announcementRepository;
        private SendNotification _sendNotification;
        private ISettingsRepository _settingsRepository;
        private IConfiguration _configuration;

        private string Message;
        private string Title;
        private string ConferenceName;
        // GET: /<controller>/
        public AnnouncementController(IAnnouncementRespository announcementRepository, 
            SendNotification sendNotification,
            ISettingsRepository settingsRepository,
            IConfiguration configuration)
        {
            _announcementRepository = announcementRepository;
            _sendNotification = sendNotification;
            _settingsRepository = settingsRepository;
            _configuration = configuration;
        }
        [HttpGet]
        public async Task<IActionResult> Create(int?page)
        {
            try
            {
                var AllAnnouncements = await _announcementRepository.GetAllAnnouncements();
                ViewData["Announcement"] = PaginatedList<UTLO_NewModel.Domain.Announcement>.Create(AllAnnouncements, page ?? 1, 15);
                return View();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
            
        }
        private async Task SetupConferenceName()
        {
            var conferencename = (await _settingsRepository.GetAllSettings())
                .Where(c => c.SettingName == UTLO_NewModel.Enums.CustomSettings.Title.ToString())
                .FirstOrDefault();
            ConferenceName = conferencename.SettingValue;
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AnnouncementViewModel model, int?page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var announcement = new UTLO_NewModel.Domain.Announcement();
                    announcement.Title = model.Tile;
                    announcement.Date = model.DateAndTime;
                    await SetupConferenceName();
                    if (!string.IsNullOrEmpty(ConferenceName))
                    {
                        Message = string.Format(_configuration["Notification:Message:Announcement"], ConferenceName);
                        await _announcementRepository.InsertAnnouncement(announcement);
                        var AllAnnouncements = await _announcementRepository.GetAllAnnouncements();
                        ViewData["Announcement"] = PaginatedList<UTLO_NewModel.Domain.Announcement>
                            .Create(AllAnnouncements, page ?? 1, 15);
                        await Task.Run(() => _sendNotification.SendNotificationToFCM(announcement.Title, Message));
                    }
                    else
                    {
                        ViewData["Message"] = "Fill in Conference settings before you can create announcement";
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int Id)
        {
            try
            {
                var announcement = await _announcementRepository.GetAnnouncementById(Id);
                var model = new AnnouncementViewModel();
                model.Tile = announcement.Title;
                model.DateAndTime = announcement.Date;
                return View(model);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AnnouncementViewModel model, int id)
        {
            try
            {
                var announcement = await _announcementRepository.GetAnnouncementById(id);
                if (ModelState.IsValid)
                {
                    if (announcement != null)
                    {
                        announcement.Title = model.Tile;
                        announcement.Date = model.DateAndTime;
                        await _announcementRepository.UpdateAnnouncement(announcement);
                        ViewData["Message"] = "Announcement Successfully Updated";
                        return View(model);
                    } 
                }
                return View(model);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
            
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var announcement = await _announcementRepository.GetAnnouncementById(id);
                if(announcement != null)
                {
                    await _announcementRepository.DeleteAnnouncement(announcement);
                }
                return RedirectToAction("Create");

            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [Authorize]
        public async Task<IActionResult> DeleteAll()
        {
            try
            {
                var AllAnnouncements = await _announcementRepository.GetAllAnnouncements();
                if(AllAnnouncements != null)
                {
                    foreach(var announcement in AllAnnouncements)
                    {
                        await _announcementRepository.DeleteAnnouncement(announcement);
                    }
                    return RedirectToAction("Create");
                }
                return NotFound();
            }
            catch (Exception ex)
            {

                return StatusCode(500, ex.Message);
            }
        }
    }
}
