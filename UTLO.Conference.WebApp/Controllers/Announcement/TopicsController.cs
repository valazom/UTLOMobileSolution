﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO_NewModel.Domain;
using UTLO.Conference.WebApp.Entities.ViewModel;
using Microsoft.AspNetCore.Authorization;
using UTLO.Conference.WebApp.RemoteNotification;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers.Announcement
{
    [Authorize]
    public class TopicsController : Controller
    {
        private ITopicRepository _topicsRepository;
        private IAnnouncementRespository _announcementRepository;
        string announcementName = string.Empty;
        string Message;
            
        const string Title = "Announcement";
        private SendNotification _sendNotification;

        // GET: /<controller>/
        public TopicsController(ITopicRepository topicsRepository,
            IAnnouncementRespository announcementRepository,
            SendNotification sendNotification)
        {
            _topicsRepository = topicsRepository;
            _announcementRepository = announcementRepository;
            _sendNotification = sendNotification;
            
        }
        [HttpGet]
        public async Task<IActionResult> Create(int id,int?page)
        {
            try
            {
                var AllTopics = await _topicsRepository.GetTopicByAnnouncementId(id);
                ViewData["Topics"] = PaginatedList<Topic>.Create(AllTopics, page ?? 1, 15);
                return View();
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex.Message);
            }
            
        }
        
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id,TopicViewModel model,int?page)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var announcement = await _announcementRepository.GetAnnouncementById(id);
                    announcementName = announcement.Title;

                    if(announcement != null)
                    {
                        var topic = new Topic();
                        topic.AnnouncementId = announcement.AnnouncementId;
                        topic.Bulletpoint = model.Bulletpoint;
                        Message = topic.Bulletpoint;
                        await _topicsRepository.InsertTopic(topic);
                        var AllTopics = await _topicsRepository.GetTopicByAnnouncementId(announcement.AnnouncementId);
                        ViewData["Topics"] = PaginatedList<Topic>.Create(AllTopics, page ?? 1, 15);
                        await Task.Run(()=>_sendNotification.SendNotificationToFCM(announcement.Title, Message));
                        return View();
                    }
                    ModelState.AddModelError("", "Invalid announcement Id");
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                var topic = await _topicsRepository.GetTopicById(id);
                var model = new TopicViewModel();
                if (topic !=null)
                {
                    
                    model.Bulletpoint = topic.Bulletpoint;
                    ViewData["Id"] = topic.AnnouncementId;
                    return View(model);
                }
                return View(model);

            }
            
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,TopicViewModel model)
        {
            try
            {
                var topic = await _topicsRepository.GetTopicById(id);
                if(topic != null)
                {
                    topic.Bulletpoint = model.Bulletpoint;
                    await _topicsRepository.UpdateTopic(topic);
                    ViewData["Message"] = "Successfully updated Topic";
                    ViewData["Id"] = topic.AnnouncementId;
                    return View(model);
                }
                return View(model);
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var topic = await _topicsRepository.GetTopicById(id);
                if(topic != null)
                {
                    await _topicsRepository.DeleteTopic(topic);
                }
                return RedirectToAction("Create",new { id = topic.AnnouncementId});
            }
            catch (Exception ex)
            {

                return StatusCode(500,ex.Message);
            }
        }
    }
}
