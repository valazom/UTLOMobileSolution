﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using UTLO.Conference.WebApp.Entities.Domain;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO.Email.Service.EmailService;
using UTLO.Email.Service.EmailMessage;
using Microsoft.Extensions.Configuration;
using UTLO.Conference.WebApp.Entities.Enums;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UTLO.Conference.WebApp.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private IEmailService _emailservice;
        private IConfiguration _configuration;
        

        // GET: /<controller>/
        public AccountController(UserManager<ApplicationUser> userManger, 
            SignInManager<ApplicationUser> signInManager,
            IEmailService emailService,
            IConfiguration configuration)
        {
            _userManager = userManger;
            _signInManager = signInManager;
            _emailservice = emailService;
            _configuration = configuration;
        }
        [HttpGet]
        public IActionResult Register()
        {
            var model = new RegisterViewModel();
            return View(model);
        }
        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser();
                user.UserName = model.Username;
                user.Email = model.Email;

                //check if the user exist;
                var isuserEmailExist = await _userManager.FindByEmailAsync(user.Email);
                var isUsernameExist = await _userManager.FindByNameAsync(user.UserName);

                if (isuserEmailExist == null && isUsernameExist == null)
                {
                    var content = BuildContentForEmailMessage(user, EmailMessageType.Registration);
                    var emailMessage = GetUserEmailMessage(user,content);
                    var emailMessageResult = await _emailservice.SendAsync(emailMessage);
                    // if email was sent create the user
                    if (emailMessageResult.Succeeded)
                    {
                        var userCreateResult = await _userManager.CreateAsync(user, model.password);

                        if (userCreateResult.Succeeded)
                        {

                            return RedirectToAction("Registered", new { Email = model.Email });
                        }
                        else
                        {
                            foreach (var err in userCreateResult.Errors)
                            {
                                ModelState.AddModelError("", err.Description);
                            }
                        }
                    }
                    ModelState.AddModelError("", "Registration was not completed because email service failed. Try again later");
                    return View(model);
                }
                ModelState.AddModelError("", "User with the same Username or Email address already exist");
            }
            return View(model);
        }

        private string BuildContentForEmailMessage(ApplicationUser user, EmailMessageType emailMessageType)
        {
            string content = string.Empty;
            switch (emailMessageType)
            {
                case EmailMessageType.Registration:
                    content = _configuration["EmailSettings:Content"] + user.UserName + "\n\n" +
                        _configuration["EmailSettings:Content1"] +
                        user.Email + ". " + _configuration["EmailSettings:Content2"] +
                        _configuration["EmailSettings:LoginUrl"] +
                        _configuration["EmailSettings:Content3"];
                    break;
                case EmailMessageType.Forgetpassword:
                    
                    content = _configuration["EmailSettings:Content"] + user.UserName + "\n\n" +
                        _configuration["EmailSettings:Content4"] + _configuration["EmailSettings:ForgetpasswordUrl"] +
                        user.Id;
                    break;
            }
            return content;
        }

        

        private EmailMessage GetUserEmailMessage(ApplicationUser user,string content)
        {
            

            var emailMessageBuilder = new EmailMessageBuilder();

            var message = emailMessageBuilder.AddFromAddress(_configuration["EmailSettings:Name"], _configuration["EmailSettings:From"])
                               .AddToAddresses(user.UserName,user.Email)
                               .AddSubject(_configuration["EmailSettings:Subject"])
                               .AddBody(content)
                               .Build();
            return message;
        }
        [HttpGet]
        public IActionResult Forgetpassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Forgetpassword(ForgetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userResult = await _userManager.FindByEmailAsync(model.EmailAddress);
                if(userResult != null)
                {
                    var content = BuildContentForEmailMessage(userResult, EmailMessageType.Forgetpassword);
                    var message = GetUserEmailMessage(userResult, content);
                    var messageResult = await _emailservice.SendAsync(message);
                    if (messageResult.Succeeded)
                    {
                        ModelState.AddModelError("", "Successfully sent link to email address provided");
                        return View();
                    }
                    ModelState.AddModelError("", "Password retrieval was not completed as email service failed");
                    return View();
                }
                ModelState.AddModelError("", "No user with email account exist");
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Changepassword(string userid)
        {
            var user = await _userManager.FindByIdAsync(userid);
            var model = new ChangePasswordViewModel();
            if(user != null)
            {
                model.Email = user.Email;
                return View(model);
            }
            ModelState.AddModelError("", "Invalid user");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Changepassword(string userid,ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(userid);
                if(user != null)
                {
                    var usertoken = await _userManager.GeneratePasswordResetTokenAsync(user);
                    await _userManager.ResetPasswordAsync(user, usertoken, model.password);
                    ModelState.AddModelError("", "Password reset successful");
                    return View(model);
                }
            }
            ModelState.AddModelError("", "Password change failed. Check entries");
            return View(model);
        }
        public IActionResult Registered(RegisteredViewModel model)
        {
            return View(model);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost,ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var loginResult =  await _signInManager.PasswordSignInAsync(model.Username,
                                                        model.password, model.rememberMe,false);
                if (loginResult.Succeeded)
                {
                    if (Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Mainmenu");
                    }
                }
                ModelState.AddModelError("", "Could not login at this time, kindly try again later");
            }
            return View(model);
        }
        public async Task<IActionResult> SignOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
