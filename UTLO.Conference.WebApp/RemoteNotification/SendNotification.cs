﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.RemoteNotification
{
    public class SendNotification
    {
        private IConfiguration _configuration;

        public SendNotification(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // send notification to FCM Service
        public void SendNotificationToFCM(string Title,string message,bool isSoundEnabled = true)
        {
            bool isPostSuccessful = false;
            using (var client = new HttpClient())
            {
                var mynotification = new Notification
                {
                    title = Title,
                    body = message,
                    sound = "Enabled"
                };
                var notificationdata = new NotificationData
                {
                    to = _configuration["Notification:Topic"],
                    notification = mynotification
                };
                var jsonData = JsonConvert.SerializeObject(notificationdata);
                client.DefaultRequestHeaders.Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = 
                    new AuthenticationHeaderValue("key", "=" +_configuration["Notification:WEB_KEY"]);
                client.DefaultRequestHeaders.Add("Sender", "id=" + _configuration["Notification:SENDER_ID"]);
                var url = new Uri(_configuration["Notification:FCMUrl"]);
                Task.WaitAll(client.PostAsync(url, new StringContent(jsonData, Encoding.UTF8, "application/json"))
                             .ContinueWith(response =>
                             {
                                 if (response.Result.IsSuccessStatusCode)
                                 {
                                     isPostSuccessful = true;
                                 }
                             }));

            }
            //return isPostSuccessful;
        }
        public async Task<LocationData> GetLocationData(string address)
        {
            var location = new LocationData();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                var locationUrl = _configuration["GeoLocation:baseurl"] + address + _configuration["GeoLocation:key"];
                var url = new Uri(locationUrl);
                var response = await client.GetAsync(locationUrl);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<RootObject>(content);
                    var geometry = data.results[0];
                    location.Latitude = geometry.geometry.location.lat;
                    location.Longitude = geometry.geometry.location.lng; 
                } 
            }
            return location;
        }
    }
}
