﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.RemoteNotification
{
    public class LocationData
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
