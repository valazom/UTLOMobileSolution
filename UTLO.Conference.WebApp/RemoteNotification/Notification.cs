﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp.RemoteNotification
{
    public class NotificationData
    {
        public string to { get; set; }
        public Notification notification { get; set; }
    }
    public class Notification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string sound { get; set; }
    }
}
