﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Routing;
using UTLO_NewModel;
using UTLO.Accommodation.Data;
using UTLO.Model.Data.Repository.ConfereenceRepository;
using UTLO.Model.Data.Repository.ConferenceRepository;
using UTLO.Conference.WebApp.Entities.Data;
using Microsoft.EntityFrameworkCore;
using UTLO.Conference.WebApp.Entities.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using AutoMapper;
using UTLO.Conference.Repository.EvaluationRepository.IRepository;
using UTLO.Conference.Repository.EvaluationRepository.Repository;
using UTLO.Conference.WebApp.RemoteNotification;
using UTLO.Conference.Repository.ConferenceRepository.IRepository;
using UTLO.Conference.Repository.ConferenceRepository.Repository;
using UTLO.Email.Service.EmailConfiguration;
using UTLO.Email.Service.EmailService;


namespace UTLO.Conference.WebApp
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                                .SetBasePath(env.ContentRootPath)
                                .AddJsonFile("appsettings.json")
                                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddAutoMapper();
            services.AddScoped(_ => new ConferenceDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new EvaluationDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new AnnouncementDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped(_ => new AccommodationDbContext(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IAbstractRespository, AbstractsRepository>();
            services.AddScoped<IAttendeeRepository, AttendeeRepository>();
            services.AddScoped<IConferenceDayRepository, ConferenceDayRepository>();
            services.AddScoped<IPresentationRepository, PresentationRepository>();
            services.AddScoped<IProgrammeRepository, ProgrammeRepository>();
            services.AddScoped<IRateRepository, RateRepository>();
            services.AddScoped<ISuitesRepository, SuitesRepository>();
            services.AddScoped<IAnnouncementRespository, AnnoucementRepository>();
            services.AddScoped<ITopicRepository, TopicRepository>();
            services.AddScoped<IEvaluationQuestionRepository, EvaluationQuestionsRepository>();
            services.AddScoped<IAnswerRepository, AnswerRepository>();
            services.AddScoped<IAccommodationRepository, AccommodationRepository>();
            services.AddScoped<ISettingsRepository, SettingsRepository>();
            services.AddSingleton(Configuration);
            services.AddSingleton<IEmailConfiguration>(Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddTransient<IEmailService, EmailService>();
            services.AddResponseCompression();
            services.AddScoped<SendNotification>();
            services.AddDbContext<UserDbContext>(options => 
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<UserDbContext>();
            services.AddIdentity<ApplicationUser, IdentityRole>().AddDefaultTokenProviders();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/StatusCode/{0}");
            }

            app.UseStatusCodePagesWithReExecute("/StatusCode/{0}");
            app.UseNodeModules(env.ContentRootPath);
            app.UseResponseCompression();
            app.UseIdentity();
            app.UseMvc(ConfigureRoutes);
            
        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Defaults", "{controller=Home}/{action=Index}/{id?}");
            routeBuilder.MapRoute("Conference", "Conference/{Controller=Conference}/{action=Index}/{id?}");
            routeBuilder.MapRoute("Evaluation", "Evaluation/{Controller=EvaluationQuestion}/{action=Index}/{id?}");
            routeBuilder.MapRoute("Announcement", "Announcement/{Controller=Announcement}/{action=Index}/{id?}");
            routeBuilder.MapRoute("Accommodation", "Accommodation/{Controller=Accommodation}/{action=Index}/{id?}");
        }
    }
}
