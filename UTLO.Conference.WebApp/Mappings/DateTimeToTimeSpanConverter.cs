﻿using AutoMapper;
using System;

namespace UTLO.Conference.WebApp.Mappings
{
    internal class DateTimeToTimeSpanConverter : ITypeConverter<DateTime, TimeSpan>
    {
        public TimeSpan Convert(DateTime source, TimeSpan destination, ResolutionContext context)
        {
            destination = source.TimeOfDay;
            return destination;
        }
    }
}