﻿using AutoMapper;
using System;

namespace UTLO.Conference.WebApp.Mappings
{
    internal class TimespanToDateTimeConverter : ITypeConverter<TimeSpan, DateTime>
    {
        public DateTime Convert(TimeSpan source, DateTime destination, ResolutionContext context)
        {
            destination = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 
                DateTime.Now.Day, source.Hours, source.Minutes, source.Seconds);
            return destination;
        }
    }
}