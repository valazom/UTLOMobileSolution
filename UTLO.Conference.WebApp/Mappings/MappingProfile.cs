﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UTLO.Conference.WebApp.Entities.ViewModel;
using UTLO_NewModel.Domain;

namespace UTLO.Conference.WebApp.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ConferenceDay, ConferenceDayViewModel>();
            CreateMap<ConferenceDayViewModel, ConferenceDay>();
            CreateMap<Programme, ProgrammeViewModel>();
            CreateMap<ProgrammeViewModel, Programme>();
            CreateMap<DateTime, TimeSpan>().ConvertUsing<DateTimeToTimeSpanConverter>();
            CreateMap<TimeSpan, DateTime>().ConvertUsing<TimespanToDateTimeConverter>();
        }
    }
}
