﻿using Excel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace UTLO.Conference.WebApp
{
    public static class Utils
    {
        public static DataTable UploadExcelFile(IFormFile upload)
        {
            var stream = upload.OpenReadStream();
            IExcelDataReader reader = null;
            if (upload.FileName.EndsWith(".xls"))
            {
                reader = ExcelReaderFactory.CreateBinaryReader(stream);
            }
            else if (upload.FileName.EndsWith(".xlsx"))
            {
                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
            }
            else
            {
                throw new Exception("File not supported");
            }
            reader.IsFirstRowAsColumnNames = true;
            var dataset = reader.AsDataSet();
            reader.Close();
            return dataset.Tables[0];
        }
    }
}
