﻿using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UTLO.Conference.WebApp.Entities.ViewModel;

namespace UTLO.Conference.WebApp
{
    public class PasswordIsValidAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (RegisterViewModel)validationContext.ObjectInstance;
            if (Regex.IsMatch(model.password, @"^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*([^\w\s]|[_]))\S{8,}$"))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage);
        }
    }
    public class ChangePasswordIsValidAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (ChangePasswordViewModel)validationContext.ObjectInstance;
            if (Regex.IsMatch(model.password, @"^(?=\S*[a-z])(?=\S*[A-Z])(?=\S*\d)(?=\S*([^\w\s]|[_]))\S{8,}$"))
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(ErrorMessage);
        }
    }
    public class TimespanIsValidAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string textValue = value as string;
            TimeSpan timeValue;
            if (!TimeSpan.TryParse(textValue, out timeValue))
            {
                return false;
            }
            return true;
        }
    }
    public class VenueORHeadingIsEmpty : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (SuitesViewModel)validationContext.ObjectInstance;
            if (string.IsNullOrEmpty(model.Venue))
                return new ValidationResult("Both Heading and Venue cannot be empty.");
            return ValidationResult.Success;

        }
    }
    public class ImageIsKeyNoteSpeaker : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (AttendeesViewModel)validationContext.ObjectInstance;
            if (model.IsKeyNoteSpeaker)
            {
                if (model.ImageUrl == null)
                {
                    return new ValidationResult("An Image must be provided for Keynote speakers");
                }
                else
                {
                    if (model.ImageUrl.Length > 536000) //image should not be greater than 5MB
                    {
                        ErrorMessage = "The size of image is greater than 500MB";
                        return new ValidationResult(ErrorMessage);
                    }
                    return ValidationResult.Success;
                }

            }
            return ValidationResult.Success;
        }
    }
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class ImageExtensionsAttribute : ValidationAttribute
    {
        private List<string> AllowedExtensions { get; set; }

        public ImageExtensionsAttribute(string fileExtensions)
        {
            AllowedExtensions = fileExtensions.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public override bool IsValid(object value)
        {
            var file = value as IFormFile;

            if (file != null)
            {
                var fileName = file.FileName;

                var afileType = AllowedExtensions.Any(y => fileName.EndsWith(y));
                if (file.Length <= 536000 && afileType) //image should not be greater than 5MB
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }
    public class IsAMultiVenueProgramme : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (ProgrammeViewModel)validationContext.ObjectInstance;
            if (!model.IsMultiVenueProgramme)
            {
                if (string.IsNullOrEmpty(model.Venue))
                {
                    return new ValidationResult("Venue must be specified when its not a multi-venue event");
                }
            }
            return ValidationResult.Success;
        }
    }
    public class TitleAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int n;
            var model = (PresentationViewModel)validationContext.ObjectInstance;
            if (!int.TryParse(model.Title, out n))
            {
                return new ValidationResult("Invalid abstract, Please fill in abstracts before presentation");
            }
            return ValidationResult.Success;
        }

    }
}
