﻿
(function () {
    
    //when a group is shown, save it as the active accordion group
    $("#accordion").on('shown', function () {
        var active = $("#accordion .in").attr('id');
        $('#accvalue').val(active);
	});
	$('#delete').click(function () {
		$('#myModal').modal('show');
	});
	$(document).ready(function () {
		$('#delete-form').on("submit", function (e) {
			var postData = $(this).serialize();
			var formUrl = $(this).attr("action");
			$.ajax({
				url: formUrl,
				type: 'POST',
				data: postData,
				success: function (data) {
					alert('Attendees successfully deleted');
					$("#delete-form").remove();
				}
			});
			e.preventDefault();
		});
		$("#delete-btn").on('click', function () {
			$("#delete-form").submit();
		});
	});
})();