﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Accommodation
    {
        [Key]
        public int accommodationId { get; set; }
        [Required]
        public string accommodationName { get; set; }
        [Required]
        public string url { get; set; }
    }
}
