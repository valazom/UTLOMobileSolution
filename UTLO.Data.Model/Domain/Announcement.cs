﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Announcement
    {
        [Key]
        public int AnnouncementId { get; set; }
        [Required, MaxLength(250, ErrorMessage = "Exceeding required charcter length")]
        public string Title { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }
        public ICollection<Topic> Topics { get; set; }
    }
}
