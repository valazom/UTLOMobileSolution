﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Suites
    {
        [Key]
        public int SuitesId { get; set; }
        [Required]
        public int ProgrammeId { get; set; }
        [Required, MaxLength(250,
            ErrorMessage = "The length of the field is more than expected")]
        public string Venue { get; set; }
        public string Heading { get; set; }
        public ICollection<Presentations> presentations { get; set; }
    }
}
