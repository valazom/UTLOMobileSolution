﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Rate
    {
        [Key]
        public int RateId { get; set; }
        [Required]
        public int presentationId { get; set; }
        public int Ratings { get; set; }
    }
}
