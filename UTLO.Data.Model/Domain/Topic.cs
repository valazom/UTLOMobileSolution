﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Topic
    {
        [Key]
        public int TopicId { get; set; }
        [Required]
        public int AnnouncementId { get; set; }
        public string Bulletpoint { get; set; }
    }
}
