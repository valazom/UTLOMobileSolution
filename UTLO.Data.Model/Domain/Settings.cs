﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using UTLO.Data.Model.Enums;

namespace UTLO.Data.Model.Domain
{
    public class Settings
    {
        [Key]
        public int SettingId { get; set; }
        [StringLength(450)]
        [Required]
        public string SettingName { get; set; }
        [Required]
        public string SettingValue { get; set; }
        [Required]
        public SettingType SettingType { get; set; }
    }
}
