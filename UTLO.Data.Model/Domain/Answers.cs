﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Answers
    {
        [Key]
        public int AnswerId { get; set; }
        [Required]
        public int EvaluationQuestionId { get; set; }
        public string answer { get; set; }
        public int Rating { get; set; }
    }
}
