﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class Attendees
    {
        [Key]
        public int AttendeesId { get; set; }
        
        public ICollection<Abstracts> abstracts { get; set; }
        public ICollection<Programme> programmes { get; set; }
        public ICollection<Presentations> presentations { get; set; }
        [Required]
        public bool IsKeynoteSpeaker { get; set; }
        public string status { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Affiliation { get; set; }
        public string Address { get; set; }
        public string ImageUrl { get; set; }
        [DataType(DataType.EmailAddress)]
        public string emailAddress { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string phoneNumber { get; set; }
        public string Biography { get; set; }
    }
}
