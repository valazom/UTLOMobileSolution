﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace UTLO.Data.Model.Domain
{
    public class ConferenceDay
    {
        public ConferenceDay()
        {
            programme = new List<Programme>();
        }
        [Key]
        public int ConferenceDayId { get; set; }
        public int Daynumber { get; set; }
        public DateTime ConferenceDate { get; set; }
        public ICollection<Programme> programme { get; set; }
    }
}
