﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.Model.Enums
{
    public enum ProgrammeType
    {
        SingleVenue,
        MultiVenue
    }
}
