﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.Model.Enums
{
    public enum RateScore
    {
        OneStar = 1,
        TwoStars,
        ThreeStars,
        FourStars,
        FiveStars
    }
}
