﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Data.Model.Enums
{
    public enum SettingType
    {
        Compulsory,
        Custom,
    }
    public enum CustomSettings
    {
        Title,
        SubTitle,
        Address,
        Longitude,
        Latitude,
        CachingTime
    }
}
