﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Email.Service.EmailMessage
{
    public class EmailMessage
    {
        public List<EmailAddress> ToAddress { get; set; } = new List<EmailAddress>();
        public List<EmailAddress> BccAddress { get; set; } = new List<EmailAddress>();
        public EmailAddress FromAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
