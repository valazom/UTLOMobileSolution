﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Email.Service.EmailService
{
    public interface IEmailService
    {
        Task<EmailMessageResult> SendAsync(EmailMessage.EmailMessage emailMessage);
        EmailMessageResult Send(EmailMessage.EmailMessage emailMessage);
    }
}
