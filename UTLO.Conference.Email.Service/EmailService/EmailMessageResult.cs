﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Email.Service.EmailService
{
    public class EmailMessageResult
    {
        public bool Succeeded { get; internal set; }
        public string ErrorMessage { get; internal set; }
    }
}
