﻿using MimeKit;
using MimeKit.Text;
using MailKit;
using MailKit.Security;
using MailKit.Net.Smtp;
using System;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using UTLO.Email.Service.EmailConfiguration;

namespace UTLO.Email.Service.EmailService
{
    public class EmailService : IEmailService
    {
        private IEmailConfiguration _emailConfiguration;

        private EmailMessageResult _emailSentMessage;

        public EmailService(IEmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
            _emailSentMessage = new EmailMessageResult();
        }
        
        private MimeMessage GetInternetAddress(EmailMessage.EmailMessage emailMessage)
        {
            var message = new MimeMessage();
            message.To.AddRange(emailMessage.ToAddress.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.Add(new MailboxAddress(emailMessage.FromAddress.Name, emailMessage.FromAddress.Address));
            message.Bcc.AddRange(emailMessage.BccAddress.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.Subject = emailMessage.Subject;

            message.Body = new TextPart(TextFormat.Plain)
            {
                Text = emailMessage.Content
            };
            return message;
        }
        /// <summary>
        ///  Send mail synchronously to an smtp server provided in the email configuration
        /// </summary>
        /// <param name="emailMessage"></param>
        /// <returns>EmailSentMessage object as response</returns>
        public EmailMessageResult Send(EmailMessage.EmailMessage emailMessage)
        {
            try
            {
                MimeMessage message = GetInternetAddress(emailMessage);

                using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    emailClient.Connect(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Authenticate(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    emailClient.Send(message);

                    _emailSentMessage.Succeeded = true;

                    emailClient.Disconnect(true);

                    return _emailSentMessage;
                }
            }
            catch (Exception ex)
            {

                _emailSentMessage.Succeeded = false;
                _emailSentMessage.ErrorMessage = ex.Message;
                return _emailSentMessage;
            }
        }
        /// <summary>
        /// Send mail asynchronously to an smtp server provided in the email configuration
        /// </summary>
        /// <param name="emailMessage"></param>
        /// <returns>EmailSentMessage object as response</returns>
        public async Task<EmailMessageResult> SendAsync(EmailMessage.EmailMessage emailMessage)
        {
            try
            {
                MimeMessage message = GetInternetAddress(emailMessage);

                using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    await emailClient.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, false);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.AuthenticateAsync(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    await emailClient.SendAsync(message);

                    _emailSentMessage.Succeeded = true;

                    await emailClient.DisconnectAsync(true);

                    return _emailSentMessage;
                }
            }
            catch (Exception ex)
            {

                _emailSentMessage.Succeeded = false;
                _emailSentMessage.ErrorMessage = ex.Message;
                return _emailSentMessage;
            }
        }
    }
}
