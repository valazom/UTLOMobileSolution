﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Email.Service.EmailConfiguration
{
    public interface IEmailConfiguration
    {
       string SmtpServer { get;}
       int SmtpPort { get; }
       string SmtpUsername { get; }
       string SmtpPassword { get; }
       
       
    }
}
