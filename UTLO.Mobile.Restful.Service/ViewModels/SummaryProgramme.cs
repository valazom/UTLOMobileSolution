﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Mobile.Restful.Service.ViewModels
{
    public class SummaryProgramme
    {
        public int rImageId { get; set; }
        public int lImageId { get; set; }
        public string programmeName { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string DayName { get; set; }
    }
}
