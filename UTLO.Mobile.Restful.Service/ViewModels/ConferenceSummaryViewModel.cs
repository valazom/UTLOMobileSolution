﻿using System;
using System.Collections.Generic;
using System.Text;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.ViewModels
{
    public class ConferenceSummaryViewModel
    {
        public DateTime conferenceDate { get; set; }
        public List<Programme> programmes { get; set; }
    }
}
