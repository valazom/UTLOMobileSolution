﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Mobile.Restful.Service.ViewModels
{
    public class AnswerViewModel
    {
        public int Rating { get; set; }
        public string answer { get; set; }
    }
}
