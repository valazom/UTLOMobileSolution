﻿using System;
using System.Collections.Generic;
using System.Text;
using UTLO.Data.Model.Domain;
using UTLO.Mobile.Restful.Service.ViewModels;

namespace UTLO.Mobile.Restful.Service.ServiceRespository
{
    public class ConferenceSummaryRepository : IConferenceSummaryRepository
    {
        private List<SummaryProgramme> SumprogList { get; set; } =
                           new List<SummaryProgramme>();
        int _rImage;
        int _lImage;

        public void AddItems(int riImageId, int leImageId, ConferenceSummaryViewModel model,
            Programme prog)
        {
            SumprogList.Add(
                new SummaryProgramme
                {
                    rImageId = riImageId,
                    lImageId = leImageId,
                    programmeName = prog.Title,
                    StartTime = prog.StartTime.ToString(),
                    EndTime = prog.EndTime.ToString(),
                    DayName = $"{model.conferenceDate.Date.Day.DisplayWithSuffix()} of " +
                    $"{model.conferenceDate.Date.ToString("MMMM")}"
                }
            );
        }

        public void deleteItems(int rImageId, int cImageId)
        {
            var ritems = SumprogList.Find(a => a.rImageId == rImageId && a.lImageId == cImageId);
            SumprogList.Remove(ritems);
        }

        public List<SummaryProgramme> GetProgList()
        {
            return SumprogList;
        }
    }
}
