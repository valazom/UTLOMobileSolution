﻿using System;
using System.Collections.Generic;
using System.Text;
using UTLO.Data.LegacyModels.Model;
using UTLO.Data.Model.Domain;
using UTLO.Mobile.Restful.Service.ViewModels;

namespace UTLO.Mobile.Restful.Service.ServiceRespository
{
    public interface IConferenceSummaryRepository
    {
        void AddItems(int rImageId, int lImageId, ConferenceSummaryViewModel model, Programme programme);
        void deleteItems(int rImageId, int cImageId);
        List<ViewModels.SummaryProgramme> GetProgList();
    }
}
