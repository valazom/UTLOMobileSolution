﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class ProgrammeService : Service
    {
        public ProgrammeService() : base()
        {

        }
        public async Task<List<Programme>> GetAllProgrammes(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                var programmes = JsonConvert.DeserializeObject<List<Programme>>(content);
                return programmes;
            }
            return new List<Programme>();
        }

        public async Task<Programme> GetProgrammeById(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                var programme = JsonConvert.DeserializeObject<Programme>(content);
                return programme;
            }
            return new Programme();
        }
    }
}
