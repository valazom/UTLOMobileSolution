﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class AnnouncementService : Service
    {
        public List<Announcement> announcements { get; set; }
        public List<Topic> topics { get; set; }
        public Topic topic { get; set; }
        public Announcement sannouncement { get; set; }
        public async Task GetAllAnnouncement(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                announcements = JsonConvert.DeserializeObject<List<Announcement>>(content);
            }
        }

        public async Task GetAllTopics(int id, string endpoint)
        {

            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                topics = JsonConvert.DeserializeObject<List<Topic>>(content);
            }
        }

        public async Task GetAnnouncementById(int id, string endpoint)
        {

            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                sannouncement = JsonConvert.DeserializeObject<Announcement>(content);
            }
        }

        public async Task GetTopicAsync(int id, string endpoint)
        {

            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                topic = JsonConvert.DeserializeObject<Topic>(content);
            }
        }
    }
}
