﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class SuiteService : Service
    {
        public List<Suites> Suites { get; set; }
        public Suites Suite { get; set; }
        public async Task GetAllSuitesAsync(string endpoint)
        {
            //var suites = new List<Suites>();
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                Suites = JsonConvert.DeserializeObject<List<Suites>>(content);

            }

        }
        public async Task GetSuitesByProgrammeIdAsync(int id, string endpoint)
        {
            //var suites = new List<Suites>();
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                Suites = JsonConvert.DeserializeObject<List<Suites>>(content);
            }

        }
        public async Task GetSuitesByIdAsync(int id, string endpoint)
        {
            //var suites = new Suites();
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                Suite = JsonConvert.DeserializeObject<Suites>(content);

            }

        }
    }
}
