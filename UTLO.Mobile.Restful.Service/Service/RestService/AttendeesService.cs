﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class AttendeesService : Service
    {
        public List<Attendees> attendees { get; set; } = new List<Attendees>();

        public List<Attendees> keynotespeakers { get; set; } = new List<Attendees>();
        public Attendees attendee { get; set; } = new Attendees();

        public byte[] speakerImage { get; private set; }
        public string content { get; set; }
        public AttendeesService() : base()
        {

        }

        public async Task GetAllAttendees(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                content = await DownloadData(endpoint);
                attendees = JsonConvert.DeserializeObject<List<Attendees>>(content);

            }

        }
        public async Task GetAllKeynoteSpeakers(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                content = await DownloadData(endpoint);
                keynotespeakers = JsonConvert.DeserializeObject<List<Attendees>>(content);

            }

        }

        public async Task GetAttendeeById(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                content = await DownloadData(endpoint);
                attendee = JsonConvert.DeserializeObject<Attendees>(content);

            }

        }
        public async Task GetKeynoteSpeakerImage(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                speakerImage = await DownloadImages(endpoint);
            }
        }
    }
}
