﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class EvaluationService : Service
    {
        public List<EvaluationQuestions> evaluationQuestions { get; set; }
        public EvaluationQuestions evaluationQuestion { get; set; }
        public ResponseMessage response { get; set; } = ResponseMessage.Failure;
        public async Task GetAllEvaluationsAsync(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                evaluationQuestions = JsonConvert.DeserializeObject<List<EvaluationQuestions>>(content);
            }
        }

        public async Task GetEvaluationAsync(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                evaluationQuestion = JsonConvert.DeserializeObject<EvaluationQuestions>(content);
            }
        }

        public async Task PostEvaluationAnswer(string endpoint, List<Answers> answer)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var serializedstring = JsonConvert.SerializeObject(answer);
                var content = new StringContent(serializedstring, Encoding.UTF8, "application/json");
                response = await PostData(endpoint, content);

            }
        }
    }
}
