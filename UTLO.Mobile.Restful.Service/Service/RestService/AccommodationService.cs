﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class AccommodationService : Service
    {
        public List<Accommodation> accommodations { get; set; }
        public Accommodation accommodation { get; set; }
        public AccommodationService() : base()
        {

        }
        public async Task GetAccommodationByIdAsync(int id, string endpoint)
        {

            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                accommodation = JsonConvert.DeserializeObject<Accommodation>(content);
            }
        }

        public async Task GetAllAccommodationAsync(string endpoint)
        {

            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                accommodations = JsonConvert.DeserializeObject<List<Accommodation>>(content);
            }
        }
    }
}
