﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class SettingsService :Service
    {
        public List<Settings> Settings { get; set; }

        public SettingsService() : base()
        {

        }

        public async Task GetAllSettings(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var data = await DownloadData(endpoint);

                Settings = JsonConvert.DeserializeObject<List<Settings>>(data);

            }
        }
    }
}
