﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class PresentationService : Service
    {
        public List<Presentations> presentations { get; set; }
        public Presentations presentation { get; set; }
        public PresentationService() : base()
        {

        }
        public async Task<List<Presentations>> GetAllPresentation(string endpont)
        {
            if (!string.IsNullOrEmpty(endpont))
            {
                var content = await DownloadData(endpont);
                presentations = JsonConvert.DeserializeObject<List<Presentations>>(content);
                return presentations;
            }
            return new List<Presentations>();
        }
        public async Task GetAllPresentation(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                presentations = JsonConvert.DeserializeObject<List<Presentations>>(content);
            }

        }
        public async Task GetPresentation(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                presentation = JsonConvert.DeserializeObject<Presentations>(content);
            }

        }
    }
}
