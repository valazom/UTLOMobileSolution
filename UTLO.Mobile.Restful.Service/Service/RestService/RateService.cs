﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.ViewModels;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class RateService : Service
    {
        public ResponseMessage response { get; set; } = ResponseMessage.Failure;
        public RateService() : base()
        {

        }
        public async Task RatePresentation(string endpoint, int id, RatingViewModel rating)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var serializedrating = JsonConvert.SerializeObject(rating);
                var content = new StringContent(serializedrating, Encoding.UTF8, "application/json");
                response = await PostData(endpoint, content);
            }

        }
    }
}
