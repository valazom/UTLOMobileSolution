﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class AbstractsService : Service
    {
        public List<Abstracts> mabstracts { get; set; }
        public Abstracts mabstract { get; set; }
        public List<Attendees> authors { get; set; }
        public async Task GetAbstracts(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                mabstract = JsonConvert.DeserializeObject<Abstracts>(content);
            }

        }

        public async Task GetAllAbstracts(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                mabstracts = JsonConvert.DeserializeObject<List<Abstracts>>(content);

            }
        }

        public async Task GetAuthorsById(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                var authors = JsonConvert.DeserializeObject<List<Attendees>>(content);
            }
        }
    }
}
