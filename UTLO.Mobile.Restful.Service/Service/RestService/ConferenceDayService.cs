﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;
using UTLO.Mobile.Restful.Service.ViewModels;

namespace UTLO.Mobile.Restful.Service.Service.RestService
{
    public class ConferenceDayService : Service
    {
        public List<ConferenceDay> ConferenceDays { get; set; }
        public ConferenceDay ConferenceDay { get; set; }
        public List<ConferenceSummaryViewModel> ConferenceDaySummary { get; set; }
        public ConferenceDayService() : base()
        {

        }
        public async Task GetAllConferenceDay(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                ConferenceDays = JsonConvert.DeserializeObject<List<ConferenceDay>>(content);
            }

        }
        public async Task GetConferenceDaySummary(string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                var content = await DownloadData(endpoint);
                ConferenceDaySummary = JsonConvert.DeserializeObject<List<ConferenceSummaryViewModel>>(content);
            }

        }
        public async Task GetConferenceDayById(int id, string endpoint)
        {
            if (!string.IsNullOrEmpty(endpoint))
            {
                endpoint = string.Format(endpoint, id);
                var content = await DownloadData(endpoint);
                ConferenceDay = JsonConvert.DeserializeObject<ConferenceDay>(content);
            }

        }
    }
}
