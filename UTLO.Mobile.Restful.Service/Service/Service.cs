﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Mobile.Restful.Service.Service
{
    public class Service
    {
        private HttpClient _httpClient;
        
        public Service()
        {
            _httpClient = new HttpClient();


        }
        
        protected async Task<string> DownloadData(string endpoint)
        {
            var uri = new Uri(String.Format(endpoint, string.Empty));
            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
            return string.Empty;
        }
        protected async Task<ResponseMessage> PostData(string endpoint, StringContent content)
        {
            var uri = new Uri(endpoint);
            var resonpse = await _httpClient.PostAsync(uri, content);
            if (resonpse.IsSuccessStatusCode)
            {
                return ResponseMessage.Successful;
            }
            return ResponseMessage.Failure;
        }
        protected async Task<byte[]> DownloadImages(string path)
        {
            using (var client = new HttpClient())
            {
                var image = await client.GetByteArrayAsync(path);
                return image;
            }
        }
    }
}
