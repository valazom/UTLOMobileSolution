﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UTLO.Mobile.Restful.Service
{
    public static class Constants
    {
        // dev db
        //public static string ConferenceDay = "http://utlowebapi.azurewebsites.net/api/days";
        ////public static string Attendees = "http://ukznwebapi.cmisservices.com/api/attendees";
        //public static string Attendees = "http://utlowebapi.azurewebsites.net/api/attendees";
        //public static string ProgrammesConferenceDays = "http://utlowebapi.azurewebsites.net/api/days/programmes";
        //public static string SuitesByProgrammeId = "http://utlowebapi.azurewebsites.net/api/suite/{0}";
        //public static string PresentationBySuiteId = "http://utlowebapi.azurewebsites.net/api/presentation/{0}";
        //public static string RatePresentation = "http://utlowebapi.azurewebsites.net/api/presentation/{0}/rate";
        ////public static string Abstracts = "http://ukznwebapi.cmisservices.com/api/abstracts";
        //public static string Abstracts = "http://utlowebapi.azurewebsites.net/api/abstracts";
        //public static string AuthorsAbstracts = "http://utlowebapi.azurewebsites.net/api/abstracts/authors/{0}";
        //public static string Accommodation = "http://utlowebapi.azurewebsites.net/api/accommodation";
        //public static string EvaluationQuestions = "http://utlowebapi.azurewebsites.net/api/evaluation";
        ////public static string EvaluationQuestions = "http://ukznwebapi.cmisservices.com/api/evaluation";
        //public static string EvaluationAnswers = "http://utlowebapi.azurewebsites.net/api/evaluation/answers";
        //public static string Announcements = "http://utlowebapi.azurewebsites.net/api/announcement";
        //public static string ConferenceDaySummary = "http://utlowebapi.azurewebsites.net/api/days/summary";
        //public static string KeynoteSpeakers = "http://utlowebapi.azurewebsites.net/api/attendees/keynotespeakers";
        //public static string KeynoteSpeakersImageBaseUrl = "http://conferencewebapp.azurewebsites.net/Uploads/Keynotespeakers/";

        //production db
        public static string Settings = "http://ukznwebapi.cmisservices.com/api/settings";
        public static string ConferenceDay = "http://ukznwebapi.cmisservices.com/api/days";
        public static string Attendees = "http://ukznwebapi.cmisservices.com/api/attendees";
        public static string ProgrammesConferenceDays = "http://ukznwebapi.cmisservices.com/api/days/programmes";
        public static string SuitesByProgrammeId = "http://ukznwebapi.cmisservices.com/api/suite/{0}";
        public static string PresentationBySuiteId = "http://ukznwebapi.cmisservices.com/api/presentation/{0}";
        public static string RatePresentation = "http://ukznwebapi.cmisservices.com/api/presentation/{0}/rate";
        public static string Abstracts = "http://ukznwebapi.cmisservices.com/api/abstracts";
        public static string AuthorsAbstracts = "http://ukznwebapi.cmisservices.com/api/abstracts/authors/{0}";
        public static string Accommodation = "http://ukznwebapi.cmisservices.com/api/accommodation";
        public static string EvaluationQuestions = "http://ukznwebapi.cmisservices.com/api/evaluation";
        public static string EvaluationAnswers = "http://ukznwebapi.cmisservices.com/api/evaluation/answers";
        public static string Announcements = "http://ukznwebapi.cmisservices.com/api/announcement";
        public static string ConferenceDaySummary = "http://ukznwebapi.cmisservices.com/api/days/summary";
        public static string KeynoteSpeakers = "http://ukznwebapi.cmisservices.com/api/attendees/keynotespeakers";
        public static string KeynoteSpeakersImageBaseUrl = "http://www.cmisservices.com/Uploads/Keynotespeakers/";
    }
    public static class IntegerExtensions
    {
        public static string DisplayWithSuffix(this int num)
        {
            if (num.ToString().EndsWith("11")) return num.ToString() + "th";
            if (num.ToString().EndsWith("12")) return num.ToString() + "th";
            if (num.ToString().EndsWith("13")) return num.ToString() + "th";
            if (num.ToString().EndsWith("1")) return num.ToString() + "st";
            if (num.ToString().EndsWith("2")) return num.ToString() + "nd";
            if (num.ToString().EndsWith("3")) return num.ToString() + "rd";
            return num.ToString() + "th";
        }
    }
}
