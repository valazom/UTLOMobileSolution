﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Model.Generic.Repository
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal DbContext _mContext;
        internal DbSet<TEntity> _mDbSet;

        public GenericRepository(DbContext context)
        {
            _mContext = context;
            _mDbSet = context.Set<TEntity>();
        }
        public DbContext GetDbContext { get { return _mContext; }}
        /// <summary>
        /// Gets all objects of Type TEntity
        /// </summary>
        /// <returns>The function returns an enumrable list</returns>
        public async Task<IEnumerable<TEntity>> All()
        {
            return await _mDbSet.AsNoTracking().ToListAsync();
        }
        
        /// <summary>
        /// Get all navigation properties pertaining to the TEntity
        /// </summary>
        /// <param name="includeproperties"> Expression to select the list navigation properties</param>
        /// <returns>The function returns an enumrable list</returns>
        public async Task<IEnumerable<TEntity>> AllIncludes
            (params Expression<Func<TEntity,object>>[] includeproperties)
        {
            return await GetAllIncluding(includeproperties).ToListAsync();
        }
        /// <summary>
        /// Include all the navigation properties by aggregating them
        /// </summary>
        /// <param name="includeproperties">An array holding the list of navigation properties</param>
        /// <returns>The function returns an enumrable list</returns>
        private IQueryable<TEntity> GetAllIncluding
            (Expression<Func<TEntity, object>>[] includeproperties)
        {
            IQueryable<TEntity> query = _mDbSet.AsNoTracking();

            return includeproperties.Aggregate
                (query, (current, includeproperty) => current.Include(includeproperty));
        }
        /// <summary>
        /// Get list of objects by list of navigation properties
        /// </summary>
        /// <param name="predicate">Condition specifying objects returned</param>
        /// <param name="includeproperties">List of navigation properties</param>
        /// <returns>The function returns an enumrable list</returns>
        public async Task<IEnumerable<TEntity>> FindByInclude(Expression<Func<TEntity,bool>> predicate,
                    params Expression<Func<TEntity,object>>[] includeproperties)
        {
            var query = GetAllIncluding(includeproperties);
            var results = await query.Where(predicate).ToListAsync();
            return results ;
        }
        /// <summary>
        /// Find objects in Db by condition
        /// </summary>
        /// <param name="predicate">Condition</param>
        /// <returns>The function returns an enumrable list</returns>
        public async Task<IEnumerable<TEntity>> FindBy(Expression<Func<TEntity,bool>> predicate)
        {
            var results = await _mDbSet.AsNoTracking().Where(predicate).ToListAsync();
            return results;
        }
        /// <summary>
        /// Find the entity using primary key property
        /// </summary>
        /// <param name="id">primary key</param>
        /// <returns>The function returns an enumrable list</returns>
        public async Task<TEntity> FinDByKey(int id)
        {
            Expression<Func<TEntity, bool>> lambda = Utilities.BuildLamdaByFindKey<TEntity>(id);
            return await _mDbSet.SingleOrDefaultAsync(lambda);
        }

        public async Task Insert(TEntity entity)
        {
            _mDbSet.Add(entity);
            await _mContext.SaveChangesAsync();
        }
        public async Task InsertRange(IEnumerable<TEntity> entities)
        {
            _mDbSet.AddRange(entities);
            await _mContext.SaveChangesAsync();
        }
        public async Task Update(TEntity entity)
        {
            _mDbSet.Attach(entity);
            _mContext.Entry(entity).State = EntityState.Modified;
            await _mContext.SaveChangesAsync();
        }
        public async Task Delete(TEntity entity)
        {
            _mDbSet.Attach(entity);
            _mDbSet.Remove(entity);
            await _mContext.SaveChangesAsync();
        }
        public async Task DeleteRange(IEnumerable<TEntity> entities)
        {
            _mDbSet.RemoveRange(entities);
            await _mContext.SaveChangesAsync();
        }
    }
}
