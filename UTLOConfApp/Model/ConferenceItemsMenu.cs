﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace UTLOConfApp.Model
{
    public  class ConferenceItemsMenu
    {
        public int ImageId { get; set; }
        public string Title { get; set; }

        

        


        int regImage = Resource.Drawable.filetype_reg_icon, calImage = Resource.Drawable.event_icon;
        public static List<ConferenceItemsMenu> GetData()
        {
            //int[] rimage = {Resource.Drawable.Calendar_icon,Resource.Drawable.Groups_Meeting_Dark_icon,
            //Resource.Drawable.BookofAbstractsicon,Resource.Drawable.Hot_Home_icon,Resource.Drawable.Location_icon,
            //Resource.Drawable.Evaluationicon,Resource.Drawable.Speaker_icon,Resource.Drawable.Website_icon,Resource.Drawable.splash_logo,
            //Resource.Drawable.ukzn_Logo};

            //string[] desc_title = { "Programme", "Presenters", "Abstracts", "Accomodation", "Location",
            //                    "Evaluation","Announcements","HEC Website","UTLO Website","UKZN Website" };

            var items = new List<ConferenceItemsMenu>() {
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Calendar_icon,
                    Title = "Programme"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Groups_Meeting_Dark_icon,
                    Title = "Presenters"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.BookofAbstractsicon,
                    Title = "Abstracts"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Hot_Home_icon,
                    Title = "Accomodation"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Location_icon,
                    Title = "Location"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Evaluationicon,
                    Title = "Evaluation"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Speaker_icon,
                    Title = "Announcements"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.Website_icon,
                    Title = "HEC Website"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.splash_logo,
                    Title = "UTLO Website"
                },
                new ConferenceItemsMenu
                {
                    ImageId = Resource.Drawable.ukzn_Logo,
                    Title = "UKZN Website"
                }
            };
            //if (rimage.Length == desc_title.Length)
            //{
            //    for (int i = 0; i < rimage.Length; i++)
            //    {
            //        items.Add(
            //                new ConferenceItemsMenu
            //                {
            //                    ImageId = rimage[i],
            //                    Title = desc_title[i]
            //                }
            //            );
            //    }
            //}

            return items;
        }
    }
}