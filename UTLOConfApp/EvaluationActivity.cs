﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using UTLOConfApp.Adapter;
using UTLO.Mobile.Restful.Service.Service.RestService;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLO.Data.Model.Domain;
using UTLOConfApp.Utils;
using Java.Net;
using Android.Support.V7.Widget;

namespace UTLOConfApp
{
    [Activity(Label = "EvaluationActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class EvaluationActivity : AppCompatActivity
    {
        
        EvaluationService evaluationService;
        TextView conf_name;
        Button PostButton;
        TextView HeaderTitle;
        List<Answers> answers;
        List<EvaluationQuestions> evaluationQuestions;
        CacheOperation<EvaluationQuestions> cache;
        CacheOperation<bool> cacheEvaluationCompleted;
        Android.Support.V7.App.AlertDialog.Builder Dialog;
        RecyclerView recyclerview;
        EvaluationRecyclerViewAdapter evaluationRecyclerViewAdapter;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EvaluationActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            SetUpToolbar();

            SetUpControls();

            await LoadDataOnline();

        }

        private void SetUpControls()
        {
            Dialog = new Android.Support.V7.App.AlertDialog.Builder(this);
            cacheEvaluationCompleted = new CacheOperation<bool>();
        }

        private async Task LoadDataOnline()
        {
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await LoadEvaluation();
            await LoadPostButton();
            progressdialog.Hide();
            progressdialog.Dismiss();
        }

        private void SetUpToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            HeaderTitle = FindViewById<TextView>(Resource.Id.conf_title);
            HeaderTitle.Text = "Evaluation";
        }

        private async Task LoadPostButton()
        {
            PostButton = FindViewById<Button>(Resource.Id.PostButton);
            PostButton.Click += PostButton_Click;
            var isInCache = await IsEvaluationCompleted(cacheEvaluationCompleted);
            if (isInCache)
            {
                PostButton.Visibility = ViewStates.Invisible;
                Dialog
                            .SetPositiveButton("Yes", (msender, args) =>
                            {
                                Finish();
                            })
                            .SetMessage("You have already completed this Evaluation. It cannot be completed more than once")
                            .SetTitle("Evaluation already completed")
                            .Show();
            }

        }
        private async Task<bool> IsEvaluationCompleted(CacheOperation<bool> cacheEvaluationCompleted)
        {
            try
            {
                // check in cache if Evaluation has been completed
                var isInCache = await cacheEvaluationCompleted.GetObjectOther(ConferenceCacheKeys.EvaluationCompleted.ToString());
                return isInCache.Item2; ;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        private async void PostButton_Click(object sender, EventArgs e)
        {
            try
            {

                var isonline = await Task.Run(() => isOnline());

                if (isonline)
                {
                    ProgressDialog progressdialog = new ProgressDialog(this);
                    progressdialog.SetCancelable(false);
                    progressdialog.Indeterminate = true;
                    progressdialog.SetMessage("Posting...");
                    progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
                    progressdialog.Show();
                    // post evaluation answers to via web api
                    await Task.Run(() => evaluationService.
                             PostEvaluationAnswer(Constants.EvaluationAnswers, evaluationRecyclerViewAdapter.GetAnswers));
                    progressdialog.Hide();
                    progressdialog.Dismiss();

                    if (evaluationService.response == ResponseMessage.Successful)
                    {
                        //Once post has been successful, build notification message
                        string message = "Thank you for co-operation in completing this evaluation";
                        string Title = (string)ConferenceSettings.csettings["Title"] + " Evaluation";
                        ConferenceUtils.BuildLocalNotification(message, Title, this, (int)NotificationCode.Evaluation,
                            Utils.Enum.NotificationMessageType.EvaluationNotificationMessage);
                        //insert into cache 
                        // This used to ensure that evakuation is filled only once on mobile device
                        await cacheEvaluationCompleted.InsertObject(ConferenceCacheKeys.EvaluationCompleted.ToString(), true);
                        Dialog
                            .SetPositiveButton("Yes", (msender, args) =>
                            {
                                Finish();
                            })
                            .SetMessage(message)
                            .SetTitle(Title)
                            .Show();
                    }
                    else
                    {
                        Dialog
                       .SetPositiveButton("Yes", (msender, args) =>
                       {

                       })
                       .SetMessage("Your answers was not posted.Please check your answers")
                       .SetTitle("Response Error")
                       .Show();
                    }
                }
                else
                {
                    Dialog
                            .SetPositiveButton("Yes", (msender, args) =>
                            {

                            })
                            .SetNegativeButton("No", (msender, args) =>
                            {

                            })
                            .SetMessage("No network connection. Your data cannot be posted")
                            .SetTitle("Network Error")
                            .Show();
                }

            }
            catch (Exception ex)
            {

                Dialog
                    .SetPositiveButton("Yes", (msender, args) =>
                    {
                        Finish();
                    })
                    .SetMessage("Could not post data. Check your internet connection")
                    .SetTitle("Application Error")
                    .Show();
            }
            /// TODO: place a call to the web api;
        }

        private async Task LoadEvaluation()
        {
            try
            {
                cache = new CacheOperation<EvaluationQuestions>();

                evaluationService = new EvaluationService();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceEvaluation);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => isOnline());
                    if (isonline)
                    {

                        await Task.Run(() => evaluationService.GetAllEvaluationsAsync(Constants.EvaluationQuestions));
                        evaluationQuestions = evaluationService.evaluationQuestions;
                        CacheOperation<EvaluationQuestions>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferenceEvaluation, evaluationQuestions);
                    }
                    else
                    {
                        Dialog
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection. You won't be able to get data")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    evaluationQuestions = cacheData.Item1;
                }
                if (evaluationQuestions != null)
                {
                    AttachEvaluationtoAnswers();
                    

                    recyclerview = FindViewById<RecyclerView>(Resource.Id.recyclerview);
                    evaluationRecyclerViewAdapter = new EvaluationRecyclerViewAdapter(this, evaluationQuestions, answers);
                    recyclerview.SetAdapter(evaluationRecyclerViewAdapter);

                    var linearLayoutManager = new LinearLayoutManager(this);
                    linearLayoutManager.Orientation = LinearLayoutManager.Vertical;
                    recyclerview.SetLayoutManager(linearLayoutManager);


                }
            }
            catch (Exception)
            {

                Dialog
                   .SetPositiveButton("Yes", (sender, args) =>
                   {
                       Finish();
                   })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("Could not load data")
                                .SetTitle("Application Error")
                                .Show();
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Dialog.Dispose();
        }

        private void AttachEvaluationtoAnswers()
        {
            answers = new List<Answers>();
            foreach (var question in evaluationQuestions)
            {
                var answer = new Answers();
                answer.EvaluationQuestionId = question.EvaluationQuestionsId;
                answers.Add(answer);
            }
        }
        private void EvaluationListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

        }

        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }


    }
}