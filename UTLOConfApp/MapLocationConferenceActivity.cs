using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using UTLOConfApp.Utils;
using System.Globalization;

namespace UTLOConfApp
{
    [Activity(Label = "MapLocationConferenceActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class MapLocationConferenceActivity : AppCompatActivity,IOnMapReadyCallback
    {
        TextView conf_title;
        TextView conf_name;
        LatLng conferenceLocation;
        FrameLayout mapFrameLayout;
        SupportMapFragment mapFragments;
        GoogleMap googleMap;
        GoogleMap Map { get; set; }
        event EventHandler MapReady;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MapLocationConferenceActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            var format = new NumberFormatInfo();
            format.NegativeSign = "-";
            format.NumberDecimalSeparator = ".";
            //load from conference settings
            var Latitude = double.Parse(ConferenceSettings.csettings["Latitude"],format);
            var Longitude = double.Parse(ConferenceSettings.csettings["Longitude"],format);

            conferenceLocation = new LatLng(Latitude, Longitude);

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = ConferenceSettings.csettings["Title"];

            conf_title = FindViewById<TextView>(Resource.Id.conf_title);
            conf_title.Text = "Map";
            mapFrameLayout = FindViewById<FrameLayout>(Resource.Id.mapFrameLayout);

            CreateFragments();
            UpdateMapView();

            
        }

        private void CreateFragments()
        {
            mapFragments = SupportFragmentManager.FindFragmentByTag("map") as SupportMapFragment;
            if(mapFragments == null)
            {
                var googleOptions = new GoogleMapOptions()
                                .InvokeMapType(GoogleMap.MapTypeNormal)
                                .InvokeZoomControlsEnabled(true)
                                .InvokeCompassEnabled(true);

                Android.Support.V4.App.FragmentTransaction fragTransaction = 
                                        SupportFragmentManager.BeginTransaction();
                mapFragments = SupportMapFragment.NewInstance(googleOptions);
                fragTransaction.Add(Resource.Id.mapFrameLayout, mapFragments, "map");
                fragTransaction.Commit();
                //mapFragments.GetMapAsync(this);
            }
        }

        private void UpdateMapView()
        {
            //var mapReadyCallBack = new LocalMapReady();
            MapReady += (sender, args) =>
            {
                googleMap = this.Map;
                if (googleMap != null)
                {
                    MarkerOptions markOptions = new MarkerOptions();
                    markOptions.SetPosition(conferenceLocation);
                    var Address = ConferenceSettings.csettings["Address"];
                    markOptions.SetTitle(Address);
                    googleMap.AddMarker(markOptions);

                    CameraUpdate cameraUpdate = CameraUpdateFactory.NewLatLngZoom(conferenceLocation, 15);
                    googleMap.MoveCamera(cameraUpdate);
                }
                
            };
            mapFragments.GetMapAsync(this);
        }

        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }

        public void OnMapReady(GoogleMap googleMap)
        {
            Map = googleMap;
            MapReady?.Invoke(this, EventArgs.Empty); 
        }

        private class LocalMapReady : Java.Lang.Object, IOnMapReadyCallback
        {
            public LocalMapReady()
            {
            }
            public GoogleMap Map { get; set; }
            public event EventHandler MapReady;
            public void OnMapReady(GoogleMap googleMap)
            {
                Map = googleMap;
                MapReady?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}