﻿using System;
using System.Collections.Generic;

using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading.Tasks;
using UTLOConfApp.Adapter;
using UTLO.Mobile.Restful.Service.Service.RestService;
using Android.Net;
using UTLO.Mobile.Restful.Service;
using UTLOConfApp.Fragments;
using UTLOConfApp.Utils;
using Java.Net;
using Android.Support.V4.View;
using Android.Support.V7.Widget;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp
{
    [Activity(Label = "BookofAbstractsActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class BookofAbstractsActivity : AppCompatActivity
    {
        Android.Support.V7.Widget.SearchView _searchView;
        TextView conf_name;
        AbstractsService abstractsService;
        List<Attendees> Authors;
        TextView HeaderTitle;
        CacheOperation<Abstracts> cache;
        List<Abstracts> abstracts;
        BookofAbstractsRecyclerAdapter bookofAbstractsRecyclerAdapter;
        RecyclerView recyclerView;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BookofAbstractActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetUpToolbar();
            await LoadDataOnline();
            // Create your application here
        }

        private async Task LoadDataOnline()
        {
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await LoadBookOfAbstracts();
            progressdialog.Hide();
            progressdialog.Dismiss();
        }

        private void SetUpToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            HeaderTitle = FindViewById<TextView>(Resource.Id.conf_title);
            HeaderTitle.Text = "Book of Abstracts";
        }

        private async Task LoadBookOfAbstracts()
        {
            try
            {
                cache = new CacheOperation<Abstracts>();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceAbstracts);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => isOnline());

                    if (isonline)
                    {
                        abstractsService = new AbstractsService();
                        await Task.Run(() => abstractsService.GetAllAbstracts(Constants.Abstracts));
                        abstracts = abstractsService.mabstracts;
                        // set caching time for the data 
                        CacheOperation<Abstracts>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferenceAbstracts, abstracts);
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection. You won't be able to get data")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    abstracts = cacheData.Item1;
                }
                if (abstracts != null)
                {
                    
                    recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                    bookofAbstractsRecyclerAdapter = new BookofAbstractsRecyclerAdapter(this, abstracts);
                    bookofAbstractsRecyclerAdapter.ItemClick += BookofAbstractsListView_ItemClick;
                    recyclerView.SetAdapter(bookofAbstractsRecyclerAdapter);

                    var linearLayoutManager = new LinearLayoutManager(this);
                    linearLayoutManager.Orientation = LinearLayoutManager.Vertical;
                    recyclerView.SetLayoutManager(linearLayoutManager);

                }
            }
            catch (Exception ex)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("Could not load data")
                                .SetTitle("Application Error")
                                .Show();
            }
        }

        private void BookofAbstractsListView_ItemClick(object sender, BookofAbstractsRecyclerAdapterClickEventArgs e)
        {
            var item = bookofAbstractsRecyclerAdapter.GetItem(e.Position);
            var AbstractsDialog = new AbstractsDialogFragment(item);
            //Display the abstracts with body, title, theme and Authors via support fragment manager
            AbstractsDialog.Show(this.SupportFragmentManager, "Abstract");
        }

        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        protected override void OnDestroy()
        {
            //abstracts.Clear() ;
            base.OnDestroy();
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            bookofAbstractsRecyclerAdapter = new BookofAbstractsRecyclerAdapter(this, abstracts);

            MenuInflater.Inflate(Resource.Menu.Attendee, menu);

            var item = menu.FindItem(Resource.Id.action_search);

            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();

            _searchView.QueryTextChange += (s, e) => bookofAbstractsRecyclerAdapter.Filter.InvokeFilter(e.NewText);

            _searchView.QueryTextSubmit += (s, e) =>
            {
                Toast.MakeText(this, "Searched for: " + e.Query, ToastLength.Short).Show();
                e.Handled = true;
                bookofAbstractsRecyclerAdapter.NotifyDataSetChanged();
            };

            MenuItemCompat.SetOnActionExpandListener(item, new SearchViewExpandListener(bookofAbstractsRecyclerAdapter));

            return true;
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}