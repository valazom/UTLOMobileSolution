﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Utils
{
    public static class ConferenceSettings
    {
        public static Dictionary<string, string> csettings { get; private set; } 

        public static void  SetupConferenceSettings(List<Settings> settings)
        {
            csettings = new Dictionary<string, string>();
            foreach (var setting in settings)
            {
                if (setting.SettingValue != null)
                {
                    csettings.Add(setting.SettingName, setting.SettingValue); 
                }
                else
                {
                    throw new NullReferenceException($"Settings Name: {setting.SettingName} has an empty value");
                }
            }
        }
    }
}