﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace UTLOConfApp.Utils
{
    public enum ConferenceCacheKeys
    {
        ConferenceSettings,
        ConferenceConferenceDays,
        ConferenceDaySummary,
        ConferenceProgramme,
        ConferenceSuites,
        ConferenceAbstracts,
        ConferencePresention,
        ConferenceRate,
        ConferenceAttendees,
        ConferenceEvaluation,
        ConferenceAnnouncement,
        ConferenceAccommodation,
        ConferenceTopics,
        ConferenceKeyNoteSpeakers,
        keynotespeakerImage,
        EvaluationCompleted,
        RatingCompleted
    }
    public enum NotificationCode
    {
        Evaluation = 2001,
        PresentationRating,
        RemoteNotification,
        PresentationNotification
    }

    public enum CacheMode
    {
        temporary,
        permanent
    }
}