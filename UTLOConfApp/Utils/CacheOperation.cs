﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Akavache;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace UTLOConfApp.Utils
{
    public class CacheOperation<T>
    {
        public static TimeSpan CachingTime { get; set; }

        
        public CacheOperation()
        {
            InitAppName();
        }
        public Tuple<List<T>, bool> cacheData { get; set; }

        public Tuple<T, bool> otherCacheData { get; set; }

        private void InitAppName()
        {
            BlobCache.ApplicationName = "UKZN Conference App";
        }
        public async Task InsertObject(ConferenceCacheKeys key,
            List<T> cacheData, CacheMode storageMode = CacheMode.temporary)

        {
            if (storageMode == CacheMode.permanent)
            {
                await BlobCache.LocalMachine // cache data for only one hour
                    .InsertObject(key.ToString(), cacheData, CachingTime=new TimeSpan(5,0,0));
            }
            else
            {
                await BlobCache.LocalMachine // cache data for only one hour
                    .InsertObject(key.ToString(), cacheData, CachingTime=new TimeSpan(1,0,0));
            }
        }
        public async Task<Tuple<List<T>, bool>> GetObject(ConferenceCacheKeys key)
        {
            try
            {
                var item1 = await BlobCache.LocalMachine
                           .GetObject<List<T>>(key.ToString());
                var item2 = item1 != null;
                cacheData = new Tuple<List<T>, bool>(item1, item2);

            }
            catch (Exception ex)
            {

                return new Tuple<List<T>, bool>(null, false);
            }
            return cacheData;
        }
        public async Task InsertObject(string key,
            List<T> cacheData, CacheMode storageMode = CacheMode.temporary)

        {
            if (storageMode == CacheMode.permanent)
            {
                await BlobCache.LocalMachine // cache data for only one hour
                    .InsertObject(key.ToString(), cacheData, CachingTime =new TimeSpan(5,0,0));
            }
            else
            {
                await BlobCache.LocalMachine // cache data for only one hour
                    .InsertObject(key.ToString(), cacheData, CachingTime = new TimeSpan(1,0,0));
            }
        }
        public async Task<Tuple<List<T>, bool>> GetObject(string key)
        {
            try
            {
                var item1 = await BlobCache.LocalMachine
                           .GetObject<List<T>>(key.ToString());
                var item2 = item1 != null;
                cacheData = new Tuple<List<T>, bool>(item1, item2);

            }
            catch (Exception ex)
            {

                return new Tuple<List<T>, bool>(null, false);
            }
            return cacheData;
        }
        public async Task InsertObject(string key,
           T cacheData)

        {
            await BlobCache.LocalMachine // cache data for only one hour
                .InsertObject(key, cacheData);
        }

        public async Task<Tuple<T, bool>> GetObjectOther(string key)
        {
            try
            {
                var item1 = await BlobCache.LocalMachine
                           .GetObject<T>(key);
                var item2 = item1 != null;
                otherCacheData = new Tuple<T, bool>(item1, item2);

            }
            catch (Exception ex)
            {

                return new Tuple<T, bool>(default(T), false);
            }

            return otherCacheData;
        }

    }
}