using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Utils
{
    public class ViewPagerOnClickListener : Java.Lang.Object, View.IOnClickListener
    {
        Activity context;
        int position;
        List<Attendees> Speakers;
        public ViewPagerOnClickListener(Activity context, int currentposition, 
            List<Attendees> speakers)
        {
            this.context = context;
            position = currentposition;
            Speakers = speakers;
        }
        public void OnClick(View v)
        {
            var Speaker = Speakers[position];
            Intent BiographyIntent = new Intent(context, typeof(BiographyActivity));
            BiographyIntent.PutExtra("SpeakerId", Speaker.AttendeesId);
            BiographyIntent.PutExtra("Biography", Speaker.Biography);
            context.StartActivity(BiographyIntent);
        }
    }
}