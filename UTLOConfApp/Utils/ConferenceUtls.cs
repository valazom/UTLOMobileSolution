using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Content.Res;
using Android.Support.V4.View;
using Android.Net;
using Android.Support.V4.App;
using System.Threading.Tasks;
using Android.Graphics;
using UTLOConfApp.Utils.Enum;

namespace UTLOConfApp.Utils
{
    public class ConferenceUtils
    {
        private int screenSize;
        private static int ScreenSize { get; set; }

        /// <summary>
        /// Sets the textsize for tabs on different screens 
        /// </summary>
        /// <param name="tabLayout">reference of the TabLayout Object</param>
        public static void FillTextTabForAllScreens(ref TabLayout tablayout, Activity context)
        {
            ScreenSize = (int)(context.Resources.Configuration.ScreenLayout & ScreenLayout.SizeMask);
            for (int i = 0; i < tablayout.TabCount; i++)
            {
                TextView tabText = tablayout.GetTabAt(i).CustomView.FindViewById<TextView>(Android.Resource.Id.Title);
                //Android.Resource.Id.T
                SelectText(ScreenSize, ref tabText);

            }
        }
        
        public static void SelectText(int screenSize, ref TextView tabText)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 25);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 35);
                    break;
            }
        }
        public static void SelectEditText(int screenSize, ref EditText tabText)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 25);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 35);
                    break;
            }
        }
        public static void SelectViewPaddiing(int screenSize,ref EditText editTextView)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    
                    break;
                case (int)ScreenLayout.SizeNormal:
                    
                    break;
                case (int)ScreenLayout.SizeLarge:
                    
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    
                    break;
            }
        }
        public static void SelectViewPaddiing(int screenSize, ref TextView editTextView)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    editTextView.SetPadding(0, 150, 0, 0);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    editTextView.SetPadding(0, 150, 0, 0);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    editTextView.SetPadding(0, 100, 0, 0);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    editTextView.SetPadding(0, 50, 0, 0);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    editTextView.SetPadding(0, 50, 0, 0);
                    break;
            }
        }
        public static void SelectViewPaddiing(int screenSize, ref TextInputLayout editTextView)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    editTextView.SetPadding(0, 120, 0, 0);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    editTextView.SetPadding(0, 120, 0, 0);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    editTextView.SetPadding(0, 90, 0, 0);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    editTextView.SetPadding(0, 60, 0, 0);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    editTextView.SetPadding(0, 30, 0, 0);
                    break;
            }
        }
        public static void SelectViewPaddiing(int screenSize, ref RadioGroup editTextView)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    editTextView.SetPadding(0, 250, 0, 0);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    editTextView.SetPadding(0, 250, 0, 0);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    editTextView.SetPadding(0, 200, 0, 0);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    editTextView.SetPadding(0, 150, 0, 0);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    editTextView.SetPadding(0, 100, 0, 0);
                    break;
            }
        }
        public static void SelectRadioButtonText(int screenSize, ref RadioButton tabText)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 25);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    tabText.SetTextSize(Android.Util.ComplexUnitType.Sp, 35);
                    break;
            }
        }
        public static void SelectButtonText(int screenSize, ref Button ButtonText)
        {
            switch (screenSize)
            {
                case (int)ScreenLayout.SizeSmall:
                    ButtonText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeNormal:
                    ButtonText.SetTextSize(Android.Util.ComplexUnitType.Sp, 18);
                    break;
                case (int)ScreenLayout.SizeLarge:
                    ButtonText.SetTextSize(Android.Util.ComplexUnitType.Sp, 25);
                    break;
                case (int)ScreenLayout.SizeXlarge:
                    ButtonText.SetTextSize(Android.Util.ComplexUnitType.Sp, 30);
                    break;
                case (int)ScreenLayout.SizeUndefined:
                    ButtonText.SetTextSize(Android.Util.ComplexUnitType.Sp, 35);
                    break;
            }
        }
        
        public static void BuildLocalNotification(string message,
                 string Title, Context context, int noteCode,
                 NotificationMessageType notificationMessageType)
        {
            NotificationCompat.BigTextStyle textStyle = new NotificationCompat.BigTextStyle();
            textStyle.BigText(message);


            switch (notificationMessageType)
            {
                case NotificationMessageType.AnnouncmentNotificationMessage:
                    textStyle.SetSummaryText("Announcement message");
                    break;
                case NotificationMessageType.EvaluationNotificationMessage:
                    textStyle.SetSummaryText("Evaluation message");
                    break;
                case NotificationMessageType.RatingNotificationMessage:
                    textStyle.SetSummaryText("Presentation rating message");
                    break;
            }
            
            var noteBuilder = new NotificationCompat.Builder(context);

            
            noteBuilder.SetStyle(textStyle);
            noteBuilder.SetContentTitle(Title);
            noteBuilder.SetContentText(message);
            noteBuilder.SetSmallIcon(Resource.Drawable.ic_stat_splash_logo);
            
            noteBuilder.SetDefaults((int)NotificationDefaults.Sound);
            if ((int)Build.VERSION.SdkInt >= 16)
            {
                noteBuilder.SetPriority((int)NotificationPriority.Max);
            }

            if ((int)Build.VERSION.SdkInt >= 21)
            {
                noteBuilder.SetCategory(Notification.CategoryEvent);
                noteBuilder.SetVisibility((int)NotificationVisibility.Public);
            }
            var notification = noteBuilder.Build();
            var notificationManager = NotificationManagerCompat.From(context);
            notificationManager.Notify(noteCode, notification);
        }



    }

}