using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Java.Lang;
using Android.Text;
using Android.Text.Style;
using UTLO.Mobile.Restful.Service.Service.RestService;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.ViewModels;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLOConfApp.Utils;
using Java.Net;

namespace UTLOConfApp
{
    [Activity(Label = "RatePresentationActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class RatePresentationActivity : AppCompatActivity
    {
        Button rateButton;
        Button cancelButton;
        TextView conf_name;
        RadioGroup RateRadioGroup;
        int PresentationId = 0;
        string PresentationName = string.Empty;
        RateService rateService;
        string SelectedRateText;
        CacheOperation<int> cacheRating;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RatePresentationActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            PresentationId = Intent.Extras.GetInt("PresentationId");
            PresentationName = Intent.Extras.GetString("PresentationName");

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];

            TextView ActTitle = FindViewById<TextView>(Resource.Id.conf_title);
            ActTitle.Text = "Ratings";
            cacheRating = new CacheOperation<int>();

            RateRadioGroup = FindViewById<RadioGroup>(Resource.Id.rateradioGroup);
            RateRadioGroup.CheckedChange += RateRadioGroup_CheckedChange;

            rateButton = FindViewById<Button>(Resource.Id.rateButton);
            rateButton.Text = "Rate";
            rateButton.Enabled = false;
            rateButton.Click += RateButton_Click;

            cancelButton = FindViewById<Button>(Resource.Id.cancelButton);
            cancelButton.Click += CancelButton_Click;



        }

        private void RateRadioGroup_CheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            SelectedRateText = FindViewById<RadioButton>(RateRadioGroup.CheckedRadioButtonId).Text;
            if (!string.IsNullOrEmpty(SelectedRateText))
            {
                rateButton.Enabled = true;
            }
            else
            {
                rateButton.Enabled = false;
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Finish();
        }
        private async Task PostRating(int id, RatingViewModel ratingModel)
        {
            rateService = new RateService();
            await Task.Run(() => rateService
                    .RatePresentation(Constants.RatePresentation, PresentationId, ratingModel));
            await cacheRating.InsertObject(PresentationId.ToString(), PresentationId);
        }
        private async void RateButton_Click(object sender, EventArgs e)
        {
            try
            {
                var isonline = await Task.Run(() => isOnline());

                if (isonline)
                {
                    if (PresentationId != 0)
                    {

                        RatingViewModel ratingModel = new RatingViewModel
                        {
                            Rating = int.Parse(SelectedRateText)
                        };
                        ProgressDialog progressdialog = new ProgressDialog(this);
                        progressdialog.SetCancelable(false);
                        progressdialog.Indeterminate = true;
                        progressdialog.SetMessage("Posting...");
                        progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
                        progressdialog.Show();
                        await PostRating(PresentationId, ratingModel);
                        progressdialog.Hide();
                        progressdialog.Dismiss();
                        if (rateService.response == ResponseMessage.Successful)
                        {
                            string message = $"You have rated the following presentation titled {PresentationName}. " +
                                $"You will not be able to rate this presentation again";
                            string Title = (string)ConferenceSettings.csettings["Title"] + " Presentation Rating";
                            int notecode = (int)NotificationCode.PresentationRating;
                            ConferenceUtils.BuildLocalNotification(message, Title, this, notecode,
                                Utils.Enum.NotificationMessageType.RatingNotificationMessage);
                            new Android.Support.V7.App.AlertDialog.Builder(this)
                            .SetPositiveButton("Yes", (esender, args) =>
                            {
                                Finish();
                            })
                            .SetMessage("Your rating was successfully posted")
                            .SetTitle("Response")
                            .Show();
                        }
                        else
                        {
                            new Android.Support.V7.App.AlertDialog.Builder(this)
                            .SetPositiveButton("Yes", (esender, args) =>
                            {

                            })

                            .SetMessage("Your rating request was not successful")
                            .SetTitle("Error")
                            .Show();
                        }
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                            .SetPositiveButton("Yes", (esender, args) =>
                            {

                            })

                            .SetMessage("No valid presentation selected")
                            .SetTitle("Error")
                            .Show();
                    }
                }
                else
                {
                    new Android.Support.V7.App.AlertDialog.Builder(this)
                        .SetPositiveButton("Yes", (esender, args) =>
                        {

                        })
                        .SetNegativeButton("No", (esender, args) =>
                        {

                        })
                        .SetMessage("No Network connection")
                        .SetTitle("Network Error")
                        .Show();
                }
            }
            catch (System.Exception ex)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                        .SetPositiveButton("Yes", (esender, args) =>
                        {

                        })
                        .SetNegativeButton("No", (esender, args) =>
                        {

                        })
                        .SetMessage("Your rate request was not successful")
                        .SetTitle("Error")
                        .Show();
            }

        }

        //public override bool OnCreateOptionsMenu(IMenu menu)
        //{
        //    MenuInflater.Inflate(Resource.Menu.home, menu);

        //    return base.OnCreateOptionsMenu(menu);
        //}
        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (System.Exception)
                {

                    return false;
                }
            }
            return false;
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}