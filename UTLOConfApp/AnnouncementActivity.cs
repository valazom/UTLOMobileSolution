﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using UTLO.Mobile.Restful.Service.Service.RestService;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;
using UTLO.Mobile.Restful.Service;
using UTLOConfApp.Utils;
using Android.Net;
using UTLOConfApp.Adapter;
using UTLOConfApp.Fragments;
using Java.Net;
using Android.Support.V7.Widget;

namespace UTLOConfApp
{
    [Activity(Label = "AnnouncementActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class AnnouncementActivity : AppCompatActivity
    {
        //ListView AnnListView;
        TextView HeaderTitle;
        TextView conf_name;
        AnnouncementService announcementService;
        List<Announcement> announcement;
        //AnnouncementAdapter annAdapter;
        RecyclerView recyclerview;
        AnnouncementRecyclerAdapter announcementRecyclerAdapter;
        CacheOperation<Announcement> cache;
        bool forceReload = false;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AnnouncementActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetupToolbar();
            await LoadDataOnline();

        }

        private async Task LoadDataOnline()
        {
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await LoadListView();
            progressdialog.Hide();
            progressdialog.Dismiss();
        }

        private void SetupToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt == BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            HeaderTitle = FindViewById<TextView>(Resource.Id.conf_title);
            HeaderTitle.Text = "Announcements";
        }

        private async Task LoadListView()
        {
            try
            {
                cache = new CacheOperation<Announcement>();
                announcementService = new AnnouncementService();
                //For announcement always reload and don't cache;
                var isonline = await Task.Run(() => isOnline());
                if (isonline)
                {

                    await Task.Run(() => announcementService
                                        .GetAllAnnouncement(Constants.Announcements));
                    announcement = announcementService.announcements;
                }
                else
                {
                    new Android.Support.V7.App.AlertDialog.Builder(this)
                                    .SetPositiveButton("Yes", (sender, args) =>
                                    {
                                        Finish();
                                    })
                                    .SetNegativeButton("No", (sender, args) =>
                                    {
                                        Finish();
                                    })
                                    .SetMessage("No network connection. You won't be able to get data")
                                    .SetTitle("Network Error")
                                    .Show();
                }


                if (announcement != null)
                {

                    recyclerview = FindViewById<RecyclerView>(Resource.Id.recyclerview);
                    announcementRecyclerAdapter = new AnnouncementRecyclerAdapter(this, announcement);
                    announcementRecyclerAdapter.ItemClick += AnnListView_ItemClick;
                    recyclerview.SetAdapter(announcementRecyclerAdapter);

                    var linearLayoutManager = new LinearLayoutManager(this);
                    linearLayoutManager.Orientation = LinearLayoutManager.Vertical;
                    recyclerview.SetLayoutManager(linearLayoutManager);

                }



            }
            catch (Exception e)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage(e.Message)
                                .SetTitle("Application Error")
                                .Show();
            }
        }

        private void AnnListView_ItemClick(object sender, AnnouncementRecyclerAdapterClickEventArgs e)
        {
            var item = announcementRecyclerAdapter.GetItem(e.Position);
            if (item.Topics != null && item.Topics.Count != 0)
            {
                var topicDialogfragment = new TopicLayoutFragment(item.Topics.ToList());
                topicDialogfragment.Show(this.SupportFragmentManager, "Topics");
            }
        }

        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}