﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Views;
using Android.App;
using Android.Widget;
using Android.Support.V7.Widget;
using UTLO.Data.Model.Domain;
using UTLO.Data.Model.Enums;

namespace UTLOConfApp.Adapter
{
    public class ProgItemRecyclerAdapter : RecyclerView.Adapter
    {
        public event EventHandler<ProgItemRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<ProgItemRecyclerAdapterClickEventArgs> ItemLongClick;

        Activity _context;
        List<Programme> progItems;
        public ProgItemRecyclerAdapter(Activity context, List<Programme> items)
        {
            _context = context;
            progItems = items;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(_context).Inflate(Resource.Layout.ProgListView, parent, false);
            var vh = new ProgItemRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;



        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = progItems[position];
            // Replace the contents of the view with that element
            var holder = viewHolder as ProgItemRecyclerAdapterViewHolder;
            holder.SetData(item);
            //holder.TextView.Text = items[position];
        }
        public Programme GetItem(int position)
        {
            return progItems[position];
        }
        public override int ItemCount => progItems.Count;

        void OnClick(ProgItemRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(ProgItemRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class ProgItemRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView Title { get; set; }
        public ImageView TimeprogImageView { get; set; }
        public TextView ProgTime { get; set; }
        public TextView Venue { get; set; }

        public TextView Presenters { get; set; }
        public TextView ProgVenueLocation { get; set; }
        public TextView PresenterName { get; set; }
        public TextView PresenterStatus { get; set; }
        public TextView PresenterAffliation { get; set; }
        public TextView ProgViewPresentation { get; set; }


        public ProgItemRecyclerAdapterViewHolder(View itemView, Action<ProgItemRecyclerAdapterClickEventArgs> clickListener,
                            Action<ProgItemRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            //TextView = v;
            InflateControls(ItemView);
            itemView.Click += (sender, e) => clickListener(new ProgItemRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new ProgItemRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
        public virtual void InflateControls(View ItemView)
        {
            Title = ItemView.FindViewById<TextView>(Resource.Id.progName);
            TimeprogImageView = ItemView.FindViewById<ImageView>(Resource.Id.timeprogImageView);
            ProgTime = ItemView.FindViewById<TextView>(Resource.Id.progTime);
            Venue = ItemView.FindViewById<TextView>(Resource.Id.Venue);
            Presenters = ItemView.FindViewById<TextView>(Resource.Id.Presenters);
            ProgVenueLocation = ItemView.FindViewById<TextView>(Resource.Id.progVenueLocation);
            PresenterName = ItemView.FindViewById<TextView>(Resource.Id.presenterName);
            PresenterStatus = ItemView.FindViewById<TextView>(Resource.Id.presenterStatus);
            PresenterAffliation = ItemView.FindViewById<TextView>(Resource.Id.presenterAffilation);
            ProgViewPresentation = ItemView.FindViewById<TextView>(Resource.Id.progViewPresentation);
        }
        public virtual void SetData(Programme item)
        {
            Title.Text = item.Title;
            TimeprogImageView.SetImageResource(Resource.Drawable.time_icon);
            ProgTime.Text = item.StartTime.ToString() + " - " + item.EndTime.ToString();

            if (!string.IsNullOrEmpty(item.Venue))
            {
                Venue.Text = "Venue";
                ProgVenueLocation.Text = item.Venue;
            }
            else
            {
                //Venue.Visibility = ViewStates.Gone;
                ProgVenueLocation.Visibility = ViewStates.Gone;
            }

            if (item.presenters != null)
            {
                
                PresenterName.Text = item.presenters.ToList()[0].status + " " + item.presenters.ToList()[0].Name;
                if (!string.IsNullOrEmpty(item.presenters.ToList()[0].Position))
                {
                    //PresenterStatus.Visibility = ViewStates.Visible;
                    PresenterStatus.Text = item.presenters.ToList()[0].Position;
                }
                else
                    PresenterStatus.Visibility = ViewStates.Gone;
                PresenterAffliation.Text = item.presenters.ToList()[0].Affiliation;
            }
            else
            {
                Presenters.Visibility = ViewStates.Gone;
                PresenterName.Visibility = ViewStates.Gone;
                PresenterStatus.Visibility = ViewStates.Gone;
                PresenterAffliation.Visibility = ViewStates.Gone;
            }
            if (item.programmeType == ProgrammeType.MultiVenue)
            {
                if (ProgViewPresentation.Visibility == ViewStates.Gone)
                    ProgViewPresentation.Visibility = ViewStates.Visible;
                ProgViewPresentation.Text = "View Venues";
            }
            else
            {
                ProgViewPresentation.Visibility = ViewStates.Gone;
            }
        }
    }

    public class ProgItemRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}