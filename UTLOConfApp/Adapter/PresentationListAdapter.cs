using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Text;
using Android.Text.Style;
using Android.Graphics.Drawables;
using Android.Content.Res;
using UTLOConfApp.Utils;
using System.Threading.Tasks;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    public class PresentationListAdapter : BaseAdapter<Presentations>
    {
        Activity context;
        List<Presentations> presentations;
        CacheOperation<int> CacheRatingCompleted;
        public PresentationListAdapter(Activity context, List<Presentations> precontainer)
        {
            this.context = context;
            presentations = precontainer;
            CacheRatingCompleted = new CacheOperation<int>();
        }
        public override Presentations this[int position]
        {
            get
            {
                return presentations[position];
            }
        }

        public override int Count
        {
            get
            {
                return presentations.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = presentations[position];
            LinearLayout ListContainer;
            //Create layout parameters for textviews of authors that will be added
            LinearLayout.LayoutParams authorLayoutParams = new LinearLayout
                .LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
            LinearLayout.LayoutParams otherLayoutParams = new LinearLayout
                .LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            int screenSize = (int)(context.Resources.Configuration.ScreenLayout & ScreenLayout.SizeMask);
            authorLayoutParams.SetMargins(20, 20, 20, 10);
            otherLayoutParams.SetMargins(20, 0, 20, 0);
            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.presentationListItemView, null);
                ListContainer = convertView.FindViewById<LinearLayout>(Resource.Id.presentationContainer);

                if (item != null)
                {
                    if ((int)item.type > 0)
                    {
                        //Adding textView for presentation type
                        TextView HeadingType = new TextView(context);
                        HeadingType.Text = "Presentation type";
                        HeadingType.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref HeadingType);
                        HeadingType.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(HeadingType, otherLayoutParams);

                        TextView PresentationType = new TextView(context);
                        PresentationType.Text = item.type.ToString();
                        PresentationType.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref PresentationType);
                        ListContainer.AddView(PresentationType, authorLayoutParams);
                    }

                    if (!string.IsNullOrEmpty(item.Venue))
                    {
                        // add venue textview;
                        TextView venueTitle = new TextView(context);
                        venueTitle.Text = "Venue";
                        venueTitle.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref venueTitle);
                        venueTitle.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(venueTitle, otherLayoutParams);

                        //add venue name textview
                        TextView venueNameTitle = new TextView(context);
                        venueNameTitle.Text = item.Venue;
                        venueNameTitle.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref venueNameTitle);
                        ListContainer.AddView(venueNameTitle, authorLayoutParams);
                    }

                    if (!string.IsNullOrEmpty(item.Title))
                    {
                        // add heading title
                        TextView HeadingTitle = new TextView(context);
                        HeadingTitle.Text = "Title";
                        HeadingTitle.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref HeadingTitle);
                        HeadingTitle.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(HeadingTitle, otherLayoutParams);

                        // adding programme title
                        TextView progTitle = new TextView(context);
                        progTitle.Text = item.Title;
                        progTitle.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref progTitle);
                        progTitle.SetTypeface(Typeface.DefaultFromStyle(TypefaceStyle.Italic), TypefaceStyle.Italic);
                        ListContainer.AddView(progTitle, authorLayoutParams);
                    }
                    if (item.Authors != null && item.Authors.Count != 0)
                    {
                        // add Heading for Authors
                        TextView HeadingAuthors = new TextView(context);
                        HeadingAuthors.Text = "Author(s)";
                        HeadingAuthors.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref HeadingAuthors);
                        HeadingAuthors.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(HeadingAuthors, otherLayoutParams);

                        foreach (var author in item.Authors)
                        {
                            // Adding textView for Authors name
                            TextView authorsName = new TextView(context);
                            authorsName.Text = author.status + " " + author.Name;
                            authorsName.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref authorsName);
                            ListContainer.AddView(authorsName, otherLayoutParams);

                            // Adding textView for Authors Position
                            TextView authorsPosition = new TextView(context);
                            authorsPosition.Text = author.Position;
                            authorsPosition.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref authorsPosition);
                            ListContainer.AddView(authorsPosition, otherLayoutParams);

                            // Adding textView for authors Affilation
                            TextView authorsAffilation = new TextView(context);
                            authorsAffilation.Text = author.Affiliation;
                            authorsAffilation.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref authorsAffilation);
                            ListContainer.AddView(authorsAffilation, otherLayoutParams);
                        }
                    }
                    if (item.Panelists != null && item.Panelists.Count != 0)
                    {
                        //adding Panelist header
                        TextView HeadingPanelists = new TextView(context);
                        HeadingPanelists.Text = "Panelists";
                        HeadingPanelists.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref HeadingPanelists);
                        HeadingPanelists.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(HeadingPanelists, otherLayoutParams);

                        foreach (var panelist in item.Panelists)
                        {
                            // Adding textView for Authors name
                            TextView panelistName = new TextView(context);
                            panelistName.Text = panelist.status + " " + panelist.Name;
                            panelistName.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref panelistName);
                            ListContainer.AddView(panelistName, otherLayoutParams);

                            // Adding textView for Authors Position
                            TextView panelistPosition = new TextView(context);
                            panelistPosition.Text = panelist.Position;
                            panelistPosition.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref panelistPosition);
                            ListContainer.AddView(panelistPosition, otherLayoutParams);

                            // Adding textView for authors Affilation
                            TextView panelistAffilation = new TextView(context);
                            panelistAffilation.Text = panelist.Affiliation;
                            panelistAffilation.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref panelistAffilation);
                            ListContainer.AddView(panelistAffilation, otherLayoutParams);
                        }
                    }
                    if (item.chairPersons != null && item.chairPersons.Count != 0)
                    {
                        //Adding textViews for chairpersons
                        TextView HeadingChairpersons = new TextView(context);
                        HeadingChairpersons.Text = "Chairpersons";
                        HeadingChairpersons.SetTextColor(Color.Black);
                        ConferenceUtils.SelectText(screenSize, ref HeadingChairpersons);
                        HeadingChairpersons.SetTypeface(Typeface.DefaultBold, TypefaceStyle.Bold);
                        ListContainer.AddView(HeadingChairpersons, otherLayoutParams);

                        foreach (var chairperson in item.chairPersons)
                        {
                            // Adding textView for chairpersons name
                            TextView chairpersonName = new TextView(context);
                            chairpersonName.Text = chairperson.status + " " + chairperson.Name;
                            chairpersonName.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref chairpersonName);
                            ListContainer.AddView(chairpersonName, otherLayoutParams);

                            // Adding textView for Authors Position
                            TextView chairpersonPosition = new TextView(context);
                            chairpersonPosition.Text = chairperson.Position;
                            chairpersonPosition.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref chairpersonPosition);
                            ListContainer.AddView(chairpersonPosition, otherLayoutParams);

                            // Adding textView for authors Affilation
                            TextView chairpersonAffilation = new TextView(context);
                            chairpersonAffilation.Text = chairperson.Affiliation;
                            chairpersonAffilation.SetTextColor(Color.Black);
                            ConferenceUtils.SelectText(screenSize, ref chairpersonAffilation);
                            ListContainer.AddView(chairpersonAffilation, otherLayoutParams);
                        }
                    }

                    Tuple<int, bool> isInCache = new Tuple<int, bool>(0, false);
                    // check if rating has been completed before
                    // You can only rate a presentation once 
                    // Perform this on the background thread and return result on the UI Thread
                    Task.Run(() => CacheRatingCompleted.GetObjectOther(item.PresentationsId.ToString()))
                        .ContinueWith(response =>
                        {
                            isInCache = response.Result;
                            if (!isInCache.Item2)
                            {
                                Button RateButton = new Button(context);
                                RateButton.Text = "Rate";
                                RateButton.SetBackgroundResource(Resource.Drawable.RateButtonShape);
                                RateButton.SetTextColor(Color.White);
                                ConferenceUtils.SelectButtonText(screenSize, ref RateButton);
                                RateButton.Click += (sender, e) =>
                                {

                                    Intent Rateintent = new Intent(context, typeof(RatePresentationActivity));
                                    Rateintent.PutExtra("PresentationId", item.PresentationsId);
                                    Rateintent.PutExtra("PresentationName", item.Title);
                                    context.StartActivity(Rateintent);
                                };

                                ListContainer.AddView(RateButton, authorLayoutParams);
                            }

                        }, TaskScheduler.FromCurrentSynchronizationContext());




                }
            }


            return convertView;
        }

    }
}