﻿using System;
using System.Linq;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Java.Lang;
using Android.App;
using System.Collections.Generic;
using Com.Bumptech.Glide;
using UTLO.Mobile.Restful.Service;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    class AttendeeRecyclerAdapter : RecyclerView.Adapter, IFilterable
    {
        public event EventHandler<AttendeeRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<AttendeeRecyclerAdapterClickEventArgs> ItemLongClick;
        Activity context;
        List<Attendees> items;
        List<Attendees> guests;

        public AttendeeRecyclerAdapter(Activity context, List<Attendees> guests)
        {
            this.context = context;
            items = guests.OrderBy(s => s.Name).ToList();
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(context).Inflate(Resource.Layout.AttendeeItemListView, parent, false);

            var vh = new AttendeeRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AttendeeRecyclerAdapterViewHolder;
            holder.SetData(item, context);
            //holder.TextView.Text = items[position];
        }

        public override int ItemCount => items.Count;
        public Attendees GetItem(int position)
        {
            return items[position];
        }
        public Filter Filter
        {
            get
            {
                return new AttendeeFilter(this);
            }
        }
        void OnClick(AttendeeRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(AttendeeRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

        public class AttendeeFilter : Filter
        {
            AttendeeRecyclerAdapter _adapter;
            public AttendeeFilter(AttendeeRecyclerAdapter adapter)
            {
                _adapter = adapter;
            }
            protected override FilterResults PerformFiltering(ICharSequence constraint)
            {
                var returnObj = new FilterResults();
                var results = new List<Attendees>();
                if (_adapter.guests == null)
                {
                    _adapter.guests = _adapter.items;
                }
                if (constraint == null) return returnObj;
                if (_adapter.guests != null && _adapter.guests.Any())
                {
                    results.AddRange
                        (_adapter.guests.Where(s => (s.status + " " + s.Name).ToLower().Contains(constraint.ToString().ToLower())));
                }
                returnObj.Values = FromArray(results.Select(r => r.ToJavaObject()).ToArray());
                returnObj.Count = results.Count;

                constraint.Dispose();

                return returnObj;
            }
            protected override void PublishResults(ICharSequence constraint, FilterResults results)
            {
                using (var values = results.Values)
                    _adapter.items = values.ToArray<Java.Lang.Object>()
                                             .Select(r => r.ToNetObject<Attendees>()).ToList();
                constraint.Dispose();
                results.Dispose();
            }
        }

    }

    public class AttendeeRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public ImageView AttendeeImage { get; set; }
        public TextView AttendeeName { get; set; }
        public TextView AttendeePosition { get; set; }
        public TextView AttendeeAffiliation { get; set; }

        public AttendeeRecyclerAdapterViewHolder(View itemView, Action<AttendeeRecyclerAdapterClickEventArgs> clickListener,
                            Action<AttendeeRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            InflateControls(itemView);
            itemView.Click += (sender, e) => clickListener(new AttendeeRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new AttendeeRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
        private void InflateControls(View itemView)
        {
            AttendeeImage = itemView.FindViewById<ImageView>(Resource.Id.AttimageView);
            AttendeeName = itemView.FindViewById<TextView>(Resource.Id.AttNametextView);
            AttendeePosition = itemView.FindViewById<TextView>(Resource.Id.attpositiontextView);
            AttendeeAffiliation = itemView.FindViewById<TextView>(Resource.Id.attaffiliationtextView);
        }
        public void SetData(Attendees attendee, Activity context)
        {

            if (!string.IsNullOrEmpty(attendee.ImageUrl))
            {
                Glide.With(context)
                 .SetDefaultRequestOptions(new Com.Bumptech.Glide.Request.RequestOptions()
                 .Placeholder(Resource.Drawable.usericon))
                 .SetDefaultRequestOptions(new Com.Bumptech.Glide.Request.RequestOptions()
                 .CircleCrop())
                 .Load(Constants.KeynoteSpeakersImageBaseUrl + attendee.ImageUrl)
                 .Into(AttendeeImage);
            }
            else
            {
                AttendeeImage.SetImageResource(Resource.Drawable.usericon);
            }

            AttendeeName.Text = attendee.status + " " + attendee.Name;
            if (!string.IsNullOrEmpty(attendee.Position))
            {
                if (AttendeePosition.Visibility == ViewStates.Gone)
                    AttendeePosition.Visibility = ViewStates.Visible;
                AttendeePosition.Text = attendee.Position;
            }
            else
            {
                AttendeePosition.Visibility = ViewStates.Gone;
            }
            if (!string.IsNullOrEmpty(attendee.Affiliation))
            {
                if (AttendeeAffiliation.Visibility == ViewStates.Gone)
                    AttendeeAffiliation.Visibility = ViewStates.Visible;
                AttendeeAffiliation.Text = attendee.Affiliation;
            }
            else
            {
                AttendeeAffiliation.Visibility = ViewStates.Gone;
            }
        }
    }

    public class AttendeeRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}