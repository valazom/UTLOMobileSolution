﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.App;
using System.Collections.Generic;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    class AccomodationRecyclerAdapter : RecyclerView.Adapter
    {
        public event EventHandler<AccomodationRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<AccomodationRecyclerAdapterClickEventArgs> ItemLongClick;
        Activity context;
        List<Accommodation> Accommodation;

        public AccomodationRecyclerAdapter(Activity context, List<Accommodation> accommodation)
        {
            this.context = context;
            Accommodation = accommodation;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            var itemView = LayoutInflater.From(context).Inflate(Resource.Layout.AccommodationListView, parent, false);

            var vh = new AccomodationRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = Accommodation[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AccomodationRecyclerAdapterViewHolder;
            holder.SetData(item);
            //holder.TextView.Text = items[position];
        }

        public Accommodation GetItem(int position)
        {
            return Accommodation[position];
        }

        public override int ItemCount => Accommodation.Count;

        void OnClick(AccomodationRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(AccomodationRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class AccomodationRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public TextView HotelName { get; set; }

        public ImageView ArrowDirection { get; set; }


        public AccomodationRecyclerAdapterViewHolder(View itemView, Action<AccomodationRecyclerAdapterClickEventArgs> clickListener,
                            Action<AccomodationRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            //TextView = v;
            InflateControls(itemView);
            itemView.Click += (sender, e) => clickListener(new AccomodationRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new AccomodationRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
        private void InflateControls(View itemView)
        {
            HotelName = itemView.FindViewById<TextView>(Resource.Id.AttNametextView);
            ArrowDirection = itemView.FindViewById<ImageView>(Resource.Id.arrowimageView);
        }
        public void SetData(Accommodation accommodation)
        {
            HotelName.Text = accommodation.accommodationName;
            ArrowDirection.SetBackgroundResource(Resource.Drawable.arrow_right_icon);
        }
    }

    public class AccomodationRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}