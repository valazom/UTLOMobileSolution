using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Java.Lang;
using UTLOConfApp.Utils;
using System.Threading.Tasks;
using Android.Graphics;
using UTLO.Mobile.Restful.Service;
using Com.Bumptech.Glide;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    public class RegPagerAdapter : PagerAdapter
    {
        Activity context;
        List<Attendees> item;
        public RegPagerAdapter(Activity context, List<Attendees> items)
        {
            this.context = context;
            item = items;
        }
        public override int Count
        {
            get
            {
                return item.Count;
            }
        }
        public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
        {
            var layoutContainer = context.LayoutInflater.Inflate(Resource.Layout.AttendeePagerView,null);
            ImageView imagebox = layoutContainer.FindViewById<ImageView>(Resource.Id.PagerAttendeeimageView);
            string url = Constants.KeynoteSpeakersImageBaseUrl + item[position].ImageUrl;
            Glide.With(context)
                .SetDefaultRequestOptions(new Com.Bumptech.Glide.Request.RequestOptions()
                .Placeholder(Resource.Drawable.usericon))
                .SetDefaultRequestOptions(new Com.Bumptech.Glide.Request.RequestOptions()
                .CircleCrop())
                .Load(url)
                .Into(imagebox);

            //layoutContainer.FindViewById<ImageView>(Resource.Id.PagerAttendeegroupmageView).SetImageResource(Resource.Drawable.Group_icon);
            layoutContainer.FindViewById<TextView>(Resource.Id.PagerAttendeetextView).Text = item[position].status
                + " " + item[position].Name;
            //if (!string.IsNullOrEmpty(item[position].Position))
            //    layoutContainer.FindViewById<TextView>(Resource.Id.positiontextView).Text = item[position].Position;
            //else
            layoutContainer.FindViewById<TextView>(Resource.Id.positiontextView).Visibility = ViewStates.Gone;
            layoutContainer.FindViewById<TextView>(Resource.Id.affiliationtextView).Text = item[position].Affiliation;
            layoutContainer.SetOnClickListener(new ViewPagerOnClickListener(context, position,item));
            container.AddView(layoutContainer);
            return layoutContainer;
        }
        public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object)
        {
            container.RemoveView(@object as View);
        }
        public override bool IsViewFromObject(View view, Java.Lang.Object @object)
        {
            return view == @object;
        }
        

        
    }
}