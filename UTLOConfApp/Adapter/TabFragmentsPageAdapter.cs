using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace UTLOConfApp.Adapter
{
    public class TabFragmentsPageAdapter : Android.Support.V4.App.FragmentPagerAdapter
    {
        private readonly Android.Support.V4.App.Fragment[] fragment;
        private readonly ICharSequence[] titles;
        Android.Support.V4.App.FragmentManager fmanger;
        
    
        public TabFragmentsPageAdapter(Android.Support.V4.App.FragmentManager fm, 
            Android.Support.V4.App.Fragment [] FragmentArray, ICharSequence[] tittle):base(fm)
        {
            fragment = FragmentArray;
            titles = tittle;
            fmanger = fm;
            
        }
        public override int Count
        {
            get
            {
                return fragment.Length;
            }
        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {

            return fragment[position];
        }
        
        public override ICharSequence GetPageTitleFormatted(int position)
        {
            return titles[position];
        }
        
    }
}