using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Mobile.Restful.Service.ViewModels;

namespace UTLOConfApp.Adapter
{
    public class RegListAdapter : BaseAdapter<SummaryProgramme>
    {
        List<SummaryProgramme> items;
        Activity context;

        public RegListAdapter(Activity context, List<SummaryProgramme> ritems)
        {
            this.context = context;
            items = ritems;
        }
        public override SummaryProgramme this[int position]
        {
            get
            {
                return items[position];
            }
        }

        public override int Count
        {
            get
            {
                return items.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            if (convertView == null)
            {
                convertView = context.LayoutInflater.Inflate(Resource.Layout.RegListView, null);
            }
            convertView.FindViewById<TextView>(Resource.Id.RegTitleName).Text = item.DayName;
            //convertView.FindViewById<ImageView>(Resource.Id.RegimageView).SetImageResource(item.rImageId);
            convertView.FindViewById<TextView>(Resource.Id.RegtextView).Text = item.programmeName;
            //convertView.FindViewById<ImageView>(Resource.Id.CalimageView).SetImageResource(item.lImageId);
            convertView.FindViewById<TextView>(Resource.Id.TimetextView).Text = item.StartTime + " - " + item.EndTime;

            return convertView;
        }
    }
}