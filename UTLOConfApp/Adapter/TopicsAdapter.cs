﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    public class TopicsAdapter : BaseAdapter<Topic>
    {

        Context context;
        List<Topic> _topics;
        public TopicsAdapter(Context context, List<Topic> topics)
        {
            this.context = context;
            _topics = topics;
        }


        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;
            TopicsAdapterViewHolder holder = null;
            var item = _topics[position];
            if (view != null)
                holder = view.Tag as TopicsAdapterViewHolder;

            if (holder == null)
            {
                holder = new TopicsAdapterViewHolder();
                var inflater = context.GetSystemService(Context.LayoutInflaterService).JavaCast<LayoutInflater>();

                view = inflater.Inflate(Resource.Layout.SuiteListView, parent, false);
                holder.Title = view.FindViewById<TextView>(Resource.Id.SuitetextView);
                view.Tag = holder;
            }
            //fill in your items
            holder.Title.Text = (position + 1) + ") " + item.Bulletpoint;

            return view;
        }

        //Fill in cound here, currently 0
        public override int Count
        {
            get
            {
                return _topics.Count;
            }
        }

        public override Topic this[int position] => _topics[position];
    }

    class TopicsAdapterViewHolder : Java.Lang.Object
    {
        //Your adapter views to re-use
        public TextView Title { get; set; }
    }
}