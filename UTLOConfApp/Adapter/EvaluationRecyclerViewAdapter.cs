﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using UTLO.Data.Model.Domain;
using UTLO.Data.Model.Enums;
using Android.App;
using Android.Text;
using Java.Lang;

namespace UTLOConfApp.Adapter
{
    public class EvaluationRecyclerViewAdapter : RecyclerView.Adapter
    {
        public event EventHandler<EvaluationRecyclerViewAdapterClickEventArgs> ItemClick;
        public event EventHandler<EvaluationRecyclerViewAdapterClickEventArgs> ItemLongClick;

        List<EvaluationQuestions> _evaluationQuestions;
        List<Answers> _answers;
        Activity context;

        public EvaluationRecyclerViewAdapter(Activity context,
                          List<EvaluationQuestions> evaluationQuestions,
                          List<Answers> answers)
        {
            _evaluationQuestions = evaluationQuestions;
            _answers = answers;
            this.context = context;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            switch (viewType)
            {
                case (int)QuestionType.FillingTheGap:
                    var itemView_Gap = LayoutInflater.From(context)
                        .Inflate(Resource.Layout.EvaluationQuestionsListItems, parent, false);
                    var vh_FillingTheGap = new EvaluationFillingTheGapRecyclerViewAdapterViewHolder(itemView_Gap, OnClick, OnLongClick);
                    return vh_FillingTheGap;
                case (int)QuestionType.Rating:
                    var itemView_Rate = LayoutInflater.From(context)
                        .Inflate(Resource.Layout.EvaluationRatingListItems, parent, false);
                    var vh_Rating = new EvaluationRatingRecyclerViewAdapterViewHolder(itemView_Rate, OnClick, OnLongClick);
                    return vh_Rating;
                default:
                    var itemView = LayoutInflater.From(context)
                        .Inflate(Resource.Layout.EvaluationQuestionsListItems, parent, false);
                    var vh = new EvaluationRecyclerViewAdapterViewHolder(itemView, OnClick, OnLongClick);
                    return vh;

            }
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var question = _evaluationQuestions[position];
            var answer = _answers[position];

            // Replace the contents of the view with that element
            switch (viewHolder.ItemViewType)
            {
                case (int)QuestionType.FillingTheGap:
                    var holder_FillingTheGap = viewHolder as EvaluationFillingTheGapRecyclerViewAdapterViewHolder;
                    holder_FillingTheGap.SetData(question, answer);
                    _answers[position] = holder_FillingTheGap.Answer;
                    break;
                case (int)QuestionType.Rating:
                    var holder_Rating = viewHolder as EvaluationRatingRecyclerViewAdapterViewHolder;
                    holder_Rating.SetData(question, answer);
                    _answers[position] = holder_Rating.Answer;
                    break;
            }

            //holder.TextView.Text = items[position];
        }
        public List<Answers> GetAnswers
        {
            get
            {
                return _answers;
            }

        }
        public override int GetItemViewType(int position)
        {
            var item = _evaluationQuestions[position];
            switch (item.Qtype)
            {
                case QuestionType.FillingTheGap:
                    return (int)QuestionType.FillingTheGap;
                    
                case QuestionType.Rating:
                    return (int)QuestionType.Rating;
                default:
                    return 0;
            }
        }
        public override int ItemCount => _evaluationQuestions.Count;

        void OnClick(EvaluationRecyclerViewAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(EvaluationRecyclerViewAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class EvaluationRecyclerViewAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }


        public EvaluationRecyclerViewAdapterViewHolder(View itemView, Action<EvaluationRecyclerViewAdapterClickEventArgs> clickListener,
                            Action<EvaluationRecyclerViewAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            //TextView = v;
            itemView.Click += (sender, e) => clickListener(new EvaluationRecyclerViewAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new EvaluationRecyclerViewAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }
    public class EvaluationFillingTheGapRecyclerViewAdapterViewHolder : EvaluationRecyclerViewAdapterViewHolder
    {
        public TextView InfoTextView { get; set; }
        public EditText MessageEditTextView { get; set; }
        public Answers Answer { get; set; }
        public EvaluationFillingTheGapRecyclerViewAdapterViewHolder(View itemView, Action<EvaluationRecyclerViewAdapterClickEventArgs> clickListener,
                            Action<EvaluationRecyclerViewAdapterClickEventArgs> longClickListener) : base(itemView, clickListener, longClickListener)
        {

            InflateControls(itemView);

            //var evaluation = evaluations[this.AdapterPosition];

        }
        private void InflateControls(View itemView)
        {
            InfoTextView = itemView.FindViewById<TextView>(Resource.Id.infoTextView);
            MessageEditTextView = itemView.FindViewById<EditText>(Resource.Id.messageEditTextView);
            MessageEditTextView.TextChanged += MessageEditTextChanged;
        }
        public void SetData(EvaluationQuestions question, Answers answers)
        {
            //ClearControls();
            Answer = new Answers();
            Answer = answers;
            if (question.QuestionsRequired)
                InfoTextView.Text = question.Question + " (Required) ";
            else
                InfoTextView.Text = question.Question;

            if (!string.IsNullOrEmpty(Answer.answer))
            {
                MessageEditTextView.Text = Answer.answer;
            }
            else
            {
                MessageEditTextView.Text = string.Empty;
            }



        }

        private void MessageEditTextChanged(object sender, TextChangedEventArgs e)
        {
            Answer.answer = MessageEditTextView.Text;
        }

    }
    public class EvaluationRatingRecyclerViewAdapterViewHolder : EvaluationRecyclerViewAdapterViewHolder
    {
        public TextView RateDescTextView { get; set; }
        public RadioGroup RateRadioGroup { get; set; }

        public Answers Answer { get; set; }

        public EvaluationRatingRecyclerViewAdapterViewHolder(View itemView, Action<EvaluationRecyclerViewAdapterClickEventArgs> clickListener,
                            Action<EvaluationRecyclerViewAdapterClickEventArgs> longClickListener) : base(itemView, clickListener, longClickListener)
        {
            InflateControls(ItemView);
            //var evaluation = evaluations[this.AdapterPosition];

        }
        private void InflateControls(View itemView)
        {
            RateDescTextView = itemView.FindViewById<TextView>(Resource.Id.rateDescTextView);
            RateRadioGroup = itemView.FindViewById<RadioGroup>(Resource.Id.rateradioGroup);
            RateRadioGroup.CheckedChange += RateCheckedChanged;
        }
        public void SetData(EvaluationQuestions question, Answers answers)
        {
            Answer = new Answers();
            Answer = answers;
            if (question.QuestionsRequired)
                RateDescTextView.Text = question.Question + " (Required) ";
            else
                RateDescTextView.Text = question.Question;

            RateDescTextView.Text = question.Question;
            if (Answer.Rating != 0)
            {
                switch (Answer.Rating)
                {
                    case 1:
                        RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton1).Checked = true;
                        break;
                    case 2:
                        RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton2).Checked = true;
                        break;
                    case 3:
                        RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton3).Checked = true;
                        break;
                    case 4:
                        RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton4).Checked = true;
                        break;
                    case 5:
                        RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton5).Checked = true;
                        break;
                }
            }
            else
            {
                RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton1).Checked = false;
                RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton2).Checked = false;
                RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton3).Checked = false;
                RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton4).Checked = false;
                RateRadioGroup.FindViewById<RadioButton>(Resource.Id.radioButton5).Checked = false;
            }

        }

        private void RateCheckedChanged(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            var radioButton = RateRadioGroup.FindViewById<RadioButton>(RateRadioGroup.CheckedRadioButtonId);
            if (radioButton != null)
            {
                Answer.Rating = int.Parse(radioButton.Text);
            }
        }


    }
    public class EvaluationRecyclerViewAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }

}