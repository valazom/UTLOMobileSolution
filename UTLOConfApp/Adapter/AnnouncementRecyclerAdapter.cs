﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Content;
using System.Collections.Generic;
using Android.App;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    class AnnouncementRecyclerAdapter : RecyclerView.Adapter
    {
        public event EventHandler<AnnouncementRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<AnnouncementRecyclerAdapterClickEventArgs> ItemLongClick;

        Activity context;
        List<Announcement> _announcement;
        public AnnouncementRecyclerAdapter(Activity context, List<Announcement> announcement)
        {
            this.context = context;
            _announcement = announcement;

        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            var itemView = LayoutInflater.From(context).Inflate(Resource.Layout.AnnouncementListviewlayout, parent, false);

            var vh = new AnnouncementRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = _announcement[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AnnouncementRecyclerAdapterViewHolder;
            holder.SetData(item);
            //holder.TextView.Text = items[position];
        }

        public override int ItemCount => _announcement.Count;
        public Announcement GetItem(int position)
        {
            return _announcement[position];
        }

        void OnClick(AnnouncementRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(AnnouncementRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class AnnouncementRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView Title { get; set; }

        public ImageView AnnImage { get; set; }

        public TextView ClickInfo { get; set; }


        public AnnouncementRecyclerAdapterViewHolder(View itemView, Action<AnnouncementRecyclerAdapterClickEventArgs> clickListener,
                            Action<AnnouncementRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            //TextView = v;
            InflateControls(itemView);
            itemView.Click += (sender, e) => clickListener(new AnnouncementRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new AnnouncementRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
        private void InflateControls(View itemView)
        {
            AnnImage = itemView.FindViewById<ImageView>(Resource.Id.AnnimageView);
            Title = itemView.FindViewById<TextView>(Resource.Id.AnnouncementtextView);
            ClickInfo = itemView.FindViewById<TextView>(Resource.Id.ClickAnnouncementtextView);
        }
        public void SetData(Announcement announcement)
        {
            Title.Text = announcement.Title;
            ClickInfo.Text = "Click here to view topics";
            AnnImage.SetBackgroundResource(Resource.Drawable.AnnImage);
        }
    }

    public class AnnouncementRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}