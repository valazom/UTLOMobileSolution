﻿using System;
using System.Linq;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.App;
using System.Collections.Generic;
using Java.Lang;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Adapter
{
    public class BookofAbstractsRecyclerAdapter : RecyclerView.Adapter, IFilterable
    {
        public event EventHandler<BookofAbstractsRecyclerAdapterClickEventArgs> ItemClick;
        public event EventHandler<BookofAbstractsRecyclerAdapterClickEventArgs> ItemLongClick;
        Activity context;
        List<Abstracts> _abstractItems;
        List<Abstracts> guests;
        public BookofAbstractsRecyclerAdapter(Activity context, List<Abstracts> abstractsItems)
        {
            this.context = context;
            _abstractItems = abstractsItems;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            itemView = LayoutInflater.From(context).
                   Inflate(Resource.Layout.BookofAbstractListView, parent, false);

            var vh = new BookofAbstractsRecyclerAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = _abstractItems[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as BookofAbstractsRecyclerAdapterViewHolder;

            holder.SetData(item);
            //holder.TextView.Text = items[position];
        }

        public override int ItemCount => _abstractItems.Count;

        public Abstracts GetItem(int position)
        {
            return _abstractItems[position];
        }

        public Filter Filter => new BookofAbstractsFilter(this);

        void OnClick(BookofAbstractsRecyclerAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(BookofAbstractsRecyclerAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

        public class BookofAbstractsFilter : Filter
        {
            BookofAbstractsRecyclerAdapter _adapter;
            public BookofAbstractsFilter(BookofAbstractsRecyclerAdapter adapter)
            {
                _adapter = adapter;
            }

            protected override FilterResults PerformFiltering(ICharSequence constraint)
            {
                var returnobj = new FilterResults();
                var results = new List<Abstracts>();
                if (_adapter.guests == null)
                {
                    _adapter.guests = _adapter._abstractItems;
                }

                if (constraint == null) return returnobj;

                if (_adapter.guests != null && _adapter.guests.Any())
                {
                    results.AddRange(
                        _adapter.guests.Where(
                            s => (s.Title).ToLower().Contains(constraint.ToString().ToLower())));
                    // if nothing returns conduct search on the theme
                    if (results.Count == 0)
                    {
                        results.AddRange(
                        _adapter.guests.Where(
                            s => (s.Theme).ToLower().Contains(constraint.ToString().ToLower())));
                    }
                }
                returnobj.Values = FromArray(results.Select(r => r.ToJavaObject()).ToArray());
                returnobj.Count = results.Count;

                constraint.Dispose();

                return returnobj;
            }

            protected override void PublishResults(ICharSequence constraint, FilterResults results)
            {
                using (var values = results.Values)
                    _adapter._abstractItems = values.ToArray<Java.Lang.Object>()
                                        .Select(r => r.ToNetObject<Abstracts>()).ToList();

                constraint.Dispose();
                results.Dispose();
            }


        }

    }

    public class BookofAbstractsRecyclerAdapterViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public TextView Title { get; set; }
        public TextView Theme { get; set; }

        public BookofAbstractsRecyclerAdapterViewHolder(View itemView, Action<BookofAbstractsRecyclerAdapterClickEventArgs> clickListener,
                            Action<BookofAbstractsRecyclerAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            InflateControls(itemView);
            itemView.Click += (sender, e) => clickListener(new BookofAbstractsRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new BookofAbstractsRecyclerAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
        private void InflateControls(View itemView)
        {
            Title = itemView.FindViewById<TextView>(Resource.Id.abstractTitleTextView);
            Theme = itemView.FindViewById<TextView>(Resource.Id.abstractThemeTextView);
        }
        public void SetData(Abstracts abstracts)
        {
            Title.Text = "Title: " + abstracts.Title;
            Theme.Text = "Theme: " + abstracts.Theme;
        }
    }

    public class BookofAbstractsRecyclerAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}