﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Iid;
using Firebase.Messaging;
using Android.Util;

namespace UTLOConfApp
{
    [Service]
    [IntentFilter(new[] {"com.google.firebase.INSTANCE_ID_EVENT"})]
    public class MyFireBaseIIDService : Firebase.Iid.FirebaseInstanceIdService
    {
        const string TOPIC = "UTLOHEC2017";
        const string TAG = "FireBaseInstanceService";
        public override void OnTokenRefresh()
        {
            Firebase.FirebaseApp.InitializeApp(this);
            var token = FirebaseInstanceId.Instance.Token;
            Log.Info(TAG, token);
            SendTokenToApplicationServer(token);
            SubscribeToTopic();
        }

        private void SubscribeToTopic()
        {
            FirebaseMessaging.Instance.SubscribeToTopic(TOPIC);
        }

        private void SendTokenToApplicationServer(string token)
        {
            // will be implemented if needed
        }
    }
}