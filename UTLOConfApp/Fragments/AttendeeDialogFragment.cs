using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;

using Android.Graphics.Drawables;
using Android.Graphics;
using Newtonsoft.Json;

namespace UTLOConfApp.Fragments
{
    public class AttendeeDialogFragment : Android.Support.V4.App.DialogFragment
    {
        Attendees attendee;
        Button contactButton;
        Button abstractButton;
        Button BiographyButton;
        Button DismissButton;
        TextView NameTtextView;
        TextView positionTextView;
        TextView affilationTextView;
        TextView addressTextView;
        Abstracts attendeeAbstract;
        public AttendeeDialogFragment(Attendees att)
        {
            attendee = att;
            //attendeeAbstract = AbstractRepository.getAbstractbyAttendee(attendee);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.Inflate(Resource.Layout.AttendeeDialogFragment, container, false);
            LoadUI(rootView);
            return rootView;
        }

        private void LoadUI(View rootView)
        {
            NameTtextView = rootView.FindViewById<TextView>(Resource.Id.sattNametextView);
            positionTextView = rootView.FindViewById<TextView>(Resource.Id.sattpositiontextView);
            affilationTextView = rootView.FindViewById<TextView>(Resource.Id.sattaffilationtextView);
            addressTextView = rootView.FindViewById<TextView>(Resource.Id.sattaddresstextView);
            NameTtextView.Text = attendee.status + " " + attendee.Name;
            positionTextView.Text = attendee.Position;
            affilationTextView.Text = attendee.Affiliation;
            addressTextView.Text = attendee.Address;

            contactButton = rootView.FindViewById<Button>(Resource.Id.ContactimageButton);
            abstractButton = rootView.FindViewById<Button>(Resource.Id.AbstractimageButton);
            BiographyButton = rootView.FindViewById<Button>(Resource.Id.BiographyimageButton);
            BiographyButton.Visibility = ViewStates.Gone;
            DismissButton = rootView.FindViewById<Button>(Resource.Id.DismissButton);
            DismissButton.Click += DismissButton_Click;
            contactButton.Click += ContactButton_Click;
            abstractButton.Visibility = ViewStates.Gone;
            //if (attendeeAbstract != null)
            //{
            //    abstractButton.Click += AbstractButton_Click;
            //}
            //else
            //{
            //    // make sure that abstract button deos show when the attendee is not presenting
            //    abstractButton.Visibility = ViewStates.Gone;
            //}

            if (!string.IsNullOrEmpty(attendee.Biography))
            {
                //BiographyButton.Click += BiographyButton_Click;
            }
            else
            {
                //Make sure the Bigrapghy does not show button if none is provided
                BiographyButton.Visibility = ViewStates.Gone;
            }
        }

        public override void OnResume()
        {
            Dialog.Window.SetLayout(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            // Make sure there is no background behind our view
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            // Disable standard dialog styling/frame/theme: our custom view should create full UI
            SetStyle(Android.Support.V4.App.DialogFragment.StyleNoFrame, Android.Resource.Style.Theme);

            base.OnResume();
        }

        private void DismissButton_Click(object sender, EventArgs e)
        {
            Dismiss();
        }

        //private void BiographyButton_Click(object sender, EventArgs e)
        //{
        //    Intent BiographyIntent = new Intent(this.Activity, typeof(BiographyActivity));
        //    string serializedAttendee = JsonConvert.SerializeObject(attendee);
        //    BiographyIntent.PutExtra("AttendeeId", serializedAttendee);
        //    StartActivity(BiographyIntent);
        //}

        //private void AbstractButton_Click(object sender, EventArgs e)
        //{
        //    Intent abstractIntent = new Intent(this.Activity, typeof(AbstractActivity));
        //    abstractIntent.PutExtra("AbstractId", attendeeAbstract.abstractId);
        //    StartActivity(abstractIntent);
        //}

        private void ContactButton_Click(object sender, EventArgs e)
        {
            Intent ContactIntent = new Intent(this.Activity, typeof(ContactAttendeeActivity));
            string serializedAttendee = JsonConvert.SerializeObject(attendee);
            ContactIntent.PutExtra("AttendeeId", serializedAttendee);
            StartActivity(ContactIntent);

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                DismissButton.Click -= DismissButton_Click;
            }
        }
    }
}