﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;
using UTLOConfApp.Adapter;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace UTLOConfApp.Fragments
{
    public class TopicLayoutFragment : Android.Support.V4.App.DialogFragment
    {
        List<Topic> _topics;
        ListView topicsListView;
        TopicsAdapter topicsAdapter;
        Button closeButton;
        Button topicItemsButton;
        public TopicLayoutFragment(List<Topic> topics)
        {
            _topics = topics;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var rootView = inflater.Inflate(Resource.Layout.TopicsDialoglayout, container, false);
            topicsListView = rootView.FindViewById<ListView>(Resource.Id.topicsListView);
            closeButton = rootView.FindViewById<Button>(Resource.Id.CloseButton);
            topicItemsButton = rootView.FindViewById<Button>(Resource.Id.topicItemsButton);
            topicItemsButton.Text = "Topic items";
            closeButton.Click += CloseButton_Click;
            topicsAdapter = new TopicsAdapter(Activity, _topics);
            topicsListView.Adapter = topicsAdapter;
            topicsListView.FastScrollEnabled = true;
            return rootView;
        }


        public override void OnResume()
        {
            Dialog.Window.SetLayout(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            // Make sure there is no background behind our view
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            // Disable standard dialog styling/frame/theme: our custom view should create full UI
            SetStyle(Android.Support.V4.App.DialogFragment.StyleNoFrame, Android.Resource.Style.Theme);

            base.OnResume();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                closeButton.Click -= CloseButton_Click;
            }
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
    }
}