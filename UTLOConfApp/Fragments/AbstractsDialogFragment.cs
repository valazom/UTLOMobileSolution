﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Data.Model.Domain;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;

namespace UTLOConfApp.Fragments
{
    public class AbstractsDialogFragment : Android.Support.V4.App.DialogFragment
    {
        Abstracts _abstracts;
        TextView AuthorsTextView;
        TextView TitleTextView;
        TextView BodyTextView;
        TextView ThemeTextView;
        TextView HeaderTitle;
        Button CloseButton;
        List<Attendees> Authors;
        AbstractsService _abstractsService;

        public AbstractsDialogFragment(Abstracts abstracts)
        {
            _abstracts = abstracts;
            _abstractsService = new AbstractsService();
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.Inflate(Resource.Layout.AbstractsDialoglayout, container, false);
            LoadUI(rootView);
            return rootView;
        }
        private void LoadUI(View rootView)
        {
            //display authors
            AuthorsTextView = rootView.FindViewById<TextView>(Resource.Id.absAuthorTextView);
            ThemeTextView = rootView.FindViewById<TextView>(Resource.Id.absThemeTextView);
            BodyTextView = rootView.FindViewById<TextView>(Resource.Id.absBodyTextView);
            TitleTextView = rootView.FindViewById<TextView>(Resource.Id.absTitleTextView);
            HeaderTitle = rootView.FindViewById<TextView>(Resource.Id.conf_title);
            CloseButton = rootView.FindViewById<Button>(Resource.Id.CloseButton);
            AuthorsTextView.Text = string.Empty;
            if (_abstracts.Authors != null || _abstracts.Authors.Count !=0)
            {
                foreach(var author in _abstracts.Authors)
                {
                    AuthorsTextView.Text += author.Name + ", ";
                }
            }
            HeaderTitle.Text = "Abstract";
            ThemeTextView.Text = _abstracts.Theme;
            TitleTextView.Text = _abstracts.Title;
            BodyTextView.Text = _abstracts.Body;
            CloseButton.Click += CloseButton_Click;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
        private async Task GetAuthors(int id)
        {
            _abstractsService = new AbstractsService();
            await Task.Run(() => _abstractsService.GetAuthorsById(id, Constants.AuthorsAbstracts));
            Authors = _abstractsService.authors;
        }
        public override void OnResume()
        {
            Dialog.Window.SetLayout(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            // Make sure there is no background behind our view
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            // Disable standard dialog styling/frame/theme: our custom view should create full UI
            
            base.OnResume();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                CloseButton.Click -= CloseButton_Click;
            }
        }
    }
}