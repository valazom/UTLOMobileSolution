using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;
using Android.Graphics;
using UTLOConfApp.Adapter;
using UTLOConfApp.Utils;
using UTLO.Mobile.Restful.Service;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp.Fragments
{
    public class SuiteDialogFragment : Android.Support.V4.App.DialogFragment
    {
        private List<Suites> suites;
        private ConferenceDay Day;
        private ListView attListView;
        private Button closeButton;
        public SuiteDialogFragment(List<Suites> item, ConferenceDay day)
        {
            suites = item;
            Day = day;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View rootView = inflater.Inflate(Resource.Layout.SuiteDialogFragment, container, false);
            LoadUI(rootView);
            return rootView;
        }

        private void LoadUI(View rootView)
        {
            attListView = rootView.FindViewById<ListView>(Resource.Id.attListView);
            attListView.Adapter = new SuiteListAdapter(this.Activity, suites);
            attListView.FastScrollEnabled = true;
            attListView.ItemClick += AttListView_ItemClick;

            closeButton = rootView.FindViewById<Button>(Resource.Id.CloseButton);
            closeButton.Click += CloseButton_Click;
        }

        private void AttListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var suite = attListView.Adapter.GetItem(e.Position).Cast<Suites>();
            Intent PresentationIntent = new Intent(this.Activity, typeof(PresentationActivity));
            PresentationIntent.PutExtra("SuiteID", suite.SuitesId);
            PresentationIntent.PutExtra("ConferenceDay", $"{Day.ConferenceDate.Date.Day.DisplayWithSuffix()} of " +
                                        $"{Day.ConferenceDate.Date.ToString("MMMM")}");
            this.StartActivity(PresentationIntent);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            Dismiss();
        }
        public override void OnResume()
        {
            Dialog.Window.SetLayout(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);

            // Make sure there is no background behind our view
            Dialog.Window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

            // Disable standard dialog styling/frame/theme: our custom view should create full UI
            SetStyle(Android.Support.V4.App.DialogFragment.StyleNoFrame, Android.Resource.Style.Theme);

            base.OnResume();
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                closeButton.Click -= CloseButton_Click;
            }
        }
    }
}