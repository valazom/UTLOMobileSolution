using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using UTLOConfApp.Adapter;
using Android.Net;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using UTLO.Data.Model.Domain;
using UTLO.Data.Model.Enums;
using UTLOConfApp.Utils;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Java.Net;

namespace UTLOConfApp.Fragments
{
    public class FirstDayFragment : Android.Support.V4.App.Fragment
    {

        //protected ListView myListView;
        protected List<Programme> programmes;
        protected ConferenceDay day;
        protected SuiteService suiteService;
        protected List<Suites> Suites;
        protected CacheOperation<Suites> cache;
        protected RecyclerView recyclerView;
        protected ProgItemRecyclerAdapter recyclerViewAdapter;

        public FirstDayFragment(List<Programme> programmes,
                                ConferenceDay Day)
        {
            this.programmes = programmes;
            day = Day;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View rootView = inflater.Inflate(Resource.Layout.FirstDayFragments, container, false);
            //myListView = rootView.FindViewById<ListView>(Resource.Id.progItemListView);
            ////LoadFragmentData();
            //myListView.Adapter = new ProgItemAdapter(this.Activity, programmes);
            //myListView.FastScrollEnabled = true;
            //myListView.ItemClick += MyListView_ItemClick;

            recyclerView = rootView.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            recyclerViewAdapter = new ProgItemRecyclerAdapter(this.Activity, programmes);
            recyclerViewAdapter.ItemClick += MyRecylerView_ItemClick;
            recyclerView.SetAdapter(recyclerViewAdapter);

            var linearLayoutManager = new LinearLayoutManager(this.Activity);
            linearLayoutManager.Orientation = LinearLayoutManager.Vertical;
            recyclerView.SetLayoutManager(linearLayoutManager);

            return rootView;
        }
        private async Task GetSuite(int id)
        {
            suiteService = new SuiteService();
            await Task.Run(() => suiteService
                 .GetSuitesByProgrammeIdAsync(id, Constants.SuitesByProgrammeId));
            Suites = suiteService.Suites;
            if (cache != null)
            {
                await cache.InsertObject(ConferenceCacheKeys.ConferenceSuites.ToString() + id, Suites);
            }
        }
        private async void MyRecylerView_ItemClick(object sender, ProgItemRecyclerAdapterClickEventArgs e)
        {
            try
            {
                //var item = myListView.Adapter.GetItem(e.Position).Cast<UTLO_NewModel.Domain.Programme>();
                var item = recyclerViewAdapter.GetItem(e.Position);
                if (item.programmeType == ProgrammeType.MultiVenue)
                {
                    cache = new CacheOperation<Suites>();
                    var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceSuites.ToString() + item.ProgrammeId);
                    if (!cacheData.Item2)
                    {
                        var isonline = await Task.Run(() => isOnline());
                        if (isonline)
                        {

                            ProgressDialog progressdialog = new ProgressDialog(this.Activity);
                            progressdialog.SetCancelable(false);
                            progressdialog.Indeterminate = true;
                            progressdialog.SetMessage("Loading...");
                            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
                            progressdialog.Show();
                            await GetSuite(item.ProgrammeId);
                            progressdialog.Hide();
                            progressdialog.Dismiss();


                        }
                        else
                        {
                            new Android.Support.V7.App.AlertDialog.Builder(this.Activity)
                                    .SetPositiveButton("Yes", (send, args) =>
                                    {
                                        // User pressed yes
                                        this.Activity.Finish();
                                    })
                                    .SetNegativeButton("No", (send, args) =>
                                    {
                                        //this.Activity.Finish();
                                        this.Activity.Finish();
                                    })
                                    .SetMessage("No network connection. You won't be able to get data")
                                    .SetTitle("Network Error")
                                    .Show();
                        }
                    }
                    else
                    {
                        Suites = cacheData.Item1;
                    }
                    if (Suites != null || Suites.Count != 0)
                    {
                        var suiteDialog = new SuiteDialogFragment(Suites, day);
                        suiteDialog.Show(this.Activity.SupportFragmentManager, "Suite items");
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this.Activity)
                            .SetPositiveButton("Yes", (send, args) =>
                            {
                                // User pressed yes
                                this.Activity.Finish();
                            })
                            .SetNegativeButton("No", (send, args) =>
                            {
                                //this.Activity.Finish();
                                this.Activity.Finish();
                            })
                            .SetMessage("No Venues available for this programme")
                            .SetTitle("Message")
                            .Show();
                    }
                }
            }
            catch (Exception)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this.Activity)
                                   .SetPositiveButton("Yes", (send, args) =>
                                   {
                                       // User pressed yes
                                       this.Activity.Finish();
                                   })
                                   .SetNegativeButton("No", (send, args) =>
                                   {
                                       //this.Activity.Finish();
                                       this.Activity.Finish();
                                   })
                                   .SetMessage("Could not load data. Check your internet connection")
                                   .SetTitle("Application Error")
                                   .Show();
            }
        }
        private bool isOnline()
        {
            var cm = (ConnectivityManager)this.Activity.GetSystemService(Context.ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }


    }
}