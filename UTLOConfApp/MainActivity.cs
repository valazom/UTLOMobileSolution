﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android.Views;
using UTLOConfApp.Adapter;
using System;
using Android.Support.V4.View;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Java.Util;
using System.Collections.Generic;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.ServiceRespository;
using UTLOConfApp.Utils;
using UTLO.Mobile.Restful.Service.ViewModels;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLO.Data.Model.Domain;
using Newtonsoft.Json;
using Java.Net;
using UTLOConfApp.Model;
using UTLO.Data.LegacyModels.Model;
using UTLO.Data.LegacyModels.Repository;

namespace UTLOConfApp
{
    [Activity(Label = "HelloToolbar", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class MainActivity : AppCompatActivity, Java.Lang.IRunnable, ViewPager.IPageTransformer
    {
        GridView confGridView;
        TextView conf_name;
        TextView conf_title;
        ListView regListView;
        ViewPager attendeeViewPager;
        ConfItemsRepository confRepo;
        SumProgRepository RegRepo;
        RegListAdapter regListAdapter;
        Timer timer;
        private float MIN_SCALE = 0.85f;
        private float MIN_ALPHA = 0.5f;
        int page = 0;
        List<KeynoteSpeakers> Speakers;
        List<Attendees> KeynoteSpeakers;
        
        int RegCount = 0;
        int progCount = 0;
        IConferenceSummaryRepository summaryRepository;
        CacheOperation<ConferenceSummaryViewModel> cache;
        CacheOperation<Attendees> cacheSpeakers;
        ConferenceDayService conferenceDayService;
        AttendeesService attendeeService;
        private List<ConferenceSummaryViewModel> conferenceDaySummary;
        Intent ServiceNotificationIntent;

        protected async override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            // diaable landscape
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            conf_title = FindViewById<TextView>(Resource.Id.conf_title);
            conf_title.Text = (string)ConferenceSettings.csettings["SubTitle"];

            
            
            //Toolbar will now take on default actionbar characteristics
            SetSupportActionBar(toolbar);
            //ActionBar.Title = "Siyaphumelela 2017";

            summaryRepository = new ConferenceSummaryRepository();
            

            //load the GridView here
            
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await loadSummaryFromCache();
            progressdialog.Hide();
            progressdialog.Dismiss();


            
        }
        private async Task loadSummaryFromCache()
        {
            try
            {
                conferenceDayService = new ConferenceDayService();
                cache = new CacheOperation<ConferenceSummaryViewModel>();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceDaySummary);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => IsOnline());

                    if (isonline)
                    {
                        await Task.Run(() => conferenceDayService
                               .GetConferenceDaySummary(Constants.ConferenceDaySummary));
                        conferenceDaySummary = conferenceDayService.ConferenceDaySummary;
                        CacheOperation<ConferenceSummaryViewModel>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferenceDaySummary,
                            conferenceDaySummary, CacheMode.permanent);
                    }
                    else
                    {
                        StartActivity(typeof(NoNetworkActivity));
                    }
                }
                else
                {
                    conferenceDaySummary = cacheData.Item1;

                }
                if(conferenceDaySummary != null)
                {
                    //load the ViewPager

                    // load conference summary
                    if (conferenceDaySummary.Count != 0 )
                    {
                        LoadConferenceSummary();

                        await LoadKeynoteSpeakers();

                        pageSwitcher(3);
                        //Load conference gridView menu

                        

                        //notification services here 
                        ServiceNotificationIntent = new Intent(this, typeof(ServiceNotification));
                        var serializedConferenceSummary = JsonConvert.SerializeObject(conferenceDaySummary);
                        ServiceNotificationIntent.PutExtra("data", serializedConferenceSummary);
                        StartService(ServiceNotificationIntent); 
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {
                                   
                               })
                               .SetMessage("Conference summary data not loaded or not available")
                               .SetTitle("Message")
                               .Show();
                    }

                }
                
                LoadConferenceMenu();
            }
            catch (Exception ex)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {
                                   Finish();
                               })
                               .SetNegativeButton("No", (sender, args) =>
                               {
                                   Finish();
                               })
                               .SetMessage("Application Error. Check your internet connection")
                               .SetTitle("Message")
                               .Show();
            }
            
        }

        private void LoadConferenceMenu()
        {
            confGridView = FindViewById<GridView>(Resource.Id.gridview);
            confRepo = new ConfItemsRepository();
            var items = ConferenceItemsMenu.GetData();
            for (int i = 0; i < items.Count; i++)
            {
                confRepo.AddItem(items[i].ImageId, items[i].Title);
            }
            confGridView.Adapter = new ConfListAdpater(this, confRepo.getAllConfitems);
            confGridView.ItemClick += confGridViewItemClick;
        }

        private void LoadKeyNoteSpeakers()
        {
            
            //Speakers = KeynoteSpeakersRepository.getAllSpeakers;
            //LoadSpeakersImages(ref Speakers);
            attendeeViewPager = FindViewById<ViewPager>(Resource.Id.RegViewPager);
            attendeeViewPager.Adapter = new RegPagerAdapter(this, KeynoteSpeakers);
            attendeeViewPager.SetPageTransformer(true, this);
        }

        private void LoadConferenceSummary()
        {
            regListView = FindViewById<ListView>(Resource.Id.RegListView);
            summaryRepository.AddItems(Resource.Drawable.filetype_reg_icon, Resource.Drawable.event_icon,
                                conferenceDaySummary[RegCount],
                                conferenceDaySummary[RegCount].programmes[progCount]);
            regListAdapter = new RegListAdapter(this, summaryRepository.GetProgList());
            regListView.Adapter = regListAdapter;
            regListView.ItemClick += regListViewItemClick;
        }

        private async Task LoadKeynoteSpeakers()
        {
            try
            {
                attendeeService = new AttendeesService();
                cacheSpeakers = new CacheOperation<Attendees>();
                var cacheData = await cacheSpeakers.GetObject(ConferenceCacheKeys.ConferenceKeyNoteSpeakers);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => IsOnline());

                    if (isonline)
                    {
                        await Task.Run(() => attendeeService.GetAllKeynoteSpeakers(Constants.KeynoteSpeakers));
                        KeynoteSpeakers = attendeeService.keynotespeakers;
                        CacheOperation<Attendees>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cacheSpeakers.
                            InsertObject(ConferenceCacheKeys.ConferenceKeyNoteSpeakers, 
                            KeynoteSpeakers);

                    }
                    else
                    {
                        StartActivity(typeof(NoNetworkActivity));
                    }
                }
                else
                {
                    KeynoteSpeakers = cacheData.Item1;
                }
                if(KeynoteSpeakers != null)
                {
                    if (KeynoteSpeakers.Count != 0)
                    {
                        LoadKeyNoteSpeakers(); 
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {

                               })
                               .SetMessage("Keynote speakers data not loaded or not available")
                               .SetTitle("Message")
                               .Show();
                    }
                }
                
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public override void OnBackPressed()
        {

            new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {
                                   if (ServiceNotificationIntent != null)
                                   {
                                       StopService(ServiceNotificationIntent); 
                                   }
                                   Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
                               })
                               .SetNegativeButton("No", (sender, args) =>
                               {
                                   
                               })
                               .SetMessage("Do you want to quit the application")
                               .SetTitle("Message")
                               .Show();
            
        }
        protected override void OnDestroy()
        {
            Finish();
            base.OnDestroy();
        }
        private bool IsOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        //private void LoadSpeakersImages(ref List<KeynoteSpeakers> speakers)
        //{
        //    for (int i = 0; i < SpeakersImage.Length; i++)
        //    {
        //        speakers[i].SpeakerId = SpeakersImage[i];
        //    }
        //}
        public void pageSwitcher(int seconds)
        {
            timer = new Timer();
            timer.ScheduleAtFixedRate(new RemindTask(this), 0, seconds * 1000);
        }

        private void regListViewItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {

        }

        private void confGridViewItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            
            var item = confRepo.getAllConfitems[e.Position];
            Intent chooser;
            switch (item.getConf_title)
            {
                case "Programme":
                    Intent progIntent = new Intent(this, typeof(ProgrammeActivity));
                    StartActivity(progIntent);
                    break;
                case "Presenters":
                    Intent AttendeeIntent = new Intent(this, typeof(AttendeeActivity));
                    StartActivity(AttendeeIntent);
                    break;
                case "Abstracts":
                    Intent speakerIntent = new Intent(this, typeof(BookofAbstractsActivity));
                    StartActivity(speakerIntent);
                    break;
                case "Accomodation":
                    Intent AccommodationIntent = new Intent(this, typeof(AccommodationListActivity));
                    StartActivity(AccommodationIntent);
                    break;
                case "Location":
                    Intent LocationIntent = new Intent(this, typeof(MapLocationConferenceActivity));
                    StartActivity(LocationIntent);
                    break;
                case "Evaluation":
                    Intent EvaluationIntent = new Intent(this, typeof(EvaluationActivity));
                    StartActivity(EvaluationIntent);
                    break;
                case "Announcements":
                    Intent AnnouncementIntent = new Intent(this, typeof(AnnouncementActivity));
                    StartActivity(AnnouncementIntent);
                    break;
                case "HEC Website":
                    Intent WebHecIntent = new Intent(Intent.ActionView);
                    chooser = Intent.CreateChooser(WebHecIntent, "Open with");
                    WebHecIntent.SetData(Android.Net.Uri.Parse("http://hec.ukzn.ac.za/"));
                    StartActivity(WebHecIntent);
                    break;
                case "UTLO Website":
                    Intent WebIntent = new Intent(Intent.ActionView);
                    chooser = Intent.CreateChooser(WebIntent, "Open with");
                    WebIntent.SetData(Android.Net.Uri.Parse("http://utlo.ukzn.ac.za/Homepage.aspx/"));
                    StartActivity(WebIntent);
                    break;
                case "UKZN Website":
                    Intent WebsIntent = new Intent(Intent.ActionView);
                    chooser = Intent.CreateChooser(WebsIntent, "Open with");
                    WebsIntent.SetData(Android.Net.Uri.Parse("http://www.ukzn.ac.za/"));
                    StartActivity(WebsIntent);
                    break;
                default:
                    Toast.MakeText(this, "An invalid item was choosen", ToastLength.Short).Show();
                    break;

            }
        }

        /// <summary>
        /// Implements the Java.Lang.IRunnable interface
        /// </summary>
        public void Run()
        {
            //keep check till you get to the end of the list
            //RegCount = RegCount == 0 ? RegCount++ : RegCount;
            if (page <= attendeeViewPager.Adapter.Count)
                attendeeViewPager.SetCurrentItem(page++, true);
            // when you get to end go and start from the beginning
            else
            {
                page = 0;
            }
            if (conferenceDaySummary != null)
            {
                if (RegCount < conferenceDaySummary.Count)
                {
                    
                    var day = conferenceDaySummary[RegCount];
                    if (progCount < day.programmes.Count)
                    {
                        ReloadRegListView(day, day.programmes[progCount]);
                        progCount++;
                    }

                    else
                    {
                        progCount = 0;
                        RegCount++;
                    } 
                }
                else
                {
                    RegCount = 0;
                }
            }
            
        }



        private void ReloadRegListView(ConferenceSummaryViewModel model, Programme programme)
        {
            summaryRepository.GetProgList().Clear();
            summaryRepository.AddItems(Resource.Drawable.filetype_reg_icon, Resource.Drawable.event_icon, model, programme);
            regListView.Adapter = new RegListAdapter(this, summaryRepository.GetProgList());
            regListAdapter.NotifyDataSetChanged();
        }

        /// <summary>
        ///  Implements the ViewPager.IPageTransformer for the transforming the page of the viewpager
        ///  as it slides
        /// </summary>
        /// <param name="page">reference to the viewpager</param>
        /// <param name="position">The current position of the viewpager</param>
        public void TransformPage(View page, float position)
        {
            int pageWidth = page.Width;
            int pageHeight = page.Height;
            if (position < -1)
            {
                page.Alpha = 0;

            }
            else if (position <= 1)
            {
                float scalefactor = Math.Max(MIN_SCALE, 1 - Math.Abs(position));
                float vertMargin = pageHeight * (1 - scalefactor) / 2;
                float hortMargin = pageWidth * (1 - scalefactor) / 2;

                if (position < 0)
                {
                    page.TranslationX = hortMargin - vertMargin / 2;
                }
                else
                {
                    page.TranslationX = -hortMargin + vertMargin / 2;
                }

                page.ScaleX = scalefactor;
                page.ScaleY = scalefactor;

                page.Alpha = MIN_ALPHA + (scalefactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA);
            }
            else
            {
                page.Alpha = 0;
            }
        }


        
        /// <summary>
        /// A timerTask that defines the Runnable to run on the UIThread
        /// </summary>
        public class RemindTask : TimerTask
        {
            Activity context;
            public RemindTask(Activity context)
            {
                this.context = context;
            }
            /// <summary>
            /// 
            /// </summary>
            public override void Run()
            {
                context.RunOnUiThread(context as Java.Lang.IRunnable);
            }
        }

    }
}

