
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using UTLOConfApp.Utils;
using UTLO.Data.LegacyModels.Model;

namespace UTLOConfApp
{
    [Activity(Label = "BiographyActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class BiographyActivity : AppCompatActivity
    {
        TextView BiographyTextView;
        TextView conf_name;
        TextView HeaderTitle;
        KeynoteSpeakers KeynoteSpeaker;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BiographyActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetUpToolbar();

            BiographyTextView = FindViewById<TextView>(Resource.Id.BiographyTextView);
            //Get the speaker Id passed from the Main Activity
            int attid = Intent.Extras.GetInt("SpeakerId");
            // Get the Biography note passed from the Main Activity
            var biographynote = Intent.Extras.GetString("Biography");
            // Assign the Biography note to the Biography textview
            BiographyTextView.Text = biographynote;

           
        }

        private void SetUpToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            HeaderTitle = FindViewById<TextView>(Resource.Id.conf_title);
            HeaderTitle.Text = "Biography";
        }

        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if(item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {
           
        }
    }
}