using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.View;
using UTLOConfApp.Fragments;
using Android.Support.V7.App;
using Android.Support.Design.Widget;
using UTLOConfApp.Adapter;
using Android.Support.V4.App;
using Android.Content.Res;
using UTLOConfApp.Utils;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLO.Data.Model.Domain;
using Java.Net;
//using UTLOConfApp.Interfaces;

namespace UTLOConfApp
{
    [Activity(Label = "ProgrammeActivity", Theme = "@style/MyTheme")]
    public class ProgrammeActivity : AppCompatActivity, TabLayout.IOnTabSelectedListener
    {
        TabLayout tabLayout;
        TextView conf_name;
        ViewPager viewpager;
        TextView conf_title;
        Android.Support.V4.App.Fragment[] fragments;
        ConferenceDayService conferenceDayService;
        Java.Lang.ICharSequence[] titles;
        CacheOperation<ConferenceDay> cache;
        List<ConferenceDay> conferenceDays;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ProgrammeActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if(Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];

            conf_title = FindViewById<TextView>(Resource.Id.conf_title);
            conf_title.Text = "Programme";
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            
            
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await InitTabLayout();
            progressdialog.Hide();
            progressdialog.Dismiss();
            
        }
        //public override bool OnCreateOptionsMenu(IMenu menu)
        //{
        //    MenuInflater.Inflate(Resource.Menu.home, menu);

        //    return base.OnCreateOptionsMenu(menu);
        //}
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {

                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }

        private async Task LoadFragmentsData()
        {
            
            conferenceDayService = new ConferenceDayService();
            await Task.Run(() => conferenceDayService.GetAllConferenceDay(Constants.ProgrammesConferenceDays));
            conferenceDays = conferenceDayService.ConferenceDays;
            CacheOperation<Programme>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));

            await cache.InsertObject(ConferenceCacheKeys.ConferenceConferenceDays, conferenceDays);
        }
        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        private async Task InitTabLayout()
        {
            try
            {
                cache = new CacheOperation<ConferenceDay>();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceConferenceDays);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => isOnline());

                    if (isonline)
                    {
                        await LoadFragmentsData();
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    // User pressed yes
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection. You won't be able to get data")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    conferenceDays = cacheData.Item1;
                }
                if (conferenceDays != null)
                {
                    CreateTabLayout();
                }

            }
            catch (Exception)
            {
                new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {
                                   Finish();
                                })
                               .SetNegativeButton("No", (sender, args) =>
                               {
                                   Finish();
                               })
                               .SetMessage("Could not load data")
                               .SetTitle("Application Error")
                               .Show();

            }

        }

        private void CreateTabLayout()
        {
            var conferencedays = conferenceDays;
            fragments = new Android.Support.V4.App.Fragment[conferencedays.Count];
            string[] tabtitles = new string[conferencedays.Count];
            for (int i = 0; i < conferencedays.Count; i++)
            {
                fragments[i] = new FirstDayFragment(conferencedays[i].programme.ToList(),
                                                   conferencedays[i]);
                tabtitles[i] = $"{conferencedays[i].ConferenceDate.Date.Day.DisplayWithSuffix()} of " +
                    $"{conferencedays[i].ConferenceDate.Date.ToString("MMMM")}";
            }
            titles = CharSequence.ArrayFromStringArray(tabtitles);
            tabLayout = FindViewById<TabLayout>(Resource.Id.sliding_tabs);
            tabLayout.SetTabTextColors(Android.Graphics.Color.Aqua, Android.Graphics.Color.AntiqueWhite);
            viewpager = FindViewById<ViewPager>(Resource.Id.tabviewpager);
            viewpager.Adapter = new TabFragmentsPageAdapter(this.SupportFragmentManager,
                fragments, titles);
            viewpager.Adapter.NotifyDataSetChanged();
            viewpager.AddOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            //viewpager.PageSelected += ViewPageSelected;
            tabLayout.SetupWithViewPager(viewpager);

            tabLayout.AddOnTabSelectedListener(this);
        }


        public void OnTabReselected(TabLayout.Tab tab)
        {
            viewpager.CurrentItem = tab.Position;

        }

        public void OnTabSelected(TabLayout.Tab tab)
        {
            viewpager.CurrentItem = tab.Position;

        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
            

        }



        
    }
    


}