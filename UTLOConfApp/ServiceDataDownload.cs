﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using UTLOConfApp.Utils;
using UTLO.Data.Model.Domain;
using Android.Net;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Graphics;
using Android.Util;
using Java.Net;

namespace UTLOConfApp
{
    [Service]
    class ServiceDataDownload : IntentService
    {
        ConferenceDayService conferenceService;
        AttendeesService attendeeService;
        AbstractsService abstractService;
        AccommodationService accommodationService;
        AnnouncementService announcementService;
        EvaluationService evaluationService;
        List<Attendees> KeynoteSpeakers;
        Bitmap bitmapImage;
        Intent NoNetworkIntent;
        int imagecount;
        public ServiceDataDownload() : base("MyServiceWorkerThread")
        {
            Initialize();
        }
        private void Initialize()
        {
            conferenceService = new ConferenceDayService();
            attendeeService = new AttendeesService();
            abstractService = new AbstractsService();
            accommodationService = new AccommodationService();
            announcementService = new AnnouncementService();
            evaluationService = new EvaluationService();
            KeynoteSpeakers = new List<Attendees>();
            imagecount = 0;
        }
        public override void OnCreate()
        {
            NoNetworkIntent = new Intent(this, typeof(NoNetworkActivity));
            NoNetworkIntent.AddFlags(ActivityFlags.NewTask);
            base.OnCreate();
        }
        protected override void OnHandleIntent(Intent intent)
        {
            try
            {
                //DownLoad all data in the background
                //Download ConferenceDay data
                GetAllConferenceDays();
                //Download Attendees data
                GetAllAttendees();
                //Download abstract data
                GetAllAbstracts();
                //Download accommodation data
                GetAllAccommodation();
                //Download Evaluation data
                GetAllEvaluation();
                //Download Announcement data
                GetAllAnnouncement();
                // Download KeynoteSpeakers
                GetAllKeynoteSpeakers();

                StopSelf();

            }
            catch (Exception ex)
            {

                StartActivity(NoNetworkIntent);
            }
        }
        private async Task getBitmapImage(string path,Attendees attendee)
        {
            await attendeeService.GetKeynoteSpeakerImage(path);
           
            CacheData(ConferenceCacheKeys.keynotespeakerImage.ToString() +
                attendee.AttendeesId.ToString(),
                           attendeeService.speakerImage);

        }
        // get images for all keynotespeakers 
        

        private void GetAllKeynoteSpeakers()
        {
            var cacheData = InitCache<Attendees>(ConferenceCacheKeys.ConferenceKeyNoteSpeakers);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    attendeeService.GetAllKeynoteSpeakers(Constants.KeynoteSpeakers).Wait();
                    CacheData(ConferenceCacheKeys.ConferenceKeyNoteSpeakers,
                            attendeeService.keynotespeakers);
                    
                }
            }
        }

        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        //insert to the cache
        private void CacheData<TParam>(ConferenceCacheKeys key,
            List<TParam> cacheData) where TParam: class
        {
            CacheOperation<TParam> cache = new CacheOperation<TParam>();
            if(cacheData != null)
            {
                var cacheItems = cache.GetObject(key);
                if (!cacheItems.Result.Item2)
                {
                    cache.InsertObject(key, cacheData,CacheMode.permanent); 
                }
            }
        }
        //Insert Image in cache
        private void CacheData<TParam>(string key,TParam cacheData)
        {
            CacheOperation<TParam> cache = new CacheOperation<TParam>();
            if (cacheData != null)
            {
                var cacheItems = cache.GetObjectOther(key);
                if (!cacheItems.Result.Item2)
                {
                    cache.InsertObject(key,cacheData);
                }
            }
        }
        private Tuple<List<T>,bool> InitCache<T>(ConferenceCacheKeys keys) where T: class
        {
            CacheOperation<T> cache = new CacheOperation<T>();
            var cacheData = cache.GetObject(keys).Result;
            return cacheData;
        }
        private Tuple<T,bool> InitCache<T>(string keys)
        {
            CacheOperation<T> cache = new CacheOperation<T>();
            var cacheData = cache.GetObjectOther(keys).Result;
            return cacheData;
        }
        private void GetAllConferenceDays()
        {
            var cacheData = InitCache<ConferenceDay>(ConferenceCacheKeys.ConferenceConferenceDays);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    conferenceService.GetAllConferenceDay(Constants.ProgrammesConferenceDays)
                                    .ContinueWith(result =>
                                    CacheData(ConferenceCacheKeys.ConferenceConferenceDays,
                                    conferenceService.ConferenceDays));
                }
            }
        }
        private void GetAllAttendees()
        {
            var cacheData = InitCache<Attendees>(ConferenceCacheKeys.ConferenceAttendees);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    attendeeService.GetAllAttendees(Constants.Attendees)
                        .ContinueWith(result =>
                        CacheData(ConferenceCacheKeys.ConferenceAttendees,
                        attendeeService.attendees));
                }
            }
        }
        private void GetAllAbstracts()
        {
            var cacheData = InitCache<Abstracts>(ConferenceCacheKeys.ConferenceAttendees);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    abstractService.GetAllAbstracts(Constants.Abstracts)
                        .ContinueWith(result =>
                         CacheData(ConferenceCacheKeys.ConferenceAbstracts,
                         abstractService.mabstracts));
                }
            }
        }
        private void GetAllAccommodation()
        {
            var cacheData = InitCache<Accommodation>(ConferenceCacheKeys.ConferenceAttendees);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    accommodationService.GetAllAccommodationAsync(Constants.Accommodation)
                        .ContinueWith(result =>
                        CacheData(ConferenceCacheKeys.ConferenceAccommodation,
                        accommodationService.accommodations));
                }
            }
        }
        private void GetAllEvaluation()
        {
            var cacheData = InitCache<EvaluationQuestions>(ConferenceCacheKeys.ConferenceAttendees);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    evaluationService.GetAllEvaluationsAsync(Constants.EvaluationQuestions)
                        .ContinueWith(result =>
                        CacheData(ConferenceCacheKeys.ConferenceEvaluation,
                        evaluationService.evaluationQuestions));
                }
            }
        }
        private void GetAllAnnouncement()
        {
            var cacheData = InitCache<Announcement>(ConferenceCacheKeys.ConferenceAttendees);
            if (!cacheData.Item2)
            {
                if (isOnline())
                {
                    announcementService.GetAllAnnouncement(Constants.Announcements)
                        .ContinueWith(result =>
                        CacheData(ConferenceCacheKeys.ConferenceAnnouncement, 
                        announcementService.announcements));
                }
            }
        }

    }
}