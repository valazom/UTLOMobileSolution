using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using UTLOConfApp.Utils;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using Android.Gms.Common;
using UTLO.Data.Model.Domain;
using Java.Net;
using UTLO.Data.Model.Enums;

namespace UTLOConfApp
{
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        CacheOperation<Settings> settingsCache;
        ConferenceDayService conferenceSummaryService;
        SettingsService settingsService;
        List<Settings> settings;
        Intent ServiceDataDownloadIntent;
        int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
        int settingsCount = 0;

        
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashView);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            //Check if GooglePlay Services is avaliable
            isGooglePlayServicesAvailable();
            // start an intentservice on the background to download all data
            ServiceDataDownloadIntent = new Intent(this, typeof(ServiceDataDownload));
            StartService(ServiceDataDownloadIntent);
            settingsCount = Enum.GetNames(typeof(CustomSettings)).Length;

            try
            {
                var isOnline = await Task.Run(() => IsOnline());
                if (isOnline) // network connectivty
                {
                    await System.Threading.Tasks.Task.Run(() =>
                    {
                        LoadAllStartingData().Wait(); //download conference settings

                    });
                    if (settings != null) // Validate settings object
                    {
                        if (settings.Count == settingsCount) // check all settings were filled
                        {
                            await Task.Run(() => {
                                 CreateSettings();
                            });
                            StartActivity(typeof(MainActivity));

                        }
                        else
                        {
                            throw new ArgumentException("Incomplete settings. Contact your conference administrators.");
                        }
                    }
                    else
                        throw new ArgumentException("Settings not available. Contact your conference administrators");
                }
                else
                {
                    StartActivity(typeof(NoNetworkActivity));
                }
            }
            catch(NullReferenceException ex)
            {
                DisplayErrorMessage(ex.Message);
            }
            catch(ArgumentException ex)
            {
                DisplayErrorMessage(ex.Message);
            }
            catch (Exception)
            {

                var message = "An error occured in fetching conference settings. Try again later";
                DisplayErrorMessage(message);
            }
        }

        private void CreateSettings()
        {
            ConferenceSettings.SetupConferenceSettings(settings);
            CacheOperation<Settings>.CachingTime = TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
        }

        private void isGooglePlayServicesAvailable()
        {
            var googleApi = GoogleApiAvailability.Instance;
            var responseCode = googleApi.IsGooglePlayServicesAvailable(this);
            if (responseCode != ConnectionResult.Success)
            {
                if (googleApi.IsUserResolvableError(responseCode))
                {
                    googleApi.GetErrorDialog(this, responseCode, PLAY_SERVICES_RESOLUTION_REQUEST).Show();
                    
                }
                var message = "Not supported on this device. " +
                                "Remote notification will not be enabled on this app";
                var Title = "Google Play Services";
                DisplayErrorMessage(message, Title);
                
            }
            
        }
        private async Task LoadAllStartingData()
        {
            
            settingsService = new SettingsService();
            
            await Task.Run(() => settingsService.GetAllSettings(Constants.Settings));

            settings = settingsService.Settings;
            
        }

        private void DisplayErrorMessage(string message,string Title="")
        {
            var myDialog = new Android.App.AlertDialog.Builder(this);
            if (Title == "Google Play Services")
            {
                
                myDialog.SetPositiveButton("Yes", (sender, args) =>
                                            {
                                                Finish();
                                            })
                                            .SetTitle(Title)
                                            .SetMessage(message)
                                            .Show();
            }
            else
            {
                new Android.App.AlertDialog.Builder(this)
                                            .SetPositiveButton("Yes", (sender, args) =>
                                            {
                                                Finish();
                                            })
                                            .SetMessage(message)
                                            .Show();
            }
            
        }

        private bool IsOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if(networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        protected override void OnResume()
        {
            base.OnResume();

        }
    }
}