﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace UTLOConfApp
{
    [Activity(Theme = "@style/Theme.Splash",NoHistory = true)]
    public class NoNetworkActivity : Activity
    {
        Button Reconnect;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.NoNetworkActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            Reconnect = FindViewById<Button>(Resource.Id.TryAgaineButton);
            Reconnect.Click += Reconnect_Click;
            // Create your application here
        }

        private void Reconnect_Click(object sender, EventArgs e)
        {
            Finish();
            Intent MainIntent = new Intent(this, typeof(SplashActivity));
            StartActivity(MainIntent);

        }
        public override void OnBackPressed()
        {
            Finish();

        }
    }
}