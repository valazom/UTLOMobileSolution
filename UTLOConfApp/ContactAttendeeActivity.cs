using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Newtonsoft.Json;
using UTLOConfApp.Utils;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp
{
    [Activity(Label = "ContactAttendeeActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class ContactAttendeeActivity : AppCompatActivity
    {
        TextView conf_title;
        TextView conf_name;
        Button sendImageButton;
        Button cancelImageButton;
        EditText mailEditText;
        EditText messageEditText;
        Attendees attendee;
        TextView infoTextView;
        TextView infouseTextView;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ContactAttendeeActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetUpToolbar();
            SetUpControls();
        }

        private void SetUpControls()
        {
            string attId = Intent.Extras.GetString("AttendeeId");
            attendee = JsonConvert.DeserializeObject<Attendees>(attId);
            infoTextView = FindViewById<TextView>(Resource.Id.infoTextView);
            infouseTextView = FindViewById<TextView>(Resource.Id.infouseTextView);
            sendImageButton = FindViewById<Button>(Resource.Id.sendMailButton);
            cancelImageButton = FindViewById<Button>(Resource.Id.cancelMailButton);
            infoTextView.Text = "Click send below and an email will be sent " +
                                   "to " + attendee.status + " " + attendee.Name + " asking " +
                                   "them to contact you via your email address";
            infouseTextView.Text = "Please tap send to post your input, or cancel to close the form.";
            sendImageButton.Click += SendImageButton_Click;
            cancelImageButton.Click += CancelImageButton_Click;
        }

        private void SetUpToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt == BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            conf_title = FindViewById<TextView>(Resource.Id.conf_title);
            conf_title.Text = "Contact";
        }

        private void CancelImageButton_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void SendImageButton_Click(object sender, EventArgs e)
        {
            //initialize email intent
            var email = new Intent(Android.Content.Intent.ActionSend);
            // store intent with email addresses of the intended attendee
            email.PutExtra(Android.Content.Intent.ExtraEmail, new[] { attendee.emailAddress });
            var subject = (string)ConferenceSettings.csettings["Title"] + " Conference contacts";
            email.PutExtra(Intent.ExtraSubject, subject);
            email.SetType("message/rfc822");
            StartActivity(email);
        }
        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}