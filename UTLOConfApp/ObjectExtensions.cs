﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace UTLO_DBS.Model
//{
//    public class JavaHolder : Java.Lang.Object
//    {
//        public readonly object Instance;

//        public JavaHolder(object instance)
//        {
//            Instance = instance;
//        }
//    }

//    public static class ObjectExtensions
//    {
//        public static TObject ToNetObject<TObject>(this Java.Lang.Object value)
//        {
//            if (value == null)
//                return default(TObject);

//            if (!(value is JavaHolder))
//                throw new InvalidOperationException("Unable to convert to .NET object. Only Java.Lang.Object created with .ToJavaObject() can be converted.");

//            TObject returnVal;
//            try { returnVal = (TObject)((JavaHolder)value).Instance; }
//            finally { value.Dispose(); }
//            return returnVal;
//        }

       
//        public static T Cast<T>(this Java.Lang.Object obj) where T : class
//        {
//            var propertyInfo = obj.GetType().GetProperty("Instance");
//            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
//        }
        

//        public static Java.Lang.Object ToJavaObject<TObject>(this TObject value)
//        {
//            if (Equals(value, default(TObject)) && !typeof(TObject).IsValueType)
//                return null;

//            var holder = new JavaHolder(value);

//            return holder;
//        }
//    }
//}

namespace UTLO.Data.Model.Domain
{
    public class JavaHolder : Java.Lang.Object
    {
        public readonly object Instance;

        public JavaHolder(object instance)
        {
            Instance = instance;
        }
    }

    public static class ObjectExtensions
    {
        public static TObject ToNetObject<TObject>(this Java.Lang.Object value)
        {
            if (value == null)
                return default(TObject);

            if (!(value is JavaHolder))
                throw new InvalidOperationException("Unable to convert to .NET object. Only Java.Lang.Object created with .ToJavaObject() can be converted.");

            TObject returnVal;
            try { returnVal = (TObject)((JavaHolder)value).Instance; }
            finally { value.Dispose(); }
            return returnVal;
        }


        public static T Cast<T>(this Java.Lang.Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }


        public static Java.Lang.Object ToJavaObject<TObject>(this TObject value)
        {
            if (Equals(value, default(TObject)) && !typeof(TObject).IsValueType)
                return null;

            var holder = new JavaHolder(value);

            return holder;
        }
    }
}
