using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using UTLOConfApp.Adapter;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using UTLOConfApp.Fragments;
using UTLO.Mobile.Restful.Service.Service.RestService;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using Newtonsoft.Json;
using UTLOConfApp.Utils;
using Java.Net;
using Android.Support.V7.Widget;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp
{
    [Activity(Label = "AttendeeActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class AttendeeActivity : AppCompatActivity
    {
        private ListView AttListtView;
        private Android.Support.V7.Widget.SearchView _searchView;
        TextView conf_Title;
        TextView conf_name;
        AttendeesService attendeeService;
        List<Attendees> attendees;
        CacheOperation<Attendees> cache;
        RecyclerView recyclerView;
        AttendeeRecyclerAdapter attendeeRecyclerAdapter;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AttendeesActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetUpToolbar();

            AttListtView = FindViewById<ListView>(Resource.Id.attListView);
            await LoadDataOnline();

        }

        private async Task LoadDataOnline()
        {
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            //get attendees from service
            await FillListView();
            progressdialog.Hide();
            progressdialog.Dismiss();
        }

        private void SetUpToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            
        }

        private async Task FillListView()
        {
            try
            {
                attendeeService = new AttendeesService();

                cache = new CacheOperation<Attendees>();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceAttendees);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => isOnline());

                    if (isonline) // network connection
                    {
                        await GetAllAttendees();
                        attendees = attendeeService.attendees;
                        // store data in cache
                        CacheOperation<Attendees>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferenceAttendees, attendees);
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection and no data available in store")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    attendees = cacheData.Item1;
                }
                if (attendees != null)
                {

                    attendeeRecyclerAdapter = new AttendeeRecyclerAdapter(this, attendees);
                    recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerView);
                    attendeeRecyclerAdapter.ItemClick += AttListtView_ItemClick;
                    recyclerView.SetAdapter(attendeeRecyclerAdapter);

                    var linearLayoutManager = new LinearLayoutManager(this);
                    linearLayoutManager.Orientation = LinearLayoutManager.Vertical;
                    recyclerView.SetLayoutManager(linearLayoutManager);

                }
            }
            catch (Exception ex)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                               .SetPositiveButton("Yes", (sender, args) =>
                               {
                                   Finish();
                               })
                               .SetNegativeButton("No", (sender, args) =>
                               {
                                   Finish();
                               })
                               .SetMessage("Could not load data")
                               .SetTitle("Application Error")
                               .Show();
            }
        }
        private async Task GetAllAttendees()
        {
            try
            {
                var endpoint = Constants.Attendees;
                await Task.Run(() => attendeeService.GetAllAttendees(endpoint));
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Check your network connection", ToastLength.Long).Show();
            }

        }

        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        private void AttListtView_ItemClick(object sender, AttendeeRecyclerAdapterClickEventArgs e)
        {

            var item = attendeeRecyclerAdapter.GetItem(e.Position);
            var dialog = new AttendeeDialogFragment(item);

            dialog.Show(this.SupportFragmentManager, "dialog");
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {

            MenuInflater.Inflate(Resource.Menu.Attendee, menu);

            var item = menu.FindItem(Resource.Id.action_search);
            // Get instance to the searchview by casting to search view of v7 
            // support Library
            var searchView = MenuItemCompat.GetActionView(item);
            _searchView = searchView.JavaCast<Android.Support.V7.Widget.SearchView>();
            // filter the data of the recyclerviewAdapter based on the serach criteria
            // provided by the user
            _searchView.QueryTextChange += (s, e) => attendeeRecyclerAdapter.Filter.InvokeFilter(e.NewText);
            // Display a toast message of what the user has requested for 
            _searchView.QueryTextSubmit += (s, e) =>
            {
                Toast.MakeText(this, "Searched for: " + e.Query, ToastLength.Short).Show();
                e.Handled = true;
                //attListAdapter.NotifyDataSetChanged();
                attendeeRecyclerAdapter.NotifyDataSetChanged();
            };

            MenuItemCompat.SetOnActionExpandListener(item, new SearchViewExpandListener(attendeeRecyclerAdapter));

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                //attendees.Clear();
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }

    internal class SearchViewExpandListener : Java.Lang.Object, MenuItemCompat.IOnActionExpandListener
    {
        private IFilterable _adapter;

        public SearchViewExpandListener(IFilterable adapter)
        {
            _adapter = adapter;
        }
        public bool OnMenuItemActionCollapse(IMenuItem item)
        {
            _adapter.Filter.InvokeFilter("");
            return true;
        }

        public bool OnMenuItemActionExpand(IMenuItem item)
        {
            return true;
        }
    }
}