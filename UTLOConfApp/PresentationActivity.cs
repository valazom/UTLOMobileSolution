using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using UTLOConfApp.Adapter;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLOConfApp.Utils;
using Java.Net;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp
{
    [Activity(Label = "SuiteActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class PresentationActivity : AppCompatActivity
    {
        TextView conftitle;
        TextView conf_name;
        ListView attListView;
        List<Suites> SuiteItems;
        TextView ConferenceTextView;
        PresentationService presentationService;
        CacheOperation<Presentations> cache;
        List<Presentations> presentations;
        int suiteId = 0;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.PresentationActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            conftitle = FindViewById<TextView>(Resource.Id.conf_title);
            conftitle.Text = "Presentation";
            ConferenceTextView = FindViewById<TextView>(Resource.Id.ConferenceDaytextView);
            ConferenceTextView.Text = Intent.Extras.GetString("ConferenceDay");

            suiteId = Intent.Extras.GetInt("SuiteID");
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            //var SuiteItem = SuitesRepositorys.GetAllSuites.Find(s => s.SuitesId == suiteId);
            await GetPresentation(suiteId);
            progressdialog.Hide();
            progressdialog.Dismiss();

        }
        private async Task GetPresentation(int id)
        {
            try
            {
                cache = new CacheOperation<Presentations>();
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferencePresention.ToString() + id);
                if (!cacheData.Item2)
                {
                    var isonline = await Task.Run(() => isOnline());

                    if (isonline)
                    {
                        presentationService = new PresentationService();
                        await Task.Run(() => presentationService
                            .GetAllPresentation(id, Constants.PresentationBySuiteId));
                        presentations = presentationService.presentations;
                        CacheOperation<Presentations>.CachingTime =
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferencePresention.ToString() + id, presentations);
                    }
                    else
                    {
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection. You won't be able to get data")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    presentations = cacheData.Item1;
                }
                if (presentations != null)
                {
                    attListView = FindViewById<ListView>(Resource.Id.attListView);
                    attListView.Adapter = new PresentationListAdapter(this, presentations);
                    attListView.FastScrollEnabled = true;
                }
            }
            catch (Exception)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("Could not load data ")
                                .SetTitle("Application Error")
                                .Show();
            }
        }
        protected override void OnResume()
        {
            if (presentations != null)
            {
                attListView = FindViewById<ListView>(Resource.Id.attListView);
                attListView.Adapter = new PresentationListAdapter(this, presentations);
                attListView.FastScrollEnabled = true;
            }
            base.OnResume();
        }
        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //return networkInfo != null && networkInfo.IsConnectedOrConnecting;
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }
        
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}