﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using UTLO.Mobile.Restful.Service.Service.RestService;
using Android.Net;
using UTLOConfApp.Utils;
using UTLO.Data.Model.Domain;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service;
using Java.Util;
using UTLO.Mobile.Restful.Service.ViewModels;
using Newtonsoft.Json;
using Android.Support.V4.App;

namespace UTLOConfApp
{
    enum NotificationType
    {
        Before,
        Now
    }
    [Service]
    class ServiceNotification : Service
    {
        
        List<ConferenceSummaryViewModel> conferenceSummary;


        public override void OnCreate()
        {

            base.OnCreate();
        }
        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            var data = intent.Extras.GetString("data", null);
            if(data != null)
            {
                conferenceSummary = JsonConvert.DeserializeObject<List<ConferenceSummaryViewModel>>(data);
                if(conferenceSummary != null)
                {
                    BuildConferenceNotification();
                }
            }
            return StartCommandResult.Sticky;
        }


        private void BuildConferenceNotification()
        {
            try
            {
                if (conferenceSummary != null && conferenceSummary.Count != 0)
                {
                    var TodayConferenceDay = conferenceSummary
                        .Find(c => c.conferenceDate.Date == DateTime.Now.Date);
                    if (TodayConferenceDay != null)
                    {
                        int requestcode = 1;
                        foreach (var programme in TodayConferenceDay.programmes)
                        {
                            Intent alarmIntent = new Intent(this, typeof(AlarmReceiver));
                            var programmeData = JsonConvert.SerializeObject(programme);
                            if (programme.StartTime >= DateTime.Now.TimeOfDay)
                            {
                                requestcode++;
                                var timeInterval = programme.StartTime - DateTime.Now.TimeOfDay;
                                alarmIntent.PutExtra("programme", programmeData);
                                if (timeInterval > new TimeSpan(0, 5, 0))
                                {
                                    timeInterval -= new TimeSpan(0, 5, 0);
                                    alarmIntent.PutExtra("notificationType", (int)NotificationType.Before);
                                }
                                else
                                {
                                    alarmIntent.PutExtra("notificationType", (int)NotificationType.Now);
                                }
                                PendingIntent pendingIntent = PendingIntent.GetBroadcast(this, requestcode, alarmIntent, PendingIntentFlags.UpdateCurrent);
                                var alarmManger = GetSystemService(AlarmService) as AlarmManager;
                                int timeInValue = (int)timeInterval.TotalSeconds;
                                alarmManger.Set(AlarmType.ElapsedRealtime, SystemClock.ElapsedRealtime() + timeInValue + requestcode, pendingIntent);
                                
                            }
                        }
                        //StopNotificationService();
                    }
                }
            }
            catch (Exception ex)
            {

                StartActivity(typeof(NoNetworkActivity));
            }
        }

        private void StopNotificationService()
        {
            if(conferenceSummary != null && conferenceSummary.Count != 0)
            {
                var LastConferenceDay = conferenceSummary.Find(c=>c.conferenceDate == conferenceSummary.Max(x=>x.conferenceDate));
                if (LastConferenceDay != null)
                {
                    if (DateTime.Now == LastConferenceDay.conferenceDate)
                    {
                        var LastProgramme = LastConferenceDay.programmes.Where(c => c.EndTime == LastConferenceDay.programmes.Max(x => x.EndTime)).FirstOrDefault();
                        if (LastProgramme != null)
                        {
                            if (DateTime.Now.TimeOfDay > LastProgramme.EndTime)
                            {
                                StopSelf();
                            }
                        }
                        else
                        {
                            StopSelf();
                        } 
                    }
                }
                else
                {
                    StopSelf();
                }
            }
            else
            {
                StopSelf();
            }
        }
        

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        
    }

    [BroadcastReceiver]
    public class AlarmReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            var programmeData = JsonConvert.DeserializeObject<Programme>(intent.Extras.GetString("programme"));
            var notType = (NotificationType)intent.Extras.GetInt("notificationType");
            NotifyUser(context, notType, programmeData);
        }
        
        private void NotifyUser(Context context,NotificationType notType, Programme programe)
        {
            NotificationCompat.BigTextStyle textStyles = new NotificationCompat.BigTextStyle();
            NotificationCompat.Builder userNotBuilder = new NotificationCompat.Builder(context);
            userNotBuilder.SetSmallIcon(Resource.Drawable.ic_stat_splash_logo);
            userNotBuilder.SetContentTitle("HEC 2017 Programme Notification");
            if((int)Build.VERSION.SdkInt >= 16)
            {
                userNotBuilder.SetPriority((int)NotificationPriority.High);
            }
            if((int)Build.VERSION.SdkInt >= 21)
            {
                userNotBuilder.SetCategory(NotificationPriorityCategory.Events.ToString());
                userNotBuilder.SetVisibility((int)NotificationVisibility.Public);
            }
            switch (notType)
            {
                case NotificationType.Before:
                    textStyles.BigText($"{programe.Title} will start in 5 minutes time. Click here for more information");
                    textStyles.SetSummaryText("Click here for more information");
                    userNotBuilder.SetStyle(textStyles);
                    break;
                case NotificationType.Now:
                    textStyles.BigText($"{programe.Title} starts in less than 5 minutes. Click here for more information");
                    textStyles.SetSummaryText("Click here for more information");
                    userNotBuilder.SetStyle(textStyles);
                    break;
                default:
                    throw new Exception("Notification type not supported");
            }
            var notification = userNotBuilder.Build();
            var notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            notificationManager.Notify((int)NotificationCode.PresentationNotification, notification);
        }
    }
}