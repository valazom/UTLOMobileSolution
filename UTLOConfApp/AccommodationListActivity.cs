using System;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using UTLOConfApp.Adapter;
using System.Threading.Tasks;
using UTLO.Mobile.Restful.Service.Service.RestService;
using UTLO.Mobile.Restful.Service;
using Android.Net;
using UTLOConfApp.Utils;
using Java.Net;
using UTLO.Data.Model.Domain;

namespace UTLOConfApp
{
    [Activity(Label = "AccommodationListActivity", Theme = "@style/MyTheme", Icon = "@drawable/ic_launcher")]
    public class AccommodationListActivity : AppCompatActivity
    {
        TextView conf_title;
        TextView conf_name;
        AccommodationService accommodationService;
        RecyclerView recyclerView;
        AccomodationRecyclerAdapter accomodationRecyclerAdapter;
        CacheOperation<Accommodation> cache;
        List<Accommodation> accommodations;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AccommodationListActivity);
            RequestedOrientation = Android.Content.PM.ScreenOrientation.Portrait;
            SetupToolbar();
            await LoadDataOnline();

        }

        private async Task LoadDataOnline()
        {
            ProgressDialog progressdialog = new ProgressDialog(this);
            progressdialog.SetCancelable(false);
            progressdialog.Indeterminate = true;
            progressdialog.SetMessage("Loading...");
            progressdialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            progressdialog.Show();
            await GetAccommodation();
            progressdialog.Hide();
            progressdialog.Dismiss();
        }

        private void SetupToolbar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            if (Build.VERSION.SdkInt == BuildVersionCodes.Lollipop)
            {
                toolbar.Elevation = 10f;
            }
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            conf_name = FindViewById<TextView>(Resource.Id.conf_name);
            conf_name.Text = (string)ConferenceSettings.csettings["Title"];
            conf_title = FindViewById<TextView>(Resource.Id.conf_title);
            conf_title.Text = "Accommodation";
        }

        private async Task GetAccommodation()
        {
            try
            {
                //initialize cache operation for accommodation data 
                cache = new CacheOperation<Accommodation>();
                // First get data from cache if available
                var cacheData = await cache.GetObject(ConferenceCacheKeys.ConferenceAccommodation);
                //if data is not available in cache go online to go fetch that data
                if (!cacheData.Item2)
                {
                    //first check if mobile device have network and internet connectivity
                    var isonline = await Task.Run(() => isOnline());

                    if (isonline)
                    {
                        //if internet access is available fetch data from web api
                        accommodationService = new AccommodationService();
                        await Task.Run(() => accommodationService.GetAllAccommodationAsync(Constants.Accommodation));
                        accommodations = accommodationService.accommodations;
                        //Once data has been retrieved store it in cache
                        //set the caching time for the data;
                        CacheOperation<Accommodation>.CachingTime = 
                            TimeSpan.FromMinutes(double.Parse(ConferenceSettings.csettings["CachingTime"]));
                        await cache.InsertObject(ConferenceCacheKeys.ConferenceAccommodation, accommodations);
                    }
                    else
                    {
                        //if data was not retrieved display error message
                        new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("No network connection. You won't be able to get data")
                                .SetTitle("Network Error")
                                .Show();
                    }
                }
                else
                {
                    accommodations = cacheData.Item1;
                }
                if (accommodations != null)
                {
                    // Once data retrieval was successfully populate recyleview with data
                    recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerview);
                    accomodationRecyclerAdapter = new AccomodationRecyclerAdapter(this, accommodations);
                    accomodationRecyclerAdapter.ItemClick += AccListView_ItemClick;
                    recyclerView.SetAdapter(accomodationRecyclerAdapter);

                    var linearLayout = new LinearLayoutManager(this);
                    linearLayout.Orientation = LinearLayoutManager.Vertical;
                    recyclerView.SetLayoutManager(linearLayout);
                }
            }
            catch (Exception)
            {

                new Android.Support.V7.App.AlertDialog.Builder(this)
                                .SetPositiveButton("Yes", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetNegativeButton("No", (sender, args) =>
                                {
                                    Finish();
                                })
                                .SetMessage("Could not load data")
                                .SetTitle("Application Error")
                                .Show();
            }
        }
        private bool isOnline()
        {
            var cm = (ConnectivityManager)GetSystemService(ConnectivityService);
            NetworkInfo networkInfo = cm.ActiveNetworkInfo;
            //first check if the mobile phone has network connection
            if (networkInfo != null && networkInfo.IsConnected)
            {
                try
                {
                    // if network connection is available ping www.google.com to test for 
                    // internet access. If ping is not successful in 2 minutes 
                    // Then no internet access
                    URL url = new URL("http://www.google.com");
                    HttpURLConnection urlc = (HttpURLConnection)url.OpenConnection();
                    urlc.ConnectTimeout = 2000;
                    urlc.Connect();
                    if (urlc.ResponseCode == HttpStatus.Ok)
                    {
                        return true;
                    }
                }
                catch (Exception)
                {

                    return false;
                }
            }
            return false;
        }

        private void AccListView_ItemClick(object sender, AccomodationRecyclerAdapterClickEventArgs e)
        {
            // retrieve the accommodation item that was clicked
            var item = accomodationRecyclerAdapter.GetItem(e.Position);
            Intent AccommodationFullViewIntent = new Intent(Intent.ActionView);
            Intent chooser = Intent.CreateChooser(AccommodationFullViewIntent, "Open with");
            // open browser on mobile device with the url supplied
            string urlPath = item.url;
            AccommodationFullViewIntent.SetData(Android.Net.Uri.Parse(urlPath));
            StartActivity(AccommodationFullViewIntent);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
        public override void OnBackPressed()
        {

        }
    }
}