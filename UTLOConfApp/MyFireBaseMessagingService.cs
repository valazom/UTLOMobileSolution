﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Firebase.Messaging;
using Android.Support.V4.App;
using UTLOConfApp.Utils;

namespace UTLOConfApp
{
    [Service]
    [IntentFilter(new[] {"com.google.firebase.MESSAGING_EVENT"})]
    public class MyFireBaseMessagingService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage message)
        {
            SendNotification(message.GetNotification());
        }

        private void SendNotification(RemoteMessage.Notification notification)
        {
            var textStyle = new NotificationCompat.BigTextStyle();
            textStyle.BigText(notification.Body);
            var intent = new Intent(this, typeof(AnnouncementActivity));
            intent.PutExtra("ForceReload", true);
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);
            textStyle.SetSummaryText("Click here for more information");
            var notBuilderbuilder = new NotificationCompat.Builder(this);
            notBuilderbuilder.SetStyle(textStyle);
            notBuilderbuilder.SetSmallIcon(Resource.Drawable.ic_stat_splash_logo);
            notBuilderbuilder.SetStyle(textStyle);
            notBuilderbuilder.SetContentTitle(notification.Title);
            //notBuilderbuilder.SetContentText(notification.Body);
            notBuilderbuilder.SetDefaults((int)NotificationDefaults.Sound);
            notBuilderbuilder.SetContentIntent(pendingIntent);
            if((int)Build.VERSION.SdkInt >= 16)
            {
                notBuilderbuilder.SetPriority((int)NotificationPriority.Max);
            }
            if((int)Build.VERSION.SdkInt >= 21)
            {
                notBuilderbuilder.SetCategory(NotificationPriorityCategory.Events.ToString());
                notBuilderbuilder.SetVisibility((int)NotificationVisibility.Public);
            }
            var notificationbody = notBuilderbuilder.Build();
            var notificationManager = NotificationManager.FromContext(this);
            notificationManager.Notify((int)NotificationCode.RemoteNotification, notificationbody);

        }
    }
}