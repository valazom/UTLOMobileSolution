﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using UTLO_NewModel.Domain;

namespace UTLO_NewModel
{
    public class ConferenceDbContext : DbContext
    {
        public ConferenceDbContext(string constring) : base(constring)
        {

        }
        public DbSet<Abstracts> AbstractObjects { get; set; }
        public DbSet<Attendees> AttendeeObjects { get; set; }
        public DbSet<ConferenceDay> ConferenceDayObjects { get; set; }
        public DbSet<Presentations> PresentationObjects { get; set; }
        public DbSet<Programme> ProgrammeObjects { get; set; }
        public DbSet<Rate> RateObjects { get; set; }
        public DbSet<Suites> SuiteObjects { get; set; }
        public DbSet<Settings> SettingsObjects { get; set; }
        
       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Conference");
            modelBuilder.Entity<Programme>()
                         .HasMany<Attendees>(p => p.presenters);
                         //.WithMany(a => a.programmes)
                         //.Map(pa =>
                         //{
                         //    pa.MapLeftKey("ProgrammeId");
                         //    pa.MapRightKey("AttendeeId");
                         //    pa.ToTable("AttendeeProgramme");
                         //});
            base.OnModelCreating(modelBuilder);
        }

    }
}
