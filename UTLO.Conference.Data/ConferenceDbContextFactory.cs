﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;

namespace UTLO.Conference.Data
{
    public class ConferenceDbContextFactory : System.Data.Entity.Infrastructure.IDbContextFactory<ConferenceDbContext>
    {
        public ConferenceDbContext Create()
        {
            var connstring = ConfigurationManager.ConnectionStrings["ConferenceDBConnectionString"].ConnectionString;
            return new ConferenceDbContext(connstring);
        }
    }
}
