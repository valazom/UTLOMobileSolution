namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateVenue : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Conference.Programmes", "Venue", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("Conference.Programmes", "Venue", c => c.String(nullable: false, maxLength: 250));
        }
    }
}
