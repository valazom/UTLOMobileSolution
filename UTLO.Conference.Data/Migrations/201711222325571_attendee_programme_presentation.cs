namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attendee_programme_presentation : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Programmes", "Attendees_AttendeesId", c => c.Int());
            AddColumn("Conference.Presentations", "Attendees_AttendeesId", c => c.Int());
            CreateIndex("Conference.Presentations", "Attendees_AttendeesId");
            CreateIndex("Conference.Programmes", "Attendees_AttendeesId");
            AddForeignKey("Conference.Presentations", "Attendees_AttendeesId", "Conference.Attendees", "AttendeesId");
            AddForeignKey("Conference.Programmes", "Attendees_AttendeesId", "Conference.Attendees", "AttendeesId");
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Programmes", "Attendees_AttendeesId", "Conference.Attendees");
            DropForeignKey("Conference.Presentations", "Attendees_AttendeesId", "Conference.Attendees");
            DropIndex("Conference.Programmes", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.Presentations", new[] { "Attendees_AttendeesId" });
            DropColumn("Conference.Presentations", "Attendees_AttendeesId");
            DropColumn("Conference.Programmes", "Attendees_AttendeesId");
        }
    }
}
