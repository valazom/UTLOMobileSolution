namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newrate : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Conference.Rates", name: "presentation_PresentationsId", newName: "Presentations_PresentationsId");
            RenameIndex(table: "Conference.Rates", name: "IX_presentation_PresentationsId", newName: "IX_Presentations_PresentationsId");
            AddColumn("Conference.Rates", "presentationId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Conference.Rates", "presentationId");
            RenameIndex(table: "Conference.Rates", name: "IX_Presentations_PresentationsId", newName: "IX_presentation_PresentationsId");
            RenameColumn(table: "Conference.Rates", name: "Presentations_PresentationsId", newName: "presentation_PresentationsId");
        }
    }
}
