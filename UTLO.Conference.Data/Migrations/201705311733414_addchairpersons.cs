namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addchairpersons : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Attendees", "Programme_ProgrammeId", c => c.Int());
            CreateIndex("Conference.Attendees", "Programme_ProgrammeId");
            AddForeignKey("Conference.Attendees", "Programme_ProgrammeId", "Conference.Programmes", "ProgrammeId");
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Attendees", "Programme_ProgrammeId", "Conference.Programmes");
            DropIndex("Conference.Attendees", new[] { "Programme_ProgrammeId" });
            DropColumn("Conference.Attendees", "Programme_ProgrammeId");
        }
    }
}
