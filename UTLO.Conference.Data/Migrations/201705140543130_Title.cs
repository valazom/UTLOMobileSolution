namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Title : DbMigration
    {
        public override void Up()
        {
            AlterColumn("Conference.Programmes", "Title", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("Conference.Programmes", "Title", c => c.String(nullable: false, maxLength: 5));
        }
    }
}
