namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class abstracts : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Abstracts", "Theme", c => c.String(nullable: false));
            AlterColumn("Conference.Abstracts", "Body", c => c.String(nullable: false));
            AlterColumn("Conference.Abstracts", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("Conference.Abstracts", "Title", c => c.String());
            AlterColumn("Conference.Abstracts", "Body", c => c.String());
            DropColumn("Conference.Abstracts", "Theme");
        }
    }
}
