namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settings1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Settings", "SettingType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Conference.Settings", "SettingType");
        }
    }
}
