namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PresentationTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Presentations", "StartTime", c => c.Time(nullable: false, precision: 7));
            AddColumn("Conference.Presentations", "EndTime", c => c.Time(nullable: false, precision: 7));
        }
        
        public override void Down()
        {
            DropColumn("Conference.Presentations", "EndTime");
            DropColumn("Conference.Presentations", "StartTime");
        }
    }
}
