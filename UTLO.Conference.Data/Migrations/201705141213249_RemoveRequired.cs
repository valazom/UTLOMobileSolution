namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRequired : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays");
            DropIndex("Conference.Programmes", new[] { "Daynumber_ConferenceDayId" });
            AlterColumn("Conference.Programmes", "Daynumber_ConferenceDayId", c => c.Int());
            CreateIndex("Conference.Programmes", "Daynumber_ConferenceDayId");
            AddForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays", "ConferenceDayId");
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays");
            DropIndex("Conference.Programmes", new[] { "Daynumber_ConferenceDayId" });
            AlterColumn("Conference.Programmes", "Daynumber_ConferenceDayId", c => c.Int(nullable: false));
            CreateIndex("Conference.Programmes", "Daynumber_ConferenceDayId");
            AddForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays", "ConferenceDayId", cascadeDelete: true);
        }
    }
}
