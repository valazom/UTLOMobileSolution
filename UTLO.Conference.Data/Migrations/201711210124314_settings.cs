namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Conference.Settings",
                c => new
                    {
                        SettingId = c.Int(nullable: false, identity: true),
                        SettingName = c.String(maxLength: 450),
                        SettingValue = c.String(),
                    })
                .PrimaryKey(t => t.SettingId)
                .Index(t => t.SettingName, unique: true);
            
        }
        
        public override void Down()
        {
            DropIndex("Conference.Settings", new[] { "SettingName" });
            DropTable("Conference.Settings");
        }
    }
}
