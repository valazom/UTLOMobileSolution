namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class attendeeabstracts : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Conference.Abstracts",
                c => new
                    {
                        abstractsId = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        Title = c.String(nullable: false),
                        Theme = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.abstractsId);
            
            CreateTable(
                "Conference.Attendees",
                c => new
                    {
                        AttendeesId = c.Int(nullable: false, identity: true),
                        IsKeynoteSpeaker = c.Boolean(nullable: false),
                        status = c.String(),
                        Name = c.String(),
                        Position = c.String(),
                        Affiliation = c.String(),
                        Address = c.String(),
                        ImageUrl = c.String(),
                        emailAddress = c.String(),
                        phoneNumber = c.String(),
                        Biography = c.String(),
                        Programme_ProgrammeId = c.Int(),
                        Programme_ProgrammeId1 = c.Int(),
                        Presentations_PresentationsId = c.Int(),
                        Presentations_PresentationsId1 = c.Int(),
                        Presentations_PresentationsId2 = c.Int(),
                    })
                .PrimaryKey(t => t.AttendeesId)
                .ForeignKey("Conference.Programmes", t => t.Programme_ProgrammeId)
                .ForeignKey("Conference.Programmes", t => t.Programme_ProgrammeId1)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId1)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId2)
                .Index(t => t.Programme_ProgrammeId)
                .Index(t => t.Programme_ProgrammeId1)
                .Index(t => t.Presentations_PresentationsId)
                .Index(t => t.Presentations_PresentationsId1)
                .Index(t => t.Presentations_PresentationsId2);
            
            CreateTable(
                "Conference.ConferenceDays",
                c => new
                    {
                        ConferenceDayId = c.Int(nullable: false, identity: true),
                        Daynumber = c.Int(nullable: false),
                        ConferenceDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ConferenceDayId);
            
            CreateTable(
                "Conference.Programmes",
                c => new
                    {
                        ProgrammeId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Venue = c.String(maxLength: 250),
                        programmeType = c.Int(nullable: false),
                        ConferenceDayId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProgrammeId)
                .ForeignKey("Conference.ConferenceDays", t => t.ConferenceDayId, cascadeDelete: true)
                .Index(t => t.ConferenceDayId);
            
            CreateTable(
                "Conference.Suites",
                c => new
                    {
                        SuitesId = c.Int(nullable: false, identity: true),
                        ProgrammeId = c.Int(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 250),
                        Heading = c.String(),
                    })
                .PrimaryKey(t => t.SuitesId)
                .ForeignKey("Conference.Programmes", t => t.ProgrammeId, cascadeDelete: true)
                .Index(t => t.ProgrammeId);
            
            CreateTable(
                "Conference.Presentations",
                c => new
                    {
                        PresentationsId = c.Int(nullable: false, identity: true),
                        SuiteId = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Title = c.String(nullable: false),
                        type = c.Int(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 250),
                        Suites_SuitesId = c.Int(),
                    })
                .PrimaryKey(t => t.PresentationsId)
                .ForeignKey("Conference.Suites", t => t.Suites_SuitesId)
                .Index(t => t.Suites_SuitesId);
            
            CreateTable(
                "Conference.Rates",
                c => new
                    {
                        RateId = c.Int(nullable: false, identity: true),
                        presentationId = c.Int(nullable: false),
                        Ratings = c.Int(nullable: false),
                        Presentations_PresentationsId = c.Int(),
                    })
                .PrimaryKey(t => t.RateId)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId)
                .Index(t => t.Presentations_PresentationsId);
            
            CreateTable(
                "Conference.AttendeesAbstracts",
                c => new
                    {
                        Attendees_AttendeesId = c.Int(nullable: false),
                        Abstracts_abstractsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Attendees_AttendeesId, t.Abstracts_abstractsId })
                .ForeignKey("Conference.Attendees", t => t.Attendees_AttendeesId, cascadeDelete: true)
                .ForeignKey("Conference.Abstracts", t => t.Abstracts_abstractsId, cascadeDelete: true)
                .Index(t => t.Attendees_AttendeesId)
                .Index(t => t.Abstracts_abstractsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Programmes", "ConferenceDayId", "Conference.ConferenceDays");
            DropForeignKey("Conference.Suites", "ProgrammeId", "Conference.Programmes");
            DropForeignKey("Conference.Presentations", "Suites_SuitesId", "Conference.Suites");
            DropForeignKey("Conference.Rates", "Presentations_PresentationsId", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId2", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId1", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Programme_ProgrammeId1", "Conference.Programmes");
            DropForeignKey("Conference.Attendees", "Programme_ProgrammeId", "Conference.Programmes");
            DropForeignKey("Conference.AttendeesAbstracts", "Abstracts_abstractsId", "Conference.Abstracts");
            DropForeignKey("Conference.AttendeesAbstracts", "Attendees_AttendeesId", "Conference.Attendees");
            DropIndex("Conference.AttendeesAbstracts", new[] { "Abstracts_abstractsId" });
            DropIndex("Conference.AttendeesAbstracts", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.Rates", new[] { "Presentations_PresentationsId" });
            DropIndex("Conference.Presentations", new[] { "Suites_SuitesId" });
            DropIndex("Conference.Suites", new[] { "ProgrammeId" });
            DropIndex("Conference.Programmes", new[] { "ConferenceDayId" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId2" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId1" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId" });
            DropIndex("Conference.Attendees", new[] { "Programme_ProgrammeId1" });
            DropIndex("Conference.Attendees", new[] { "Programme_ProgrammeId" });
            DropTable("Conference.AttendeesAbstracts");
            DropTable("Conference.Rates");
            DropTable("Conference.Presentations");
            DropTable("Conference.Suites");
            DropTable("Conference.Programmes");
            DropTable("Conference.ConferenceDays");
            DropTable("Conference.Attendees");
            DropTable("Conference.Abstracts");
        }
    }
}
