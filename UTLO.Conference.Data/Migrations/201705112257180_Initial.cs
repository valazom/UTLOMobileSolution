namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Conference.Abstracts",
                c => new
                    {
                        abstractsId = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.abstractsId);
            
            CreateTable(
                "Conference.Attendees",
                c => new
                    {
                        AttendeesId = c.Int(nullable: false, identity: true),
                        IsKeynoteSpeaker = c.Boolean(nullable: false),
                        status = c.String(),
                        Name = c.String(),
                        Position = c.String(),
                        Affiliation = c.String(),
                        Address = c.String(),
                        ImageUrl = c.String(),
                        emailAddress = c.String(),
                        phoneNumber = c.String(),
                        Biography = c.String(),
                        Presentations_PresentationsId = c.Int(),
                        Presentations_PresentationsId1 = c.Int(),
                        Presentations_PresentationsId2 = c.Int(),
                    })
                .PrimaryKey(t => t.AttendeesId)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId1)
                .ForeignKey("Conference.Presentations", t => t.Presentations_PresentationsId2)
                .Index(t => t.Presentations_PresentationsId)
                .Index(t => t.Presentations_PresentationsId1)
                .Index(t => t.Presentations_PresentationsId2);
            
            CreateTable(
                "Conference.Presentations",
                c => new
                    {
                        PresentationsId = c.Int(nullable: false, identity: true),
                        SuiteId = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        type = c.Int(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 250),
                        Attendees_AttendeesId = c.Int(),
                        Suites_SuitesId = c.Int(),
                    })
                .PrimaryKey(t => t.PresentationsId)
                .ForeignKey("Conference.Attendees", t => t.Attendees_AttendeesId)
                .ForeignKey("Conference.Suites", t => t.Suites_SuitesId)
                .Index(t => t.Attendees_AttendeesId)
                .Index(t => t.Suites_SuitesId);
            
            CreateTable(
                "Conference.Rates",
                c => new
                    {
                        RateId = c.Int(nullable: false, identity: true),
                        Ratings = c.Int(nullable: false),
                        presentation_PresentationsId = c.Int(),
                    })
                .PrimaryKey(t => t.RateId)
                .ForeignKey("Conference.Presentations", t => t.presentation_PresentationsId)
                .Index(t => t.presentation_PresentationsId);
            
            CreateTable(
                "Conference.Programmes",
                c => new
                    {
                        ProgrammeId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 5),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Venue = c.String(nullable: false, maxLength: 250),
                        Daynumber_ConferenceDayId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProgrammeId)
                .ForeignKey("Conference.ConferenceDays", t => t.Daynumber_ConferenceDayId, cascadeDelete: true)
                .Index(t => t.Daynumber_ConferenceDayId);
            
            CreateTable(
                "Conference.ConferenceDays",
                c => new
                    {
                        ConferenceDayId = c.Int(nullable: false, identity: true),
                        Daynumber = c.Int(nullable: false),
                        ConferenceDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ConferenceDayId)
                .Index(t => t.Daynumber, unique: true)
                .Index(t => t.ConferenceDate, unique: true);
            
            CreateTable(
                "Conference.Suites",
                c => new
                    {
                        SuitesId = c.Int(nullable: false, identity: true),
                        ProgrammeId = c.Int(nullable: false),
                        Venue = c.String(nullable: false, maxLength: 250),
                        Heading = c.String(),
                    })
                .PrimaryKey(t => t.SuitesId)
                .ForeignKey("Conference.Programmes", t => t.ProgrammeId, cascadeDelete: true)
                .Index(t => t.ProgrammeId);
            
            CreateTable(
                "Conference.AttendeesAbstracts",
                c => new
                    {
                        Attendees_AttendeesId = c.Int(nullable: false),
                        Abstracts_abstractsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Attendees_AttendeesId, t.Abstracts_abstractsId })
                .ForeignKey("Conference.Attendees", t => t.Attendees_AttendeesId, cascadeDelete: true)
                .ForeignKey("Conference.Abstracts", t => t.Abstracts_abstractsId, cascadeDelete: true)
                .Index(t => t.Attendees_AttendeesId)
                .Index(t => t.Abstracts_abstractsId);
            
            CreateTable(
                "Conference.AttendeeProgramme",
                c => new
                    {
                        ProgrammeId = c.Int(nullable: false),
                        AttendeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProgrammeId, t.AttendeeId })
                .ForeignKey("Conference.Programmes", t => t.ProgrammeId, cascadeDelete: true)
                .ForeignKey("Conference.Attendees", t => t.AttendeeId, cascadeDelete: true)
                .Index(t => t.ProgrammeId)
                .Index(t => t.AttendeeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Suites", "ProgrammeId", "Conference.Programmes");
            DropForeignKey("Conference.Presentations", "Suites_SuitesId", "Conference.Suites");
            DropForeignKey("Conference.AttendeeProgramme", "AttendeeId", "Conference.Attendees");
            DropForeignKey("Conference.AttendeeProgramme", "ProgrammeId", "Conference.Programmes");
            DropForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays");
            DropForeignKey("Conference.Presentations", "Attendees_AttendeesId", "Conference.Attendees");
            DropForeignKey("Conference.Rates", "presentation_PresentationsId", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId2", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId1", "Conference.Presentations");
            DropForeignKey("Conference.Attendees", "Presentations_PresentationsId", "Conference.Presentations");
            DropForeignKey("Conference.AttendeesAbstracts", "Abstracts_abstractsId", "Conference.Abstracts");
            DropForeignKey("Conference.AttendeesAbstracts", "Attendees_AttendeesId", "Conference.Attendees");
            DropIndex("Conference.AttendeeProgramme", new[] { "AttendeeId" });
            DropIndex("Conference.AttendeeProgramme", new[] { "ProgrammeId" });
            DropIndex("Conference.AttendeesAbstracts", new[] { "Abstracts_abstractsId" });
            DropIndex("Conference.AttendeesAbstracts", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.Suites", new[] { "ProgrammeId" });
            DropIndex("Conference.ConferenceDays", new[] { "ConferenceDate" });
            DropIndex("Conference.ConferenceDays", new[] { "Daynumber" });
            DropIndex("Conference.Programmes", new[] { "Daynumber_ConferenceDayId" });
            DropIndex("Conference.Rates", new[] { "presentation_PresentationsId" });
            DropIndex("Conference.Presentations", new[] { "Suites_SuitesId" });
            DropIndex("Conference.Presentations", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId2" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId1" });
            DropIndex("Conference.Attendees", new[] { "Presentations_PresentationsId" });
            DropTable("Conference.AttendeeProgramme");
            DropTable("Conference.AttendeesAbstracts");
            DropTable("Conference.Suites");
            DropTable("Conference.ConferenceDays");
            DropTable("Conference.Programmes");
            DropTable("Conference.Rates");
            DropTable("Conference.Presentations");
            DropTable("Conference.Attendees");
            DropTable("Conference.Abstracts");
        }
    }
}
