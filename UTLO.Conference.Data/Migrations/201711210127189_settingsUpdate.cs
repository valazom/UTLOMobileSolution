namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class settingsUpdate : DbMigration
    {
        public override void Up()
        {
            DropIndex("Conference.Settings", new[] { "SettingName" });
            AlterColumn("Conference.Settings", "SettingName", c => c.String(nullable: false, maxLength: 450));
            AlterColumn("Conference.Settings", "SettingValue", c => c.String(nullable: false));
            CreateIndex("Conference.Settings", "SettingName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("Conference.Settings", new[] { "SettingName" });
            AlterColumn("Conference.Settings", "SettingValue", c => c.String());
            AlterColumn("Conference.Settings", "SettingName", c => c.String(maxLength: 450));
            CreateIndex("Conference.Settings", "SettingName", unique: true);
        }
    }
}
