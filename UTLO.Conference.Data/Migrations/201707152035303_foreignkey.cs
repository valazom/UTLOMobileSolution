namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignkey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays");
            DropIndex("Conference.Programmes", new[] { "Daynumber_ConferenceDayId" });
            RenameColumn(table: "Conference.Programmes", name: "Daynumber_ConferenceDayId", newName: "ConferenceDayId");
            AlterColumn("Conference.Programmes", "ConferenceDayId", c => c.Int(nullable: false));
            CreateIndex("Conference.Programmes", "ConferenceDayId");
            AddForeignKey("Conference.Programmes", "ConferenceDayId", "Conference.ConferenceDays", "ConferenceDayId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Conference.Programmes", "ConferenceDayId", "Conference.ConferenceDays");
            DropIndex("Conference.Programmes", new[] { "ConferenceDayId" });
            AlterColumn("Conference.Programmes", "ConferenceDayId", c => c.Int());
            RenameColumn(table: "Conference.Programmes", name: "ConferenceDayId", newName: "Daynumber_ConferenceDayId");
            CreateIndex("Conference.Programmes", "Daynumber_ConferenceDayId");
            AddForeignKey("Conference.Programmes", "Daynumber_ConferenceDayId", "Conference.ConferenceDays", "ConferenceDayId");
        }
    }
}
