namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Attendees : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Conference.AttendeesAbstracts", "Attendees_AttendeesId", "Conference.Attendees");
            DropForeignKey("Conference.AttendeesAbstracts", "Abstracts_abstractsId", "Conference.Abstracts");
            DropForeignKey("Conference.Presentations", "Attendees_AttendeesId", "Conference.Attendees");
            DropForeignKey("Conference.AttendeeProgramme", "ProgrammeId", "Conference.Programmes");
            DropForeignKey("Conference.AttendeeProgramme", "AttendeeId", "Conference.Attendees");
            DropIndex("Conference.Presentations", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.AttendeesAbstracts", new[] { "Attendees_AttendeesId" });
            DropIndex("Conference.AttendeesAbstracts", new[] { "Abstracts_abstractsId" });
            DropIndex("Conference.AttendeeProgramme", new[] { "ProgrammeId" });
            DropIndex("Conference.AttendeeProgramme", new[] { "AttendeeId" });
            AddColumn("Conference.Attendees", "Abstracts_abstractsId", c => c.Int());
            AddColumn("Conference.Attendees", "Programme_ProgrammeId1", c => c.Int());
            CreateIndex("Conference.Attendees", "Abstracts_abstractsId");
            CreateIndex("Conference.Attendees", "Programme_ProgrammeId1");
            AddForeignKey("Conference.Attendees", "Abstracts_abstractsId", "Conference.Abstracts", "abstractsId");
            AddForeignKey("Conference.Attendees", "Programme_ProgrammeId1", "Conference.Programmes", "ProgrammeId");
            DropColumn("Conference.Presentations", "Attendees_AttendeesId");
            DropTable("Conference.AttendeesAbstracts");
            DropTable("Conference.AttendeeProgramme");
        }
        
        public override void Down()
        {
            CreateTable(
                "Conference.AttendeeProgramme",
                c => new
                    {
                        ProgrammeId = c.Int(nullable: false),
                        AttendeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProgrammeId, t.AttendeeId });
            
            CreateTable(
                "Conference.AttendeesAbstracts",
                c => new
                    {
                        Attendees_AttendeesId = c.Int(nullable: false),
                        Abstracts_abstractsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Attendees_AttendeesId, t.Abstracts_abstractsId });
            
            AddColumn("Conference.Presentations", "Attendees_AttendeesId", c => c.Int());
            DropForeignKey("Conference.Attendees", "Programme_ProgrammeId1", "Conference.Programmes");
            DropForeignKey("Conference.Attendees", "Abstracts_abstractsId", "Conference.Abstracts");
            DropIndex("Conference.Attendees", new[] { "Programme_ProgrammeId1" });
            DropIndex("Conference.Attendees", new[] { "Abstracts_abstractsId" });
            DropColumn("Conference.Attendees", "Programme_ProgrammeId1");
            DropColumn("Conference.Attendees", "Abstracts_abstractsId");
            CreateIndex("Conference.AttendeeProgramme", "AttendeeId");
            CreateIndex("Conference.AttendeeProgramme", "ProgrammeId");
            CreateIndex("Conference.AttendeesAbstracts", "Abstracts_abstractsId");
            CreateIndex("Conference.AttendeesAbstracts", "Attendees_AttendeesId");
            CreateIndex("Conference.Presentations", "Attendees_AttendeesId");
            AddForeignKey("Conference.AttendeeProgramme", "AttendeeId", "Conference.Attendees", "AttendeesId", cascadeDelete: true);
            AddForeignKey("Conference.AttendeeProgramme", "ProgrammeId", "Conference.Programmes", "ProgrammeId", cascadeDelete: true);
            AddForeignKey("Conference.Presentations", "Attendees_AttendeesId", "Conference.Attendees", "AttendeesId");
            AddForeignKey("Conference.AttendeesAbstracts", "Abstracts_abstractsId", "Conference.Abstracts", "abstractsId", cascadeDelete: true);
            AddForeignKey("Conference.AttendeesAbstracts", "Attendees_AttendeesId", "Conference.Attendees", "AttendeesId", cascadeDelete: true);
        }
    }
}
