namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateprogramme : DbMigration
    {
        public override void Up()
        {
            AddColumn("Conference.Programmes", "programmeType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Conference.Programmes", "programmeType");
        }
    }
}
