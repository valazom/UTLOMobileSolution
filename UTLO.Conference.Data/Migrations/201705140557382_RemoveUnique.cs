namespace UTLO.Conference.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUnique : DbMigration
    {
        public override void Up()
        {
            DropIndex("Conference.ConferenceDays", new[] { "Daynumber" });
            DropIndex("Conference.ConferenceDays", new[] { "ConferenceDate" });
        }
        
        public override void Down()
        {
            CreateIndex("Conference.ConferenceDays", "ConferenceDate", unique: true);
            CreateIndex("Conference.ConferenceDays", "Daynumber", unique: true);
        }
    }
}
