﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO_NewModel
{
    public class AnnouncementDbContext :DbContext
    {
        public AnnouncementDbContext(string connstring) : base(connstring)
        {

        }
        public DbSet<Announcement> AnnouncementObjects { get; set; }
        public DbSet<Topic> TopicObjects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Announcement");
            base.OnModelCreating(modelBuilder);
        }
    }
}
