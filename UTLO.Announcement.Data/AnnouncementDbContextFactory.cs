﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;

namespace UTLO.Announcement.Data
{
    public class AnnouncementDbContextFactory : IDbContextFactory<AnnouncementDbContext>
    {
        public AnnouncementDbContext Create()
        {
            var connstring = ConfigurationManager.ConnectionStrings["ConferenceDBConnectionString"].ConnectionString;

            return new AnnouncementDbContext(connstring);
        }
    }
}
