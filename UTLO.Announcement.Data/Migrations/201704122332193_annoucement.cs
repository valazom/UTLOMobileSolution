namespace UTLO.Announcement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class annoucement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Announcement.Announcements",
                c => new
                    {
                        AnnouncementId = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 250),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AnnouncementId);
            
            CreateTable(
                "Announcement.Topics",
                c => new
                    {
                        TopicId = c.Int(nullable: false, identity: true),
                        AnnouncementId = c.Int(nullable: false),
                        Bulletpoint = c.String(),
                    })
                .PrimaryKey(t => t.TopicId)
                .ForeignKey("Announcement.Announcements", t => t.AnnouncementId, cascadeDelete: true)
                .Index(t => t.AnnouncementId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Announcement.Topics", "AnnouncementId", "Announcement.Announcements");
            DropIndex("Announcement.Topics", new[] { "AnnouncementId" });
            DropTable("Announcement.Topics");
            DropTable("Announcement.Announcements");
        }
    }
}
