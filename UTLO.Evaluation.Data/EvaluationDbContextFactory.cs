﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel;

namespace UTLO.Evaluation.Data
{
    public class EvaluationDbContextFactory : System.Data.Entity.Infrastructure.IDbContextFactory<EvaluationDbContext>
    {
        public EvaluationDbContext Create()
        {
            var connstring = ConfigurationManager.ConnectionStrings["ConferenceDBConnectionString"].ConnectionString;
            return new EvaluationDbContext(connstring);
        }
    }
}
