namespace UTLO.Evaluation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class category : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Evaluation.EvaluationQuestionCategories",
                c => new
                    {
                        EvaluationQuestionCategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.EvaluationQuestionCategoryId);
            
            AddColumn("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId", c => c.Int());
            CreateIndex("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId");
            AddForeignKey("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId", "Evaluation.EvaluationQuestionCategories", "EvaluationQuestionCategoryId");
        }
        
        public override void Down()
        {
            DropForeignKey("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId", "Evaluation.EvaluationQuestionCategories");
            DropIndex("Evaluation.EvaluationQuestionRatings", new[] { "QuestionCategory_EvaluationQuestionCategoryId" });
            DropColumn("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId");
            DropTable("Evaluation.EvaluationQuestionCategories");
        }
    }
}
