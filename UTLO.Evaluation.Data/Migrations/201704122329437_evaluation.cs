namespace UTLO.Evaluation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class evaluation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Evaluation.Evaluations",
                c => new
                    {
                        EvaluationId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.EvaluationId);
            
            CreateTable(
                "Evaluation.FilledEvaluations",
                c => new
                    {
                        FilledEvaluationId = c.Int(nullable: false, identity: true),
                        EvaluationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilledEvaluationId)
                .ForeignKey("Evaluation.Evaluations", t => t.EvaluationId, cascadeDelete: true)
                .Index(t => t.EvaluationId);
            
            CreateTable(
                "Evaluation.EvaluationQuestions",
                c => new
                    {
                        EvaluationQuestionsId = c.Int(nullable: false, identity: true),
                        EvaluationId = c.Int(nullable: false),
                        Question = c.String(),
                        Answer = c.String(),
                    })
                .PrimaryKey(t => t.EvaluationQuestionsId)
                .ForeignKey("Evaluation.Evaluations", t => t.EvaluationId, cascadeDelete: true)
                .Index(t => t.EvaluationId);
            
            CreateTable(
                "Evaluation.EvaluationQuestionRatings",
                c => new
                    {
                        EvaluationQuestionsRatingId = c.Int(nullable: false, identity: true),
                        EvaluationId = c.Int(nullable: false),
                        Question = c.String(),
                        Rating = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EvaluationQuestionsRatingId)
                .ForeignKey("Evaluation.Evaluations", t => t.EvaluationId, cascadeDelete: true)
                .Index(t => t.EvaluationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Evaluation.EvaluationQuestionRatings", "EvaluationId", "Evaluation.Evaluations");
            DropForeignKey("Evaluation.EvaluationQuestions", "EvaluationId", "Evaluation.Evaluations");
            DropForeignKey("Evaluation.FilledEvaluations", "EvaluationId", "Evaluation.Evaluations");
            DropIndex("Evaluation.EvaluationQuestionRatings", new[] { "EvaluationId" });
            DropIndex("Evaluation.EvaluationQuestions", new[] { "EvaluationId" });
            DropIndex("Evaluation.FilledEvaluations", new[] { "EvaluationId" });
            DropTable("Evaluation.EvaluationQuestionRatings");
            DropTable("Evaluation.EvaluationQuestions");
            DropTable("Evaluation.FilledEvaluations");
            DropTable("Evaluation.Evaluations");
        }
    }
}
