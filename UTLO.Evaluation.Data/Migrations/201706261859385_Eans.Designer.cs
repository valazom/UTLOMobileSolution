// <auto-generated />
namespace UTLO.Evaluation.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Eans : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Eans));
        
        string IMigrationMetadata.Id
        {
            get { return "201706261859385_Eans"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
