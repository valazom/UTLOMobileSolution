namespace UTLO.Evaluation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Eans : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Evaluation.FilledEvaluations", "EvaluationId", "Evaluation.Evaluations");
            DropForeignKey("Evaluation.EvaluationQuestions", "EvaluationId", "Evaluation.Evaluations");
            DropForeignKey("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId", "Evaluation.EvaluationQuestionCategories");
            DropForeignKey("Evaluation.EvaluationQuestionRatings", "EvaluationId", "Evaluation.Evaluations");
            DropIndex("Evaluation.FilledEvaluations", new[] { "EvaluationId" });
            DropIndex("Evaluation.EvaluationQuestions", new[] { "EvaluationId" });
            DropIndex("Evaluation.EvaluationQuestionRatings", new[] { "EvaluationId" });
            DropIndex("Evaluation.EvaluationQuestionRatings", new[] { "QuestionCategory_EvaluationQuestionCategoryId" });
            CreateTable(
                "Evaluation.Answers",
                c => new
                    {
                        AnswerId = c.Int(nullable: false, identity: true),
                        EvaluationQuestionId = c.Int(nullable: false),
                        answer = c.String(),
                        Rating = c.Int(nullable: false),
                        EvaluationQuestions_EvaluationQuestionsId = c.Int(),
                    })
                .PrimaryKey(t => t.AnswerId)
                .ForeignKey("Evaluation.EvaluationQuestions", t => t.EvaluationQuestions_EvaluationQuestionsId)
                .Index(t => t.EvaluationQuestions_EvaluationQuestionsId);
            
            AddColumn("Evaluation.EvaluationQuestions", "Qtype", c => c.Int(nullable: false));
            AlterColumn("Evaluation.EvaluationQuestions", "Question", c => c.String(nullable: false));
            DropColumn("Evaluation.EvaluationQuestions", "EvaluationId");
            DropColumn("Evaluation.EvaluationQuestions", "Answer");
            DropTable("Evaluation.Evaluations");
            DropTable("Evaluation.FilledEvaluations");
            DropTable("Evaluation.EvaluationQuestionRatings");
            DropTable("Evaluation.EvaluationQuestionCategories");
        }
        
        public override void Down()
        {
            CreateTable(
                "Evaluation.EvaluationQuestionCategories",
                c => new
                    {
                        EvaluationQuestionCategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.EvaluationQuestionCategoryId);
            
            CreateTable(
                "Evaluation.EvaluationQuestionRatings",
                c => new
                    {
                        EvaluationQuestionsRatingId = c.Int(nullable: false, identity: true),
                        EvaluationId = c.Int(nullable: false),
                        Question = c.String(),
                        Rating = c.Int(nullable: false),
                        QuestionCategory_EvaluationQuestionCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.EvaluationQuestionsRatingId);
            
            CreateTable(
                "Evaluation.FilledEvaluations",
                c => new
                    {
                        FilledEvaluationId = c.Int(nullable: false, identity: true),
                        EvaluationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FilledEvaluationId);
            
            CreateTable(
                "Evaluation.Evaluations",
                c => new
                    {
                        EvaluationId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.EvaluationId);
            
            AddColumn("Evaluation.EvaluationQuestions", "Answer", c => c.String());
            AddColumn("Evaluation.EvaluationQuestions", "EvaluationId", c => c.Int(nullable: false));
            DropForeignKey("Evaluation.Answers", "EvaluationQuestions_EvaluationQuestionsId", "Evaluation.EvaluationQuestions");
            DropIndex("Evaluation.Answers", new[] { "EvaluationQuestions_EvaluationQuestionsId" });
            AlterColumn("Evaluation.EvaluationQuestions", "Question", c => c.String());
            DropColumn("Evaluation.EvaluationQuestions", "Qtype");
            DropTable("Evaluation.Answers");
            CreateIndex("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId");
            CreateIndex("Evaluation.EvaluationQuestionRatings", "EvaluationId");
            CreateIndex("Evaluation.EvaluationQuestions", "EvaluationId");
            CreateIndex("Evaluation.FilledEvaluations", "EvaluationId");
            AddForeignKey("Evaluation.EvaluationQuestionRatings", "EvaluationId", "Evaluation.Evaluations", "EvaluationId", cascadeDelete: true);
            AddForeignKey("Evaluation.EvaluationQuestionRatings", "QuestionCategory_EvaluationQuestionCategoryId", "Evaluation.EvaluationQuestionCategories", "EvaluationQuestionCategoryId");
            AddForeignKey("Evaluation.EvaluationQuestions", "EvaluationId", "Evaluation.Evaluations", "EvaluationId", cascadeDelete: true);
            AddForeignKey("Evaluation.FilledEvaluations", "EvaluationId", "Evaluation.Evaluations", "EvaluationId", cascadeDelete: true);
        }
    }
}
