﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO_NewModel
{
    public class EvaluationDbContext: DbContext
    {
        public EvaluationDbContext(string constring) : base(constring)
        {

        }
        
        public DbSet<EvaluationQuestions> EvaluationQuestionsObjects { get; set; }
        public DbSet<Answers> AnswersObjects { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Evaluation");
            base.OnModelCreating(modelBuilder);
        }
    }

}
