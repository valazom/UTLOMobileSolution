﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO.Accommodation.Data
{
    public class AccommodationDbContextFactory : IDbContextFactory<AccommodationDbContext>
    {
        public AccommodationDbContext Create()
        {
            var connstring = ConfigurationManager.ConnectionStrings["ConferenceDBConnectionString"].ConnectionString;
            return new AccommodationDbContext(connstring);
        }
    }
    
}
