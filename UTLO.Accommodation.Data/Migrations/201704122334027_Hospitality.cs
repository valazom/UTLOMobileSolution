namespace UTLO.Accommodation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Hospitality : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Hospitality.Accommodations",
                c => new
                    {
                        accommodationId = c.Int(nullable: false, identity: true),
                        accommodationName = c.String(),
                        url_Fragment = c.String(),
                        url_Host = c.String(),
                        url_Password = c.String(),
                        url_Path = c.String(),
                        url_Port = c.Int(nullable: false),
                        url_Query = c.String(),
                        url_Scheme = c.String(),
                        url_UserName = c.String(),
                    })
                .PrimaryKey(t => t.accommodationId);
            
        }
        
        public override void Down()
        {
            DropTable("Hospitality.Accommodations");
        }
    }
}
