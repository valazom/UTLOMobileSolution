namespace UTLO.Accommodation.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class urlupdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Hospitality.Accommodations",
                c => new
                    {
                        accommodationId = c.Int(nullable: false, identity: true),
                        accommodationName = c.String(nullable: false),
                        url = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.accommodationId);
            
        }
        
        public override void Down()
        {
            DropTable("Hospitality.Accommodations");
        }
    }
}
