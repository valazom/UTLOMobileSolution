﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_NewModel.Domain;

namespace UTLO.Accommodation.Data
{
    public class AccommodationDbContext : DbContext
    {
        public AccommodationDbContext(string connstring) : base(connstring)
        {
                
        }
        public DbSet<UTLO_NewModel.Domain.Accommodation> AccommodationObjects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Hospitality");
            base.OnModelCreating(modelBuilder);
        }
    }
}
