﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class SumProgRepository
    {
        private List<SummaryProgramme> SumProgItems;
        public SumProgRepository()
        {
            SumProgItems = new List<SummaryProgramme>();
        }
        public void AddItems(int rImageId,int cImageId,string textdesc,string timedesc,string Dayname)
        {
            SumProgItems.Add(
                   new SummaryProgramme
                   {
                       RegImageId = rImageId,
                       CalImageId = cImageId,
                       TextDesc = textdesc,
                       TimeDesc = timedesc,
                       DayName = Dayname,
                   }
                );
        }
        public void deleteItems(int rImageId, int cImageId)
        {
            var ritems = SumProgItems.Find(a => a.RegImageId == rImageId && a.CalImageId == cImageId);
            SumProgItems.Remove(ritems);
        }
        public List<SummaryProgramme> GetAllItems
        {
            get
            {
                return SumProgItems;
            }
        }
    }
}
