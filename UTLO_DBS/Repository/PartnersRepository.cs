﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class PartnersRepository
    {
        public static List<Partners> getAllConfPartnes
        {
            get
            {
                return ConfPartners;
            }
        }
        private static List<Partners> ConfPartners = new List<Partners>()
        {
            new Partners
            {
                PartnerId = 1,
                PartnerName = "Kresge",
                Description = "In April 2012, the Kresge Foundation announced a new commitment to south" +
                "African higher education that builds on the foundation's efforts in the United States to improve" +
                "access and help students succeed academically",
                webportal = new UriBuilder("http://kresge.org")

            }.BuildUri(),
            new Partners
            {
                PartnerId = 2,
                PartnerName = "Hei Partners",
                individualPartners = new List<IndividualPartners>()
                {
                    new IndividualPartners
                    {
                        IndividualPartnerId = 1,
                        IndividualName = "Durban University of Technology",
                        webportal = new UriBuilder("http://www.dut.ac.za")
                    }.BuildUri(),
                    new IndividualPartners
                    {
                        IndividualPartnerId = 2,
                        IndividualName = "Nelson Mandela Metropolitan",
                        webportal = new UriBuilder("http://www.nmmu.ac.za")
                    }.BuildUri(),
                    new IndividualPartners
                    {
                        IndividualPartnerId =3,
                        IndividualName = "University of Free State",
                        webportal = new UriBuilder("http://www.ufs.ac.za")
                    }.BuildUri(),
                    new IndividualPartners
                    {
                        IndividualPartnerId = 4,
                        IndividualName = "University of Pretoria",
                        webportal = new UriBuilder("http://www.up.ac.za")
                    }.BuildUri()
                }
            },
            new Partners
            {
                PartnerId = 3,
                PartnerName = "Saide",
                Description = "Saide, the South African Institute for Distance Education, is non-governmental" +
                "organization based in Johannesburg but conduting projects throughout South Africa and sub-Saharan Africa.",
                webportal = new UriBuilder("http://www.saide.org.za")
            }
        };
     }   
}
