﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class KeynoteSpeakersRepository
    {
        public static KeynoteSpeakers getKeynoteSpeakerByID(int SpeakerID)
        {
            return getAllSpeakers.Find(s => s.SpeakerId == SpeakerID);
        }
        public static List<KeynoteSpeakers> getAllSpeakers
        {
            get
            {
                return Speakers;
            }
        }
        private static List<KeynoteSpeakers> Speakers = new List<KeynoteSpeakers>() {
            new KeynoteSpeakers
            {
                Speakername = "Chandra K. Raju",
                Status = "Professor",
                Affilation = "Indian Social Science Academy",
                Biography = "Professor C. K. Raju holds an MSc in Mathematics and a PhD from the Indian Statistical Institute, Kolkata. He taught and researched formal mathematics (functional analysis) and responsible for porting applications on the first Indian supercomputer Param. He has authored 12 books and dozens of articles, mainly on the subjects of Physics, Mathematics, and the history and philosophy of Science. In the Cultural Foundations of Mathematics (Pearson Longman, 2007) he proposed a new philosophy of mathematics called zeroism, and compiled evidence for the development of calculus in India and its transmission to Europe. In his books, Raju has proposed a new physics, using functional differential equations; and new ways to relate science and religion through time. He has developed and taught decolonised courses on Mathematics, and the history and philosophy of Science. He has wide-ranging interests and is Vice-President of the Indian Social Science Academy.",

            },
            new KeynoteSpeakers
            {
                Speakername = "Jonathan Janson",
                Status = "Professor",
                Affilation = "Standford University",
                Biography = "Jonathan Jansen is currently a Fellow at the Center for Advanced Study in the Behavioral Sciences at " +
                "Stanford University; a Fellow of the American Educational Association and a Fellow of the Academy of " +
                "Science of the Developing World and President of the South African Institute of Race Relations; and the " +
                "former Vice-Chancellor and Rector of the University of the Free State. He holds a PhD from Stanford " +
                "University, a MS Degree from Cornell University, and honorary Doctorates of Education from the University of " +
                "Edinburgh and Cleveland State University; and an Honorary Doctorate of Letters from the University of " +
                "Vermont. In 2013, he was awarded the Education Africa Lifetime Achiever Award and the Spendlove Award "+
                "from the University of California for his contributions to tolerance, democracy, and human rights. He is " +
                "internationally recognised for his books" 
                
            },
            new KeynoteSpeakers
            {
                Speakername = "Kris Chesky",
                Status = "Professor",
                Affilation = "University of North Texas",
                Biography = "Professor Kris Chesky is a Professor in the College of " +
                "Music at the University of North Texas and is the Director of the Texas " +
                "Center for Performing Arts Health. As founding Co-Director of Research and " +
                "Education for the Texas Center for Performing Arts Health, Chesky developed and " +
                "served as the Executive Director for the Health Promotion in Schools of Music project " +
                "(www.unt.edu/hpsm) which led to new health and safety standards for all NASM accredited " +
                "schools in the United States. He’s served as editorial board member and/ or guest reviewer "+
                "for many academic journals. He has served in leadership roles for the National Hearing " +
                "Conservation Association, the Performing Arts Medicine Association, the International Society " +
                "of Music Education, and other national and international associations.  "
            },
            new KeynoteSpeakers
            {
                Speakername = "Pali Lehohla",
                Status = "Dr",
                Affilation="Statistics South Africa",
                Biography = "Pali Jobo Lehohla is the Statistician-General of South Africa since 2000. He holds a BA with a double major in Economics and Statistics, and a Postgraduate Diploma in Population Studies. He’s served as Co-Chair of PARIS21 and Chair of the United Nations Statistics Commission. He was the founding Chair of the Statistics Commission of Africa (StatCom Africa) and Chairs the African Symposium for Statistical Development (ASSD). He was the Vice-President of the International Statistics Institute (ISI), and sponsors the Young African Statistician (YAS) Movement. He served as one of a twenty five member panel on Data Revolution, and recently appointed to the Independent Accountability Panel for the health of women, children and adolescents. Lehohla has been a forceful advocate for improving the Civil Registration and Vital Statistics systems in Africa. He was recognized by his alma mater, the University of Ghana for his contribution to the development of statistics in 2015 and was also awarded an Honorary Doctorate by the University of Stellenbosch in the same year. "
            }
        };

    }
}
