﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class AccommodationsRepository
    {
        public static Accommodations getAccommodationsByID(int Id)
        {
            return getAllAccomodations.Find(s => s.accommodationId == Id);
        }
        public static List<Accommodations> getAllAccomodations
        {
            get
            {
                return accommodations;
            }
        }
        private static List<Accommodations> accommodations = new List<Accommodations>()
        {
            new Accommodations
            {
                accommodationId =1,
                accommodationName = "Protea Hotel Edward",
                url = new UriBuilder("http://www.marriott.com")
            }.BuildUri("/hotels/travel/dured-protea-hotel-edward/"),

            new Accommodations
            {
                accommodationId = 2,
                accommodationName = "Balmoral Hotel 4 Star",
                url = new UriBuilder("http://www.raya-hotels.com")
            }.BuildUri("/rayahotels/durban/index.html"),

            new Accommodations
            {
                accommodationId = 3,
                accommodationName = "Marine Parade Garden Court 3 Star",
                url = new UriBuilder("https://www.tsogosun.com")
            }.BuildUri("/m/garden-court-marine-parade"),

            new Accommodations
            {
                accommodationId = 4,
                accommodationName = "Parade Hotel 2 Star (250m north of the Protea Hotel Edward)",
                url = new UriBuilder("http://www.paradehotel.co.za")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 5,
                accommodationName = "Gooderson Tropicana Hotel 3 Star (300m south of the Protea Hotel Edward)",
                url = new UriBuilder("http://www.goodersonleisure.co.za")
            }.BuildUri("/gooderson-tropicana-hotel/"),

            new Accommodations
            {
                accommodationId = 6,
                accommodationName = "Acorn (BnB)",
                url = new UriBuilder("http://www.acornbandb.co.za/")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 7,
                accommodationName = "Essenwood House (BnB)",
                url = new UriBuilder("http://www.essenwoodhouse.co.za/")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 8,
                accommodationName = "Garland Place (BnB)",
                url = new UriBuilder("http://www.garlandplace.co.za/")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 9,
                accommodationName = "Rosetta House (BnB)",
                url = new UriBuilder("http://www.rosettahouse.co.za/")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 10,
                accommodationName = "Madeline Grove (BnB)",
                url = new UriBuilder("http://www.madeline.co.za/")
            }.BuildUri(),

            new Accommodations
            {
                accommodationId = 11,
                accommodationName = "Napier House (BnB)",
                url = new UriBuilder("http://napierhouse.co.za")
            }.BuildUri("/bedandbreak-fast/"),
            new Accommodations
            {
                accommodationId = 12,
                accommodationName = "The Heron Guest House (BnB)",
                url = new UriBuilder("http://www.theheron.co.za/")
            }.BuildUri()
        };
    }
}
