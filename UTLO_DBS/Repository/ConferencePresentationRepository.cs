﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class ConferencePresentationRepository
    {
        public static List<ConferencePresentations> GetAllPresentations
        {
            get
            {
                return Presentations;
            }
        }
        private static List<ConferencePresentations> Presentations = new List<ConferencePresentations>()
        {
            new ConferencePresentations
            {
                presentationId = 1,
                Venue = "TLHEC10-075",
                Title = "Improving the design and delivery of Information " +
                        "Systems undergraduate course design: A case for large " +
                        "classes in the Western Cape, South Africa",
                type = PresentationType.Oral,
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 2),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 3)
                }
            },
            new ConferencePresentations
            {
                presentationId = 2,
                Venue = "TLHEC10-175",
                Title = "The Role of Assistive Technology at North West University (Mafikeng)",
                type = PresentationType.Oral,
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 4),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 5)
                }
            },
            new ConferencePresentations
            {
                presentationId = 3,
                Venue = "TLHEC10-055",
                Title = "Recreation and Reinterpretation at the point of infidelity: " +
                        "Using Theory and Principles from Adaptation Studies in the" +
                        "Adaptation of F2F Materials for Online Study",
                type = PresentationType.Oral,
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 6)
                }
            },
            new ConferencePresentations
            {
                presentationId = 4,
                Venue = "TLHEC10-071",
                Title = "Doing work that matters: reflections on community- based learning",
                type = PresentationType.Oral,
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 7)
                }
            },
            new ConferencePresentations
            {
                presentationId = 5,
                Venue = "TLHEC10-005",
                Title = "Lessons toward community involvement in health professional education",
                type = PresentationType.Oral,
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 8),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 9)
                }
            },
            new ConferencePresentations
            {
                presentationId = 6,
                Venue = "TLHEC10-196",
                type = PresentationType.Poster,
                Title = "The Role of the Academic in a Transformative South African Society",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 10)
                }
            },
            new ConferencePresentations
            {
                presentationId = 7,
                Venue = "TLHEC10-036",
                type = PresentationType.Oral,
                Title = "A Proposed Conceptual Framework for Pervasive Computing Education in a Blended Learning Environment",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 11)
                }
            },
            new ConferencePresentations
            {
                presentationId = 8,
                Venue = "TLHEC10-030",
                type = PresentationType.Oral,
                Title = "An exploration of social networking site use and academic performance among social work students at UKZN",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 12)
                }
            },
            new ConferencePresentations
            {
                presentationId = 9,
                Venue = "TLHEC10-200",
                type = PresentationType.Oral,
                Title = "Gender Differences in Pre-service Teachers’ Motivation in learning Biological Sciences",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 13)
                }
            },
            new ConferencePresentations
            {
                presentationId = 10,
                Venue = "TLHEC10-232",
                type = PresentationType.Oral,
                Title = "Gender-based violence and innovation in the university curriculum",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 14)
                }
            },
            new ConferencePresentations
            {
                presentationId = 11,
                Venue = "TLHEC10-232",
                type = PresentationType.Oral,
                Title = "Gender-based violence and innovation in the university curriculum",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 14)
                }
            },
            new ConferencePresentations
            {
                presentationId = 12,
                Panelists = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 15),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 16),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 17),
                },
                chairPersons = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 5)
                }
            },
            new ConferencePresentations
            {
                presentationId =13,
                Venue = "TLHEC10-263",
                type = PresentationType.Oral,
                Title = "African Languages in Higher Education: Directions and Challenges",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 7)
                }
            },
            new ConferencePresentations
            {
                presentationId = 14,
                Venue = "TLHEC10-264",
                type = PresentationType.Oral,
                Title = "Linguistic Rights and Conceptual Incarceration in African Education",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 8)
                }

            },
            new ConferencePresentations
            {
                presentationId = 15,
                Venue = "TLHEC10-265",
                type = PresentationType.Oral,
                Title = "What is the difference, if any, between SOTL and educational research?" +
                        "An interactive workshop",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 18)
                }
            },
            new ConferencePresentations
            {
                presentationId = 16,
                Venue = "TLHEC10-266",
                type = PresentationType.Oral,
                Title = "#TeachersMustFall: Transgressive teaching in post-apartheid higher education institutions",
                chairPersons = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 20)
                },
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 19),
                }
            },
            new ConferencePresentations
            {
                presentationId = 17,
                Venue = "TLHEC10-042",
                type = PresentationType.Oral,
                Title = "Insights into Pre-service technology teachers’ reflective practice of their teaching",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 21),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 18)
                }
            },
            new ConferencePresentations
            {
                presentationId = 18,
                Venue = "TLHEC10-103",
                type = PresentationType.Oral,
                Title = "Attitudes towards mathematics and achievement of various groupings of pre-service accounting teachers",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 23),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 24)
                }
            },
            new ConferencePresentations
            {
                presentationId = 19,
                Venue = "TLHEC10-217",
                type = PresentationType.Oral,
                Title = "Teacher transformation through curriculum innovating in environment and sustainability education",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 25),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 26)
                }
            },
            new ConferencePresentations
            {
                presentationId = 20,
                Venue = "TLHEC10-189",
                type = PresentationType.Oral,
                Title = "E-Learning In Nursing Education In Rwanda: Benefits And Challenges. An Exploration Of Participants’ Perceptives",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 27),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 28)
                }
            },
            new ConferencePresentations
            {
                presentationId = 21,
                Venue = "TLHEC10-191",
                type = PresentationType.Oral,
                Title = "Implementing e- assessments in an isiZulu Second language course’ Perceptives",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 12),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 13)
                }
            },
            new ConferencePresentations
            {
                presentationId = 22,
                Venue = "TLHEC10-185",
                type = PresentationType.Oral,
                Title = "The rationale of using technology as a learning resource in teaching undergraduates: Lecturers’ reflections",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 27),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 28)
                }
            },
           
        };
    }
}
