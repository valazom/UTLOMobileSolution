﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class AbstractRepository
    {
        
        public void AddItem(Abstract ab)
        {
            confAbstract.Add(ab);
        }
        public void RemoveItem(Abstract ab)
        {
            confAbstract.Remove(ab);
        }
        public void RemoveItem(int abstractId)
        {
            var abstractName = confAbstract.Find(a => a.abstractId == abstractId);
            confAbstract.Remove(abstractName);
        }
        public static Abstract getAbstractbyAttendee(int Id)
        {
            var abstractForAttendee = (from abs in confAbstract
                                      from auth in abs.Authors
                                      where auth.AttendeeId == Id
                                      select abs).FirstOrDefault();
            return abstractForAttendee;                      
        }
        public static List<Abstract> getAllAbstract
        {
            get
            {
                return confAbstract;
            }
        }
        private static List<Abstract> confAbstract = new List<Abstract>()
        {
            new Abstract
            {
                abstractId = 1,
                Title = "Risk Analysis and Detection in order to" +
                         "Assist and Retain Students (RADAR) at NMMU",
                Body = "The project was originally started in the Faculty of Law to monitor " +
                       "student's progress, and to identify students who were struggling with their " +
                       "studies as early on in the academic year as possible",
                Authors = new List<Attendees>()
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 6)
                }
            },
            new Abstract
            {
                abstractId = 2,
                Title = "Can data in a Learning Management System support student success? A case study at the University of Pretoria",
                Body = "Student success is an underlying concept embedded in the Teaching and learning plan and priorities" +
                "of the University of Pretoria (UP) for 2016.",
                Authors = new List<Attendees>()
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 19)
                }
            },
            new Abstract
            {
                abstractId = 3,
                Title = "Evaluating the student interventions using predictive modelling and text mining methods" +
                "at the University of Witwatersrand",
                Body = "The paper looks at a pilot study that was done at the University of Witswatersrand to determine " +
                "if there is a relationship between interventions performed and academic success at first year.",
                Authors = new List<Attendees>()
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 20)
                }
            },
            new Abstract
            {
                abstractId = 4,
                Title = "An early warning system based on probabilistic distance clustering algorithm for student at risk detection",
                Body = "The South African Higher Education system has been plagued with high student failure rates which could be " +
                "attributed to unprepared students and students who cannot cope with the pressures of the higher education systems",
                Authors = new List<Attendees>()
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 21)
                }
            },
            new Abstract
            {
                abstractId = 5,
                Title = "Towards an understanding of Supplemental Instruction (SI) as a student academic development "+
                "community of practice",
                Body = "This paper will provide an overview of Supplemental Instruction (SI) as an International" +
                "community of practice",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 23)
                }
            },
            new Abstract
            {
                abstractId = 6,
                Title = "Promising Practices: Faculty Student Advisors",
                Body = "Over the last five years, the University of Pretoria has injected a substantial amount " +
                "of energy into providing support to students success.",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 26)
                }

            },
            new Abstract
            {
                abstractId = 7,
                Title = "Understanding students 'as people in the world' through analytics",
                Body = "Expanding student entrollments in higher education has implications for quality of provisioning " +
                "not only for undergraduate education, but for all levels of the system including doctoral studies.",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 8)
                }
            },
            new Abstract
            {
                abstractId = 8,
                Title = "UKZN Cohort Model of Doctoral Supervision",
                Body = "Expanding student entrolments in higher education has implications for quality of provisioning " +
                ", not only for undergraduate education, not only for undergraduate education, but for all levels of the " +
                "system including doctoral studies.",
                Authors = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 27),
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 28)
                }
            }
        };


    }
}
