﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class SuitesRepositorys
    {

       public static List<ConferencePresentations> GetPresentationsBySuiteId(int SuiteId)
        {
            return GetAllSuites.Find(s => s.SuitesId == SuiteId).presentations.ToList();
        }
        public static List<Suites> GetAllSuites
       {
            get
            {
                return suites;
            }
        }
        private static List<Suites> suites = new List<Suites>
        {
            new Suites
            {
                SuitesId = 1,
                Venue = "SUITE 5",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 1),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 2),
                    ConferencePresentationRepository.
                    GetAllPresentations.Find(a=>a.presentationId == 3),
                }
            },
            new Suites
            {
                SuitesId = 2,
                Venue = "SUITE 4",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 4),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 5),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 6)
                }
            },
            new Suites
            {
                SuitesId = 3,
                Venue = "NORTH ILANGA",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 7),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 8),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 9)
                }

            },
            new Suites
            {
                SuitesId = 4,
                Venue = "EAST ILANGA",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 10),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 11),
                }
            },
            new Suites
            {
                SuitesId = 5,
                Heading = "PLENARY PANEL: SCHOLARSHIP ON TEACHING AND LEARNING PANEL",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 12)
                }
            },
            new Suites
            {
                SuitesId = 6,
                Venue = "GREAT ILANGA",
                Heading = "POST KEYNOTE DISCUSSION",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 13),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 14)
                }
            },
            new Suites
            {
                SuitesId = 7,
                Venue = "SUITE 1",
                Heading = "WORKSHOP",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 15),

                }
            },
            new Suites
            {
                SuitesId = 8,
                Heading = "KEYNOTE ADDRESS",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 16),
                }
            },
            new Suites
            {
                SuitesId = 9,
                Venue = "SUITE 5",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 17),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 18),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 19)
                }
            },
            new Suites
            {
                SuitesId = 10,
                Venue = "SUITE 4",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 20),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 21),
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 22)
                }
            },
            new Suites
            {
                SuitesId = 11,
                Heading = "KEYNOTE ADDRESS",
                presentations = new List<ConferencePresentations>
                {
                    ConferencePresentationRepository
                    .GetAllPresentations.Find(a=>a.presentationId == 16),
                }
            }
        };
    }
}
