﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;
using UTLO_DBS.Repository;

namespace UTLO_DBS.Repository
{
    public class conferenceProgrammeRepository
    {
        public static List<Suites> GetSuitesByProgrammeID(int programmeID)
        {
            var item = GetAllProgrammes.Find(s => s.ProgrammeID == programmeID).Suite.ToList();
            return item;
        }
        public static List<ConferenceProgromme> GetProgrammesByDay(conferenceDay day)
        {
            return programmes.Where(a => a.Daynumber == day).ToList();
        }
        public static List<ConferenceProgromme> GetAllProgrammes
        {
            get
            {
                return programmes;
            }
        }
        private static List<ConferenceProgromme> programmes = new List<ConferenceProgromme>
        {
            new ConferenceProgromme
            {
                ProgrammeID = 1,
                Title = "REGISTRATION, TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani Hotel, Durban",
                StartTime = new TimeSpan(7,30,0),
                EndTime = new TimeSpan(8,45,0),
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 2,
                Title = "WELCOME & INTRODUCTION",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                presenters = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 4)
                },
                StartTime = new TimeSpan(8,45,0),
                EndTime = new TimeSpan(9,0,0),
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 3,
                Title = "OPENING ADDRESS",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                presenters = new List<Attendees>
                {
                    AttendeesRepository.getAllConfAttendees.Find(a=>a.AttendeeId == 6)
                },
                StartTime = new TimeSpan(9,0,0),
                EndTime = new TimeSpan(9,30,0),
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 4,
                Title = "KEYNOTE ADDRESS",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(9,30,0),
                EndTime = new TimeSpan(10,30,0),
                Suite = new List<Suites>
                {
                   SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 11)
                },
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 5,
                Title = "TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(10,30,0),
                EndTime = new TimeSpan(10,45,0),
                Daynumber = conferenceDay.First,
            },
            new ConferenceProgromme
            {
                ProgrammeID = 6,
                Title = "SESSION 1",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(10,45,0),
                EndTime = new TimeSpan(11,45,0),
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 1),
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 2)
                },
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 7,
                Title = "SESSION 2",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(11,45,0),
                EndTime = new TimeSpan(12,50,0),
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 3),
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 4)
                },
                Daynumber = conferenceDay.First
            },
            new ConferenceProgromme
            {
                ProgrammeID = 8,
                Title = "TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(8,30,0),
                EndTime = new TimeSpan(9,0,0),
                Daynumber = conferenceDay.Second
            },
            new ConferenceProgromme
            {
                ProgrammeID = 9,
                Title = "SESSION 5",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(9,0,0),
                EndTime = new TimeSpan(10,0,0),
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 5)
                },
                Daynumber = conferenceDay.Second
            },
            new ConferenceProgromme
            {
                ProgrammeID = 10,
                Title = "TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(11,0,0),
                EndTime = new TimeSpan(11,20,0),
                Daynumber = conferenceDay.Second
            },
            new ConferenceProgromme
            {
                ProgrammeID = 11,
                Title = "SESSION 7",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 6),
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 7)
                },
                StartTime = new TimeSpan(11,20,0),
                EndTime = new TimeSpan(13,20,0),
                Daynumber = conferenceDay.Second
            },
            new ConferenceProgromme
            {
                ProgrammeID = 12,
                Title = "LUNCH",
                Venue = "LINGELA RESTAURANT, Ground Floor of the Southern Sun Elangeni & Maharani, " +
                            "Durban OCEAN BREEZE, Second Floor of the Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(13,25,0),
                EndTime = new TimeSpan(14,25,0),
                Daynumber = conferenceDay.Second
            },
            new ConferenceProgromme
            {
                ProgrammeID = 13,
                Title = "TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(8,30,0),
                EndTime = new TimeSpan(9,0,0),
                Daynumber = conferenceDay.Third
            },
            new ConferenceProgromme
            {
                ProgrammeID = 14,
                Title = "SESSION 11",
                Venue = "Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 8)
                },
                StartTime = new TimeSpan(9,0,0),
                EndTime = new TimeSpan(10,0,0),
                Daynumber = conferenceDay.Third
            },
            new ConferenceProgromme
            {
                ProgrammeID = 15,
                Title = "SESSION 12",
                Venue ="Great Ilanga, First Floor, Southern Sun Elangeni & Maharani, Durban",
                Suite = new List<Suites>
                {
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 9),
                    SuitesRepositorys.GetAllSuites.Find(a=>a.SuitesId == 10)
                },
                StartTime = new TimeSpan(10,5,0),
                EndTime = new TimeSpan(11,5,0),
                Daynumber = conferenceDay.Third
            },
            new ConferenceProgromme
            {
                ProgrammeID = 16,
                Title = "TEA/COFFEE",
                Venue = "First Floor, Conference Foyer, Southern Sun Elangeni & Maharani, Durban",
                StartTime = new TimeSpan(11,5,0),
                EndTime = new TimeSpan(11,20,0),
                Daynumber = conferenceDay.Third
            }
        };
    }
}
