﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class AttendeesRepository
    {

        
        public void AddAttendees(Attendees r)
        {
            confAttendees.Add(r);
        }
        public void RemoveAttendees(Attendees r)
        {
            confAttendees.Remove(r);
        }
        public void RemoveAttendees(int aId)
        {
            var att = confAttendees.Find(a => a.AttendeeId == aId);
            confAttendees.Remove(att);
        }
        public static Attendees getAttendeeByID(int id)
        {
            return confAttendees.Find(s => s.AttendeeId == id);
        }
        public static List<Attendees> getSpeakers
        {
            get
            {
                //get list of programmes with presentations
                var speakers = (from abstracts in AbstractRepository.getAllAbstract
                               from author in abstracts.Authors
                               select author).ToList();
                //var prog = (from programmes in ProgrammeRepository.getAllprograms
                //                where programmes.presentations != null
                //                select programmes).ToList();
                //var speakers = (from progrommes in prog
                //                from presentations in progrommes.presentations
                //                from attendees in presentations.presenters
                //                select attendees).ToList();
                                                                                              
                return speakers;
            }
        }
        public static List<Attendees> getAllConfAttendees
        {
            get
            {
                return confAttendees;
            }
        }

        private static List<Attendees> confAttendees = new List<Attendees>()
        {
            new Attendees
            {
                AttendeeId = 1,
                status = "Ms",
                Name = "Caroline Altman Smith",
                Position ="Deputy Director - Education",
                Affiliation = "The Kresge Foundation",
                emailAddress = "caroline@gamil.com",
                phoneNumber = "0782648238"
            },
            new Attendees
            {
                AttendeeId = 2,
                status = "Professor",
                Name = "Alan Amory",
                Affiliation = "Saide",
                emailAddress = "alen@gamil.com",
                phoneNumber = "03523536235"
            },
            new Attendees
            {
                AttendeeId = 3,
                status = "Professor",
                Name = "Yunus Ballim",
                Position = "Vice Chancellor",
                Affiliation = "Sol Plaatje University",
                emailAddress = "yunus@gmaiil.com",
                phoneNumber = "0762323289",
                Biography = "Yunus Ballim holds B.Sc, M.Sc, and PhD degrees in civil Engineering from Wits University"
            },
            new Attendees
            {
                AttendeeId = 4,
                status = "Professor",
                Name = "Ahmed Baws",
                Position = "CEO",
                Affiliation = "Universities South Africa (USAf)",
                emailAddress = "ahmed@gmail.com",
                phoneNumber = "0834673733",
            },
            new Attendees
            {
                AttendeeId = 5,
                status = "Professor",
                Name = "Richard Berned",
                Position = "Deputy Vice Chancellor Academic",
                Affiliation = "University of Mpumalanga",
                emailAddress = "richard@gmail.com",
                phoneNumber = "0783673463"
            },
            new Attendees
            {
                AttendeeId = 6,
                status = "Mrs",
                Name = "Lynn Biggs",
                Position = "Faculty of Law - Lecturer and FTLC Chairperson",
                Affiliation = "Nelson Mandela Metropolitan University",
                emailAddress = "lynn@gmail.com",
                phoneNumber = "063998484",
            },
            new Attendees
            {
                AttendeeId = 7,
                status = "Mr",
                Name = "David Bleezard",
                Position = "Institutional Planning",
                Affiliation = "Cape Peninsula University of Technology",
                emailAddress = "0837374373",
                phoneNumber = "0837374373",
            },
            new Attendees
            {
                AttendeeId = 8,
                status = "Dr",
                Name = "Consuelo Boronat",
                Position = "Director operational analysis and information management",
                Affiliation = "Florida International University",
                Biography = "Dr Consuelo Boronat is the Director of operational analysis in the office "+
                "analysis and information management at Florida international University, in Miami, Florida " +
                "United States of America.",
                emailAddress = "consuelo@gmail.com",
                phoneNumber = "07636353531"
                
            },
            new Attendees
            {
                AttendeeId = 9,
                status = "Dr",
                Name = "Jan Lyddon",
                Position = "Data coach",
                Affiliation = "Siyaphumelela",
                Address = "Columbus, Ohio USA",
                emailAddress = "jan@gmail.com",
                phoneNumber = "0812325732"
            },
            new Attendees
            {
                AttendeeId = 10,
                status = "Professor",
                Name = "Murray Leibbrandt",
                Position = "Pro Vice-Chancellor poverty and inequality",
                Affiliation = "University of Cape Town",
                Biography = "Murray Leibbrandt is a professor in the school of Economics at the " +
                "University of Cape Town and the Director of the Southern Africa Labour and Developement " +
                "Research Unit.",
                emailAddress = "murray@gmail.com",
                phoneNumber = "08438434683"
            },
            new Attendees
            {
                AttendeeId = 11,
                status = "Dr",
                Name = "Allison Calhoun-Brown",
                Position = "Associate vice president for student success",
                Affiliation = "Georgia State University",
                Address = "Atlanta, USA",
                Biography = "Dr. Allison Calhoun-Brown is Associate Vice President for Student Success at Georgia State" +
                "University."
            },
            new Attendees
            {
                AttendeeId = 12,
                status = "Prossefor",
                Position = "Director Academic Centre for Teaching and Learning",
                Name = "Francois Strydom",
                Affiliation = "University of the Free State",
                Biography = "Prof Francois Strydom is currently the Director of the Centre for Teaching and " +
                "Learning at the University of the Free State. He has been the project leader of the South African " +
                "Surveys of student Engagement (SASSE) since 2007."
            },

            new Attendees
            {
                AttendeeId = 13,
                status = "Dr",
                Name = "Timothy Renick",
                Position = "VP Enrollment management & student success/vice provost",
                Affiliation = "Georgia State University",
                Biography = "Timothy Renick is Vice President for Enrollment Management and Student Success, " +
                "Vice Provost, and Professor at Georgia State University in Atlanta."
            },
            new Attendees
            {
                AttendeeId = 14,
                status = "Dr",
                Name = "Diane Grayson",
                Position = "Director Institional Audits, Council on Higher Education",
                Affiliation = "Council of Higher Education",
                Biography = "Prof Diane Grayson is the Director of instiional Audits at the council of Higher Education. She " +
                "She is responsible for the Quality Enhancement Project, which aims to improve student success in all " +
                "higher education institutions in South Africa."
            },
            new Attendees
            {
                AttendeeId = 15,
                status = "Ms",
                Position = "Executive Director",
                Name = "Jenny Glennie",
                Affiliation = "Saide"
            }, 
            new Attendees
            {
                AttendeeId = 16,
                status = "Dr",
                Name = "Diane Parker",
                Position = "Deputy Director General (Universities Branch)",
                Affiliation = "DHET",
            },

             new Attendees
             {
                 AttendeeId = 17,
                 status = "Dr",
                 Name = "Sizwe Nxasana",
                 Position = "Ministratial Task Team on financing"
             },

             new Attendees
             {
                 AttendeeId = 18,
                 status = "Ms",
                 Name = "Mashudu Nthulane",
                 Position = "SRC University of Venda",
                 Affiliation = "University of Venda"
             },
             new Attendees
             {
                 AttendeeId = 19,
                 status = "Mr",
                 Name = "Dolf Jordaan",
                 Position = "Department for Education Innovation",
                 Affiliation = "University of Pretoria"
             },
             new Attendees
             {
                 AttendeeId = 20,
                 status = "Mr",
                 Position = "BI Information specialist",
                 Name = "Innocent Mamvura",
                 Affiliation = "University of The Witswatersrand"
             },
             new Attendees
             {
                 AttendeeId = 21,
                 status = "Ms",
                 Position = "Lecturer",
                 Name = "Sol Plaatje University"
             },
             new Attendees
             {
                 AttendeeId = 23,
                 status = "Mrs",
                 Position = "Snr AD Professional & Head: SI Nattional Centre Southern Africa",
                 Name = "Liesl Smith",
                 Affiliation ="Nelson Mandela Metropolitan University",
                 Address = "Port Elizabeth, South Africa"
             },
             new Attendees
             {
                 AttendeeId = 24,
                 status = "Dr",
                 Position = "Programme Speacialist Quality Assurance",
                 Affiliation = "Saide",
                 Name = "Ephraim Mhlanga"
             },
             new Attendees
             {
                 AttendeeId = 25,
                 status = "Mr",
                 Name = "Ari Simon",
                 Position = "Vice president, Chief program & strategy officer",
                 Affiliation = "The Kresge Foundation"
             },
             new Attendees
             {
                 AttendeeId = 26,
                 status = "Professor",
                 Position = "Deputy Director in Education Innovation",
                 Name = "Ana Naidoo",
                 Affiliation = "University of Pretoria",
             },
             new Attendees
             {
                 AttendeeId = 27,
                 status = "Dr",
                 Position = "Director of teaching & learning",
                 Name = "Rubby Dhunpath",
                 Affiliation = "University of KwaZulu-Natal"
             },
             new Attendees
             {
                 AttendeeId = 28,
                 status = "Professor",
                 Position = "School of education",
                 Name = "Micheal Samuel",
                 Affiliation = "University of KwaZulu-Natal"
             }

        };
    }
}
