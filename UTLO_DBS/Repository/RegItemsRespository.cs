﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTLO_DBS.Model;

namespace UTLO_DBS.Repository
{
    public class RegItemsRespository
    {
        private List<RegItems> regItems;
        public RegItemsRespository()
        {
            regItems = new List<RegItems>();
        }
        public void addItems(int rImageId, int cImageId, string txt_desc, string ti_desc)
        {
            regItems.Add(new RegItems(rImageId, cImageId, txt_desc, ti_desc));
        }
        public void deleteItems(int rImageId,int cImageId)
        {
            var ritems = regItems.Find(a => a.getRegImageId == rImageId && a.getCalImageId == cImageId);
            regItems.Remove(ritems);
        }
        public List<RegItems> getAllRegItems
        {
            get
            {
                return regItems;
            }
        }
    }
}
