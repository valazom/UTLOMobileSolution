﻿using System.Collections.Generic;

namespace UTLO_DBS.Model
{
    public enum PresentationType
    {
        Oral = 1,
        Poster
    }
    public class ConferencePresentations
    {
        public int presentationId { get; set; }
        public ICollection<Attendees> Authors { get; set; }
        public string Title { get; set; }
        public PresentationType type { get; set; }
        public ICollection<Attendees> chairPersons { get; set; }
        public ICollection<Attendees> Panelists { get; set; }
        public string Venue;
    }
}