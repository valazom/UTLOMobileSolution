﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class Abstract
    {
        public int abstractId { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public List<Attendees> Authors { get; set; }
    }
}
