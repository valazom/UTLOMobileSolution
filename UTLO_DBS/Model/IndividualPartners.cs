﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class IndividualPartners
    {
        public int IndividualPartnerId { get; set; }
        public string IndividualName { get; set; }
        public UriBuilder webportal { get; set; }
        public IndividualPartners BuildUri(string path = "")
        {
            webportal.Path = path;
            return this;
        }
    }
}
