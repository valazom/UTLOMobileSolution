﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class KeynoteSpeakers
    {
        
        public int SpeakerId { get; set; }
        public string Speakername { get; set; }
        public string position { get; set; }
        public string Affilation { get; set; }
        public string Status { get; set; }
        public string Biography { get; set; }
    }
}
