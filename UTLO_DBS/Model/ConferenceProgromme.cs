﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public enum conferenceDay
    {
        First = 1,
        Second,
        Third
    }
    public class ConferenceProgromme
    {
        [Key]
        public int ProgrammeID { get; set; }
        public string Title { get; set; }
        public List<Attendees> presenters { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public ICollection<Suites> Suite { get; set; }
        public string Venue { get; set; }
        public conferenceDay Daynumber { get; set; }
    }
}
