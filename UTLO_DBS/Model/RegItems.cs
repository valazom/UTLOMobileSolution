﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class RegItems
    {
        private int RegImageId;
        private int CalImageId;
        private string text_desc;
        private string time_desc;

        public RegItems(int rImageId,int cImageId,string txt_desc, string ti_desc)
        {
            RegImageId = rImageId;
            CalImageId = cImageId;
            text_desc = txt_desc;
            time_desc = ti_desc;
        }
        public int getRegImageId
        {
            get
            {
                return RegImageId;
            }
        }
        public int getCalImageId
        {
            get
            {
                return CalImageId;
            }
        }
        public string getText_desc
        {
            get
            {
                return text_desc;
            }
        }
        public string getTime_desc
        {
            get
            {
                return time_desc;
            }
        }
    }
}
