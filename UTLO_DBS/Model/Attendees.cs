﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class Attendees
    {
        
        public int AttendeeId { get; set; }
        public string status { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Affiliation { get; set; }
        public string Address { get; set; }
        public string emailAddress { get; set; }
        public string phoneNumber { get; set; }
        public string Biography { get; set; }
        
    }
}
