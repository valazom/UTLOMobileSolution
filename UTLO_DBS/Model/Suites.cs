﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UTLO_DBS.Model
{
    public class Suites
    {
        [Key]
        public int SuitesId { get; set; }
        public string Venue { get; set; }
        public string Heading { get; set; }
        public ICollection<ConferencePresentations> presentations { get; set; }
    }
}