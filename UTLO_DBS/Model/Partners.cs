﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class Partners
    {
        [Key]
        public int PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string Description { get; set; }
        public UriBuilder webportal { get; set; }
        public List<IndividualPartners> individualPartners { get; set; }
        public Partners BuildUri(string path = "")
        {
            webportal.Path = path;
            return this;
        }

    }
}
