﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class SummaryProgramme
    {
        public int RegImageId { get; set; }
        public int CalImageId { get; set; }
        public string  TimeDesc { get; set; }
        public string TextDesc { get; set; }
        public string DayName { get; set; }
    }
}
