﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTLO_DBS.Model
{
    public class Accommodations
    {
        [Key]
        public int accommodationId { get; set; }
        public string accommodationName { get; set; }
        public UriBuilder url{ get; set; }
        public Accommodations BuildUri(string path = "")
        {
            url.Path = path;
            return this;
        }
    }
}
