﻿using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace UTLO_DBS.Model
{
    public class Presentation
    {
        [Key]
        public int presentationId { get; set; }
        public string Title { get; set; }
        public string body { get; set; }
        public TimeSpan startTime { get; set; }
        public TimeSpan endTime { get; set; }
        public List<Attendees> presenters { get; set; }
        [Range(1,5,ErrorMessage ="Value {0} must be between the range {1} and {2}")]
        public int rate { get; set; }
        public Abstract abstracts { get; set; }
    }
}